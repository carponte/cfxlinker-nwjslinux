'use strict';

angular.module('megaride')
.service('PromiseService', function($log){
            	
        var PromiseService = {};

        PromiseService.getPromise = function () {
            var result = {};
            result.promise = new Promise(function (resolve, reject) {
                    result.resolvePromise = resolve;
                    result.rejectPromise = reject;
                  }
              );
            return result;
        }

        
        return PromiseService;
        
    })