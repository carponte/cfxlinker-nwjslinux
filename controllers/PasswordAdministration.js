
angular.module('megaride')    
.controller('pwdAdministrationCtrl', 
function ($scope, $stateParams, $rootScope, $state, $translate, LoggingService, ShutdownService, DatabaseService) 
	{
		
		$scope.parameters = {};
		$scope.ErrorMsg = '';
		$scope.changingPassword = false;
		$scope.parameters.newpassword='';
		$scope.parameters.confirmpassword='';

		$scope.goToHome = function() {
			$state.go('splash');
		}

		$scope.okButton = function() {
			var callBack = function (user) {
				if ($scope.checkUser(user)) {
					$scope.goNext(user);
				}
			}
			DatabaseService.checkUserAndPassword($scope.parameters.username, $scope.parameters.password, callBack);
	
			/* if ($scope.parameters.password == "support") {
				 $state.go('administration', 
				 	{userProfile:"support"}
				 );
				}
			else if ($scope.parameters.password == "eRc1936") {
				 $state.go('administration', 
				 	{userProfile:"super"}
				 );
			}
			else if ($scope.parameters.password == "superSupport") {
				$state.go('administration', 
					{userProfile:"superSupport"}
				);
		 }*/
		}

		$scope.checkUser = function (user) {
			if (user == null)
				$scope.ErrorMsg = $translate.instant('GDPR.wrongCredentials');
			return user != null;
		}

		$scope.goNext = function (User) {
			if ($scope.changingPassword) {
				DatabaseService.updateUserPassword(User, $scope.parameters.newpassword);
				$scope.changingPassword = false;
				$scope.ErrorMsg = $translate.instant('GDPR.passwordUpdated');
				User.PasswordToBeChanged = 0;
				//return;
			}
			
			if (User.PasswordToBeChanged == 1 ) {
				$scope.ErrorMsg = $translate.instant('GDPR.changePassword');
				$scope.changingPassword = true;
				return;
			}
			 
			if ($stateParams.nextState == 'administration') {
				if (User.Role != 'support' && User.Role != 'super' && User.Role != 'superSupport' && User.Role != 'admin') {
						$scope.ErrorMsg = $translate.instant('GDPR.userNotAllowed');
					}
				else
					$state.go('administration', 
							{userProfile: User.Role}
					);
				}
			else if ($stateParams.nextState == 'archive') {
				$state.go('archive', 
						{user: JSON.stringify(User)}
					);
			} 
			else if ($stateParams.nextState == 'report') {
				$state.go('report', 
						{user: JSON.stringify(User)}
					);
			} 

		}


		$scope.btnOkDisabled = function () {
			if ($scope.changingPassword == false)
				return false;
			 return $scope.parameters.newpassword != $scope.parameters.confirmpassword || $scope.parameters.newpassword.length == 0;
			}


		$scope.removeStoredPassword = function () {
			chrome.passwordsPrivate.getSavedPasswordList(function(passwords) {
		      passwords.forEach((p, i) => {
		        chrome.passwordsPrivate.removeSavedPassword(i);
		      });
		    });
		    chrome.privacy.services.passwordSavingEnabled.set({ value: false });
		}

		$scope.keyUp = function () {
		  alert("ku");
		}

		$scope.resetErrorMessage = function () {
			$scope.ErrorMsg = '';
			if ($scope.changingPassword && $scope.parameters.newpassword != $scope.parameters.confirmpassword && 
					$scope.parameters.newpassword.length > 0 && $scope.parameters.confirmpassword.length > 0 ) 
				$scope.ErrorMsg = $translate.instant('GDPR.passwordMismatch');
				
		}

		$scope.init = function() {
			$scope.removeStoredPassword();
			LoggingService.info('Apro la pagina di login');
            		ShutdownService.setOperation(ShutdownService.OPERATIONS.SPLASH);
		}

		$scope.init();
		
	}
);
   
    	
