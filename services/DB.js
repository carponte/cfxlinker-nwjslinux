'use strict';

//QUESTO SERVICE SI INTERFACCIA CON IL DB SQLITE

//QUERY PER SELEZIONE DELLE STATISTICHE TRATTAMENTI   SELECT DISTINCT(Protocol) AS Protocol, COUNT(ID) AS Totale FROM Treatments GROUP BY Protocol

var currentDBVersion = 0;

var dateFormat;
try {
     dateFormat = require('dateformat');
     }
catch (ex) {

}

var fs;
try {
  fs = require('fs');
}
catch (err) {
    fs = null;
}

var SystemCalls;
try {
  SystemCalls = require('./modules/SystemCalls.js');
}
catch (err) {
    SystemCalls = null;
}

angular.module('megaride')
.service('DatabaseService', function($log, LoggingService){
            	
        var DatabaseService = {};

        DatabaseService.db = null;
        DatabaseService.currentDBVersion = 1;


        DatabaseService.openDB = function () {

              try {
            
                    DatabaseService.db = openDatabase('cfxLinker.db', '1.0', 'CFXLinker', 1);
                    
                    var dataPath = require('nw.gui').App.dataPath;
                    LoggingService.info("Percorso del db locale:" + dataPath+"/Databases");

                    
                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS Treatments (ID integer primary key autoincrement, patientName TEXT, patientSurname TEXT, patientAge TEXT, patientSex TEXT, Protocol TEXT, "+
                            "Pathology TEXT, Operator TEXT, DateOfTreatment Date, Duration INTEGER, ThPoint INTEGER, KMax INTEGER, Dx INTEGER, Dy INTEGER, power INTEGER, steps INTEGER)");

                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS Configuration (ID integer primary key, jumpVerifiyCalibration INTEGER, fakeImbibition INTEGER, resetWhiteLed INTEGER, demoEnabled INTEGER, "+
                            "timerSCEnabled INTEGER, showReport INTEGER, giorniLogOnLine INTEGER, disableHardware INTEGER, privacy INTEGER)");

                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS DBVersion (ID integer primary key, currentVersion INTEGER)");
                        
                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS SerialLogs (ID integer primary key autoincrement, pathFile TEXT, DateOfLog Date, UNIQUE(DateOfLog))");

                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS SystemLogs (ID integer primary key autoincrement, pathFile TEXT, DateOfLog Date, UNIQUE(DateOfLog))");

                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS SystemProperties (ID integer primary key autoincrement, serSkDriver TEXT, serTesta TEXT, serAlimentatore TEXT, "+
                                                "serControlUnit TEXT, serCfxLinker TEXT, notes TEXT)");

                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS ConfigReferto (ID integer primary key autoincrement, nomeStruttura TEXT, IndirizzoStruttura Text)");

                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS CalibrationChecks (ID integer primary key autoincrement, tipoVerifica TEXT, motivoVerifica TEXT, dataVerifica Date, "+
                                                "totMinutiUtilizzo INTEGER, potenzaEmessa INTEGER, potenzaMisurata INTEGER, esitoVerifica INTEGER)");

                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS Reports (ID integer primary key autoincrement, PatientName TEXT, PatientSurname TEXT, DateOfTreatment Date, "+
                                                "Operator TEXT, FilePath TEXT)");

                    DatabaseService.executeSql("CREATE TABLE IF NOT EXISTS Users (ID integer primary key autoincrement, Username TEXT, Name TEXT, Surname TEXT, DateOfPasswordExpiry Date, "+
                                                "UserAlias TEXT, Password TEXT, Role TEXT, PasswordToBeChanged INTEGER DEFAULT 1, userDefault DEFAULT 0)");

                    DatabaseService.executeSql("DELETE FROM ConfigReferto WHERE ID > 1");

                    LoggingService.info("DatabaseService.  Aperto e creato il db locale");
                    DatabaseService.saveLogs();
                    DatabaseService.upgradeDB();
                    }
                catch (e) {
                    LoggingService.info("DatabaseService.  Errore durante creazione db: "+JSON.stringify(e));
                    DatabaseService.sb = null;
                    }

            //updateDB();
            }

        DatabaseService.transaction = function(func) {
            if (DatabaseService.db == null)
                return;
            DatabaseService.db.transaction(func);
            }

        DatabaseService.executeSql = function (sql, values, funcResult) {
                DatabaseService.transaction(
                    function (tx) {
                            if (funcResult == null)
                                tx.executeSql(sql);
                            else
                                tx.executeSql(sql, values, funcResult);
                        }
                    );
            }

        DatabaseService.addColumnIfNotExists = function(field, table, type, defaultValue) {
            var callBack = function(tx, result) { 
                //Se entra qui il campo esiste e non occorre aggiungerlo
            }

            var err = function (tx, result) {
                if (result != null && result.message != null && result.message.indexOf('no such column') > -1) {
                    var sql1 = "ALTER TABLE "+table+" ADD "+field+" " +type;
                    if (defaultValue) 
                        sql1 += " DEFAULT "+defaultValue;
                    DatabaseService.executeSql(sql1);
                }
            }

            var sql = "SELECT " + field +" FROM "+table+" limit 1";
            DatabaseService.transaction(
                    function (tx) {
                           tx.executeSql(sql, [], callBack, err);
                        }
                    );
        }

        DatabaseService.upgradeDB = function () {
            /*var DBVersion = 1;
            DatabaseService.executeSql(
                 "SELECT * FROM DBVersion WHERE ID = 1", [],
                    function (tx, results) {
                        if (results.rows.length == 0) {
                            DatabaseService.executeSql("INSERT INTO DBVersion (ID, currentVersion) VALUES (1, 1)");
                        }
                        else
                            DBVersion = results.rows[0].currentVersion;
                        DatabaseService.executeScripts(DBVersion);
                    }
                );*/
            DatabaseService.addColumnIfNotExists("MirrorCamera", "Configuration", "INTEGER", 0);
            DatabaseService.addColumnIfNotExists("JumpStep", "Configuration", "INTEGER", 0);
            DatabaseService.addColumnIfNotExists("RotateCamera", "Configuration", "INTEGER", 0);
            DatabaseService.addColumnIfNotExists("privacy", "Configuration", "INTEGER", 0);      
            DatabaseService.addColumnIfNotExists("giorniReportOnLine", "Configuration", "INTEGER", 150);
            DatabaseService.addColumnIfNotExists("checksum", "SystemLogs", "TEXT");
            DatabaseService.addColumnIfNotExists("creator", "Reports", "INTEGER", 0);
            DatabaseService.addColumnIfNotExists("visible", "Reports", "INTEGER", 1);
            DatabaseService.addColumnIfNotExists("passwordExpiryDate", "Users", "Date", 0);
        }

        //Sostituisce gli apici nella stringa in input.  Serve per evitare di scrivere apostrofi in sql
        DatabaseService.checkString = function(input) {
            if (input != null)                
                return input.replace(/["']/g,"''");
        }

        DatabaseService.isDBLoaded = function () {
            return DatabaseService.db != null;
        }

        DatabaseService.executeScripts = function (fromVersion) {
            LoggingService.info("Faccio l'upgradeDB del DB dalla versione "+ fromVersion+" alla "+DatabaseService.currentDBVersion);

        }

	DatabaseService.resetFabbrica = function() {
                   
                    DatabaseService.executeSql("DELETE FROM Treatments");
//                    DatabaseService.executeSql("DELETE FROM Configuration");
                    DatabaseService.executeSql("DELETE FROM SerialLogs");
                    DatabaseService.executeSql("DELETE FROM SystemLogs");
//                    DatabaseService.executeSql("DELETE FROM SystemProperties");
                    DatabaseService.executeSql("DELETE FROM ConfigReferto");
                    DatabaseService.executeSql("DELETE FROM CalibrationChecks");
                    DatabaseService.executeSql("DELETE FROM Reports");
                    DatabaseService.executeSql("DELETE FROM Users");
                    var config = DatabaseService.getDefaultConfiguration();
                    DatabaseService.setConfiguration(config);
                    LoggingService.info("DatabaseService.  Resettato il db locale");		
	}


        DatabaseService.saveLogs = function() {
            var pathLog = LoggingService.getLogPath();
            DatabaseService.insertLog(LoggingService.getLogPath());
            DatabaseService.insertSerialLog(LoggingService.getSerialLogPath());
        }

        DatabaseService.insertSerialLog = function(pathFile) {
            LoggingService.info("DatabaseService. Inserisco il seriallog "+pathFile);
            var dateStr= dateFormat(new Date(), "yyyy-mm-dd");
            var sql = "INSERT INTO SerialLogs (pathFile, DateOfLog) VALUES('"+pathFile+"', '"+dateStr+"')";
            DatabaseService.executeSql(sql);
        }

        DatabaseService.insertLog = function(pathFile) {
            LoggingService.info("DatabaseService. Inserisco il log "+pathFile);
            var dateStr= dateFormat(new Date(), "yyyy-mm-dd");
            var sql = "INSERT INTO SystemLogs (pathFile, DateOfLog) VALUES('"+pathFile+"', '"+dateStr+"')";
            DatabaseService.executeSql(sql);
        }


        DatabaseService.saveTreatmentOnDB = function (treatment) {
            LoggingService.info("DatabaseService. Salvo il trattamento "+JSON.stringify(treatment));
            var dateStr= '';
            try {
                 dateStr = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");
                }
            catch (e) {
                 dateStr = 'Err in debug';
            }
            var sql = "INSERT INTO Treatments (Protocol, DateOfTreatment, Duration, power, steps";
            var isMegaride = treatment.protocol == 'Megaride';
            if (isMegaride)
                sql += ", ThPoint, KMax, Dx, Dy";
            sql += ") VALUES ('"+
                treatment.protocol+"', '"+ dateStr+"', "+treatment.duration+", "+treatment.power+", "+treatment.steps;
            if (isMegaride)
                sql += ", "+treatment.thickness+ ", "+treatment.kMax+", "+ treatment.VertexDistanceX+", "+ treatment.VertexDistanceY;
            sql += ")";
            LoggingService.debug("DatabaseService.  Questa sarà la query: "+sql);
            DatabaseService.executeSql(sql);
        }

        DatabaseService.updateTreatment = function (patientData) {
            LoggingService.info("DatabaseService. Aggiungo al trattamento i dati relativi al paziente: "+JSON.stringify(patientData));
            var sql = "UPDATE Treatments set ";
            
            sql += "patientName = '"+patientData.name+"', ";
            sql += "patientSurname = '"+patientData.surname+"', ";
            sql += "patientAge = '"+patientData.age+"', ";
            sql += "patientSex = '"+patientData.sex+"', ";
            sql += "Pathology = '"+patientData.pathology+"', ";
            sql += "Operator = '"+patientData.operator+"'";
            
            sql+= " WHERE Treatments.ID = (SELECT MAX (ID) FROM Treatments)";

            LoggingService.info("DatabaseService. SQL: "+ sql);
            try {
                DatabaseService.executeSql(sql);
                }
            catch (ex) {

            }
        }

        DatabaseService.getDefaultConfiguration = function () {
            var result = {};
            result.jumpVerifiyCalibration = 0;
            result.fakeImbibition = 0;
            result.resetWhiteLed = 0;
            result.demoEnabled = 1;
            result.timerSCEnabled = 1;
            result.showReport = 1;
            result.giorniLogOnLine = 120;
            result.disableHardware = false;
            result.MirrorCamera = 0;
            result.RotateCamera = 0;
            result.JumpStep = 0;
            result.giorniReportOnLine=150;
            return result;
        }

        DatabaseService.getConfiguration = function (callback) {
            LoggingService.info("DatabaseService. Recupero i dati di configurazione");
            var sql = "SELECT * FROM Configuration WHERE ID = 1";

            var callBack1 = function (tx, results) {
                if (results.rows.length == 0) {
                    LoggingService.info("DatabaseService.  Non è presente alcuna configurazione. Carico quella predefinita");
                    sql = "INSERT INTO Configuration (jumpVerifiyCalibration, fakeImbibition, resetWhiteLed, demoEnabled, timerSCEnabled, showReport, giorniLogOnLine, disableHardware, privacy) VALUES ("+
                    "0, 0, 0, 1, 1, 1, 120, 0, 0)";
                    try {
                        DatabaseService.executeSql(sql);
                        }
                    catch (ex) {

                    }
                    var result = DatabaseService.getDefaultConfiguration();
                    callback(result);
                }
                else {
                    LoggingService.info("DatabaseService. Dati di configurazione recuperati.  Invoco la callBack");
                    callback(results.rows[0]);
                }
            }
            
            try {
                DatabaseService.executeSql(sql, [], callBack1);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca della configurazione "+ ex);
            }
        }

        DatabaseService.setConfiguration = function(Configuration) {

            var sql = "UPDATE Configuration SET ";
            sql += "jumpVerifiyCalibration="
            if (Configuration.jumpVerifiyCalibration)
                sql += "1";
            else
                sql += "0";
            
            sql += ", fakeImbibition="
            if (Configuration.fakeImbibition)
                sql += "1";
            else
                sql += "0";
            sql += ", resetWhiteLed="
            if (Configuration.resetWhiteLed)
                sql += "1";
            else
                sql += "0";
            sql += ", demoEnabled="
            if (Configuration.demoEnabled)
                sql += "1";
            else
                sql += "0";
            sql += ", timerSCEnabled="
            if (Configuration.timerSCEnabled)
                sql += "1";
            else
                sql += "0";
            sql += ", showReport="
            if (Configuration.showReport)
                sql += "1";
            else
                sql += "0";
            sql += ", giorniLogOnLine="+Configuration.giorniLogOnLine;

            sql += ", MirrorCamera=";
            if (Configuration.MirrorCamera)
                sql += "1";
            else
                sql += "0";
            sql += ", RotateCamera=";
            if (Configuration.RotateCamera)
                sql += "1";
            else
                sql += "0";

            sql += ", JumpStep=";
            if (Configuration.JumpStep)
                sql += "1";
            else
                sql += "0";

            sql += ", privacy=";
            if (Configuration.privacyEnabled)
                sql += "1";
            else
                sql += "0";

            sql += ", giorniReportOnLine=";
            sql += Configuration.giorniReportOnLine;
                

            sql += " WHERE ID = 1";

            try {
                DatabaseService.executeSql(sql);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nel salvataggio della configurazione "+ ex);
            }

        }

        DatabaseService.clearSystemLogsBefore = function (dateStr) {
            var callBack = function (tx, results) {
                for (var i=0; i< results.rows.length; i++) {
                    LoggingService.info("Elimino il log "+ results.rows[i].pathFile);
                    if (fs != null) {
                        try {
                             fs.unlinkSync(results.rows[i].pathFile);
                            }
                        catch (ex) {
                        }
                    }
                    var sql1 = "DELETE FROM SystemLogs WHERE ID = "+results.rows[i].ID;
                    DatabaseService.executeSql(sql1);

                }
            }
            var sql = "SELECT * FROM SystemLogs WHERE CAST(strftime('%s', DateOfLog)  AS  integer) <= CAST(strftime('%s', '"+
                      dateStr+"')  AS  integer)";
            DatabaseService.executeSql(sql, [], callBack); 
        }

        DatabaseService.clearSerialLogsBefore = function (dateStr) {
            var callBack = function (tx, results) {
                for (var i=0; i< results.rows.length; i++) {
                    LoggingService.info("Elimino il log "+ results.rows[i].pathFile);
                    if (fs != null) {
                        try {
                             fs.unlinkSync(results.rows[i].pathFile);
                            }
                        catch (ex) {
                        }
                    }
                    var sql1 = "DELETE FROM SerialLogs WHERE ID = "+results.rows[i].ID;
                    DatabaseService.executeSql(sql1);

                }
                //per estrema sicurezza li tolgo anche con riga di comando
                var bashCmd = "find ./log ! -newermt \""+dateStr+" 00:00:00\" | xargs rm -rf";
                SystemCalls.execCommand(bashCmd);
                
            }
            var sql = "SELECT * FROM SerialLogs WHERE CAST(strftime('%s', DateOfLog)  AS  integer) <= CAST(strftime('%s', '"+
                      dateStr+"')  AS  integer)";
            DatabaseService.executeSql(sql, [], callBack); 
        }

        DatabaseService.getStatisticheTrattamenti = function (callBack) {
            var sql = "SELECT DISTINCT(Protocol) AS Protocol, COUNT(ID) AS Totale FROM Treatments GROUP BY Protocol";
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati relativi allo storico dei trattamenti"+ ex);
            }

        }


        DatabaseService.getDettaglioStatisticheTrattamento = function (filter, callBack) {
            var sql = "SELECT * FROM Treatments ";
            if (filter != null)
                sql += " WHERE Protocol = '"+filter+"'";
            sql += "ORDER BY DateOfTreatment DESC";
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati relativi al dettaglio dei trattamenti"+ ex);
            }
        }

        DatabaseService.getDettaglioSerialLogs = function (callBack) {
            var sql = "SELECT * FROM SerialLogs ORDER BY DateOfLog DESC";
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dettagli dei log seriali"+ ex);
            }
        }


        DatabaseService.getDettaglioApplicationLogs = function (callBack) {
            var sql = "SELECT * FROM SystemLogs ORDER BY DateOfLog DESC";
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dettagli dei log seriali"+ ex);
            }
        }
        

        DatabaseService.getSystemProperties = function (callBack) {

            LoggingService.info("DatabaseService. Recupero i dati di setting del dispositivo");
            var sql = "SELECT * FROM SystemProperties WHERE ID = 1";

            var callBack1 = function (tx, results) {
                if (results.rows.length == 0) {
                    LoggingService.info("DatabaseService.  Non sono presenti dati di setting. Inizializzazione");

                    sql = "INSERT INTO SystemProperties (serSkDriver, serTesta, serAlimentatore, serControlUnit, serCfxLinker, notes) VALUES ("+
                    "'SERIALE SCHEDA DRIVER ', 'SERIALE TESTA OTTICA', 'SERIALE ALIMENTATORE', 'SERIALE UNITA CONTROLLO', 'SERIALE DISPOSITIVO','NOTE')";
                    try {
                        DatabaseService.executeSql(sql);
                        }
                    catch (ex) {

                    }
                    var result = {};
                    result.serSkDriver = 'SERIALE SCHEDA DRIVER ';
                    result.serTesta = 'SERIALE TESTA OTTICA';
                    result.serAlimentatore = 'SERIALE ALIMENTATORE';
                    result.serControlUnit = 'SERIALE UNITA CONTROLLO';
                    result.serCfxLinker = 'SERIALE DISPOSITIVO';
                    result.notes = 'NOTE';
                    callBack(result);
                }
                else {
                    LoggingService.info("DatabaseService.  Ho trovato dati di setting");
                    callBack(results.rows[0]);
                }
            }
            
            try {
                DatabaseService.executeSql(sql, [], callBack1);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati di setting "+ ex);
            }

                
        }

        DatabaseService.setSystemProperties = function (props) {

            var sql = "UPDATE SystemProperties SET ";
            sql += "serSkDriver='"+ props.serSkDriver+"', ";
            sql += "serTesta='"+ props.serTesta+"', ";
            sql += "serAlimentatore='"+ props.serAlimentatore+"', ";
            sql += "serControlUnit='"+ props.serControlUnit+"', ";
            sql += "serCfxLinker='"+props.serCfxLinker+"', ";
            sql += "notes= '"+ props.notes+"' ";
            sql += " WHERE ID = 1";

            try {
                DatabaseService.executeSql(sql);
                alert("Le proprietà di sistema sono state salvate");
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nel salvataggio delle SystemProperties "+ ex);
                alert("Errore durante il salvataggio delle proprietà di sistema");
            }
        }

        DatabaseService.getConfigReferto = function (callBack) {
            LoggingService.info("DatabaseService. Recupero i dati di configurazione del referto");
            var sql = "SELECT * FROM ConfigReferto WHERE ID = 1";

            var callBack1 = function (tx, results) {
                if (results.rows.length == 0) {
                    LoggingService.info("DatabaseService.  Non sono presenti dati di configurazione del referto. Inizializzazione");

                    sql = "INSERT INTO ConfigReferto (ID, nomeStruttura, IndirizzoStruttura) VALUES ("+
                    "1, 'STRUTTURA', 'INDIRIZZO')";
                    try {
                        DatabaseService.executeSql(sql);
                        }
                    catch (ex) {

                    }
                    var result = {};
                    result.nomeStruttura = 'STRUTTURA';
                    result.IndirizzoStruttura = 'INDIRIZZO';
                    callBack(result);
                }
                else {
                    LoggingService.info("DatabaseService.  Ho trovato dati di configurazione del referto");
                    callBack(results.rows[0]);
                }
            }
            
            try {
                DatabaseService.executeSql(sql, [], callBack1);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati di setting "+ ex);
            }

        }

        DatabaseService.setConfigReferto = function (conf) {
            var sql = "UPDATE ConfigReferto SET ";
            sql += "nomeStruttura='"+ conf.nomeStruttura+"', ";
            sql += "IndirizzoStruttura='"+ conf.IndirizzoStruttura+"'";
            sql += " WHERE ID = 1";

            try {
                DatabaseService.executeSql(sql);
                alert("La configurazione del referto è stata salvata");
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nel salvataggio delle impostazioni del referto "+ ex);
                alert("Errore durante il salvataggio delle impostazioni del referto");
            }
        }

        DatabaseService.clearLogsBefore = function (dateStr) {
             LoggingService.info("DatabaseService.  Elimino i log a partire dal  "+dateStr);
             DatabaseService.clearSystemLogsBefore(dateStr);
             DatabaseService.clearSerialLogsBefore(dateStr);
            }

        DatabaseService.clearLogs = function() {
            LoggingService.info("DatabaseService.  Elimino i log antecedenti");
            //Callback eseguita dopo la select
            var callBack = function (tx, result) {
                var giorni = 120;
                if (result.rows.length > 0)
                    giorni = result.rows[0].giorniLogOnLine;
                var today = new Date()
                var priorDate = new Date().setDate(today.getDate()-giorni);
                var dateStr = '';
                try {
                     dateStr = dateFormat(priorDate, "yyyy-mm-dd");
                    }
                catch (e) {
                     dateStr = 'Err in debug';
                }
                LoggingService.info("DatabaseService.  Elimino i log più vecchi di "+giorni+" giorni");
                DatabaseService.clearLogsBefore(dateStr);
            }

            var sql = "SELECT giorniLogOnLine FROM Configuration WHERE ID = 1";
            DatabaseService.executeSql(sql, [], callBack);
        }

        DatabaseService.getChecksumForLog = function (row, callBack) {
            var result = {};
            result.pathFile = row.pathFile;
            result.ID = row.ID;
            var ckb = function (ret) {                
                result.checksum = ret;
                callBack(result);        
            };
            SystemCalls.getFileChecksum(row.pathFile, ckb);

        }

        DatabaseService.checkLogsChecksum = function () {
            LoggingService.info("DatabaseService.  Verifico la presenza dei checksum");
            //Callback eseguita dopo la select
            var callBack = function (tx, result) {
                for (var i=0; i< result.rows.length; i++) {
                    //VERIFICARE PASSAGGIO VARIABILI DA FUNZIONE ESTERNA A INTERNA                    
                    var ckb = function (ret) {
                        var sql1 = "UPDATE SystemLogs SET checksum='"+ret.checksum+"' WHERE ID ="+ret.ID;
                        DatabaseService.executeSql(sql1);                     
                    };
                    DatabaseService.getChecksumForLog(result.rows[i], ckb);
                }
            }

            var today = new Date();
            var priorDate = new Date().setDate(today.getDate()-1);
            var dateStr = '';
            try {
                    dateStr = dateFormat(priorDate, "yyyy-mm-dd");
                }
            catch (e) {
                    dateStr = 'Err in debug';
            }

            var sql = "SELECT * FROM SystemLogs WHERE checksum IS NULL AND CAST(strftime('%s', DateOfLog)  AS  integer) <= CAST(strftime('%s', '"+
                        dateStr+"')  AS  integer)";;
            DatabaseService.executeSql(sql, [], callBack);
            
        }


           DatabaseService.clearReportsBefore = function (dateStr) {
            LoggingService.info("DatabaseService.  Elimino i report a partire dal  "+dateStr);
            var callBack = function (tx, results) {
                for (var i=0; i< results.rows.length; i++) {
                    LoggingService.info("Elimino il report "+ results.rows[i].ID);
                    if (fs != null) {
                        try {
                             fs.unlinkSync(results.rows[i].FilePath);
                            }
                        catch (ex) {
                        }
                    }
                    var sql1 = "DELETE FROM Reports WHERE ID = "+results.rows[i].ID;
                    DatabaseService.executeSql(sql1);

                }                
            }
            var sql = "SELECT * FROM Reports WHERE CAST(strftime('%s', DateOfTreatment)  AS  integer) <= CAST(strftime('%s', '"+
                      dateStr+"')  AS  integer)";
            DatabaseService.executeSql(sql, [], callBack); 
        }

       DatabaseService.clearReports = function() {
           LoggingService.info("DatabaseService.  Elimino i report antecedenti");
           //Callback eseguita dopo la select
           var callBack = function (tx, result) {
               var giorni = 150;
               if (result.rows.length > 0)
                   giorni = result.rows[0].giorniReportOnLine;
               var today = new Date()
               var priorDate = new Date().setDate(today.getDate()-giorni);
               var dateStr = '';
               try {
                    dateStr = dateFormat(priorDate, "yyyy-mm-dd");
                   }
               catch (e) {
                    dateStr = 'Err in debug';
               }
               LoggingService.info("DatabaseService.  Elimino i report più vecchi di "+giorni+" giorni");
               DatabaseService.clearReportsBefore(dateStr);
           }

           var sql = "SELECT giorniReportOnLine FROM Configuration WHERE ID = 1";
           DatabaseService.executeSql(sql, [], callBack);
       }


        DatabaseService.getUtilizzoDevice = function (callback) {
            LoggingService.info("DatabaseService. Recupero i dati relativi al minutaggio del dispositivo");
            var sql = "SELECT SUM (Duration) AS 'totMinutes' from Treatments";
            try {
                DatabaseService.executeSql(sql, [], callback);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati relativi al minutaggio del dispositivo "+ ex);
            }
        }



        DatabaseService.insertVerify = function (calibrationCheck) {

            LoggingService.info("DatabaseService. Salvo la verifica di calibrazione  "+JSON.stringify(calibrationCheck));
            var dateStr= '';
            try {
                 dateStr = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");
                }
            catch (e) {
                 dateStr = 'Err in debug';
            }
            var sql = "INSERT INTO CalibrationChecks (tipoVerifica, motivoVerifica, dataVerifica, totMinutiUtilizzo, potenzaEmessa, potenzaMisurata, esitoVerifica) VALUES "+
                      "('"+calibrationCheck.tipoVerifica+"', '"+ calibrationCheck.motivoVerifica+"', '"+ dateStr+"', "+
                      calibrationCheck.totMinutiUtilizzo+", "+calibrationCheck.potenzaEmessa+", "+calibrationCheck.potenzaMisurata+", "+
                      calibrationCheck.esitoVerifica+")";

            LoggingService.info("DatabaseService.  Questa sarà la query di inserimento del dettaglio della verifica: "+sql);
            DatabaseService.executeSql(sql);
        }



        DatabaseService.getCalibrationChecks = function (callback) {
            LoggingService.info("DatabaseService. Recupero i dati relativi alle verifiche di calibrazione");
            var sql = "SELECT * FROM CalibrationChecks ORDER BY dataVerifica DESC";
            try {
                DatabaseService.executeSql(sql, [], callback);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati relativi alle verifiche di calibrazione "+ ex);
            }
        }

        DatabaseService.getLastCalibrationCheck = function (callback) {
            LoggingService.info("DatabaseService. Recupero i dati relativi all'ultima verifica di calibrazione");
            var sql = "SELECT * from CalibrationChecks ORDER BY dataVerifica DESC LIMIT 1";
            try {
                 DatabaseService.executeSql(sql, [], callback);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati relativi alle verifiche di calibrazione "+ ex);
            }
        }

        DatabaseService.getCalibrationsChecksAfterPeriod = function (callBack) {
             var today = new Date();
             var giorni = 10 * 31;
             var priorDate = new Date().setDate(today.getDate()-giorni);


            var dateStr = '';
            try {
                 dateStr = dateFormat(priorDate, "yyyy-mm-dd");
                }
            catch (e) {
                 dateStr = 'Err in debug';
            }
            var sql = "SELECT * FROM CalibrationChecks WHERE CAST(strftime('%s', dataVerifica)  AS  integer) >= CAST(strftime('%s', '"+
                      dateStr+"')  AS  integer) AND esitoVerifica = 0 ORDER BY dataVerifica DESC";
            DatabaseService.executeSql(sql, [], callBack); 
        }

        DatabaseService.getReports = function (userID, callBack) {
            var sql = "SELECT * FROM Reports ";
            if (userID>=0)
                sql+= "WHERE creator="+userID+" ";
            sql += "ORDER BY DateOfTreatment DESC";
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati relativi al dettaglio dei trattamenti"+ ex);
            }
        }

        DatabaseService.getNextReportID = function(callBack) {
            var dateStr= dateFormat(new Date(), "yyyy-mm-dd");
            var sql = "INSERT INTO Reports (PatientName, PatientSurname, Operator, FilePath, DateOfTreatment) VALUES ('', '', '', '', '"+dateStr+"')";
            
            var callBack1 = function(result) {
                var sql1 = "SELECT MAX(ID) AS ID FROM Reports";
                DatabaseService.executeSql(sql1, [], callBack);
            } 
            try {
                DatabaseService.executeSql(sql, [], callBack1);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati relativi al dettaglio dei trattamenti"+ ex);
            }            
        }

        DatabaseService.updateReportData = function(idReport, data, userID, callBack) {
            var sql = "UPDATE Reports set ";
            if (data.name)
                sql+= "PatientName = '"+data.name+"', ";
            if (data.surname)
                sql+="PatientSurname='"+data.surname+"', ";
            if (data.operator)
                sql+="Operator= '"+data.operator+"',";
            var Vis = null;
            if (data.visible == undefined)
              Vis = 1;
            else
              Vis = data.visible;
            sql+= "visible="+Vis;
            if (userID > -1)
              sql+=", creator="+userID;
            sql += " WHERE ID ="+idReport;
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {

            }
        }

        DatabaseService.showHideReportData = function(idReport, data, callBack) {
            var sql = "UPDATE Reports set visible = "+data.visible+ " WHERE ID ="+idReport;
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {

            }
        }

        DatabaseService.deleteReportRecord = function(idReport) {
            var sql = "DELETE FROM Reports WHERE ID ="+idReport;
            try {
                DatabaseService.executeSql(sql);
                }
            catch (ex) {
            }
        }

        DatabaseService.checkDefaultUsers = function () {

          /*  CREATE TABLE IF NOT EXISTS Users (ID integer primary key autoincrement, Username TEXT, Name TEXT, Surname TEXT, DateOfPasswordExpiry Date, "+
                                                "UserAlias TEXT, Password TEXT, Role TEXT, PasswordToBeChanged INTEGER DEFAULT 1, userDefault DEFAULT 0)");
                                                */
            LoggingService.info("DatabaseService. Verifico che esistano gli utenti di default");
            
            var sql = "SELECT * FROM Users WHERE Username='support' AND Password='support'";
            var callBack= function (tx, results) {
                if (results.rows.length == 0) {
                    LoggingService.info("DatabaseService.  Non presente utente support. Inizializzazione");

                    sql = "INSERT INTO Users (Username, Password, Role, PasswordToBeChanged, userDefault) VALUES ("+
                    "'support', 'support', 'support', 0, 1)";
                    try {
                        DatabaseService.executeSql(sql);
                        }
                    catch (ex) {

                    }
                }
                else {
                    
                }
            }
            
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore in inizializzazione utenti "+ ex);
            }

            var sql = "SELECT * FROM Users WHERE Username='support' AND Password='superSupport'";
            var callBack= function (tx, results) {
                if (results.rows.length == 0) {
                    LoggingService.info("DatabaseService.  Non presente utente SuperSupport. Inizializzazione");

                    sql = "INSERT INTO Users (Username, Password, Role, PasswordToBeChanged, userDefault) VALUES ("+
                    "'support', 'superSupport', 'superSupport', 0, 1)";
                    try {
                        DatabaseService.executeSql(sql);
                        }
                    catch (ex) {

                    }
                }
                else {
                    
                }
            }
            
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore in inizializzazione utenti "+ ex);
            }

            var sql = "SELECT * FROM Users WHERE Username='admin' AND Password='eRc1936'";
            var callBack= function (tx, results) {
                if (results.rows.length == 0) {
                    LoggingService.info("DatabaseService.  Non presente utente admin. Inizializzazione");

                    sql = "INSERT INTO Users (Username, Password, Role, PasswordToBeChanged, userDefault) VALUES ("+
                    "'admin', 'eRc1936', 'super', 0, 1)";
                    try {
                        DatabaseService.executeSql(sql);
                        }
                    catch (ex) {

                    }
                }
                else {
                    
                }
            }
            
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore in inizializzazione utenti "+ ex);
            }

            var sql = "SELECT * FROM Users WHERE Username='administrator'";
            var callBack= function (tx, results) {
                if (results.rows.length == 0) {
                    LoggingService.info("DatabaseService.  Non presente utente administrator. Inizializzazione");

                    sql = "INSERT INTO Users (Username, Password, Role, PasswordToBeChanged, userDefault) VALUES ("+
                    "'administrator', 'administrator', 'admin', 1, 1)";
                    try {
                        DatabaseService.executeSql(sql);
                        }
                    catch (ex) {

                    }
                }
                else {
                    
                }
            }
            
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore in inizializzazione utenti "+ ex);
            }   
        }

        DatabaseService.getUsers = function (callBack) {
            var sql = "SELECT * FROM Users WHERE userDefault=0";
            try {
                DatabaseService.executeSql(sql, [], callBack);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati relativi al dettaglio dei trattamenti"+ ex);
            }
        }

        DatabaseService.updateUserPassword = function(user, newPassword) {
            var sql = "UPDATE Users SET Password='" + newPassword+"', PasswordToBeChanged=0 WHERE ID="+user.ID;            
            try {
                DatabaseService.executeSql(sql);
                LoggingService.info("DatabaseService.  Errore in inizializzazione utenti "+ ex);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore in inizializzazione utenti "+ ex);
            }  
        }

        DatabaseService.checkUserAndPassword = function(userName, password, callBack) {
            LoggingService.info("DatabaseService. Verifico le credenziali dell'utente "+userName);
            var sql = "SELECT * FROM Users WHERE Username='"+userName+"' AND Password='"+password+"'";
    
            var callBack1 = function (tx, results) {
                if (results.rows.length > 0) {
                    var user = results.rows[0];
                    if (user.userDefault == 0) {
                            user.PasswordValidity = DatabaseService.getDaysBeforeExpiry(user);
                            if (user.PasswordValidity <= 0)
                                user.PasswordToBeChanged = 1;
                        }
                    callBack(user);
                }
                else {
                    callBack(null);
                }
            }
            
            try {
                DatabaseService.executeSql(sql, [], callBack1);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella ricerca dei dati dell'utente "+ ex);
            }
        }

        DatabaseService.checkCanAddUser = function (user, callBack) {
            var sql = "SELECT * FROM Users WHERE Username='"+user.Username+"'";
    
            var callBack1 = function (tx, results) {
                callBack(results.rows.length <= 0);                
            }
            
            try {
                DatabaseService.executeSql(sql, [], callBack1);
                }
            catch (ex) {
                LoggingService.info("DatabaseService.  Errore nella checkCanAddUser "+ ex);
            }
        }

        DatabaseService.getDatePasswordExpiry = function (user) {
            if (user.PasswordValidity == null)
              user.PasswordValidity = 180;
            var dateExpiry = new Date();
            dateExpiry = dateExpiry.setDate(dateExpiry.getDate()+Number(user.PasswordValidity));
            var dateStr = '';
            try {
                 dateStr = dateFormat(dateExpiry, "yyyy-mm-dd");
                }
            catch (e) {
                 dateStr = 'Err in debug';
            }
            return dateStr;
        }

        DatabaseService.getDaysBeforeExpiry = function (user) {
            if (user.passwordExpiryDate == null)
              return 180;
            const [year, month, day] = user.passwordExpiryDate.split("-");
            var dateExpiry = new Date(+year, +month-1, +day);
            var today = new Date();
            var Difference_In_Time = dateExpiry.getTime() - today.getTime();
            var Difference_In_Days = Math.ceil(Difference_In_Time / (1000*3600*24));
            return Difference_In_Days;//dateExpiry.getDate() - today.getDate();
        }

        DatabaseService.addUser = function (user) {
            var dateStr = DatabaseService.getDatePasswordExpiry(user);
            user = DatabaseService.checkUserForSql(user);
            var sql = "INSERT INTO Users (Username, Name, Surname, UserAlias, Password, Role, PasswordToBeChanged, userDefault, passwordExpiryDate) VALUES ("+
                     "'"+user.Username+"', '"+user.Name+"', '"+user.Surname+"', '"+user.UserAlias+"', '"+
                     user.Password+"', '"+user.Role+"', "+user.PasswordToBeChanged+", 0, '"+dateStr+"')";
            try {
                DatabaseService.executeSql(sql);
                }
            catch (ex) {

            }
            LoggingService.info("DatabaseService.  Aggiunto utente "+ user);
        }

        DatabaseService.updateUser = function (user) {
            user = DatabaseService.checkUserForSql(user);
            var sql = "UPDATE Users SET ";
            if (user.Username)
                sql+="Username='"+user.Username+"', ";
            if (user.Name)              
                sql+="Name='"+user.Name+"', ";
            if (user.Surname)
                sql+="Surname='"+user.Surname+"', ";
            if (user.UserAlias)
                sql+="UserAlias='"+user.UserAlias+"', ";
            if (user.Password)
                sql+="Password='"+user.Password+"', ";
            if (user.Role)
                sql+="Role='"+user.Role+"', ";
            if (user.PasswordToBeChanged)
                sql+="PasswordToBeChanged="+user.PasswordToBeChanged+", ";
            if (user.userDefault)
                sql+="userDefault="+user.userDefault+", ";
            sql = sql.substring(0, sql.length-2);
            
            sql+= ", passwordExpiryDate='"+DatabaseService.getDatePasswordExpiry(user)+"'";
            sql+=" WHERE ID="+user.ID;
            try {
                DatabaseService.executeSql(sql);
                }
            catch (ex) {

            }
            LoggingService.info("DatabaseService.  Aggiornato utente "+ user);
        }

        DatabaseService.deleteUser = function (user) {
            var sql = "DELETE FROM Users WHERE ID = "+user.ID;
            try {
                DatabaseService.executeSql(sql);
                }
            catch (ex) {

            }
            LoggingService.info("DatabaseService.  Rimosso utente "+ user);
        }

        DatabaseService.checkUserForSql = function (user) {
            if (user.Username)
                user.Username = DatabaseService.checkString(user.Username);
            if (user.Name) 
                user.Name = DatabaseService.checkString(user.Name);
            if (user.Surname) 
                user.Surname = DatabaseService.checkString(user.Surname);
            if (user.UserAlias) 
                user.UserAlias = DatabaseService.checkString(user.UserAlias);
            if (user.Password) 
                user.Password = DatabaseService.checkString(user.Password);
            return user;
        }



        DatabaseService.init = function() {
            DatabaseService.openDB();
            DatabaseService.clearLogs();
            DatabaseService.clearReports();
            DatabaseService.checkDefaultUsers();
            DatabaseService.checkLogsChecksum();
        }


        DatabaseService.init();
        
        return DatabaseService;
        
    })
