
angular.module('megaride')    
.controller('CtrlMegarideCalibration', 
    function ($scope, $stateParams, $rootScope, $state, CalibrationService) {

    	$scope.state = {};
    	$scope.state.numSteps = 3;		//Numero di step richiesti per completare la calibrazione
    	$scope.state.step = 1;
    	
    	//0: Avvio procedura; 
    	//1: posizionare uv; 
    	//2: Inserimento valore;	
    	//3: Calibrazione completata
    	$scope.state.status = 0;

    	$scope.startCalibration = function () {
    		$scope.state.status = 1;
    		CalibrationService.startCalibration();
    	}

        $scope.insertSpessore = function () {
            $state.go('megarideSpessore'); 
        }

    	$scope.updateCalibration = function() {
    		CalibrationService.updateVal($scope.state.valUvMeter);
    		$scope.state.step++;
    		$scope.state.valUvMeter = '';
    		if ($scope.state.step == ($scope.state.numSteps+1))	{
    			$scope.state.status = 3;
                CalibrationService.setProtocolCalibrated('megaride');
    			$scope.insertSpessore();
    		}
    	}

        if (CalibrationService.isProtocolCalibrated('megaride')) {
            $scope.insertSpessore();
        }
	}
);
   
    	