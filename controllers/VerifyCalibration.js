
angular.module('megaride')    
.controller('CtrlVerifyCalibration', 
    function ($scope, $stateParams, $rootScope, $state, $mdDialog, $timeout, 
            CalibrationService, HwCommunicationService, ShutdownService, LoggingService, CameraService, hotkeys, exe4Camera) {

    	$scope.state = {};
    	$scope.state.numSteps = 3;		//Numero di step richiesti per completare la calibrazione
    	$scope.state.step = 1;
        $scope.state.calibration = 0;
        $scope.state.valUvMeter= '';
        $scope.state.verificationValue = 0;
        
        $scope.numClick = 0;    //In questa variabile vengono conteggiati i click per saltare la verifica

        $scope.state.warning = 0;       // Viene messo a 1 in caso di warning e a 2 in caso di errore

        var STATES = {
            "INIT": 0,                  
            "POS_UV": 1,
            "INIT_VERIFY": 2,
            "SET_POWER": 3,
            "INSERT_VALUE": 4
        }
        
        $scope.state.status = STATES.POS_UV;    

        var TIPI_COMANDO = { 
            "VERIFICA_TESTA" : 0,
            "CALIBRA_TESTA" : 1
        }
        
        $rootScope.stateDialog = {};
        $rootScope.stateDialog.tipoComando = TIPI_COMANDO.CALIBRA_TESTA;


        $scope.apply = function() {
            if ($scope.$$phase) 
                return;
            try {
                $scope.$apply();
            }
            catch (err) {

            }
        }

        //Passaggio allo stato successivo
        $scope.goNext = function () {
            $scope.gotoProtocolSelection();
        }

        $scope.goToHome = function () {
            if (exe4Camera)
                CameraService.detachVideo();
            CalibrationService.PowerOff().then(function(result){
                    CalibrationService.setFocus(0).then(function(result){
                        $state.go('splash');
                    })
                });
        }

        //Va alla selezione del protocollo
        $scope.gotoProtocolSelection = function () {
            CalibrationService.PowerOff();
            CalibrationService.setFocus(0);
            if ($scope.state.warning == 2)
                $state.go('splash');
            else
                $state.go('selectProtocol');
        }

        $scope.gotoSplash = function () {
            $state.go('splash');
        }

        //Messaggio di warning
        $scope.getWarningMessage = function () {
            return 'Calibration.suggestions';
            if ($scope.state.warning == 1)
                return 'Calibration.warning';
            else if ($scope.state.warning == 2)
                return 'Calibration.error';
        }


        //chiamata dopo l'inserimento di un valore in fase di verifica
        $scope.updateVerifyValue= function() {

        }

        $scope.isCameraExe = function() {
            return exe4Camera;
        }

        //Funzione associata al pulsante OK
        $scope.okCalibration = function() {
            var stato = $scope.state.status;
            switch ($scope.state.status) {
                case STATES.POS_UV:
                        CameraService.detachVideo();
                        CalibrationService.setFocus(0).then(function(result){
                                CalibrationService.startVerifyCalibration().then(function(result) {
                                    $scope.powerLedOn();
                                })
                            });

                        break;
               case STATES.INIT_VERIFY:    
                        //CalibrationService.startVerifyCalibration();
                        $scope.powerLedOn();
                        break;
                case STATES.INSERT_VALUE:
                        if ($scope.state.warning == 1) {
                            $scope.state.warning = 0;
                            $scope.goNext();
                            break;
                        }
                        else if ($scope.state.warning != 0) {
                            $scope.state.warning = 0;
                            $scope.gotoSplash();
                            break;
                        }

                        $scope.valueUpdated();
                        break;
                default:  
                        $scope.valueUpdated();
                        break;
            }
            
        }

        $scope.addCalibrationCheck = function () {
            var calibrationCheck = {};

            calibrationCheck.tipoVerifica= "VERIFICA SPOT";
            calibrationCheck.motivoVerifica = "";
            calibrationCheck.potenzaEmessa = $scope.state.verificationValue;
            calibrationCheck.potenzaMisurata = Number($scope.state.valUvMeter.replace(",", "."))*1000; 
            calibrationCheck.esitoVerifica = $scope.state.warning;
            CalibrationService.insertVerify(calibrationCheck);
        }



    	$scope.valueUpdated = function() {
			$timeout(function() {	//sostituzione $apply
                var toCheck = $scope.state.verificationValue; //$scope.state.tableCal[$scope.state.step-1].uvmeter;
                var rangeInf = toCheck * 0.90;
                var rangeSup = toCheck * 1.10;
                var inserito = Number($scope.state.valUvMeter.replace(",", "."))*1000;
                LoggingService.info("Verifica stato di calibrazione della macchina");
                LoggingService.info("Valore emesso: "+$scope.state.verificationValue);
                LoggingService.info("Valore inserito dall'utente: "+inserito);

                $scope.state.warning = 0;
                if (inserito > rangeSup || inserito < rangeInf) {
                    rangeInf = toCheck * 0.80;
                    rangeSup = toCheck * 1.20;
                    if (inserito > rangeSup || inserito < rangeInf)
                        $scope.state.warning = 2;
                    else
                        $scope.state.warning = 1;
                    }

                $scope.addCalibrationCheck();
                if ($scope.state.warning == 2) {
                    //$scope.apply();
                    CalibrationService.PowerOff();
                    return;
                }
                $scope.state.step++;    		
                $scope.state.valUvMeter = '';
                if ($scope.state.step == ($scope.state.numSteps+1)) {
                    CalibrationService.PowerOff().then(function(){
                        if ($scope.state.warning == 0)
                        $scope.goNext();    
                    })
                }
                else if ($scope.state.warning < 2)
                    $scope.powerLedOn();
            });
    	}



        function DialogController($scope, $mdDialog, $state) {
            $scope.password = '';
            $scope.hide = function() {
              $mdDialog.hide();
            };

            $scope.cancel = function() {
              $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
              $mdDialog.hide(answer);
            };

            $scope.okButton = function (ev) {
                if ($scope.password == 'megaride') {
                    $mdDialog.hide();
                    if ($rootScope.stateDialog.tipoComando == TIPI_COMANDO.CALIBRA_TESTA)
                        $state.go('calibration');
                    else if ($rootScope.stateDialog.tipoComando == TIPI_COMANDO.VERIFICA_TESTA)
                        $state.go('verifyOptHead');
                }
                else {
                    $mdDialog.show(
                      $mdDialog.alert()
                        //.parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Attenzione')
                        .textContent('Password errata')
                        .ariaLabel('Password errata')
                        .ok('Ok')
                        .targetEvent(ev)                        
                    );
                }

            }
          }

        $scope.verificaTesta = function(ev) {
            $rootScope.stateDialog.tipoComando = TIPI_COMANDO.VERIFICA_TESTA;
            $scope.showPasswordDialog(ev);
        }

        $scope.calibraFabbrica = function(ev) {
            $rootScope.stateDialog.tipoComando = TIPI_COMANDO.CALIBRA_TESTA;
            $scope.showPasswordDialog(ev);
        }

        $scope.showPasswordDialog = function (ev) {
            $mdDialog.show({
                  controller: DialogController,
                  template: 'Password: <form ng-submit="okButton()"><input type=\'password\' ng-model=\'password\' autofocus></input><md-button class="md-raised" ng-click="okButton()">Ok</md-button></form>',
                  parent: angular.element(document.body),
                  targetEvent: ev,
                  clickOutsideToClose:true,
                  fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                .then(function(answer) {
                  //$scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                  //$scope.status = 'You cancelled the dialog.';
                });
        }


        $scope.calibraFabbricaOld = function(ev) {
            var confirm = $mdDialog.prompt()
                  .title('Azioni di amministrazione')
                  .textContent('Necessaria password per accedere a funzioni di amministrazione.')
                  .placeholder('Password')
                  .ariaLabel('Inserire password')
                  .targetEvent(ev)
                  .ok('ok')
                  .cancel('Annullare');
            $mdDialog.show(confirm).then(function(result) {
              //if (result == 'iromedgroup') {
                if (result == 'megaride') {
                    $state.go('calibration');
                }
                else {
                    $mdDialog.show(
                      $mdDialog.alert()
                        //.parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Attenzione')
                        .textContent('Password errata')
                        .ariaLabel('Password errata')
                        .ok('Ok')
                        .targetEvent(ev)                        
                    );
                }

            }, function() {

              
            });
          };

        //Accende il led per la verifica al valore necessario
        $scope.powerLedOn = function () {
            $timeout(function() {	//sostituzione $apply
                $scope.state.verificationValue = CalibrationService.getVerifyValue();
                var afterPower = function(result) {
                        $scope.state.status = STATES.INSERT_VALUE;
                        //$scope.apply();
                    };
                if ($scope.state.step <= $scope.state.numSteps) {
                    CalibrationService.PowerOn($scope.state.verificationValue).then(afterPower);
                }
                else{
                    CalibrationService.PowerOff().then(afterPower);
                }
            });
        }


        $scope.loadCalibrationTable = function() {
            $timeout(function() {	//sostituzione $apply
                //INIZIALIZZAZIONE VARIABILI
                $scope.state.step = 1;
                $scope.state.numSteps = 1;
                $scope.state.status = STATES.POS_UV;
                //Accendo i led rossi
                CalibrationService.setFocus(1);
                //$scope.apply();
            });
        }

        $scope.onclick = function () {
            $scope.numClick ++;
            if ($scope.numClick == 3) {
                $scope.goNext();
            }
            $timeout( function() {$scope.numClick = 0}, 3000);            
        }

        $scope.handleVideo = function (stream) {
            video.src = window.URL.createObjectURL(stream);
            }
         
        $scope.videoError = function(e) {
            // do something
            }



         
         
        $scope.init = function () {

            //Aggiungo listener per eventi di interesse di questa pagina
            $rootScope.$on('CalibrationTable', function (event, message) {
                $scope.loadCalibrationTable();                 
             });

           ShutdownService.setOperation(ShutdownService.OPERATIONS.SHUTDOWN);
           /*if (!HwCommunicationService.isInited()) 
             $scope.gotoSplash();*/

            CameraService.attachCamera();

            
            /*video = document.querySelector("#videoElementVC");
            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
             
            if (navigator.getUserMedia) {       
                navigator.getUserMedia({video: true}, $scope.handleVideo, $scope.videoError);
            }

            document.addEventListener('DOMContentLoaded', function(){
            v = document.getElementById('videoElementVC');
            canvas = document.getElementById('canvas');
            context = canvas.getContext('2d');
            w = canvas.width;
            h = canvas.height;

            },false);*/
        
            $scope.loadCalibrationTable();
        }

        $scope.init();
	}
);
   
    	