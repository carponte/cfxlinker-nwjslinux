'use strict';

var ApplicationLogger;
var SerialLogger;
var SerialRawLogger;

    try {
      ApplicationLogger = require('./modules/ApplicationLogger.js');
    }
    catch (err) {
    }

    try {
        SerialLogger = require('./modules/SerialLogger.js');
      }
      catch (err) {
          console.log(err);
      }  

    try {
        SerialRawLogger = require('./modules/SerialRawLogger.js');
    }
    catch (err) {
        console.log(err);
    }  



function Log(str) {
   if (typeof ApplicationLogger !== 'undefined') 
      ApplicationLogger.info(str);
}

function debug (str) {
    if (typeof ApplicationLogger !== 'undefined') 
      ApplicationLogger.debug(str);   
}


function serialLog(str) {
  if (typeof SerialLogger !== 'undefined') 
    setTimeout(SerialLogger.info(str), 100);
}

function serialRawLog(str) {
    if (typeof SerialRawLogger !== 'undefined') 
      setTimeout(SerialRawLogger.info("\n"+str), 100);
  }

function resolveLogPath() {
    if (typeof ApplicationLogger !== 'undefined') 
      return ApplicationLogger.getLoggerPath();
    return "";
}

function resolveSerialLogPath() {
    if (typeof SerialLogger !== 'undefined') 
      return SerialLogger.getSerialLoggerPath();
    return "";
}



angular.module('megaride')
.service('LoggingService', function($translate, $log){
            	
        var LoggingService = {}
        var log = {};
        log.debug = false;
        log.log = true;
        log.warn = true;
        log.error = true;

        var remoteLoggingFunction;
        LoggingService.serialListener = null;

	LoggingService.checkDir = function () {
	var fs = require('fs');
	var dir = './log';

	if (!fs.existsSync(dir)) {
	    fs.mkdirSync(dir);
	  }


        }


        LoggingService.init = function () {
	  LoggingService.checkDir();
        }

        LoggingService.log = function(msg) {
            Log(msg);
            /*if (!log.log)
                return;
            $log.log(msg);
            if (remoteLoggingFunction != null) {
                remoteLoggingFunction(msg);
            }*/
        }

        LoggingService.info = function(msg) {
            Log(msg);
            /*if (!log.info)
                return;
            $log.info(msg);
            if (remoteLoggingFunction != null) {
                remoteLoggingFunction(msg);
            }*/
        }

        LoggingService.warn = function(msg) {
            /*if (!log.warn)
                return;
            $log.warn(msg);
            if (remoteLoggingFunction != null) {
                remoteLoggingFunction(msg);
            }*/
        }

        LoggingService.error = function(msg) {
            Log(msg);
            /*if (!log.error)
                return;
            $log.error(msg);
            if (remoteLoggingFunction != null) {
                remoteLoggingFunction(msg);
            }*/
        }

        LoggingService.debug = function(msg) {
            if (!log.debug)
                return;
            debug(msg);            
        }

        LoggingService.uncaughtException = function(msg) {
            Log("==== START OF UNCAUGHT EXCEPTION =====" );
            Log(JSON.stringify(msg));
            Log("===== END OF UNCAUGHT EXCEPTION =====");
        }

        LoggingService.setSerialListener = function (lstn) {
            LoggingService.serialListener = lstn;
        }

        LoggingService.serialLog = function (msg) {
            if (LoggingService.serialListener != null)
                LoggingService.serialListener(msg);
            serialLog(msg);
        }

        LoggingService.serialRawLog = function (msg) {
            serialRawLog(msg);
        }
        

        LoggingService.setRemoteLoggingFunction = function (func){
            //remoteLoggingFunction = func;
        }

        LoggingService.getLogPath = function() {
            try {
                var path = resolveLogPath();
                return path;
                }
            catch (e) {
                LoggingService.info("LoggingService. Error in getLogPath "+e);
            }
        }

        LoggingService.getSerialLogPath = function() {
            try {
                var path = resolveSerialLogPath();
                return path;
                }
            catch (e) {
                LoggingService.info("LoggingService. Error in getSerialLogPath "+e);
            }
        }


	    LoggingService.init();
        
        return LoggingService;
        
    })
