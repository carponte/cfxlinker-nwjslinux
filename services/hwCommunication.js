'use strict';

var SystemCalls;
    try {
      SystemCalls = require('./modules/SystemCalls.js');
    }
    catch (err) {
    }

var SerialPort = null;
    try {
//      SerialPort = require('serialport');
    }
    catch (err) {
      console.log(err);
    }


angular.module('megaride')
.service('HwCommunicationService', function($rootScope, $interval, $timeout, LoggingService, DebugModeService, PromiseService, 
    $mdDialog, $translate, $q){
            	
        var HwCommunicationService = {}

        HwCommunicationService.inited = false;
        HwCommunicationService.hardwareInited = false;
        HwCommunicationService.debugMode = DebugModeService.disableHardware();

        HwCommunicationService.timeOutSend = 500;
        HwCommunicationService.timeOutReceive = 500;

        HwCommunicationService.isFreezed = false;

        HwCommunicationService.productId = 24577;
        HwCommunicationService.vendorId = 1027;

        HwCommunicationService.firmwareVersion=-1;
        HwCommunicationService.firmwareVersionWithDecimal = 2.2;  // da quale versione del firmware la base dei tempi è passata a decimi di secondo
        HwCommunicationService.IntervalFirmwareReading = undefined;
        
        HwCommunicationService.timestampLastRead = new Date();
        HwCommunicationService.pollingEnabled = true;
        HwCommunicationService.pollingMSecs = 4000;
        HwCommunicationService.pollingTimer = null;
        HwCommunicationService.pollingPaused = false;

        HwCommunicationService.pollingInterrupted = false;
        HwCommunicationService.options = null;




  HwCommunicationService.serialPort = '/dev/ttyS0';

  //togliere il commento sotto per debug
// HwCommunicationService.serialPort = '/dev/ttyS1';
//	HwCommunicationService.serialPort = null;

        var websocket; 
        var rebootSent = false;

        var serialStatusListener = null;

        //Messaggi in debug
        var DEBUG_CONFIGURATION = {
          "errorMessage": false,
          "resultMessage": false,
          "packetType": false
        };


         //STATI INTERNI DELLA MACCHINA A STATI
        var SM_STATES = {
            "PROCESSOR_INIT": 0, "EEPROM_FAIL" : 1, "SYSTEM_FAILURE" : 2, "TEST_MODE": 3,
            "SYSTEM_IDLE": 4, "SYSTEM_READY": 5, "MANUAL_MODE_GET_PAR": 6, "MANUAL_MODE_READY": 7,
            "DRESDA_MODE_GETPAR": 8, "DRESDA_MODE_READY": 9, "IONTOFORESI_MODE_GETPAR": 10,
            "IONTOFORESI_MODE_READY": 11, "MEGARIDE_MODE_GETPAR": 12, "MEGARIDE_MODE_READY": 13,
            "PROCESS_INIT": 14, "PROCESS_READY": 15, "PROCESS_RUN": 16, "PROCESS_RUNNING": 17,
            "PROCESS_STEP_COMPLETE": 18, "WAIT_NEXT_STEP": 19, "PROCESS_COMPLETE": 20,
            "PROCESS_STOP": 21, "PROCESS_PAUSE": 22, "PROCESS_PAUSED": 23, "PROCESS_RESUME":24,
            "PROCESS_CALIBRATE": 25
            };

        HwCommunicationService.PACKET_TYPES = {
            "FOCUS": 1, 
            "CALIB_GET_TABLE": 2,
            "CALIB_RESET": 3,
            "CALIB_DONE": 4,
            "CALIB_CURRENT": 5,
            "CALIB_POWER": 6,
            "TREATMENT_SELECT": 7,
            "TREATMENT_STATUS": 8,
            "TREATMENT_PARAMS": 9,
            "IDLE": 10,
            "REBOOT": 11,
            "WHITE_LED": 12, 
            "QUERY_SERIAL": 13,
            "SWITCH_USER": 14,
            "BEEP": 15
        };

        HwCommunicationService.messages = [];
        HwCommunicationService.sendingPacket = null;

        HwCommunicationService.serialConnectionInfo = null;
        HwCommunicationService.serialSearchLock = false;

        //Il servizio ha due livelli di promessa.  Il primo è verso l'hardware ed il secondo verso il chiamante.
        //Il meccanismo serve a garantire il dispatch della risposta dell'hardware già all'interno di questo servizio prima di propagarlo
        // 
        HwCommunicationService.timeoutPromesse = 4000;
        HwCommunicationService.L1promiseResolve = function(msg) {};
        HwCommunicationService.L1promiseReject = function(msg) {};
        HwCommunicationService.timeoutPromiseL1 = null;         //Timeout promessa di livello 1

        HwCommunicationService.L2promiseResolve = function(msg) {};
        HwCommunicationService.L2promiseReject = function(msg) {};
        HwCommunicationService.timeoutPromiseL2 = null;         //Timeout promessa di livello 2

        HwCommunicationService.bufferSerial = {};
        HwCommunicationService.bufferSerial.buffer = [];
        HwCommunicationService.bufferSerial.currentCommand =  "";
        HwCommunicationService.bufferSerial.mustAppendNext = false;
        HwCommunicationService.bufferSerial.stringBuffer = "";
        HwCommunicationService.bufferSerial.processingSignalMsg = false;  //Viene messa a true se si sta processando un messagio di segnalazione
        HwCommunicationService.bufferSerial.logEnabled = true;
        HwCommunicationService.bufferSerial.currentPromise = null;
        HwCommunicationService.bufferSerial.promises = [];

        /* Interprets an ArrayBuffer as UTF-8 encoded string data. */
        HwCommunicationService.ab2str = function(buf) {
          try {
            var bufView = new Uint8Array(buf);
            var encodedString = String.fromCharCode.apply(null, bufView);
            return decodeURIComponent(escape(encodedString));
            }
          catch (err) {            
          }
        };

        /* Converts a string to UTF-8 encoding in a Uint8Array; returns the array buffer. */
        HwCommunicationService.str2ab = function(str) {
          var encodedString = unescape(encodeURIComponent(str));
          var bytes = new Uint8Array(encodedString.length);
          for (var i = 0; i < encodedString.length; ++i) {
            bytes[i] = encodedString.charCodeAt(i);
          }
          return bytes.buffer;
        };

        HwCommunicationService.onReceiveError = function (info) {
          //Errori che possono accadere: "timeout", "disconnected", "device_lost",  "break", "frame_error", "overrun", "buffer_overflow", "parity_error", "system_error"
          var error = info.error;
          LoggingService.serialRawLog('ERRORE'+JSON.stringify(info));
          if (error == "timeout")
            return;
          if (error == "frame_error" || error == "overrun" || error == "buffer_overflow" || error == "parity_error") {
            LoggingService.info("HwCommunicationService.  onReceiveError "+ JSON.stringify(info));
            return;  
          }
          LoggingService.info("HwCommunicationService.  onReceiveError "+ JSON.stringify(info));
          //chrome.serial.disconnect(info.connectionId, HwCommunicationService.onSerialDisconnect);
          HwCommunicationService.hardwareInited = false;
          HwCommunicationService.serialConnectionInfo = null;
          //HwCommunicationService.serialConnectionInfo = null;
          HwCommunicationService.freezeApplication();
        }

       
        HwCommunicationService.onPortOpen = function (ConnectionInfo) {
          HwCommunicationService.serialConnectionInfo = ConnectionInfo;
          HwCommunicationService.hardwareInited = true;
          LoggingService.info("Comunicazione seriale instaurata");
          console.log("Porta seriale aperta");
          console.log(ConnectionInfo);
        
          
          chrome.serial.onReceive.addListener(HwCommunicationService.onReceive);
          chrome.serial.onReceiveError.addListener(HwCommunicationService.onReceiveError);   
              HwCommunicationService.IntervalFirmwareReading = $interval(function(){
                HwCommunicationService.loadFirmwareVersion().then(
                  function (result) {
                    if (HwCommunicationService.firmwareVersion == -1)
                      return;
                    if (angular.isDefined(HwCommunicationService.IntervalFirmwareReading)) {
                      $interval.cancel(HwCommunicationService.IntervalFirmwareReading);
                      HwCommunicationService.IntervalFirmwareReading = undefined;
                    }
                    //HwCommunicationService.unFreezeApplication();          
                    //$rootScope.$broadcast('hardwareStatus', null);
                  }
                );
              }, 2000);
              HwCommunicationService.unFreezeApplication();          
              $rootScope.$broadcast('hardwareStatus', null);
        }

      HwCommunicationService.openConnection = function (portPath) {
         if (typeof chrome == 'undefined')
          return;
        
          //Mi connetto con nodejs
          if (SerialPort != null) { 
            HwCommunicationService.serialPort = new SerialPort(portPath, { autoOpen: false });
            HwCommunicationService.serialPort.on('open', function() {
              console.log('Aperta porta con nodejs');
            })

            HwCommunicationService.serialPort.open(function (err) {
              if (err) {
                return console.log('Error opening port: ', err.message)
              }
              // Because there's no callback to write, write errors will be emitted on the port:
              HwCommunicationService.serialPort.write('ver');
            })

            // The open event is always emitted
            

          }
          //mi connetto con chromium
          else {
            if (HwCommunicationService.options != null) {
               /*console.log('Esco dal tentativo di effettuare una doppia connessione alla porta seriale tentato da');
               console.trace();*/
               return;
              }
              
            HwCommunicationService.options = {
              'bitrate': 115200,
              'dataBits': 'eight',
              'parityBit': 'no',
              'stopBits': 'one',
              "receiveTimeout": HwCommunicationService.timeOutReceive,
              "sendTimeout": HwCommunicationService.timeOutSend
            }
            LoggingService.info("Provo a connettermi con chromium alla porta seriale  "+portPath);
/*            console.log('Sono in openConnection chiamato da ');
            console.trace();*/
            if (HwCommunicationService.serialConnectionInfo == null || !HwCommunicationService.hardwareInited) {
              try {
                chrome.serial.connect(portPath, HwCommunicationService.options, HwCommunicationService.onPortOpen);
                  }
              catch(err) {
                LoggingService.info("HwCommunicationService. Errore in openConnection "+err);
                HwCommunicationService.options = null;
              }
            }
          }
        
 
      }

     
      HwCommunicationService.onSerialDisconnect = function () {
        if (DebugModeService.disableHardware())
          return;
        LoggingService.info("HwCommunicationService. Chiusura forzata della connessione");
	      HwCommunicationService.hardwareInited = false;
        HwCommunicationService.freezeApplication();
      };

      HwCommunicationService.disconnect = function () {
        try  {
	  chrome.serial.disconnect(HwCommunicationService.serialConnectionInfo.connectionId,  HwCommunicationService.onSerialDisconnect);
	 
        }
        catch (err) {
          console.log(err);
	  chrome.serial.getConnectionInfos(function (info){console.log(info)});
        }

      }

      HwCommunicationService.onGetDevices = function (ports) {
         if(chrome.runtime.lastError) {
            LoggingService.info("HwCommunicationService.onGetDevices.  Chrome runtime error "+ chrome.runtime.lastError);
            return;
            }
          try {
              for (var i=0; i<ports.length; i++) {
                if (ports[i].productId== HwCommunicationService.productId && 
                    ports[i].vendorId==HwCommunicationService.vendorId) {
                        HwCommunicationService.openConnection(ports[i].path);
                    }
                }
              HwCommunicationService.serialLock = false;
            }
          catch (err) {
             LoggingService.info("HwCommunicationService.onGetDevices "+ err);
          }
      }

      HwCommunicationService.onGetConnectionInfos = function (connectionInfos) {
          if (HwCommunicationService.serialLock)
            return;
          if (HwCommunicationService.serialConnectionInfo == null){
            LoggingService.info("Cerco una porta seriale disponibile");
            if (typeof chrome !== 'undefined') {
                HwCommunicationService.serialLock = true;
                try {
                    chrome.serial.getDevices(HwCommunicationService.onGetDevices);
                  }
                catch(err) {
                   LoggingService.info("HwCommunicationService.onGetConnectionInfo "+ err);
                }
              }
            return;
          }
          /*else 
            HwCommunicationService.onPortOpen(HwCommunicationService.serialConnectionInfo);*/
          var connectionToReset = HwCommunicationService.serialConnectionInfo == null;
          for (var i=0; i<connectionInfos.length; i++) {
            if (connectionInfos[i].connectionId == HwCommunicationService.serialConnectionInfo.connectionId) {
              if (connectionInfos[i].paused) {
                 try  {
                    chrome.serial.disconnect(connectionInfos[i].connectionId, HwCommunicationService.onSerialDisconnect);
                  }
                 catch (err) {
                    LoggingService.info("HwCommunicationService.onGetConnectionInfo.  Errore in chisura connessione "+ err);
                 }
              }
            }
          }
      }

      HwCommunicationService.checkChrome = function () {
        return  typeof chrome != 'undefined' && typeof chrome.serial != 'undefined' && chrome != null && chrome.serial != null;
      }

      HwCommunicationService.openSerialPort = function () {
          if (!HwCommunicationService.checkChrome())
            return;
          if (DebugModeService.disableHardware())
            return;
          if (HwCommunicationService.serialPort != null) {
          if (!HwCommunicationService.hardwareInited)
                HwCommunicationService.openConnection(HwCommunicationService.serialPort);
            }
          else
            chrome.serial.getConnections(HwCommunicationService.onGetConnectionInfos);
      }


      HwCommunicationService.resolveDefer = function (defer) {
        var app = [];
        
        for (var i=0; i<HwCommunicationService.bufferSerial.buffer.length; i++){
            app.push(HwCommunicationService.bufferSerial.buffer[i]);
        }
        HwCommunicationService.bufferSerial.buffer = [];
        HwCommunicationService.bufferSerial.currentPromise.resolve(app);
        HwCommunicationService.bufferSerial.currentPromise = null;
      }

      HwCommunicationService.rejectDefer = function (defer, cause) {
//        defer.reject(cause);
        HwCommunicationService.bufferSerial.currentPromise = null;
      }

      HwCommunicationService.handlePromises = function() {
        //Se c'è una promessa pending che non viene risolta entro 5 secondi la tolgo dalla coda
        if (HwCommunicationService.bufferSerial.currentPromise != null && (Date.now() - HwCommunicationService.bufferSerial.timeStamp > 5000)) {
          LoggingService.serialRawLog('\t\t\t\t\t\t\t\t\t------ENTRATO NEL CASO DI EMERGENZA');
          HwCommunicationService.bufferSerial.currentPromise.nAttempt++;
          if (HwCommunicationService.bufferSerial.currentPromise.nAttempt <2) {
            LoggingService.serialRawLog('\t\t\t\t\t\t\t\t\t------PROVO A RISCRIVERE IL MSG '+HwCommunicationService.bufferSerial.currentPromise.cmd);
            HwCommunicationService.handlePromise(HwCommunicationService.bufferSerial.currentPromise);
          }
          else
            HwCommunicationService.rejectDefer(HwCommunicationService.bufferSerial.currentPromise);
          return;
        }
        
        if (HwCommunicationService.bufferSerial.promises.length == 0 || HwCommunicationService.bufferSerial.currentPromise != null)
          return;
        
        LoggingService.serialRawLog('\t\t\t\t\t\t\t\t\t------CODA MESSAGGI');
        for (var i=0; i<HwCommunicationService.bufferSerial.promises.length; i++)
          LoggingService.serialRawLog('\t\t\t\t\t\t\t\t\t------'+HwCommunicationService.bufferSerial.promises[i].cmd);
        HwCommunicationService.rawlogBuffer();
        LoggingService.serialRawLog('\t\t\t\t\t\t\t\t\t FINE CODA MESSAGGI----------');
        HwCommunicationService.handlePromise(HwCommunicationService.bufferSerial.promises.splice(0, 1)[0]);
      }

      HwCommunicationService.handlePromise = function(defer) {
        HwCommunicationService.bufferSerial.currentPromise = defer;
        HwCommunicationService.bufferSerial.timeStamp = Date.now();
        var cmdP = HwCommunicationService.str2ab(defer.cmd + String.fromCharCode(13));
        HwCommunicationService.bufferSerial.currentCommand = defer.cmd;
        HwCommunicationService.bufferSerial.logEnabled = defer.logEnabled;

        
        if (HwCommunicationService.serialConnectionInfo == null) {
            HwCommunicationService.rejectDefer(defer, '(HwCommunicationService.serialConnectionInfo is null');
            return;
         }

         if (defer.logEnabled) {
          LoggingService.serialLog('------>');
          LoggingService.serialLog(defer.cmd);
          LoggingService.serialLog('\n<-------\n');
          LoggingService.serialRawLog('\t\t\t\t\t\t\t\t\t------>'+defer.cmd);
         }

        try {
          chrome.serial.send(HwCommunicationService.serialConnectionInfo.connectionId, cmdP, 
            function (receiveInfo) {
              }
            );
        }
        catch (err) {
          HwCommunicationService.rejectDefer(defer, err);
        }
        //chrome.serial.flush(HwCommunicationService.serialConnectionInfo.connectionId, function (){});
      }
      

      HwCommunicationService.writeCMD = function (content) {
        /*if (DebugModeService.disableHardware())
          return;*/

        var defer = $q.defer();
        //let var4Promise = PromiseService.getPromise();

        defer.cmd = content.command;
        defer.logEnabled = true;
        defer.nAttempt = 0;          
        if (content.logDisabled)
         defer.logEnabled = false;
        if (HwCommunicationService.serialConnectionInfo != null)
          HwCommunicationService.bufferSerial.promises.push(defer);
        return defer.promise;
      }

      HwCommunicationService.isCommandInQueue = function (cmd) {
        for (var i=0; i<HwCommunicationService.bufferSerial.promises.length; i++)
          if (HwCommunicationService.bufferSerial.promises[i].cmd == cmd)
            return true;
        return false;
      }

     HwCommunicationService.writeCMDWithoutPromise = function (cmd) {
        if (DebugModeService.disableHardware())
          return;
        if (HwCommunicationService.bufferSerial.logEnabled) {
          LoggingService.serialLog('------>');
          LoggingService.serialLog(cmd);
          LoggingService.serialLog('<-------');
        }
        var cmdP = HwCommunicationService.str2ab(cmd + String.fromCharCode(13)); 

        try {
          chrome.serial.send(HwCommunicationService.serialConnectionInfo.connectionId, cmdP, 
            function (receiveInfo) {
               // LoggingService.Log(receiveInfo);
              }
            );
        }
        catch (err) {
          //$timeout(function () {HwCommunicationService.L1promiseResolve("")}, 1000);
          LoggingService.log("HwCommunicationService.  writeCMDWithoutPromise "+err);
        }
        
      }


       HwCommunicationService.onReceive = function (receiveInfo) {
          HwCommunicationService.timestampLastRead = new Date();
          var str = HwCommunicationService.ab2str(receiveInfo.data);
          if (str == null) {
            return;
          }
          HwCommunicationService.processPacket(str);
          HwCommunicationService.sendingPacket = null;
        }

      HwCommunicationService.setFocus = function (focus) {
         HwCommunicationService.writeCMD("focus*"+focus);         
      }

        HwCommunicationService.send = function (content) {
            if (content.pckType == 'shutdown')
                HwCommunicationService.sendPacket(content);
            else {
              return HwCommunicationService.writeCMD(content);
            }
            return true;
        }

        HwCommunicationService.sendWithPromise = function (packet) {
          return HwCommunicationService.send(packet);
        }

        HwCommunicationService.sendWithoutPromise = function (packet) {
            if (packet.pckType == 'shutdown')
                HwCommunicationService.sendPacket(packet);
            else {
              HwCommunicationService.writeCMDWithoutPromise(packet.command);
            }
            return true;
        }

        HwCommunicationService.processPacketsFifo = function() {
           if (HwCommunicationService.messages.length == 0 || HwCommunicationService.sendingPacket != null)
             return;
           if (websocket == null)
             return;
           HwCommunicationService.sendingPacket = HwCommunicationService.messages.shift();
           HwCommunicationService.sendPacket(HwCommunicationService.sendingPacket);
        }

        HwCommunicationService.sendPacket = function(packet) {
           /*LoggingService.debug("Sending websocket message");
           LoggingService.debug(packet);
           var contentJson = angular.toJson(packet);
           LoggingService.debug(contentJson);
           try {
              websocket.send(contentJson);
            }
            catch (ex) {
              websocket = null;
            }*/
            alert("Sending packet" + JSON.stringify(packet));
            HwCommunicationService.writeCMD(packet.command);

        }


        HwCommunicationService.onMessageWS = function (evt) {
            var message = angular.fromJson(evt.data);
            HwCommunicationService.processPacket(message);
            HwCommunicationService.sendingPacket = null;
        }

        HwCommunicationService.onOpenWS = function () {
           LoggingService.debug("HwCommunicationService.  Websocket opened");
           HwCommunicationService.inited = true;
           $rootScope.$broadcast('hardwareInitedEvent', null);
           HwCommunicationService.sendQuerySerial();
        }

        HwCommunicationService.onCloseWS = function () {
           LoggingService.error("HwCommunicationService.  Websocket connection is closed...");
           HwCommunicationService.inited = false;
           HwCommunicationService.hardwareInited = false;
           websocket = null;
           HwCommunicationService.messages = [];
        }

        HwCommunicationService.isAsyncMessage = function (string) {
           return string.indexOf('MSG')>-1;
          }

        HwCommunicationService.dicardString = function (string) {
          return 
            (string.indexOf('MSG:UV Photodiode loop') > -1) ||
            (strings.indexOf('MSG:UV Current loop') > -1);          
        }

        HwCommunicationService.rawlogBuffer = function() {
          var buffPrefix = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
          
          var BuffString = "";
          var stringsApp = HwCommunicationService.bufferSerial.stringBuffer.split("\n");
            for (var i=0; i<stringsApp.length; i++){
              BuffString += buffPrefix+stringsApp[i].trim()+"\n"; 
              }

          LoggingService.serialRawLog(buffPrefix+ "////-------Contenuto del buffer: \n"+BuffString+buffPrefix+"-------////");
        }

        HwCommunicationService.processSignallingMessage = function (string) {
          console.log("Processo il messaggio asincrono "+string+".  Buffer: "+HwCommunicationService.bufferSerial.stringBuffer);
          LoggingService.serialRawLog("\t\t\t\t\t\t\t\t\t\t\t\tPROCESSO IL MESSAGGIO ASINCRONO "+string+".  Buffer: "+HwCommunicationService.bufferSerial.stringBuffer+";");
          if (string.indexOf('STEP COMPLETED') >-1) {
             $rootScope.$broadcast('stepCompleted', string);
            }
          else if (string.indexOf('PROCESS_RUNNING')>-1) {
             $rootScope.$broadcast('stepRunning', string);
            }
          else if (string.indexOf('PROCESS COMPLETED')>-1) {
            $rootScope.$broadcast('processCompleted', string);
           }
          else if (string.indexOf('PROCESS PAUSED')>-1) {
            $rootScope.$broadcast('processPaused', string);
          }
          else if (string.indexOf('PROCESS INIT')>-1) {
            //$rootScope.$broadcast('processPaused', string);
          }
        }

        HwCommunicationService.checkAndProcessSignallingMsg = function () {
          var idx1 = 0;
          var idx2 = 0;
          while (idx1 != -1) {
            idx1 = HwCommunicationService.bufferSerial.stringBuffer.lastIndexOf('MSG');
            if (idx1 == -1)
              break;
            idx2 = HwCommunicationService.bufferSerial.stringBuffer.indexOf('\n', idx1);
            if (idx2 == -1)
               return;
            LoggingService.serialRawLog('ESTRAGGO IL MSG ASINCRONO');
            HwCommunicationService.rawlogBuffer();
            var stringMessage = HwCommunicationService.bufferSerial.stringBuffer.substr(idx1, idx2-idx1);
            HwCommunicationService.bufferSerial.stringBuffer = HwCommunicationService.bufferSerial.stringBuffer.replace(stringMessage+'\n', "");
            LoggingService.serialRawLog('Msg Estratto');
            LoggingService.serialRawLog(stringMessage);
            HwCommunicationService.rawlogBuffer();
            HwCommunicationService.processSignallingMessage(stringMessage);
          }
        }

        HwCommunicationService.processAsyncMessages = function (strings) {
            if (HwCommunicationService.bufferSerial.logEnabled) {
              var buffPrefix = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
              var BuffString = "";
              var stringsApp = strings.split("\n");
                for (var i=0; i<stringsApp.length; i++){
                  BuffString += buffPrefix+stringsApp[i].trim()+"\n"; 
                  }
              LoggingService.serialRawLog(buffPrefix+"<---"+strings.replaceAll('\n', "#10").replaceAll('\r', "#13"));
            }
            
              
            //Verifico se c'è un echo
            var stringToReplace = HwCommunicationService.bufferSerial.currentCommand+String.fromCharCode(13)+String.fromCharCode(10);
            var index = strings.indexOf(stringToReplace);
            if (index > -1) {
              //if (strings.charCodeAt(stringToReplace.length) == 13 && strings.charCodeAt(stringToReplace.length+1) == 10)
                strings = strings.substring (index + stringToReplace.length, strings.length);
                strings = strings.trim();
                 if (strings.length == 0) {
                    HwCommunicationService.bufferSerial.mustAppendNext = true;
                    return;
                  }
            }           
            HwCommunicationService.bufferSerial.stringBuffer += strings;
            if (HwCommunicationService.bufferSerial.logEnabled) {
               HwCommunicationService.rawlogBuffer();
              }

//            console.log("HW.  Ricevuto "+strings +".  Buffer: "+HwCommunicationService.bufferSerial.stringBuffer);

            
            //Processo i messaggi di segnalazione
            HwCommunicationService.checkAndProcessSignallingMsg();
            

            //Verifico se sono arrivato alla fine del comando
            var cummandApp = HwCommunicationService.bufferSerial.currentCommand.split("*");
            var strCmd = cummandApp[0];
            if (strCmd.indexOf("reboot")>-1)
              index =  HwCommunicationService.bufferSerial.stringBuffer.indexOf("credits OK")
            else
              index = HwCommunicationService.bufferSerial.stringBuffer.indexOf(strCmd+" ");
            if (index == -1){
                HwCommunicationService.bufferSerial.mustAppendNext = true;
              }
            else {
              if (HwCommunicationService.bufferSerial.stringBuffer.indexOf("\n", index)>-1||
              //INIZIO GESTIONE DEI CASI SPECIALI "GIOVANNI"
              HwCommunicationService.bufferSerial.stringBuffer.indexOf(strCmd+" OK")>-1 || 
              HwCommunicationService.bufferSerial.stringBuffer.indexOf(strCmd+" FAIL")>-1
              //FINE GESTIONE DEI CASI
              )
                HwCommunicationService.bufferSerial.mustAppendNext = false;
              else
                HwCommunicationService.bufferSerial.mustAppendNext = true;
            }
            if (HwCommunicationService.bufferSerial.mustAppendNext)
              return;

            //Processo il comando
            var stringsApp = HwCommunicationService.bufferSerial.stringBuffer.split("\n");
            for (var i=0; i<stringsApp.length; i++){
                if (!HwCommunicationService.dicardString(stringsApp[i]))
                  HwCommunicationService.bufferSerial.buffer.push(stringsApp[i].trim());
              }
            
            if (HwCommunicationService.bufferSerial.logEnabled) {
              LoggingService.serialLog("------------");
              LoggingService.serialLog(stringsApp);
              LoggingService.serialLog("------------");
            }
            //console.log("HWCS.  RSP MSG: "+HwCommunicationService.bufferSerial.buffer);
           
            //HwCommunicationService.bufferSerial.stringBuffer = "";
            HwCommunicationService.bufferSerial.stringBuffer = HwCommunicationService.bufferSerial.stringBuffer.trim();
            var endIndex = HwCommunicationService.bufferSerial.stringBuffer.indexOf("\n", index);
            if (endIndex == -1)
              HwCommunicationService.bufferSerial.stringBuffer = "";
            else {
             // console.log("HW.  TI HO BECCATO, VAFANCOOOOL");
              HwCommunicationService.bufferSerial.stringBuffer = HwCommunicationService.bufferSerial.stringBuffer.substring(endIndex);
              //console.log("HW.  Il buffer è: "+HwCommunicationService.bufferSerial.stringBuffer);
              HwCommunicationService.checkAndProcessSignallingMsg();
            }
              
            return;
        }


        HwCommunicationService.processPacket = function(packet) {
            LoggingService.debug("HwCommunicationService. Got weboscketMessage: Message is received...");
            if (DEBUG_CONFIGURATION.errorMessage)
              LoggingService.debug(packet.errorMessage);
            if (DEBUG_CONFIGURATION.resultMessage)
              LoggingService.debug(packet.resultMessage);
            if (DEBUG_CONFIGURATION.packetType)
              LoggingService.debug(packet.packetType);
            if (packet.pckType && packet.pckType == 'QRYSER') {
              HwCommunicationService.hardwareInited = packet.resultMessage;
              $rootScope.$broadcast('hardwareInitedEvent', packet);
            }
            if (packet.pckType && packet.pckType == 'SER_STATUS') {
              HwCommunicationService.hardwareInited = packet.resultMessage;
              if (!HwCommunicationService.hardwareInited)
                HwCommunicationService.freezeApplication();
              else
                HwCommunicationService.unFreezeApplication();
              $rootScope.$broadcast('hardwareStatus', packet);
            }

             HwCommunicationService.processAsyncMessages(packet);
             if (HwCommunicationService.checkCurrentCommandConsistency()) {
              //$timeout.cancel(HwCommunicationService.timeoutPromiseL1);
              HwCommunicationService.resolveDefer();
             }
             //$rootScope.$broadcast('hardwareEvent', inStr);                        
        }


        HwCommunicationService.chechHardwareAndConnection = function () {
              /*if (HwCommunicationService.inited && HwCommunicationService.hardwareInited)
                  return;
                if (websocket == null || websocket.readyState  == websocket.CLOSED || websocket.readyState  == websocket.CLOSING)
                  HwCommunicationService.inited = false;
                try {
                    if (!HwCommunicationService.inited) {
                      websocket =  new WebSocket("ws://localhost:3000/");
                        websocket.onopen = HwCommunicationService.onOpenWS;
                        websocket.onmessage = HwCommunicationService.onMessageWS;
                        websocket.onclose = HwCommunicationService.onCloseWS;
                        }

                    }
                  catch (err) {
                    console.log("ERRORE NELL'APERTURA DELLA COMUNICAZIONE");
                  }*/

                }



       

        HwCommunicationService.processCalibrationPacket = function (packet) {
             LoggingService.debug("Got calib table from device");
             LoggingService.debug(packet);
        }

        HwCommunicationService.isInited = function () {
            var out = HwCommunicationService.hardwareInited || HwCommunicationService.debugMode;
            return out;
        }

        HwCommunicationService.getCommandDelay = function() {
          return 1200;
        }

        HwCommunicationService.isDebugMode = function () {
          return HwCommunicationService.debugMode;
        }

        HwCommunicationService.sendTestPacket = function (){
            var packet = {};
            packet.command = "Command";
            HwCommunicationService.send(packet);
        }

        HwCommunicationService.log = function (msg) {
          /*var packet = {};
          packet.command = "Command";
          packet.pckType="log";
          packet.message = msg;
          HwCommunicationService.send(packet);*/
          LoggingService.info(msg);
        }

        LoggingService.setRemoteLoggingFunction(HwCommunicationService.log);

        //Questa funzione fa l'handle di un comando generico
        HwCommunicationService.handleGenericCommand = function (msg, okFunction, errorFunction) {
            if (msg != 'OK') {
                if (errorFunction != null)
                  errorFunction();
                return;
            }

        if (okFunction != null)
            okFunction();
        }


        //SOFT RESET
        HwCommunicationService.getRebootMessage = function () {
            return "reboot";
        }
       
        //MODALITA BASSO CONSUMO
        HwCommunicationService.getIdleMessage = function () {
            return "Idle";
        }
       
        //PARAMETRI LIMITE DEL LED UV
        //current: corrente in milliAmpere
        //power: massima potenza ottica
        HwCommunicationService.getUVLedMessage = function (current, power) {
            return "uvled*"+current+","+power;
        }
       
        //PARAMETRI LIMITE DEL LED BIANCO
        //current: corrente in milliAmpere
        HwCommunicationService.getWhiteLedMessage = function (current) {
            return "uvled*"+current;
        }

        //LETTURA TEMPERATURA DEL LED UV
        HwCommunicationService.getReadTemperatureMessage = function () {
            return "gettemp";
        }

        //LETTURA STATO INTERNO DEL LED UV
        HwCommunicationService.getQueryStateMessage = function () {
            return "query";
        }

        HwCommunicationService.reboot = function (okFunction, errorFunction) {
            //WebSocketService.send(HwCommunicationService.getRebootMessage);
            
        }





/****************
INIZIO FUNZIONI GENERICHE
*****************/

      //ATTIVA IL BLINKING DEI LED ROSSI
      //stato del blinking: 1 attivo, 0 disattivo
      HwCommunicationService.sendFocusMessage = function (statusFocus) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.FOCUS;
           packet.command = "focus*"+statusFocus;
           return HwCommunicationService.sendWithPromise(packet);
      }

      //SPEGNE IL SISTEMA
      HwCommunicationService.sendShutDownMessage = function () {
           /*var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.FOCUS;
           packet.pckType = 'shutdown';
           packet.command = 'shutdown';
           HwCommunicationService.send(packet);*/
          SystemCalls.shutDown();
      }

      //MODIFICA L'UTENTE DI SISTEMA CORRENTE
      HwCommunicationService.sendSwitchUser = function () {
           /*var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.SWITCH_USER;
           packet.pckType = 'switchuser';
           packet.command = 'switchuser';
           HwCommunicationService.send(packet);*/
           SystemCalls.switchUser();
      }

      //SPEGNE IL SISTEMA
      HwCommunicationService.sendIdle = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.IDLE;
           packet.pckType = 'CMD';
           packet.command = 'idle';
           return HwCommunicationService.sendWithPromise(packet);
      }

      //RICHIEDE INFO SULLA VERSIONE
      HwCommunicationService.sendVer = function (polling) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.IDLE;
           packet.pckType = 'CMD';
           packet.command = 'ver';
           if (polling)
             packet.logDisabled = true;
           return HwCommunicationService.sendWithPromise(packet);
      }

      //ACCENDE O SPEGNE IL LED BIANCO
      HwCommunicationService.setWhiteLed = function (status) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.WHITE_LED;
           packet.pckType = 'CMD';
           packet.command = 'whon*'+status;
           //packet.command = 'Whiteled*5'
           return HwCommunicationService.sendWithPromise(packet);
      }

      //CONFIGURA LA MODALITA' DI FUNZIONAMENTO DEL LED BIANCO (1 automatico, 0 manuale)
      HwCommunicationService.setWhiteLedMode = function (status) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.WHITE_LED;
           packet.pckType = 'CMD';
           packet.command = 'whmode*'+status;
           //packet.command = 'Whiteled*5'
           return HwCommunicationService.sendWithPromise(packet);
      }

//SPEGNE IL SISTEMA
      HwCommunicationService.sendReboot = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.REBOOT;
           packet.pckType = 'CMD';
           packet.command = 'reboot';
          /* if (rebootSent || websocket == null)
            return;
           rebootSent = true;*/
           return HwCommunicationService.sendWithPromise(packet);
      }

      HwCommunicationService.sendRebootNP = function() {
          var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.REBOOT;
           packet.pckType = 'CMD';
           packet.command = 'reboot';
          /* if (rebootSent || websocket == null)
            return;
           rebootSent = true;*/
	   $timeout(function () {
		HwCommunicationService.disconnect(); 
		}, 200);
           return HwCommunicationService.sendWithoutPromise(packet);
      }

      //CHIEDE LO STATO DELLA CONNESSIONE SERIALE
      HwCommunicationService.sendQuerySerial = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.QUERY_SERIAL;
           packet.pckType = 'QRYSER';
           packet.command = 'qryser';
           return HwCommunicationService.sendWithPromise(packet);
      }

      HwCommunicationService.getL2Promise = function () {
         return new Promise ( function (resolve, reject) {
                HwCommunicationService.L2promiseResolve = resolve;
                HwCommunicationService.L2promiseReject = reject;
              }
          );
      }

      //Questa funzione risolve la promessa sul chiamante
      HwCommunicationService.resolveL2Promise = function (result) {
        if (result == null)
          HwCommunicationService.L2promiseResolve("");
        else  
          HwCommunicationService.L2promiseResolve(result.slice(0));
        HwCommunicationService.bufferSerial.buffer = [];
        //HwCommunicationService.bufferSerial.stringBuffer = "";
      }


/****************
FINE FUNZIONI GENERICHE
*****************/


/****************
INIZIO FUNZIONI RELATIVE ALLA CALIBRAZIONE
*****************/

      //RICHIEDE LA TABELLA DI CALIBRAZIONE
      HwCommunicationService.sendRequestCalibration = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.CALIB_GET_TABLE;
           packet.command = "calibtable*";
           return HwCommunicationService.sendWithPromise(packet);
      }


       //RESET DELLA TABELLA DI CALIBRAZIONE
      HwCommunicationService.sendResetCalibration = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.CALIB_RESET;
           packet.command = "clearcalib";
           return HwCommunicationService.sendWithPromise(packet);
      } 

      //SETTA LA CORRENTE PER LA CALIBRAZIONE
      //TODO gestione ritorno
       HwCommunicationService.sendCurrentCalibration = function (val) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.CALIB_CURRENT;
           packet.command = "calib*"+val;
           return HwCommunicationService.sendWithPromise(packet);
       }

      //INDICA LA CORRENTE REALMENTE LETTA
      //TODO gestione ritorno
       HwCommunicationService.setCalibPower = function (val) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.CALIB_POWER;
           packet.command = "setcalibpower*"+val;
           return HwCommunicationService.sendWithPromise(packet);       
       }


       //CALIBRAZIONE COMPLETATA
      HwCommunicationService.sendCalibrationDone = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.CALIB_DONE;
           packet.command = "calibdone";
           return HwCommunicationService.sendWithPromise(packet);
      }     

      
/****************
FINE FUNZIONI RELATIVE ALLA CALIBRAZIONE
*****************/


/****************
INIZIO FUNZIONI TRATTAMENTI
*****************/

      //SETTA LA MODALITA' DI FUNZIONAMENTO IN DRESDA
      HwCommunicationService.setDresdaMode = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_SELECT;
           packet.command = "dresdamode";
           return HwCommunicationService.sendWithPromise(packet);
      }

      //SETTA LA MODALITA' DI FUNZIONAMENTO IN IONTOFORESI
      HwCommunicationService.setIontoMode = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_SELECT;
           packet.command = "iontoforesimode";
           return HwCommunicationService.sendWithPromise(packet);
      }      
      
      //SETTA LA MODALITA' DI FUNZIONAMENTO IN MANUALE
      HwCommunicationService.setManualMode = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_SELECT;
           packet.pckType = 'CMD';
           packet.command = "manualmode";
           return HwCommunicationService.sendWithPromise(packet);
      }
      
      //SETTA LA MODALITA' DI FUNZIONAMENTO IN PULSATA
      HwCommunicationService.setPulsedMode = function () {
        var packet = {};
        packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_SELECT;
        packet.pckType = 'CMD';
        packet.command = "pulsedmode";
        return HwCommunicationService.sendWithPromise(packet);
      }

      //SETTA LA MODALITA' DI FUNZIONAMENTO IN MEGARIDE
      HwCommunicationService.setMegarideMode = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_SELECT;
           packet.pckType = 'CMD';
           packet.command = "megaridemode";
           return HwCommunicationService.sendWithPromise(packet);
      } 

      //AVVIA IL TRATTAMENTO SELEZIONATO
      HwCommunicationService.startTreatment = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_STATUS;
           packet.pckType = 'CMD';
           packet.command = "run";
           return HwCommunicationService.sendWithPromise(packet);
      } 

      //FERMA IL TRATTAMENTO SELEZIONATO
      HwCommunicationService.stopTreatment = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_STATUS;
           packet.pckType = 'CMD';
           packet.command = "stop";
           return HwCommunicationService.sendWithPromise(packet);
      } 

      //METTE IN PAUSA IL TRATTAMENTO SELEZIONATO
      HwCommunicationService.pauseTreatment = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_STATUS;
           packet.pckType = 'CMD';
           packet.command = "pause";
           return HwCommunicationService.sendWithPromise(packet);
      } 
      
      //RESUME IL TRATTAMENTO SELEZIONATO
      HwCommunicationService.resumeTreatment = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_STATUS;
           packet.pckType = 'CMD';
           packet.command = "resume";
           return HwCommunicationService.sendWithPromise(packet);
      } 

      //ESEGUE IL PASSO SUCCESSIVO DEL TRATTAMENTO
      HwCommunicationService.nextStepTreatment = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_STATUS;
           packet.pckType = 'CMD';
           packet.command = "nextstep";
           return HwCommunicationService.sendWithPromise(packet);
      } 

      HwCommunicationService.setManualModeParams = function(seconds, power) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_PARAMS;
           packet.pckType = 'CMD';
           if (HwCommunicationService.firmwareVersion >= HwCommunicationService.firmwareVersionWithDecimal)
              seconds = seconds*10;
           packet.command = "manualsetpar*"+seconds+","+power;
           return HwCommunicationService.sendWithPromise(packet);
      }
      
      HwCommunicationService.setPulsedModeParams = function(secondsOn, secondsOff, secondsIrraggiamento, power) {
        var packet = {};
        packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_PARAMS;
        packet.pckType = 'CMD';
        
        var par1 = secondsOn *10;
        var par2 = secondsOff *10;
        var par3 = secondsIrraggiamento *10;
        
        packet.command = "pulsedsetpar*"+par1+","+par2+","+par3+","+power;
        return HwCommunicationService.sendWithPromise(packet);
   }
      
      HwCommunicationService.setMegaridePoints = function(nPoints) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_PARAMS;
           packet.pckType = 'CMD';
           packet.command = "megaridenpoints*"+nPoints;
           return HwCommunicationService.sendWithPromise(packet);
      }

      HwCommunicationService.setMegaridePoint = function(seconds, power) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_PARAMS;
           packet.pckType = 'CMD';
           if (HwCommunicationService.firmwareVersion >= HwCommunicationService.firmwareVersionWithDecimal)
              seconds = seconds*10;
           packet.command = "megaridesetpoint*"+seconds+","+power;
           return HwCommunicationService.sendWithPromise(packet);
      }

      HwCommunicationService.setDresdaParams = function(seconds, power) {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_PARAMS;
           packet.pckType = 'CMD';
           packet.command = "dresdasetpar";
           return HwCommunicationService.sendWithPromise(packet);
      }

      //Chiede info circa il trattamento corrente
      HwCommunicationService.queryStatusTreatment = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_STATUS;
           packet.pckType = 'CMD';
           packet.command = "pstat";
           return HwCommunicationService.sendWithPromise(packet);
      }

      //Restituisce true se è abilitata la pulsata, false altrimenti
      HwCommunicationService.isPulsedAvailable = function() {
        return HwCommunicationService.firmwareVersion >= HwCommunicationService.firmwareVersionWithDecimal;
      }

      //Restituisce il countdown in secondi.  Dipende dalla versione del firmware
      HwCommunicationService.countDownInSeconds = function (countdown) {
        if (HwCommunicationService.isPulsedAvailable())
          return countdown/10;
        return countdown;
      }

      //Resetta completamente la testa ottica
      HwCommunicationService.initEEprom = function () {
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.TREATMENT_STATUS;
           packet.pckType = 'CMD';
           packet.command = "initeeprom";
           return HwCommunicationService.sendWithPromise(packet);
      } 

/****************
FINE FUNZIONI TRATTAMENTI
*****************/      

      //FUNZIONE CHE VERIFICA SE HO RICEVUTO UNA RISPOSTA CONSISTENTE DALLA TESTA
      /*HwCommunicationService.checkCurrentCommandConsistency = function () {
              if (HwCommunicationService.hardwareInited == false)
                return true;
              if (HwCommunicationService.bufferSerial.buffer.length == 0)
                return false;
              if (HwCommunicationService.bufferSerial.currentCommand.indexOf('reboot')>-1)
                return false;
              return true;
            }*/


      HwCommunicationService.sendBeepMessage = function () {
           /*console.log("Chiedo il beep");
           var packet = {};
           packet.packetType = HwCommunicationService.PACKET_TYPES.BEEP;
           packet.pckType = "BEEP";
           packet.command = "beep";
           HwCommunicationService.send(packet);*/
           if (SystemCalls != null)
            SystemCalls.beep();
          }

        HwCommunicationService.setSerialStatusListener = function (serListener) {
            HwCommunicationService.serialStatusListener = serListener;
          }       

        HwCommunicationService.notifySerialListener = function () {
              if (HwCommunicationService.serialStatusListener != null) {
                  try {
                      HwCommunicationService.serialStatusListener();
                     }
                  catch (error) {
                  }
                }
          }

        HwCommunicationService.configurationChanged = function () {
             /*if (HwCommunicationService.serialConnectionInfo != null)
                return;
             HwCommunicationService.hardwareInited = false;
             HwCommunicationService.debugMode = DebugModeService.disableHardware();
             $rootScope.$broadcast('hardwareStatus', null);
             if (!DebugModeService.disableHardware())
                $interval(HwCommunicationService.openSerialPort, 2000);
             else {
                LoggingService.info("Hw Communication Service.  L'hardware è stato disabilitato per motivi di debug");
                $rootScope.$broadcast('hardwareStatus', null);
                HwCommunicationService.unFreezeApplication();
              }*/
            HwCommunicationService.hardwareInited = false;
            HwCommunicationService.debugMode = DebugModeService.disableHardware();
            $rootScope.$broadcast('hardwareStatus', null);
            if (!DebugModeService.disableHardware() && ! HwCommunicationService.pollingEnabled)
              $interval(HwCommunicationService.openSerialPort, 2000);
            else {
              LoggingService.info("Hw Communication Service.  L'hardware è stato disabilitato per motivi di debug");
              $rootScope.$broadcast('hardwareStatus', null);
              HwCommunicationService.unFreezeApplication();
            }
        }


      //FUNZIONE CHE VERIFICA SE HO RICEVUTO UNA RISPOSTA CONSISTENTE DALLA TESTA
      HwCommunicationService.checkCurrentCommandConsistency = function () {
              if (HwCommunicationService.bufferSerial.buffer.length == 0)
                return false;
              
              if (HwCommunicationService.hardwareInited == false)
                return true;
              if (HwCommunicationService.bufferSerial.currentCommand.indexOf('reboot')>-1)
                return false;
              
              return ! HwCommunicationService.bufferSerial.mustAppendNext;
            }


        HwCommunicationService.loadFirmwareVersion = function () {
          var promise = PromiseService.getPromise();
          if (HwCommunicationService.firmwareVersion != -1) {
            promise.resolvePromise(HwCommunicationService.firmwareVersion);
          }
          else {
            HwCommunicationService.sendVer().then(
              function (result) {
                  var StringApp = ""+result;
                  var stringsApp = StringApp.split(",");
                  var strApp = stringsApp[0];
                  var strVer = strApp.substr(strApp.indexOf("UVMAD4CHIP")+10).trim();
                  var verTry = parseFloat(strVer);
                  if (!isNaN(verTry) && verTry > -1)
                    HwCommunicationService.firmwareVersion = parseFloat(strVer);
                  if (isNaN(HwCommunicationService.firmwareVersion) || HwCommunicationService.firmwareVersion <=1.9)
                    HwCommunicationService.firmwareVersion = -1;
                  LoggingService.info("Firmware version: "+HwCommunicationService.firmwareVersion.toFixed(2));                  
                  promise.resolvePromise(HwCommunicationService.firmwareVersion);
              }
            );
          }
          return promise.promise; 
        }

        //Questa funzione consente di interrompere e riprendere il polling che per default è attivo
        HwCommunicationService.setPollingInterrupted = function (val) {
          HwCommunicationService.pollingInterrupted = val;
        }



       //Funzione di polling
        HwCommunicationService.doPolling = function() {
          if (HwCommunicationService.pollingPaused)
            return;
          var now = new Date();
          //Verifico se il tempo trascorso dall'ultima lettura è superiore a quello di polling configurato e se il polling non è stato interrotto
          if ((now - HwCommunicationService.timestampLastRead < HwCommunicationService.pollingMSecs) || HwCommunicationService.pollingInterrupted )
            return;
          //Verifico se è in corso l'esecuzione di un altro comando
          if (HwCommunicationService.bufferSerial.mustAppendNext)
            return;
          if (HwCommunicationService.isCommandInQueue("ver"))
            return;          
          //in questa variabile memorizzo se ho ricevuto una risposta dalla seriale
          var interrupted = false;
          //Interrogo la seriale
          HwCommunicationService.sendVer(true).then(
              function (result) {
                if (result == null || result == '')
                  return;
                interrupted = true;
                //Se ricevo una risposta 
                //Verifico che l'applicazione era bloccata 

                if (!$rootScope.hardwareOk ){
                    //HwCommunicationService.apply();
                    $timeout(function(){  
                      $rootScope.hardwareOk = true;	  
                    });
                  }
                if (HwCommunicationService.hardwareInited)
                  return;                
                //sblocco l'applicazione
                HwCommunicationService.hardwareInited = true;
                HwCommunicationService.unFreezeApplication();
                $rootScope.$broadcast('hardwareStatus', null);
              }
            );
            $timeout(function () {
              //se non ho ricevuto una risposta entro un secondo blocco l'applicazione
              if (!interrupted) {
                 HwCommunicationService.hardwareInited = false;
                 HwCommunicationService.freezeApplication();
                 $rootScope.$broadcast('hardwareStatus', null);
                 HwCommunicationService.openSerialPort();
                }
            }, 1000);
        }

        HwCommunicationService.pausePolling = function () {
           HwCommunicationService.pollingPaused = true;
      }

        HwCommunicationService.resumePolling = function () {
            HwCommunicationService.pollingPaused = false;
        }



        HwCommunicationService.init = function() {
           /*HwCommunicationService.openSerialPort();
           if (!DebugModeService.disableHardware())
              $interval(HwCommunicationService.openSerialPort, 2000);
           else {
              LoggingService.info("Hw Communication Service.  L'hardware è stato disabilitato per motivi di debug");
              $rootScope.$broadcast('hardwareStatus', null);
              HwCommunicationService.unFreezeApplication();
            }*/
            HwCommunicationService.openSerialPort();
            if (HwCommunicationService.pollingEnabled) 
              HwCommunicationService.pollingTimer = $interval(HwCommunicationService.doPolling, 
                  HwCommunicationService.pollingMSecs);
            
            $interval(HwCommunicationService.handlePromises, 500);            

            //Ascolto gli eventi relativi al cambio di configurazione
            $rootScope.$on('configurationChanged', function (event, message) {
                HwCommunicationService.configurationChanged();
              });
            $rootScope.hardwareOk = false;
          }



        HwCommunicationService.showFreezeDialog = function () {
              HwCommunicationService.dialogSerial = 
                      $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(false)
                        //.title('This is an alert title')
                        .textContent($translate.instant('Messages.CHECK_CONNECTION'))
                        .ariaLabel('Warning')
                        .ok($translate.instant('Messages.SHUTDOWN_TEXT'));
                  $mdDialog.show(HwCommunicationService.dialogSerial
                          ).then( 
                                  function(res) {
                                    if (res == true){
                                      HwCommunicationService.sendShutDownMessage();
                                      HwCommunicationService.dialogSerial = null;
                                    }
                                  }

                      );
            }

         HwCommunicationService.hideFreezeDialog = function () {
              $mdDialog.hide(HwCommunicationService.dialogSerial);
            }

        HwCommunicationService.apply = function() {
          try {
            $rootScope.$apply();
          }
          catch (err) {
            
          }
        }


        HwCommunicationService.freezeApplication = function() {
            $timeout(function() {	//sostituzione $apply
              if(HwCommunicationService.isFreezed)
                return;
              HwCommunicationService.notifySerialListener();           
              HwCommunicationService.isFreezed = true;
              $rootScope.hardwareOk = false;
              //HwCommunicationService.apply();
              //HwCommunicationService.showFreezeDialog();
            });
        }

        HwCommunicationService.unFreezeApplication = function () {
          $timeout(function() {	//sostituzione $apply
            HwCommunicationService.notifySerialListener();
            HwCommunicationService.isFreezed = false;
            $rootScope.hardwareOk = true;	  
            //HwCommunicationService.apply();
          });
        }

        HwCommunicationService.init();    
        
        
        return HwCommunicationService;
        
    })
