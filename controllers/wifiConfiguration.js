var wifi = require('node-wifi');

angular.module('megaride')    
.controller('CtrlWifi', 
function ($scope, $stateParams, $rootScope, $state, $translate, $timeout, LoggingService) 
	{

	$scope.networks = [];
	$scope.lang = $translate.use();
	$scope.selectedNetwork = null;
	$scope.wifiEn = $rootScope.wifiEnabled;

	$scope.gridOptions = {
		enableSorting: true,
		rowHeight:50,
		enableFiltering: true,
		paginationPageSizes: [5, 10, 15, 20],
		paginationPageSize: 10,
		rowTemplate: '<div ng-click="grid.appScope.onRowSelected(row)" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.uid" class="ui-grid-cell" ng-class="col.colIndex()" ui-grid-cell></div>',
		columnDefs: [

			/*{ field: 'show', displayName:'', enableFiltering: false, enableSorting: false, enableHiding:false, width: 100,
				  cellTemplate: '<md-button class="md-icon-button md-accent" aria-label="Favorite" style="width:100px; height: 30px" '+
				  			    'ng-click="grid.appScope.connect(row.entity)"><md-icon md-font-icon="far fa-arrow-right" class="fa s32 md-primary md-hue-2" '+
				  			    'style="font-size:40px; width:100px; '+
            					'height: 100px; color: #6079a0"></md-icon></md-button>'
				},	*/
			{ field: 'ssid', displayName: $translate.instant('SSID'), enableFiltering: true}
				/*{ field: 'PatientSurname', displayName: $translate.instant('Report.Surname') },
				{ 
			       field: 'DateOfTreatment',  displayName: $translate.instant('ControlPanel.Reports.Date'),
			       cellFilter: 'date:"DD/MM/YYYY"',
        		   filters:[{ condition: checkDate, type: 'date'}],
        		},
				{ field: 'Operator', displayName: $translate.instant('Report.Operator'), enableFiltering: true },*/
				
				],

		onRegisterApi: function (gridApi) {
				$scope.grid1Api = gridApi;
			}
		};

	$scope.updateWifiStatus = function() {
		$rootScope.wifiEnabled = $scope.wifiEn;
		if ($rootScope.wifiEnabled) {
			SystemCalls.switchOnWifi();
			$timeout($scope.scanWifi, 3000);
			}
		else { 
			 SystemCalls.switchOffWifi();
			 $scope.gridOptions.data = [];
			}

	}

	$scope.scanWifi = function() {
		$scope.dataLoading = true;
		$scope.selectedNetwork = null;
		LoggingService.info("Scanning wifi");
		// Scan networks
		//wifi.getCurrentConnections(function(err, currentConnections) {
		wifi.scan(function(err, networks) {
			if (err) {
				LoggingService.info("Error during Scanning wifi");
				LoggingService.info(err);
			} else {
				$scope.gridOptions.data = networks;
				$scope.dataLoading = false;
				$scope.networks = networks;
				LoggingService.info(networks);
				/*
				networks = [
					{
					ssid: '...',
					bssid: '...',
					mac: '...', // equals to bssid (for retrocompatibility)
					channel: <number>,
					frequency: <number>, // in MHz
					signal_level: <number>, // in dB
					quality: <number>, // same as signal level but in %
					security: 'WPA WPA2' // format depending on locale for open networks in Windows
					security_flags: '...' // encryption protocols (format currently depending of the OS)
					mode: '...' // network mode like Infra (format currently depending of the OS)
					},
					...
				];
				*/
			}
		});
	}

	$scope.goToHome = function() {
		$state.go('splash');
	}


	$scope.onRowSelected = function(network) {
		$scope.selectedNetwork = network.entity;
		$scope.security=network.security;
		
	}

	$scope.connect = function(network) {
		LoggingService.info("Connecting to network");
		LoggingService.info($scope.selectedNetwork);

		wifi.connect({ ssid : $scope.selectedNetwork.ssid, password : $scope.selectedNetwork.password}, function(err) {
			if (err) {
				LoggingService.info('Error during connection to networ');
				LoggingService.info(err);
				return;
			}
			LoggingService.info('Connected');
			$scope.goToHome();
		});

	}

	$scope.init = function () {
		wifi.init({
			iface : null // network interface, choose a random wifi interface if set to null
		});
		
		$scope.scanWifi();
	}

	$scope.init();

		
	}
);
   
    	