angular.module('megaride')    
.controller('CtrlDresdenSteps', 
    function ($scope, $stateParams, $rootScope, $state, ShutdownService, LoggingService) {

    	$scope.setProtocolOption = function(opt) {
    		var optString;
    		if (opt == 1)
    			optString = "rapida";
    		else
				optString = "normale";
    		LoggingService.info("Selezionato protocollo di dresda con durata "+optString);
            $state.go('treatment', {protocol: 'Dresden', dresdamode: opt,
                      duration:0 , intensity:0, thickness:0, steps:0, cornVertexDistanceX:0, cornVertexDistanceY:0, cornVertexRadius:0, kerApex:0, spotDiameter:0,
                      diagnosis: -1, stepsDetails: null
                });
        }

        $scope.back = function() {
             $state.go('selectProtocol');
        }

        ShutdownService.setOperation(ShutdownService.OPERATIONS.SELECT_PROTOCOL);

    	
	}
);