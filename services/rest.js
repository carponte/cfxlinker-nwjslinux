'use strict';
/*Istruzioni per aggiornamento:
  sudo apt install npm
  npm install express
*/


angular.module('megaride')
.service('RestService', function(DatabaseService, ReportService, $log){
            	
        var RestService = {};

        RestService.server = null;

        RestService.startServer = function () {
          if (!fsys.existsSync('./sslCerts'))
            return;
          if ( RestService.server != null)
            return;
          const key = fsys.readFileSync('./sslCerts/key.pem');
          const cert = fsys.readFileSync('./sslCerts/cert.pem');
          const express = require ('express');
          const https = require ('https');
          const appEx = express();
          RestService.server = https.createServer({key:key, cert:cert}, appEx);

          appEx.get('/test', (req, res) => {
              res.json({prova: "test"});
          });

          appEx.get('/treatments', (req, res) => {
            var callBack = function (tx, result) {
              var resultOut = [];
              for (var i=0; i<result.rows.length; i++) {
                var strDate = result.rows[i].DateOfTreatment;
                var splitted = strDate.split("-");
                resultOut.push({ID: result.rows[i].ID, PatientName: result.rows[i].PatientName, PatientSurname: result.rows[i].PatientSurname,
                          Operator: result.rows[i].Operator, DateOfTreatment: splitted[2]+"/"+splitted[1]+"/"+splitted[0]
                          });
                  }
                res.json(resultOut);
              }
            DatabaseService.getReports(callBack);
          });

          appEx.get('/report', (req, res) => {
            var id = req.query.ID;
            var path = ReportService.getReportPath(id);
            var file = fsys.createReadStream(path);
            var stat = fsys.statSync(path);
            res.setHeader('Content-Length', stat.size);
            res.setHeader('Content-Type', 'application/pdf');
            res.setHeader('Content-Disposition', 'attachment; filename=quote.pdf');
            file.pipe(res);
            //res.json({idRichiesto: id, pathRelativo: path});
        });

        var port = 2000;
        const confFile = './sslCerts/config.json';
        if (fsys.existsSync(confFile)) {
          var obj = {};
          try {
               obj = JSON.parse(fsys.readFileSync(confFile, 'utf8'));
            }
          catch (err1) {
            console.log(err1);
          }
          if (obj.port)
            port = obj.port;
        }

         RestService.server.listen(port, () => { console.log('listening on '+port) });
        }

        RestService.init = function () {
          try {
            RestService.startServer();
          }
          catch (err) {
            console.log(err);
          }
        }

        RestService.init();
        return RestService;
        
    })