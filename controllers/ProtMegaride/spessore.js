var SystemCalls;
try {
    SystemCalls = require('SystemCalls')
 }
catch (ex) {
    SystemCalls = null;
}

angular.module('megaride')    
.controller('CtrlMegarideSpessore', 
    function ($scope, $stateParams, $rootScope, $state, CalibrationService, MegarideService, ShutdownService, DiagnosisService) {

    	$scope.parameters = {};
    	//$scope.parameters.thickness = 0;
        $scope.parameters.options = {};
        $scope.parameters.selectedId = -1;	

        $scope.diagnosis = [];

        $scope.radiusEnable = false;	


        $scope.startTreatment = function () {
            var selected = $scope.parameters.treatment;

            var cvdX = $scope.parameters.cornVertexDistanceX;
            if (cvdX == null)
                cvdX = 0;
            var cvdY = $scope.parameters.cornVertexDistanceY;
            if (cvdY == null)
                cvdY = 0;
            var cvdR = $scope.parameters.cornVertexRadius;
            if (cvdR == null)
                cvdR = 0;
            var kerApex = $scope.parameters.kerApex;
            if (kerApex == null)
                kerApex = 0;

            var duration = 0;
            if (selected != null)
                duration = selected.duration;
            var intensity = 0;
            if (selected != null)
                intensity = selected.intensity;
            var spotDiameter = 0;
            if (selected != null)
                spotDiameter = selected.spotDiameter;
            if ($scope.parameters.diagnosis == 3)
                spotDiameter = 9;
            

            $state.go('megaridesteps', {
                protocol:"Megaride", duration: duration, intensity: intensity, spotDiameter: spotDiameter, 
                thickness: $scope.parameters.thickness, steps: 0,  dresdamode: 0,
                cornVertexDistanceX: cvdX,  cornVertexDistanceY: cvdY, kerApex: kerApex, cornVertexRadius: cvdR,
                diagnosis: $scope.parameters.diagnosis 
            });            
        }

        $scope.changeStatus = function() {
            $scope.radiusEnable = !$scope.radiusEnable;
            $scope.updateOptions();
        }

        $scope.isNumber = function(n) {
	     if (n== null)
		return false;
             if (n.endsWith('.'))
                return false;
              return !isNaN(parseFloat(n)) && isFinite(n) ;
              //return Number(parseFloat(n)) === n;
            }

        $scope.scBlur = function () {
            alert("CIAO");
        }

        //QUESTA FUNZIONE VERIFICA CHE SIANO STATI IMPOSTATI TUTTI I PARAMETRI NECESSARI ALLE PROPOSTE DI TRATTAMENTO
        $scope.isDistanceFromKerApexSet = function () {
            //VERIFICO SE E' STATA SELEZIONATA UNA DIAGNOSI
            if ($scope.parameters.diagnosis == null)
                return false;
            //VERIFICO CHE SIA STATO INSERITO LO SPESSORE (Lo spessore va sempre inserito)
            if (!$scope.isNumber($scope.parameters.thickness))
                return false;
            //VERIFICO DI NON ESSERE IN UNO DEI PROTOCOLLI CHE RICHIEDE IL KMAX
            if (!$scope.isKMaxVisible())
                return true;
            //VERIFICO CHE SIA STATO INSERITO IL KMAX
            var KmaxNum = 0;
            try {
                KmaxNum = Number($scope.parameters.kerApex);
                }
            catch (err) {
            }
            if (!$scope.parameters.kerApex ||  (KmaxNum < 20 || KmaxNum > 100))
                return false;
            //VERIFICO CHE SIA STATO INSERITO O IL RAGGIO O LE COORDINATE
            if ($scope.radiusEnable){ 
                   return $scope.isNumber($scope.parameters.cornVertexRadius);
                   // return $scope.parameters.cornVertexRadius != null && $scope.parameters.cornVertexRadius != '';
                }
            else { 
                return $scope.isNumber($scope.parameters.cornVertexDistanceX) && $scope.isNumber($scope.parameters.cornVertexDistanceY);
                    /*return ($scope.parameters.cornVertexDistanceX  != null && $scope.parameters.cornVertexDistanceX  != '') && 
                    ($scope.parameters.cornVertexDistanceY != null && $scope.parameters.cornVertexDistanceY != '');*/
                }
        }

        $scope.svuotaCampi = function () {
            $scope.parameters.thickness = '';
            $scope.parameters.kerApex = '';
            $scope.parameters.cornVertexDistanceX = '';
            $scope.parameters.cornVertexDistanceY = '';
            $scope.parameters.cornVertexRadius = '';
        }

        $scope.changeDiagnosis = function () {
            $scope.svuotaCampi();
            $scope.updateOptions();
        }

         $scope.getDistanceFromKerApex = function() {
            var distance = 0;
            if ($scope.radiusEnable) {
                if ($scope.parameters.cornVertexRadius) {
                    try {
                         distance = Number($scope.parameters.cornVertexRadius);
                        }
                    catch (err) {
                         distance = 0;
                        }
                    }
                }
            else if ($scope.parameters.cornVertexDistanceX && $scope.parameters.cornVertexDistanceY){
                try {
                    var x = Number ($scope.parameters.cornVertexDistanceX);
                    var y = Number ($scope.parameters.cornVertexDistanceY);
                    distance = Math.sqrt(x*x + y*y);
                }
                catch (err) {
                    console.log(err);
                   distance = 0; 
                }
            }
            if (distance == NaN)
                distance = 0;
            return distance;
        }

        $scope.isKMaxVisible = function () {
            return $scope.parameters.diagnosis != null && $scope.parameters.diagnosis < 3;
        }

        $scope.isTreatmentBarVisible = function () {
            return $scope.parameters.treatment != null || ($scope.parameters.diagnosis == 3 && Number($scope.parameters.thickness) >=500 && (Number($scope.parameters.kerApex) >0)) 
        }

        //questa funzione viene chiamata per verificare se X, Y o R sono nei range consentiti
        $scope.checkXYR = function (val, allowNegatives) {
            var posPoint;
            try {
                posPoint = val.indexOf('.');
                }
            catch (err) {
                return false;
            }
            var retVal = '';
            if (posPoint == -1)
              retVal = val.substring(0, 2);
            else
              retVal = val.substring(0, 3+posPoint);
            if ($scope.isNumber(val)) {
              var x = Number (val);
              if (x < -10)
                retVal = 1;
              else if (x > 10)
                retVal = 10;
            }
            if (!allowNegatives && retVal < 0 )
                retVal = -1 * retVal;
            return retVal;
        }

        $scope.updateOptions = function() {
            /*if (!$scope.parameters.kerApex || ! $scope.parameters.thickness || $scope.parameters.diagnosis == null || !$scope.isDistanceFromKerApexSet())*/ 
            //Controlli sul thickness
            if($scope.parameters && $scope.parameters.thickness) {
              var posPoint = $scope.parameters.thickness.indexOf('.');
              if (posPoint == -1)
                $scope.parameters.thickness = $scope.parameters.thickness.substring(0, 3);
              else
               $scope.parameters.thickness = $scope.parameters.thickness.substring(0, 3+posPoint);
              /*if ($scope.isNumber($scope.parameters.thickness)) {
                var x = Number ($scope.parameters.thickness);
                if (x < 200)
                    $scope.parameters.thickness = 200;
                else if (x > 600)
                    $scope.parameters.thickness = 600;
              }*/

            }
            //Controlli sul kmax
            if($scope.parameters && $scope.parameters.kerApex){
              var posPoint = $scope.parameters.kerApex.indexOf('.');
              if (posPoint == -1)
                $scope.parameters.kerApex = $scope.parameters.kerApex.substring(0, 2);
              else
                $scope.parameters.kerApex = $scope.parameters.kerApex.substring(0, 3+posPoint);
              /*if ($scope.isNumber($scope.parameters.kerApex)) {
                var x = Number ($scope.parameters.kerApex);
                if (x < 20)
                    $scope.parameters.kerApex = 20;
                else if (x > 90)
                    $scope.parameters.kerApex = 90;
              }*/
            }

            //Controlli sulla distanza X
            if($scope.parameters && $scope.parameters.cornVertexDistanceX){
                $scope.parameters.cornVertexDistanceX = $scope.checkXYR($scope.parameters.cornVertexDistanceX, true);
              }    
            //Controlli sulla distanza Y
            if($scope.parameters && $scope.parameters.cornVertexDistanceY){
                $scope.parameters.cornVertexDistanceY = $scope.checkXYR($scope.parameters.cornVertexDistanceY, true);
            }    
            //Controlli sul raggio
            if($scope.parameters && $scope.parameters.cornVertexRadius){
                $scope.parameters.cornVertexRadius = $scope.checkXYR($scope.parameters.cornVertexRadius, false);
              }                
            
            if (!$scope.isDistanceFromKerApexSet())
                $scope.parameters.treatment = null;
            else
                $scope.parameters.treatment = MegarideService.getProposal($scope.parameters.thickness, $scope.parameters.kerApex, 
                    $scope.getDistanceFromKerApex(), $scope.parameters.diagnosis);
            return;
            
        }

        $scope.getSelectedOption = function() {
          if ($scope.parameters.selectedId == -1)
            return {};
          for (var i=0; i<$scope.parameters.options.length; i++) {
              if ($scope.parameters.options[i].id == $scope.parameters.selectedId)
                return $scope.parameters.options[i];
          }
          return {};          
        }

        $scope.startTreatmentReady = function () {
            return $scope.parameters.selectedId > 0;
        }

        $scope.goToProtocolSelection = function() {
            $state.go('selectProtocol');
        }

        $scope.showKeyboard = function() {
            if (SystemCalls != null)
                SystemCalls.showKeyboard();
        }

        $scope.initParameters = function () {
            if ($stateParams.thickness > 0)
                $scope.parameters.thickness = $stateParams.thickness;
            if ($stateParams.cornVertexDistanceX > 0)                
                $scope.parameters.cornVertexDistanceX = $stateParams.cornVertexDistanceX;
            if ($stateParams.cornVertexDistanceY > 0)
                $scope.parameters.cornVertexDistanceY = $stateParams.cornVertexDistanceY;
            if ($stateParams.kerApex > 0)
                $scope.parameters.kerApex = $stateParams.kerApex;
            if ($stateParams.cornVertexRadius > 0)
                $scope.parameters.cornVertexRadius = $stateParams.cornVertexRadius;
            if ($stateParams.diagnosis != '') {
                for (var i=0; i<$scope.diagnosis.length; i++) {
                        if ($scope.diagnosis[i].type == $stateParams.diagnosis) {
                            $scope.parameters.diagnosis = $scope.diagnosis[i].type;
                            break;
                        }
                    }
                }
            $scope.updateOptions();
        }

        $scope.back = function() {
            if ($stateParams.oldState != null && $stateParams.oldState != '')
                $state.go($stateParams.oldState);
            else
                $state.go('selectProtocol');
        }

        $scope.radiusEnChanged = function() {
            window.localStorage.setItem('radiusEn', $scope.radiusEnable);
            $scope.updateOptions();
        }

       

        $scope.init = function () {
           //$scope.showKeyboard(); 
           if (window.localStorage.getItem('radiusEn') == null)
             window.localStorage.setItem('radiusEn', true);
          $scope.radiusEnable = localStorage.getItem('radiusEn') == "true" ? true : false;
          $scope.diagnosis = DiagnosisService.getDiagnosis();
          $scope.initParameters();  
        }

        $scope.init();

        ShutdownService.setOperation(ShutdownService.OPERATIONS.SELECT_PROTOCOL);

    	
	}
);
   
    	
