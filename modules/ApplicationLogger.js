
var dateFormat = require('dateformat');
var dateSuffix = dateFormat(new Date(), "yyyy_mm_dd");
var winston = require('winston');

var logger;



function getLoggerPath() {
   return 'log/debug'+dateSuffix+'.log';
}

//INIZIO LOGGER
logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ json: false, timestamp:  function () {
       return (new Date()).toLocaleString("it-IT");
      } 
    }),
    new winston.transports.File({filename: getLoggerPath(), json: false, timestamp:  function () {
       return (new Date()).toLocaleString("it-IT");
      }
    })
  ],
  exceptionHandlers: [
    new (winston.transports.Console)({ json: false, timestamp: 
     function () {
            return (new Date()).toLocaleString("it-IT");
          }
     }),
    new winston.transports.File({filename:'log/exceptions.log', json: false })
  ],
  exitOnError: false
});


function info (str) {
	logger.info(str);
}

function debug (str) {
  logger.info("(DEBUG) " +str);
}

exports.info=info;
exports.debug=debug;
exports.getLoggerPath=getLoggerPath;
//FINE LOGGER