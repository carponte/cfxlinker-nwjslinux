'use strict';

//QUESTO SERVICE ATTIVA O DISATTIVA COMPORTAMENTI PARTICOLARI PER ACCELERARE LA FASE DI DEBUG

angular.module('megaride')
.service('DebugModeService', function($log, $rootScope, DatabaseService){
            	
        var DebugModeService = {};

        DebugModeService.configuration = {};
        
        DebugModeService.configuration.jumpVerifiyCalibration = false;
        DebugModeService.configuration.fakeImbibition = false;
        DebugModeService.configuration.resetWhiteLed = false; 
        DebugModeService.configuration.demoEnabled = true;  
        DebugModeService.configuration.timerSCEnabled = true;   //Abilita o disabilita lo shortcut che fa saltare il timer
        DebugModeService.configuration.showReport = true;
        DebugModeService.configuration.giorniLogOnLine = 12; 

        DebugModeService.configuration.disableHardware = true;
        DebugModeService.configuration.MirrorCamera = true;
        DebugModeService.configuration.RotateCamera = false;
        DebugModeService.configuration.JumpStep = false;
        DebugModeService.configuration.privacyEnabled=false;


        DebugModeService.jumpVerifiyCalibration = function () {
            return DebugModeService.configuration.jumpVerifiyCalibration;
        }

        DebugModeService.isFakeImbibition = function() {
            return DebugModeService.configuration.fakeImbibition;
        }

        DebugModeService.needResetForWhiteLed = function() {
            return DebugModeService.configuration.resetWhiteLed;
        }

        DebugModeService.disableHardware = function() {
            return DebugModeService.configuration.disableHardware;
        }

        DebugModeService.demoEnabled = function () {
            return DebugModeService.configuration.demoEnabled;
        }

        DebugModeService.isTimerShortCutEnabled = function () {
            return DebugModeService.configuration.timerSCEnabled;
        }

        DebugModeService.isReportVisible = function () {
            return DebugModeService.configuration.showReport;   
        }

        DebugModeService.getMonthsLogOnLine = function () {
            return DebugModeService.configuration.mesiLogOnLine;
        }

        DebugModeService.isJumpStepActive = function () {
            return DebugModeService.configuration.JumpStep;
        }



        DebugModeService.loadFromDB = function () {
            var callBack = function(result) {
                DebugModeService.configuration.jumpVerifiyCalibration = result.jumpVerifiyCalibration == 1;
                DebugModeService.configuration.fakeImbibition = result.fakeImbibition == 1;
                DebugModeService.configuration.resetWhiteLed = result.resetWhiteLed == 1;
                DebugModeService.configuration.demoEnabled = result.demoEnabled == 1;
                DebugModeService.configuration.timerSCEnabled = result.timerSCEnabled == 1;
                DebugModeService.configuration.disableHardware = result.disableHardware == 1;
                DebugModeService.configuration.showReport = result.showReport == 1;
                DebugModeService.configuration.giorniLogOnLine = result.giorniLogOnLine;
                DebugModeService.configuration.MirrorCamera = result.MirrorCamera == 1;
                DebugModeService.configuration.JumpStep = result.JumpStep ==1;
                DebugModeService.configuration.RotateCamera = result.RotateCamera == 1;
                DebugModeService.configuration.privacyEnabled = result.privacy == 1;
                DebugModeService.configuration.giorniReportOnLine = result.giorniReportOnLine;
                $rootScope.$broadcast('configurationChanged', null);
            }
            if (DatabaseService.isDBLoaded())
                DatabaseService.getConfiguration(callBack);
            else 
                $rootScope.$broadcast('configurationChanged', null);
        }

        DebugModeService.getConfiguration = function () {
            return DebugModeService.configuration;
        }

        DebugModeService.saveConfiguration = function (fConfig) {
            DebugModeService.configuration = fConfig;
            DatabaseService.setConfiguration(fConfig);
        }


        DebugModeService.loadFromDB();
       
        
        return DebugModeService;
        
    })