'use strict';

angular.module('megaride')
.service('UpdateService', function($rootScope, $log, $mdDialog, $translate, ConfigurationService, DatabaseService){
            	
        var UpdateService = {};

        UpdateService.localCommit = '';
        UpdateService.remoteCommit = '';
        UpdateService.branch = '';

        UpdateService.checkUpdates = function () {
            
          UpdateService.localBranch();   

        }

        UpdateService.localBranch = function() {
          var cmdFetch = 'git fetch';
          const exec = require('child_process').exec;
                const child = exec(cmdFetch,
                  (error, stdout, stderr) => {

                var command = 'git branch';
                const exec = require('child_process').exec;
                    const child = exec(command,
                      (error, stdout, stderr) => {
                        /*console.log(`stdout: ${stdout}`);
                        console.log(`stderr: ${stderr}`);*/
                        if (error !== null) {
                          console.log(`exec error: ${error}`);
                        }
                      if (stdout) {
                        //var result = stdout.split(" ");
                        var result = stdout.split("\n");
                        for (var i=0; i<result.length; i++) {
                          if ((result[i].charAt(0)  == '*') && (result[i].length > i)) {
                            UpdateService.branch = result[i].substring(1).trim();
                            UpdateService.remoteBranch (UpdateService.branch);
                            console.log('Branch: '+UpdateService.branch);
                          }
                            
                        }
                        }
                    });  
          });  
        }

        UpdateService.branchInfo = function (info)  {
          var app = info.split(' ');
          var res = [];
          for (var i=0; i<app.length; i++) {
            if (app[i].trim().length > 0)
              res.push(app[i].trim());
          }
          var offset = 0;          
          var result = {};
          result.selected = res[0] == '*';
          if (result.selected)
            offset++;
          result.branch = res[0+offset];
          result.commit = res[1+offset];
          result.remote = result.branch.includes('remotes');
          if (result.remote) {
            result.fullBranch = result.branch;
            var splitted = result.branch.split("/");
            result.origin = splitted [1];
            result.branch = splitted [2];
          }
          return result;
        }

        UpdateService.remoteBranch = function(requiredBranch) {
          if (UpdateService.localCommit != '' && UpdateService.remoteCommit != '')
            return;
          var command = 'git branch -av';
          const exec = require('child_process').exec;
              const child = exec(command,
                (error, stdout, stderr) => {
                  /*console.log(`stdout: ${stdout}`);
                  console.log(`stderr: ${stderr}`);*/
                  if (error !== null) {
                    console.log(`UpdateService.  remoteBranch exec error: ${error}`);
                    }
                  var res = stdout.trim().split("\n");
                  var localBranchInfo = "";
                  var remoteBranchInfo = "";
                  requiredBranch = requiredBranch.trim();
                  for (var i =0; i<res.length; i++) {
                      var info = UpdateService.branchInfo(res[i]);
                      if (info.branch == requiredBranch) {
                         if (info.remote)
                            UpdateService.remoteCommit = info.commit;
                          else
                            UpdateService.localCommit = info.commit;
                      }
                    }                  
                  console.log("Branch corrente: "+ requiredBranch+"; Local commit: "+UpdateService.localCommit+", Remote commit: "+UpdateService.remoteCommit);
                  if (requiredBranch.includes("develop"))
                    $rootScope.disableWifiDuringEmission = false;
                  if ((UpdateService.remoteCommit.length > 0) && (UpdateService.remoteCommit != UpdateService.localCommit)) 
                    $rootScope.$broadcast('updateSW', null);
                  else
                    $rootScope.$broadcast('branchUpdated', null);
                });  
        }

        UpdateService.reboot = function() {
          UpdateService.dialogUpdate = 
            $mdDialog.alert()
              .parent(angular.element(document.querySelector('#popupContainer')))
              .clickOutsideToClose(false)
              .textContent($translate.instant('Messages.REBOOT_UPDATE'))
              .ariaLabel('Warning')
              .ok($translate.instant('Messages.REBOOT'));
             $mdDialog.show(UpdateService.dialogUpdate).then( 
                            function(res) {
                              SystemCalls.restart();
                            }

                );
        }

        UpdateService.replaceReportLogo = function (callBack) {
          var sourcePath = './images/logoRefertoUser';
          var destPath = './images/logoReferto';
          if (fs.existsSync(sourcePath)) {
                fs.copyFile(sourcePath, destPath, (err1) => {
                if (err1) throw err;
                callBack();
                });
            }
          else {
            callBack();
          }
        }

        UpdateService.update = function() {
          var callBack = function (result) {
            var serialNumber = result.serCfxLinker;
            
            //var command = 'git pull';
            var command = 'rm -rf /home/cfxlinker/repo; cd /home/cfxlinker/; mkdir repo; cd repo; git clone https://carponte@bitbucket.org/carponte/cfxlinker-nwjslinux.git;';
            command+='cd /home/cfxlinker/repo/cfxlinker-nwjslinux/; git checkout '+UpdateService.branch+'; sh /home/cfxlinker/repo/cfxlinker-nwjslinux/scripts/preupdate.sh -b "'+UpdateService.branch+'"';
              const exec = require('child_process').exec;
              const child = exec(command,
                (error, stdout, stderr) => {
                  //Finito il pull 
                  //copio i file di personalizzazione
                  var command1 = '';
                  if ($rootScope.Fidia) 
                    //command1 = 'cp -rT /home/cfxlinker/cfxlinker/customization/fidia_files/ /home/cfxlinker/cfxlinker/';
                    command1 = 'sh /home/cfxlinker/repo/cfxlinker-nwjslinux/scripts/afterupdate.sh -c "fidia"';
                  else
                    //command1 = 'cp -rT /home/cfxlinker/cfxlinker/customization/iromed_files/ /home/cfxlinker/cfxlinker/';
                    command1 = 'sh /home/cfxlinker/repo/cfxlinker-nwjslinux/scripts/afterupdate.sh -c "iromed"';
                  
                  const exec1 = require('child_process').exec;
                  const child1 = exec(command1,
                    (error1, stdout1, stderr1) => {
                      //finita copia dei file di personalizzazione
                      //Verifico se occorre rimettere a posto il logo del referto e poi riavvio
                      ConfigurationService.storeUpdateInCloud(serialNumber, UpdateService.remoteCommit, function(){
                          UpdateService.replaceReportLogo(UpdateService.reboot);
                      })

                    });  
                  //finita copia file di personalizzazione
                                  
                });  
            
          }
          DatabaseService.getSystemProperties(callBack);

              
        }
        
        return UpdateService;
        
    })