
angular.module('megaride')    
.controller('languageCtrl', 
    	function ($scope, $stateParams, $rootScope, $state, ConfigurationService, DebugModeService) 
    		{

            $scope.locales = [
                {"lang": "it"},
                {"lang": "gb"},
                {"lang": "de"},
                {"lang": "fr"},
                {"lang": "es"},
                {"lang": "cn"},
                {"lang": "pl"},
                {"lang": "fi"}
            ];


            $scope.getFlagClass = function (locale) {
                return "flag flag-icon flag-icon-"+locale.lang;
            }

             $scope.goToVerifyCalibration = function() {
                $state.go('verifycalibration');
             }

             $scope.setLocale = function (locale) {
                ConfigurationService.setLocale(locale);
                $scope.goNext();
             }

             $scope.goToProtocols = function() {
                $state.go('selectProtocol');
             }

             $scope.goToHome = function () {
               $state.go('splash');  
             }


             $scope.goNext = function () {
                $scope.goToHome();
                }

    		}
    	);
   
    	