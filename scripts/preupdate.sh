#!/bin/bash

#HO MESSO IN !REM LA FUNZIONE ORIGINARIA
helpFunction()
{
   echo ""
#REM   echo "Usage: $0 -b Branch -b parameterB -c parameterC"
#REM   echo -e "\t-a Description of what is parameterA"
#REM   echo -e "\t-b Description of what is parameterB"
#REM   echo -e "\t-c Description of what is parameterC"
   echo "Usage: $0 -b Branch" 
  exit 1 # Exit script after printing help
}

while getopts "a:b:c:" opt
do
   case "$opt" in
      a ) parameterA="$OPTARG" ;;
      b ) Branch="$OPTARG" ;;
      c ) parameterC="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
#REM if [ -z "$parameterA" ] || [ -z "$parameterB" ] || [ -z "$parameterC" ]
if [ -z "$Branch" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

# Begin script in case all parameters are correct
rm -rf /home/cfxlinker/cfxlinker/customization/
cp -af /home/cfxlinker/repo/cfxlinker-nwjslinux/. /home/cfxlinker/cfxlinker/
cd /home/cfxlinker/cfxlinker/ 
git config user.email "technicalsupport@servimedindustrial.com"
git config user.name "iromed"
git stash
git checkout "$Branch" 
git pull

if [ ! -d /home/cfxlinker/cfxlinker/node_modules/googleapis ]; then
    npm install googleapis --save
fi

git stash
git pull origin "$Branch" 

chmod +x /home/cfxlinker/cfxlinker/cfxlinker
cd /home/cfxlinker/cfxlinker


#verifico la versione di nwjs e nel caso non è l'ultima la aggiorno
npmVersion=$(nw/nw --version | cut -d' ' -f2)
if [ "$npmVersion" != "96.0.4664.55" ]; then  
  #nw
  mkdir oldNw  
  wget -O nw_temp  https://dl.nwjs.io/v0.59.0/nwjs-v0.59.0-linux-x64.tar.gz
  if test -f "nw_temp"; then
    if [ "$checksum" = "db7132c89594062aab41ea9ffcc724860206dd867fb645cc69f32d8909d51bb2" ]; then  
      tar -xf nw_temp  
      mv -f nw oldNw/$npmVersion  
      mv nwjs-v0.59.0-linux-x64 nw
    fi  
  fi
  rm nw_temp
  #nwdebug
  mkdir oldNwDebug      
  wget -O nw_temp  https://dl.nwjs.io/v0.59.0/nwjs-sdk-v0.59.0-linux-x64.tar.gz
  if test -f "nw_temp"; then
    if [ "$checksum" = "3e724f632f0c1a6dd84049361979d8a1fc947680619d9ee10e42caf54ae46b07" ]; then
      tar -xf nw_temp
      mv -f nwdebug oldNwDebug/$npmVersion
      mv nwjs-sdk-v0.59.0-linux-x64 nwdebug
    fi
  fi
  rm nw_temp
fi

#installo serialport
#npm install serialport