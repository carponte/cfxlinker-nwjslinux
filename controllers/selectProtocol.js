
angular.module('megaride')    
.controller('CtrlselectProtocol', 
function ($scope, $stateParams, $rootScope, $state, $sce, $translate, $timeout, 
			HwCommunicationService, ProtocolClaimsService, ShutdownService, DebugModeService, LoggingService, ReportService, 
			CalibrationService, CameraService, TreatmentService) 
	{
		$scope.protocols = [];

		$scope.protocols.push({"name": "Dresden", id: 0, display: $translate.instant("Protocols.Dresden")});
		if(typeof CustomFastDisabled == 'undefined' || !CustomFastDisabled)
		  $scope.protocols.push({"name": "Custom Fast", id: 1, display: $translate.instant("Protocols.CustomFast")});
		$scope.protocols.push({"name": "Iontoforesi", id: 2, display: $translate.instant("Protocols.Iontophoresis")});
		$scope.protocols.push({"name": "Cheratiti", id:3, display: $translate.instant("Protocols.Keratitis")});
		//if (TreatmentService.getFullCustomTreatments().length > 0)
			$scope.protocols.push({"name": "Full Custom", id:6, display: $translate.instant("Protocols.FullCustom")});
		//$scope.protocols.push({"name": "PRK/LASIK", id:4});
		if (DebugModeService.demoEnabled())
			$scope.protocols.push({"name": "Demo", id:5, display: $translate.instant("Protocols.Demo")});
		
		$scope.selectedProtocol = $scope.protocols[0];
		$scope.protocolClaims = [];		//In questo array saranno riempiti i claim dei singoli protocolli


		$scope.setProtocol = function (protocol) {
			LoggingService.info('Selezionato il protocollo "'+protocol.name+'"');
			if (protocol.id == 1)
				$state.go('megaride', {
					thickness:0, cornVertexDistanceX: 0, cornVertexDistanceY: 0, kerApex:0, cornVertexRadius: 0, oldState: 'selectProtocol', diagnosis: -1
				});
			else if (protocol.id == 0) {
				//$state.go('dresdensteps');
				$state.go('treatment', {protocol: 'Dresden', dresdamode: 1,
                      duration:0 , intensity:0, thickness:0, steps:0, cornVertexDistanceX:0, cornVertexDistanceY:0, cornVertexRadius:0, kerApex:0, spotDiameter:9,
                      diagnosis: -1, stepsDetails: null
                });
			}
			else if (protocol.id == 4 || protocol.id == 3)
			    $state.go('prkLasik', {protocol: protocol.name});
			else if (protocol.id == 6)
				$state.go('fullCustom', {edit: 0});
			else
				//$state.go('treatment', {protocol: protocol.name});
			$state.go('treatment', {protocol: protocol.name, duration: 0, intensity:0, thickness:0, steps:0, dresdamode:0, 
				cornVertexDistanceX:0, cornVertexDistanceY:0, kerApex:0, spotDiameter:9, cornVertexRadius:0, diagnosis: -1, 
				stepsDetails: null });
		}

		$scope.home = function() {
			$state.go('splash');
		}

		$scope.shutDown = function() {
			 HwCommunicationService.sendShutDownMessage();
		}

		$scope.getProtocolString = function (protocol)  {
			return 'ProtocolDescription.'+protocol.name;
		}

		$scope.protocolChanged = function (protocol) {
			$scope.selectedProtocol = protocol;	
			$scope.protocolOption = 0;
			$scope.updateProtocolClaims();
		}

		$scope.isProtocolDisabled = function (protocol) {
			switch (protocol.id) {
				//DRESDEN
				case 0: 	return !CalibrationService.isValueInRange(3);
				//CUSTOM FAST
				case 1: 	return !CalibrationService.isValueInRange(2.0) || !CalibrationService.isValueInRange(3);
				//IONTOFORESI
				case 2: 	return !CalibrationService.isValueInRange(10);
				//CHERATITI
				case 3: 	return false;
				//DEMO
				case 5: 	return !CalibrationService.isValueInRange(3);
			}
			return false;
		}



		//Questa funzione costruisce la GUI relativa all'elenco dei claim per i singoli protocolli
		$scope.updateProtocolClaims = function () {
			var protocol = $scope.selectedProtocol;
			var claimsIndex = "ProtocolClaims."+protocol.name;
			$scope.protocolClaims = $translate.instant(claimsIndex);
		}


		$scope.startProtocol = function () {
			$scope.setProtocol($scope.selectedProtocol);
		}

		$scope.getButtonClass = function (protocol) {
			var classButton = "md-raised";
			if (protocol.name == $scope.selectedProtocol.name)
				classButton +=" md-primary";
			return classButton;
		}

		//Consente di iniettare codice html con angular
		$scope.getHTML = function(str){
			var result = str;
			result = $translate.instant(str);
			result = result.replace('<!>', '<b>');
			result = result.replace('</!>', '</b>');
			result = result.replace('<?>', '<i>');
			result = result.replace('</?>', '</i>');
			result = $sce.trustAsHtml(result);
			return result;
		}

		$scope.logProtocols = function() {
			var strProtocols = "";
			for (var i=0; i<$scope.protocols.length; i++) {
				strProtocols += $scope.protocols[i].name;
				if ($scope.isProtocolDisabled($scope.protocols[i]))
					strProtocols += " (DISABLED)";
				else
					strProtocols += " (ENABLED)";
				if (i<$scope.protocols.length-1)
					strProtocols += ", ";
			}
			LoggingService.info('Protocolli disponibili: {'+strProtocols+'}');			
		}

		$scope.getNumAvailableProtocols = function() {
			var result = 0;
			for (var i=0; i<$scope.protocols.length; i++) {
				if (!$scope.isProtocolDisabled($scope.protocols[i]))
					result++;
			}

			return result;
		}

		$scope.checkCalibTableLoading = function() {
			$timeout(function() {	//sostituzione $apply
				if ($scope.getNumAvailableProtocols ()< 3)
					CalibrationService.forceTableReload().then(
						function (result) {
							try {	
									LoggingService.info('SelectProtocols Ctrl.  Forzato load della tabella di calibrazione');
									$scope.logProtocols();
									//$scope.$apply();
								}
							catch(ex) {

								}
							}
						);
			});
		} 

		$scope.init = function () {
			//console.log("Sono in scope.init di Controller SelectProtocol");
			$timeout(
				function() { 
					LoggingService.info('Controller SelectProtocol.  Inizio Init ');
					HwCommunicationService.sendFocusMessage(0);
					$scope.updateProtocolClaims();
					$scope.checkCalibTableLoading();
					$scope.logProtocols();
					ShutdownService.setOperation(ShutdownService.OPERATIONS.SHUTDOWN);
					ReportService.unlockWinReferto();
					CameraService.detachVideo();
					LoggingService.info('Controller SelectProtocol.  Fine Init');
				}
				, 100);
			}

		$scope.init();





		$scope.description = [];
		
		$scope.getProtocolClaims = function (){
			//return ProtocolClaimsService.getClaims($scope.selectedProtocol.name);
			if ($scope.description[$scope.selectedProtocol.name] == null)
				$scope.description[$scope.selectedProtocol.name] = 
					ProtocolClaimsService.getClaims($scope.selectedProtocol.name);
			return $scope.description[$scope.selectedProtocol.name];
		}


	}
);
   
    	