'use strict';

angular.module('megaride')
.service('CalibrationService', function($translate, $rootScope, HwCommunicationService, LoggingService, $timeout, DatabaseService, usePromises, PromiseService){
            	
        var CalibrationService = {}

        var config = {};
        var inited = false;

   
        var CalibrationTable = [];      //Tabella per calibrazione di fabbrica
        var verifyTable = [];           //Tabella ricevuta dal dispositivo per la verifica della calibrazione

        var STATES = {
            "IDLE": 0, 
            "VERIFY_CALIBRATION": 1,
            "DO_CALIBRATION": 2
        }

        var status = STATES.IDLE;
        var focusState = 0;

        CalibrationService.totMinutiUtilizzo = 0;
        CalibrationService.CalibrationTable=[];

        CalibrationService.MinValue = 0;    //Questa variabile memorizza il valore minimo calibrato
        CalibrationService.MaxValue = 0;    //Questa variabile memorizza il valore massimo calibrato
        CalibrationService.isTableLoaded = false;
        CalibrationService.tolleranzaCalib30mW = 2;
        CalibrationService.MaxAvailableValue = 30;    //Valore in mW della massima potenza erogabile da FT

        
        //Questa funzione restituisce la tabella di calibrazione
        CalibrationService.getCalibrationTable = function () {
            return CalibrationTable;
        }

        //Genera un numero random nel range da verificare
        CalibrationService.getVerifyValue = function () {
          var max = 5000;
          var min = 1000;
          //return 3000;
          return Math.floor(Math.random()*(max-min+1)+min);
        }

        //Restituisce la tabella di verifica.
        CalibrationService.getVerifyTable = function () {
            /*if (verifyTable.length == 0) {
              LoggingService.error("Tabella di verifica non ricevuta.  Verrà utilizzata una random");
              verifyTable.push({"uvmeter": 2000, "current": 0.0});
            }
            return verifyTable;*/
            var table = [];
            table.push({"uvmeter": CalibrationService.getVerifyValue(), "current": 0.0});
            return table;
        }

        //Inizia la procedura di verifica della calibrazione
        CalibrationService.startVerifyCalibration = function() {
            var promise = PromiseService.getPromise();
            HwCommunicationService.setManualMode().then(function(result) {
                    promise.resolvePromise(result);
                });
            return promise.promise;
        }


        //Avvia la procedura di calibrazione
        CalibrationService.startCalibration = function () {
          HwCommunicationService.setPollingInterrupted(true);
          if (usePromises){
            var promise = PromiseService.getPromise();
            HwCommunicationService.sendIdle().then( function(result) {
                      HwCommunicationService.sendFocusMessage(1).then(function (result) {
                                HwCommunicationService.sendResetCalibration().then(function (result) {
                                      CalibrationService.isTableLoaded = false;
                                      promise.resolvePromise(result);
                                    })
                              })
                          });

             return promise.promise;
            }
          else  {
            HwCommunicationService.sendIdle();
            HwCommunicationService.sendFocusMessage(1);
            HwCommunicationService.sendResetCalibration();
          }
        }

         //Interagisce coi led di posizionamento
        CalibrationService.setFocus = function(focus) {
           focusState = focus;
           if (usePromises){
                var promise = PromiseService.getPromise();
                  if (focus == 1) {
                    HwCommunicationService.setManualMode().then(function() {
                          HwCommunicationService.sendFocusMessage(focus).then(function(result) {
                                promise.resolvePromise(result);
                            })
                        });
                  }
                  else
                    HwCommunicationService.sendFocusMessage(focus).then(function(result) {
                        promise.resolvePromise(result);
                    });
                return promise.promise;
              }
            else {
              if (focus == 1) {
                  HwCommunicationService.setManualMode(); //questo comando consente di non far spegnere i led rossi
                }
                HwCommunicationService.sendFocusMessage(focus);
            }

        }

         //Accende il led uv in calibrazione
        CalibrationService.setCurrentLed = function (val) {
            if (usePromises){
               var promise = PromiseService.getPromise();
               HwCommunicationService.sendCurrentCalibration(val).then(function(result) {
                      promise.resolvePromise(result);
                    });
               return promise.promise;
              }
            else {
              HwCommunicationService.sendCurrentCalibration(val);
            }
        }


        //Comunica al dispositivo la potenza letta dal power meter
        CalibrationService.setCalibPower = function (val) {
            try {
              val = val.replace(',', '.');
            }
            catch (ex) {
            }
            try {
              val = Number(val);
            }
            catch (ex) {

            }

            //var val1 = Math.trunc(val) *1000;
            var val1 = Math.trunc(val *1000);
             if (usePromises){
               var promise = PromiseService.getPromise();
               HwCommunicationService.setCalibPower(val1).then(function(result) {
                      promise.resolvePromise(result);
                    });
               return promise.promise;
              }
            else {
              HwCommunicationService.setCalibPower(val1);
            }
        }

        //Procedura di calibrazione completata
        CalibrationService.sendCalibrationDone = function () {
          HwCommunicationService.setPollingInterrupted(false);
          if (usePromises){
               var promise = PromiseService.getPromise();
               HwCommunicationService.sendCalibrationDone().then(function(result) {
                      CalibrationService.initCalibrationTable();
                      promise.resolvePromise(result);
                    });
               return promise.promise;
              }
          else {
              HwCommunicationService.sendCalibrationDone();
            }
        }




        //Accende per un minuto il led in manual mode al valore di potenza che si intende verificare
        CalibrationService.PowerOn = function (power) {
          var promise = PromiseService.getPromise();
          HwCommunicationService.stopTreatment().then(function(result){
                HwCommunicationService.setManualMode().then(function(result){
                    HwCommunicationService.setManualModeParams (1800, power).then(function(result){
                        HwCommunicationService.startTreatment().then(function(result){
                          promise.resolvePromise(result);
                        });
                    })
                })
            });
          return promise.promise;
          /*HwCommunicationService.stopTreatment();
          $timeout(HwCommunicationService.setManualMode, 100);
          $timeout(function () {HwCommunicationService.setManualModeParams (1800, power)}, 200);
          $timeout(HwCommunicationService.startTreatment, 300);*/
        }

        CalibrationService.PowerOff = function () {
          var promise = PromiseService.getPromise();
          HwCommunicationService.stopTreatment().then(function(result) {
                      promise.resolvePromise(result);
                    });
               return promise.promise;
        }




        

        



          //Restituisce true se è stato ricevuta la tabella di calibrazione
         CalibrationService.isGetCalibrationTable = function (message) {
            return message != null && message.packetType != null && message.packetType == HwCommunicationService.PACKET_TYPES.CALIB_GET_TABLE;
          }

         //Restituisce true se è stato ricevuto ok su messaggio calibrazione
         CalibrationService.isResetCalibrationMessage = function (message) {
            return message != null && message.packetType != null && message.packetType == HwCommunicationService.PACKET_TYPES.CALIB_RESET;
          }

          //Restituisce true se è stato ricevuto ok su messaggio di calibCurrent
         CalibrationService.isCalibCurrentMessage = function (message) {
            return message != null && message.packetType != null && message.packetType == HwCommunicationService.PACKET_TYPES.CALIB_CURRENT;
          }

          //Restituisce true se è stato ricevuto ok su messaggio di calibCurrent
         CalibrationService.isCalibPowerMessage = function (message) {
            return message != null && message.packetType != null && message.packetType == HwCommunicationService.PACKET_TYPES.CALIB_POWER;
          }




          //Restituisce true se il pacchetto si riferisce alla calibrazione
         CalibrationService.isCalibrationMessage  = function (message) {
            return CalibrationService.isGetCalibrationTable(message) || CalibrationService.isResetCalibrationMessage(message) ||  
                CalibrationService.isCalibCurrentMessage(message) || CalibrationService.isCalibPowerMessage(message); 
          }

          //Richiede la tabella di calibrazione
         CalibrationService.queryCalibrationTable = function() {
          HwCommunicationService.sendRequestCalibration();
         }

         CalibrationService.loadCalibrationTable = function (message){
          verifyTable = [];
          
          //var rows = message.rows;
          var rows = message;
          if (rows.length == 0)
            return null;
          for (var i=0; i<rows.length; i++) {
              var row = rows[i];
              if (row.indexOf("CALTABLE") > -1) {
                  var idx1 = row.indexOf(')')+2;
                  var idx2 = row.indexOf('-');
                  var pm = Number(row.substr(idx1, idx2-idx1)/ 1000).toFixed(0);
                  idx1 = row.lastIndexOf(',');
                  var current = Number(row.substr(idx1+1, row.length-idx1-2)).toFixed(0);
                  verifyTable.push({"power": pm, "current": current});
              }
              /*else if (appRow == "OK") {
                console.log("CalibrationService.loadCalibrationTable.  CalibrationTable:");
                console.log(verifyTable);
              }
              else{
                LoggingService.error("Errore nel parsing della tabella di calibrazione.  Trovato "+appRow+".");                
                return;
              }*/

            }
           return verifyTable;

         }

         CalibrationService.getCalibrationTable = function () {
            var promise = PromiseService.getPromise();
            
            HwCommunicationService.sendRequestCalibration().then(function (result) {
                promise.resolvePromise(CalibrationService.loadCalibrationTable(result));
              });
            return promise.promise;
         }

         CalibrationService.initCalibrationTable = function() {
            var promise = PromiseService.getPromise();
            if (CalibrationService.isTableLoaded) {
              $timeout(promise.resolvePromise(""), 200);
              return promise.promise;
            }

            CalibrationService.CalibrationTable = [];
          
            CalibrationService.MinValue = 0;    
           // CalibrationService.MaxValue = 100; 
            CalibrationService.MaxValue = 29; 
            
            
            HwCommunicationService.sendRequestCalibration().then(function (result) {
                if (result && result.length > 0)  {
                    CalibrationService.MinValue = 100;    
                    CalibrationService.MaxValue = 0;
                  }
                CalibrationService.loadCalibrationTable(result);
                for (var i=0; i<verifyTable.length; i++) {
                   CalibrationService.CalibrationTable.push(verifyTable[i]);
                   CalibrationService.MinValue = Math.min(CalibrationService.MinValue, verifyTable[i].power);
                   CalibrationService.MaxValue = Math.max(CalibrationService.MaxValue, verifyTable[i].power);
                }
                if (CalibrationService.CalibrationTable.length>0)
                  CalibrationService.isTableLoaded = true;
                promise.resolvePromise("");
                if (result && result.length > 0)  {
                  LoggingService.info("La macchina è calibrata nell'intervallo "+CalibrationService.MinValue+" - "+ CalibrationService.MaxValue);
                }

              });
            return promise.promise;
         }


         CalibrationService.processCalibrationMessage = function (message) {
            if (CalibrationService.isGetCalibrationTable(message)) {
                CalibrationService.loadCalibrationTable(message);
                $rootScope.$broadcast('CalibrationTable', message);    
                }
            else if (CalibrationService.isResetCalibrationMessage (message)) {
                $rootScope.$broadcast('ResetCalibration', message);   
                }
            else if (CalibrationService.isCalibCurrentMessage (message)) {
                $rootScope.$broadcast('CalibCurrent', message);   
                }
            else if (CalibrationService.isCalibPowerMessage (message)) {
                $rootScope.$broadcast('CalibPower', message);   
                }

          }

          CalibrationService.insertVerify = function (calibrationCheck) {
               calibrationCheck.totMinutiUtilizzo = CalibrationService.totMinutiUtilizzo;
               DatabaseService.insertVerify(calibrationCheck);
          }

          CalibrationService.insertFakeVerify = function() {
             var calibrationCheck = {};
             calibrationCheck.tipoVerifica= "PRIMA VERIFICA";
             calibrationCheck.motivoVerifica = "";
             calibrationCheck.potenzaEmessa = 3000;
             calibrationCheck.potenzaMisurata = 3000; 
             calibrationCheck.esitoVerifica = 0;
             CalibrationService.insertVerify(calibrationCheck); 
          }

          //Questa funzione verifica se è necessaria una verifica di calibrazione
          CalibrationService.checkIfNeededVerify = function () {
            
            var callbackTotUtilizzo = function (tx, result) {

                    //Recupero il minutaggio complessivo del device
                    try {                      
                        CalibrationService.totMinutiUtilizzo = result.rows[0].totMinutes;
                      }
                    catch (err) {
                        LoggingService.error("CalibrationService.  Errore nella ricezione del minutaggio del dispositivo" +err);
                    }
                    
                    //Prendo le verifiche di calibrazione successive ad un determinato periodo stabilito nella funzione DatabaseService.getCalibrationsChecksAfterPeriod
                    var callBackLastVerify = function (tx, results) {
                      var checkNeed = false;
                      //se non ho effettuato verifiche con esito positivo nel periodo devo effettuare la verifica
                      if (results.rows.length == 0) {
                        checkNeed = true;
                      }
                      //Verifico se l'ultima verifica con esito positivo è avvenuta prima del periodo di utilizzo stabilito  
                      else {
                        checkNeed = (CalibrationService.totMinutiUtilizzo - results.rows[0].totMinutiUtilizzo) > 300*60;
                      }
                      

                    var message = {};
                    message.checkNeeded = checkNeed;
                    $rootScope.$broadcast('VerifyCalibrationToBeChecked', message);
                      
                  }
                  //DatabaseService.getLastCalibrationCheck(callBackLastVerify);
                  DatabaseService.getCalibrationsChecksAfterPeriod(callBackLastVerify);
            }

            DatabaseService.getUtilizzoDevice(callbackTotUtilizzo);


          }


        CalibrationService.isValueInRange = function (val) {
          if (val == 30 && Math.abs(CalibrationService.MaxValue - 30) < CalibrationService.tolleranzaCalib30mW)
             val = CalibrationService.MaxValue;
          return val >= CalibrationService.MinValue &&  val <= CalibrationService.MaxValue;
        }

        CalibrationService.getCalibTable = function ()  {
          return CalibrationService.CalibrationTable;
        }

        CalibrationService.loadCalibrationFromValues = function (table) {
             var promise = PromiseService.getPromise();
             if (table.length == 0){
               promise.resolvePromise(0);
               return promise.promise;
             }

             var loadItem = function(itm) {
                    if (itm == table.length) {
                      CalibrationService.sendCalibrationDone();
                      return;
                    }
                    var row = table[itm];
                    console.log("Faccio il load dell'Item "+row);
                    HwCommunicationService.sendCurrentCalibration(row.current).then(
                        function(result) {
                           $timeout( function ()
                            {HwCommunicationService.setCalibPower(row.power*1000).then(
                              function(result) {
                                  if (itm <= table.length) {
                                    itm ++;
                                    loadItem(itm);
                                  }
                              });
                            }, 1000);

                        }
                      );
                    

                };

              HwCommunicationService.sendResetCalibration().then(function (result) {
                loadItem(0);
              });

             return promise.promise;
        }

        CalibrationService.forceTableReload = function() {
          var promise = PromiseService.getPromise();
          CalibrationService.isTableLoaded = false;
          CalibrationService.initCalibrationTable().then(
                function(result){
                  promise.resolvePromise("");
                });
          return promise.promise;
        }

        CalibrationService.autoReloadTable = function () {
          CalibrationService.initCalibrationTable().then(
                function(result){
                  if ((isNaN(CalibrationService.MinValue) || isNaN (CalibrationService.MaxValue)) || !CalibrationService.isTableLoaded) 
                    $timeout(CalibrationService.initCalibrationTable(), 1000);
                  });
        }

        CalibrationService.get30MwValue = function () {
          if (HwCommunicationService.isPulsedAvailable())
            return 30;
          if (CalibrationService.CalibrationTable  == null || CalibrationService.CalibrationTable.length == 0)
            return -1;
          if (Math.abs(CalibrationService.MaxValue-30) < CalibrationService.tolleranzaCalib30mW)
            return CalibrationService.MaxValue;
          return -1;
        }

        CalibrationService.getMaxMwAvailable = function () {
          return Math.min(CalibrationService.MaxValue, CalibrationService.MaxAvailableValue);
        }




        CalibrationService.init = function() {
            //Ascolto gli eventi hardware
            $rootScope.$on('hardwareEvent', function (event, message) {
                if (CalibrationService.isCalibrationMessage(message)) {
                     CalibrationService.processCalibrationMessage(message);
                    }

              });
            //Appena connesso alla seriale chiedo la tabella di calibrazione
            $rootScope.$on('hardwareStatus', function (event, message) {
              if (CalibrationService.isTableLoaded)
                return;
              CalibrationService.autoReloadTable();
            });
        }



	    CalibrationService.init();
        
        return CalibrationService;
        
    })