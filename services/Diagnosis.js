'use strict';

angular.module('megaride')
.service('DiagnosisService', function($log, $translate){
            	
        var DiagnosisService = {};

        DiagnosisService.DIAGNOSIS = [
            {id: "DIAGNOSIS_1", type: 0},         //CHERATOCONO
            {id: "DIAGNOSIS_2", type: 1},         //ECTASIE SECONDARIE IATROGENE POST-LASIK O PRK
            {id: "DIAGNOSIS_3", type: 2},         //DEGENERAZIONE PELLUCIDA
           // {id: "DIAGNOSIS_4", type: 3},         //MIOPIA-IPERMETROPIA-ASTIGMATISMO
            {id: "DIAGNOSIS_5", type: 4}          //ECTASIA SECONDARIA POST RK
          ]

        DiagnosisService.CHERATITE = 100;

        DiagnosisService.getDiagnosis = function () {
            var result = [];
            var DiagnosisIndex = "";
            /*for (var i=0; i< DiagnosisService.DIAGNOSIS.length; i++) {
                  DiagnosisIndex = "Megaride.Diagnosis.DIAGNOSIS_"+(i+1);
                 result.push({"type":i, "description": $translate.instant(DiagnosisIndex)});
                }*/
            DiagnosisService.DIAGNOSIS.forEach(function (diag){
                    result.push({"type":diag.type, "description": $translate.instant("Megaride.Diagnosis."+diag.id)});
                });
            return result;
        }

        DiagnosisService.getDiagnosisDescription = function (type) {
            if (type ==  DiagnosisService.CHERATITE) 
                return $translate.instant('Protocols.Keratitis');
            var diag = DiagnosisService.getDiagnosis();           
            for (var i=0; i<diag.length; i++) {
                if (diag[i].type == type)
                    return diag[i].description;
            }
        } 
        
        return DiagnosisService;
        
    })