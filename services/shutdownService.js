'use strict';

angular.module('megaride')
.service('ShutdownService', function($log, $mdDialog, $translate, $state, $rootScope, $timeout, HwCommunicationService, CameraService, exe4Camera){
            	
        var ShutdownService = {};
        ShutdownService.cameraWasAttached = false;

        ShutdownService.OPERATIONS = {
          "NO_OP": 0,
          "SELECT_PROTOCOL": 1,
          "STOP_TREATMENT": 2,
          "SHUTDOWN": 3, 
          "SPLASH": 4
        }

        ShutdownService.operation = ShutdownService.OPERATIONS.NO_OP;


        ShutdownService.reattachCamera = function () {
          if (!exe4Camera)
            return;
          if (ShutdownService.cameraWasAttached)
            CameraService.attachCamera(); 
        }

        ShutdownService.setOperation = function (oper) {
          ShutdownService.operation = oper;          
        }

        ShutdownService.doOperation = function() {
          ShutdownService.cameraWasAttached = CameraService.isCameraAttached();
          if (exe4Camera)
            CameraService.detachVideo();
          if (ShutdownService.operation == ShutdownService.OPERATIONS.SELECT_PROTOCOL) 
            ShutdownService.doSelectProtocol();
          else if (ShutdownService.operation == ShutdownService.OPERATIONS.SHUTDOWN)
            ShutdownService.doShutdown();
          else if (ShutdownService.operation == ShutdownService.OPERATIONS.STOP_TREATMENT)
            ShutdownService.doStopTreatment();
          else if (ShutdownService.operation == ShutdownService.OPERATIONS.SPLASH)
            ShutdownService.doSplash();
        }

        ShutdownService.doSplash = function() {
           $state.go('splash');
        }


        ShutdownService.doShutdown = function() {
            var confirm = $mdDialog.confirm()
                  .title($translate.instant('Messages.SHUTDOWN_TITLE'))
                  .textContent($translate.instant('Messages.SHUTDOWN_TEXT'))
                  //.ariaLabel('Lucky day')
                  //.targetEvent(ev)
                  .ok($translate.instant('Messages.OK'))
                  .cancel($translate.instant('Messages.DISCARD'));
            $mdDialog.show(confirm).then(function(res) {
              if (res == true)
                HwCommunicationService.sendShutDownMessage(); 
              else 
                ShutdownService.reattachCamera();
            }, function() {
                ShutdownService.reattachCamera();
            });
          };

        ShutdownService.doSelectProtocol = function() {
            var confirm = $mdDialog.confirm()
                  .title($translate.instant('Messages.SELECT_PROTOCOL_TITLE'))
                  .textContent($translate.instant('Messages.SELECT_PROTOCOL_TEXT'))
                  //.ariaLabel('Lucky day')
                  //.targetEvent(ev)
                  .ok($translate.instant('Messages.OK'))
                  .cancel($translate.instant('Messages.DISCARD'));
            $mdDialog.show(confirm).then(function(res) {
              if (res == true)
                $timeout($state.go('selectProtocol'), 100);
            }, function() {
                ShutdownService.reattachCamera();
            });
          };

           ShutdownService.doStopTreatment = function() {
            var confirm = $mdDialog.confirm()
                  .title($translate.instant('Messages.STOP_TREATMENT_TITLE'))
                  .textContent($translate.instant('Messages.STOP_TREATMENT_TEXT'))
                  //.ariaLabel('Lucky day')
                  //.targetEvent(ev)
                  .ok($translate.instant('Messages.OK'))
                  .cancel($translate.instant('Messages.DISCARD'));
            $mdDialog.show(confirm).then(function(res) {
              if (res == true) {
                  HwCommunicationService.stopTreatment();
                  $rootScope.$broadcast('stopTreatment', null);
                  $timeout(function() {$state.go('selectProtocol');
                                      }, 100);
                  
                 }
            }, function() {
                ShutdownService.reattachCamera();
            });
          };

        
        return ShutdownService;
        
    })