var AngularLogger = null;
try {
    AngularLogger = require('./modules/AngularLogger.js');
  }
  catch (err) {
  }

var SystemCalls = null;
try {
    SystemCalls = require('./modules/SystemCalls.js');
  }
  catch (err) {
  }

const isOnline = require('is-online');
const fsys = require('fs');


var app = angular.module('megaride', ['ui.router', 'ngMaterial', 'pascalprecht.translate','ngCookies', 
   'ng-mfb', 'angular-virtual-keyboard', 'cfp.hotkeys', 'ngSanitize', 'timer', 'ui.grid', 'ui.grid.pagination', 'ui.grid.selection']);

//direttiva per la visualizzazione del report
app.directive('pdf', function($compile) {
  return {
    restrict: 'E',
    scope: {
        src: "=",
        height: "="
    },
    link: function (scope, element, attr) {
        function update(url) {
                element.html('<object data="' + url + '#toolbar=0" type="application/pdf" width="100%" height="100%"></object>' );
                $compile(element.contents())(scope);
        }
        scope.$watch('src', update);
    }
};});

//Prova per eliminare pagina bianca causata da errore "possibly-unhandled-rejection-error" probabilmente in pascalprecht
app.config(['$qProvider', function ($qProvider) {
  $qProvider.errorOnUnhandledRejections(false);
}]);

if (!MaxEnergyFullCustom)
  var MaxEnergyFullCustom = 10;
if (!GDPREnabled)
  var GDPREnabled = true;
if (!TimerImbibitionEnabled)
  var TimerImbibitionEnabled = false;

//INIZIALIZZAZIONE VARIABILI GLOBALI
app.run(function($rootScope,$location, $translate, $window, $timeout, ConfigurationService, ShutdownService, hotkeys, HwCommunicationService, 
                 LoggingService, CameraService, UpdateService, RestService, RemoteAssistanceDisabled, MaxEnergyFullCustom) {
      $rootScope.pageSetting= {};
      $rootScope.pageSetting.displayNavigation = false;
      $rootScope.isAdmin = false;
      $rootScope.configCamera = false;
      $rootScope.wifiEnabled = false;
      $rootScope.disableWifiDuringEmission = true;
      $rootScope.RemoteAssistanceDisabled = RemoteAssistanceDisabled;
      $rootScope.updateAvailable=false;

      $rootScope.Fidia = FidiaCustom;
      //$rootScope.Fidia = true;
      
      var lang = ConfigurationService.getTranslationLanguage();
      $translate.use(lang);

      $rootScope.shutDown = function() {
        ShutdownService.doOperation();
        //$rootScope.$broadcast('shutDown');
      }

      SystemCalls.WifiStatus(function(status) {
        $rootScope.wifiEnabled = status.wifiEnabled;
        console.log("Rilevato stato wifi:  "+$rootScope.wifiEnabled );        
      })

      hotkeys.add({
                    combo: 'esc+r',
                    description: 'Refresh',
                    callback: function() {
                      HwCommunicationService.disconnect();
                      CameraService.detachVideo();
                      location.reload();
                        //HwCommunicationService.sendSwitchUser();
                    }
                  });

      hotkeys.add({
          combo: 'esc+u',
          description: 'Support mode',
          callback: function() {
              HwCommunicationService.sendSwitchUser();
          }
        });

      hotkeys.add({
          combo: 'esc+c',
          description: 'Console',
          callback: function() {
              if (SystemCalls != null)
                SystemCalls.openCommandPrompt();
          }
        });

      hotkeys.add({
          combo: 'esc+t',
          description: 'Console',
          callback: function() {
              if (SystemCalls != null)
                SystemCalls.openTaskManager();
          }
        });
      
      /*process.setUncaughtExceptionCaptureCallback(function(e) {
        LoggingService.uncaughtException(e);
      });*/

      $rootScope.exitAdmin = function () {
        $rootScope.isAdmin = false;
      };

      var checkPenDrive = function () {
        command = 'ls /media/cfxlinker/';			
						const exec = require('child_process').exec;
							 const child = exec(command,
								 (error, stdout, stderr) => {
									 if (stdout !== null && stdout != "") {
                      $rootScope.pathPenDrive = "/media/cfxlinker/"+stdout.trim()+"/report_cfxlinker/";
                      if (!fsys.existsSync($rootScope.pathPenDrive))
                        fsys.mkdirSync($rootScope.pathPenDrive);
										 }
									 else {
										$rootScope.pathPenDrive = null;
									 }
									 if (error !== null) {
										$rootScope.pathPenDrive = null;
									 }
							 });
      }

      try {
          fsys.watch('/media/cfxlinker/', { encoding: 'buffer' }, (eventType, filename) => {
            checkPenDrive();
            /*if (filename) {
              console.log(filename);
              // Prints: <Buffer ...>
            }*/
          });
        }
      catch(ex) {

      }
      checkPenDrive();

      RestService.init();

   }); 


//CONFIGURAZIONE DEL LOGGER DI ANGULAR JS PER BUG DETECTION
 app.config(function($provide) {
    $provide.decorator("$exceptionHandler", ['$delegate', function($delegate) {
        return function(exception, cause) {
            $delegate(exception, cause);
            if (AngularLogger != null) {
               try {
                    AngularLogger.info(exception.message, exception.stack);
                    }
                catch (err) {
                  alert(err);
                }
              }
            //LoggingService.uncaughtException(exception.message);
        };
    }]);
});

//CONFIGURAZIONE NECESSARIA PER UTILIZZARE ng-href
 app.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob|chrome-extension|data|local):/);
    }
]);



  app.constant('version', '1.4.2');
  app.constant('usePromises', true);
  app.constant('exe4Camera', false);  
  app.constant('configDir', 'config');
  app.constant('RemoteAssistanceDisabled', RemoteAssistanceDisabled);
  app.constant('MaxEnergyFullCustom', MaxEnergyFullCustom);
  app.constant('GDPREnabled', GDPREnabled);
  app.constant('TimerImbibitionEnabled', TimerImbibitionEnabled);

  //Configuro il tema per la progress bar per step effettuato
  app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('treatDoneTheme')
      .primaryPalette('green') 
      });

  //Configuro il tema per la progress bar per step in corso
  app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('treatDoingTheme')
      .primaryPalette('red') 
      });

  //Configuro il tema per la progress bar per step da effettuare
  app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('treatToDoTheme')
      .primaryPalette('blue') 
      });

  //Configuro il date Picker
  app.config(function($mdDateLocaleProvider) {
      $mdDateLocaleProvider.formatDate = function(date) {
         if (date == null || date == '')
          return '';
         return moment(date).format('DD/MM/YYYY');
      };
  });


   app.config(function($stateProvider, $urlRouterProvider, $translateProvider, $mdThemingProvider) {

        //CONFIGURAZIONE ANGULAR MATERIALS
        var blueTheme = $mdThemingProvider.theme('blueTheme', 'default');
        var bluePalette = $mdThemingProvider.extendPalette('blue', {
          '500': '#1F497D'
        });
        $mdThemingProvider.definePalette('bluePalette', bluePalette);
        blueTheme.primaryPalette('bluePalette');

        //INIZIALIZZAZIONE SUPPORTO INTERNAZIONALIZZAZIONE
       // $translateProvider.useSanitizeValueStrategy('sanitize');

        $translateProvider.useStaticFilesLoader({
          prefix: 'languages/locale-',
          suffix: '.json'
         });

        

        $translateProvider.preferredLanguage('gb');
        $translateProvider.useLocalStorage();
        //$translateProvider.fallbackLanguage('gb');

        //Dialog che sospende la GUI se viene rimossa la testa ottica
        

      //INIZIALIZZAZIONE STATI
      $urlRouterProvider.otherwise('language');
      $urlRouterProvider.otherwise('splash');
      //$urlRouterProvider.otherwise('calibration');
      //$urlRouterProvider.otherwise('report');
      //$urlRouterProvider.otherwise('endtreatment');
      //$urlRouterProvider.otherwise('fullCustomDetail/1');
      //$urlRouterProvider.otherwise('wifi');
      //$urlRouterProvider.otherwise('pwdAdministration');
      
      
	    $stateProvider

      .state('language', {
         url: "/language",
         controller: 'languageCtrl',
         templateUrl: "views/language.html"
      })

	   .state('splash', {
		   url: "/splash",
		   controller: 'splashCtrl',
		   templateUrl: "views/splash.html"
	   })

     .state('pwdAdministration', {
       url: "/pwdAdministration/:nextState",
       controller: 'pwdAdministrationCtrl',
       templateUrl: "views/pwdAdministration.html"
     })

     .state('administration', {
       url: "/administration/:userProfile",
       controller: 'CtrlAdministration',
       templateUrl: "views/administration.html"
     })

	   .state('verifycalibration', {
		   url: "/verifycalibration",
		   controller: 'CtrlVerifyCalibration',
		   templateUrl: "views/verifyCalibration.html"
	   })

     .state('calibration', {
       url: "/calibration",
       controller: 'CtrlCalibration',
       templateUrl: "views/calibration.html"
     })

     .state('verifyOptHead', {
       url: "/verifyOptHead",
       controller: 'CtrlCalibrationCheck',
       templateUrl: "views/checkCalibrationInWarehouse.html"
     })

     .state('megarideCalibration', {
       url: "/megarideCalibration",
       controller: 'CtrlMegarideCalibration',
       templateUrl: "views/ProtMegaride/calibration.html"
     })
  
     .state('megaride', {
       url: "/megarideSpessore/:thickness/:cornVertexDistanceX/:cornVertexDistanceY/:kerApex/:oldState/:cornVertexRadius/:diagnosis",
       controller: 'CtrlMegarideSpessore',
       templateUrl: "views/ProtMegaride/spessore.html"
     })
	   
	   .state('selectProtocol', {
		   url: "/selectProtocol",
		   controller: 'CtrlselectProtocol',
		   templateUrl: "views/selectProtocol.html"
	   })

/*
     .state('megaride', {
       url: "/megaride",
       controller: 'CtrlMegaride',
       templateUrl: "views/megaride.html"
     })
*/

    .state('treatment', {
       url: "/treatment/:protocol/:duration/:intensity/:thickness/:steps/:dresdamode/:cornVertexDistanceX/:cornVertexDistanceY/:kerApex/:spotDiameter/:cornVertexRadius/:diagnosis/:stepsDetails",
       controller: 'CtrlTreatment',
       templateUrl: "views/treatment.html"
     })

     .state('dresdensteps', {
       url: "/dresdensteps",
       controller: 'CtrlDresdenSteps',
       templateUrl: "views/ProtDresda/selectModality.html"
     })

     .state('prkLasik', {
       url: "/prkLasik",
       controller: 'CtrlPrkLasik',
       templateUrl: "views/ProtPRKLASIK/selectModality.html"
     })

     .state('megaridesteps', {
       url: "/megaridesteps/:protocol/:duration/:intensity/:thickness/:steps/:dresdamode/:cornVertexDistanceX/:cornVertexDistanceY/:kerApex/:spotDiameter/:cornVertexRadius/:diagnosis",
       controller: 'CtrlMegarideSteps',
       templateUrl: "views/ProtMegaride/selectModality.html"
     })
     
    
    .state('endtreatment', {
       url: "/endtreatment",
       controller: 'CtrlEndTreatment',
       templateUrl: "views/endTreatment.html"
     })
    

     .state('dresda', {
       url: "/dresda",
       controller: 'CtrlDresda',
       //templateUrl: "views/dresda.html"
       templateUrl: "views/protocol.html"
     })

     .state('ionto', {
       url: "/ionto",
       controller: 'CtrlIonto',
       //templateUrl: "views/ionto.html"
       templateUrl: "views/protocol.html"
     })

     .state('cheratiti', {
       url: "/cheratiti",
       controller: 'CtrlIonto',
       //templateUrl: "views/ionto.html"
       templateUrl: "views/protocol.html"
     })

     .state('megarideTreatment', {
       url: "/megarideTreatment/:duration/:intensity/:thickness",
       controller: 'CtrlMegaride',
       //templateUrl: "views/ionto.html"
       templateUrl: "views/protocol.html"
     })

     .state('report', {
       url: "/report/:user",
       controller: 'CtrlReport',
       templateUrl: "views/report.html"
     })

      .state('serialTerminal', {
       url: "/serialTerminal",
       controller: 'CtrlSerialTerminal',
       templateUrl: "views/serialTerminal.html"
     })

     .state('fullCustom', {
       url: "/fullCustom/:edit",
       controller: 'CtrlFullCustom',
       templateUrl: "views/ProtFullCustom/FullCustom.html"
     })
     
     .state('fullCustomDetail', {
       url: "/fullCustomDetail/:name",
       controller: 'CtrlFullCustomDetail',
       templateUrl: "views/ProtFullCustom/fullCustomDetail.html"
     })

     .state('archive', {
       url: "/archive/:user",
       controller: 'CtrlArchive',
       templateUrl: "views/archive.html"
     })

     .state('wifi', {
      url: "/wifi",
      controller: 'CtrlWifi',
      templateUrl: "views/wifiConfiguration.html"
    })
	   
   })




   app.config(['VKI_CONFIG',function(VKI_CONFIG) {

  
    VKI_CONFIG.layout['Testo'] = {
        'name': "Testo", 'keys': [
        [["'", '"'], ["1", "!", "\u00b9"], ["2", "@", "\u00b2"], ["3", "#", "\u00b3"], ["4", "$", "\u00a3"], ["5", "%", "\u00a2"], ["6", "\u00a8", "\u00ac"], ["7", "&"], ["8", "*"], ["9", "("], ["0", ")"], ["-", "_"], ["=", "+", "\u00a7"], ["Bksp", "Bksp"]],
        [["Tab", "Tab"], ["q", "Q", "/"], ["w", "W", "?"], ["e", "E", "\u20ac"], ["r", "R"], ["t", "T"], ["y", "Y"], ["u", "U"], ["i", "I"], ["o", "O"], ["p", "P"], ["è", "é"],/*["\u00b4", "`"], ["[", "{", "\u00aa"],*/ ["Enter", "Enter"]],
        [["Caps", "Caps"], ["a", "A"], ["s", "S"], ["d", "D"], ["f", "F"], ["g", "G"], ["h", "H"], ["j", "J"], ["k", "K"], ["l", "L"], ["ò", "ì"], ["à", "ù"],/*["\u00e7", "\u00c7"], ["~", "^"], ["]", "}", "\u00ba"],*/ ["/", "?"]],
        [["Shift", "Shift"], ["\\", "|"], ["z", "Z"], ["x", "X"], ["c", "C", "\u20a2"], ["v", "V"], ["b", "B"], ["n", "N"], ["m", "M"], [",", "<"], [".", ">"], /*[":", ":"],*/ ["Shift", "Shift"]],
        [[" ", " ", " ", " "], ["AltGr", "AltGr"]]
      ], 'lang': ["pt-BR"] };
      VKI_CONFIG.layout.Numerico = {
        'name': "Numerico", 'keys': [
        [["1", '1'], ["2", "2"], ["3", "3"], ["-", "-"]],
        [["4", "4"], ["5", "5"], ["6", '6'], ["←", "Bksp"]],
        [["7", "7"], ["8", "8"], ["9", "9"], ["✓", "Enter"]],
        [[], ["0", "0"], [], ["."] /*["-"], ["+"],*/ ]
      ], 'lang': ["pt-BR-num"] };

      VKI_CONFIG.layout.NumericoIntero = {
        'name': "NumericoIntero", 'keys': [
        [["1", '1'], ["2", "2"], ["3", "3"], ["-", "-"]],
        [["4", "4"], ["5", "5"], ["6", '6'], ["←", "Bksp"]],
        [["7", "7"], ["8", "8"], ["9", "9"], ["✓", "Enter"]],
        [[], ["0", "0"], [], [] /*["-"], ["+"],*/ ]
      ], 'lang': ["pt-BR-numInt"] };

     
   }])
