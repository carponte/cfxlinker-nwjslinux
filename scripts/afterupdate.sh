#!/bin/bash

#HO MESSO IN !REM LA FUNZIONE ORIGINARIA
helpFunction()
{
   echo ""
#REM   echo "Usage: $0 -b Branch -b parameterB -c parameterC"
#REM   echo -e "\t-a Description of what is parameterA"
#REM   echo -e "\t-b Description of what is parameterB"
#REM   echo -e "\t-c Description of what is parameterC"
   echo "Usage: $0 -c Customization" 
  exit 1 # Exit script after printing help
}

while getopts "a:b:c:" opt
do
   case "$opt" in
      a ) parameterA="$OPTARG" ;;
      b ) Branch="$OPTARG" ;;
      c ) Customization="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
#REM if [ -z "$parameterA" ] || [ -z "$parameterB" ] || [ -z "$parameterC" ]
if [ -z "$Customization" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

# Begin script in case all parameters are correct
cp -rT /home/cfxlinker/cfxlinker/customization/"$Customization"_files/ /home/cfxlinker/cfxlinker/
rm -rf /home/cfxlinker/repo

