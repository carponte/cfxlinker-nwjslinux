var SystemCalls;
try {
  SystemCalls = require('./modules/SystemCalls.js');
}
catch (err) {
    SystemCalls = null;
}

angular.module('megaride')    
.controller('CtrlTreatment', 
function ($scope, $stateParams, $rootScope, $state, $interval, $translate, $mdDialog, $timeout, TreatmentService, ShutdownService, 
							HwCommunicationService, DebugModeService, ReportService, LoggingService, PromiseService, CameraService, 
							DiagnosisService, $mdToast, exe4Camera, TimerImbibitionEnabled) 
	{

	  var STEP_TYPE = TreatmentService.getStepTypes();

	  var TIMER_DIRECTION = {
	  	"ASC": 1,
	  	"DESC": 2
	  }

	  var OPTICAL_UNIT_STATE = {
		  "IDLE":1,
		  "RUNNING":2,
		  "PAUSE": 3,
		  "STEP_COMPLETED": 4
	  }
	  
	  var video;

	  //Spessore cornea (usato in modalità megaride)
	  $scope.thickness = $stateParams.thickness;
	  //Tipologia di protocollo
	  $scope.protocol = $stateParams.protocol;
	  //Tabella che contiene i singoli step per il protocollo
	  $scope.tableSteps = [];
	  //Puntatore allo step corrente
	  $scope.currentStep = 0;
	  //Variabili necessarie per il timer
	  $scope.timer = {};
	  $scope.timer.display = "00:00";	//Valore visualizzato
	  $scope.timer.minutes = 0;  	//Minuti
	  $scope.timer.seconds = 0;		//secondi
	  $scope.timer.timerString = "";
	  $scope.timer.timestamp = 0;			//Timestamp (secondi trascorsi dall'ultimo azzeramento del timer)	
	  $scope.timer.active = false;
	  $scope.timer.direction = TIMER_DIRECTION.ASC;	
	  $scope.timer.duration = 0;
	  $scope.timer.timeout = -1;  //Questa variabile consente di impostare un timeout al timer
	  $scope.timer.numClick = 0;  //Questa variabile conta il numero di click sul timer.  Serve per lo shortcut
	  $scope.timer.handle = null;	//Variabile che contiene il puntatore all'oggetto timer
	  $scope.timer.countdown = 0;
	  $scope.timer.jumpEnabled = 0;
	  $scope.timer.allineamentoConTestaInProgress = false;		//Questa variabile viene alzata durante l'allineamento con pstat.


	  $scope.wifiSuspended = false;		//Questa variabile viene settata a true se il wi-fi è abilitato ma viene forzato a spegnersi durante l'irraggiamento
	  $scope.isPause = false;
	  $scope.backButtonVisible = true;
	  $scope.TreatmentStarted = false;
	  $scope.whiteLed = 0;
	  $scope.focus = 0;
	  $scope.headReset = false;

	  
	  $scope.treatmentHistory = {};
	  $scope.treatmentHistory.visible= true;
	  $scope.treatmentHistory.counterSteps = 0;

	  $scope.paramsCamera = {};
	  $scope.paramsCamera.brightness = 0;
	  $scope.paramsCamera.contrast = 0;
	  $scope.paramsCamera.saturation = 0;

	  $scope.statusConnection = false;
	  $scope.hoursVisible = false;
	  $scope.timerVisible = true;

	  $scope.opticalUnitState = OPTICAL_UNIT_STATE.IDLE;
	  $scope.OpticalUnitNotified = false;		//In questa variabile viene memorizzato quando viene richiesto alla testa di far partire un trattamento.  Usato durante il jump

	  $scope.timerImbibitionMode = TimerImbibitionEnabled;

		$scope.goToProtocolSelection = function() {
			$state.go('selectProtocol');
		}

		$scope.updateTimerDisplay = function () {
			$scope.timer.minutes = 	Math.trunc($scope.timer.timestamp / 60);
			if ($scope.timer.minutes > 60)
				$scope.timer.minutes = 	Math.trunc($scope.timer.minutes % 60);
			$scope.timer.seconds = 	Math.trunc($scope.timer.timestamp % 60);
			$scope.timer.hours = 	Math.trunc($scope.timer.timestamp / (60*60));

			$scope.timer.display = "";
			if ($scope.timer.hours > 0)
				$scope.timer.display = $scope.timer.hours+":";
			if ($scope.timer.minutes < 10)
				$scope.timer.display += "0";
			$scope.timer.display += $scope.timer.minutes+":";
			if ($scope.timer.seconds < 10)
				$scope.timer.display += "0";
			$scope.timer.display += $scope.timer.seconds;

			$scope.timer.display += "a";
			
			if ($scope.timer.display.indexOf('0-1') > -1) {				
				$scope.timer.display = "00:00";
			}

			$scope.updateHorizontalStatus();			
			//$scope.apply();
		}

		$scope.updateHorizontalStatus = function() {
			 if (!$scope.treatmentHistory.visible)
			 	return;
			 var step = $scope.getCurrentStep();
			 step.progress = step.duration - $scope.timer.timestamp;
			}

		$scope.onTimestamp = function () {
			return;
			if (!$scope.timer.active || $scope.isPause)
				return;
			if (isNaN($scope.timer.timestamp))
				return;			
			if ($scope.timer.direction == TIMER_DIRECTION.ASC) {
				if ($scope.timer.timestamp < $scope.timer.duration)
					$scope.timer.timestamp++;
				else {
					$scope.timer.active = false;
					$scope.handleTimerTrigger();
				}

			}
			else {
				if ($scope.timer.timestamp >0)
					$scope.timer.timestamp--;
				else {
					$scope.timer.active = false;
					$scope.handleTimerTrigger();
				}
			}
			$scope.updateTimerDisplay();
		}

		//Setta il timer come se fosse orologio
		$scope.setCountUp = function (seconds) {
			$scope.timer.active = false;
			$scope.timer.timestamp = 0;
			$scope.timer.duration = seconds;
			$scope.timer.direction = TIMER_DIRECTION.ASC;	
			$scope.timer.active = true;
			$scope.enableTimer();
		}

		//Funzione che viene richiamata quando il timer va alla fine
		$scope.handleTimerTrigger = function() {
			
		}

		$scope.completeTimer = function() {
			//alert("TreatmentController.  Sono in complete Timer")
			$scope.timer.active = false;
			if ($scope.timer.direction == TIMER_DIRECTION.DESC){
				$scope.timer.timestamp = 0;
				$scope.updateTimerDisplay();
			}
			//$scope.handleTimerTrigger();
			//01.10.2021 $scope.nextStep();
		}

		$scope.enableTimer = function () {
			$scope.timer.handle = $interval($scope.onTimestamp, 1000);
		}

		//INIZIO FUNZIONI TIMER NUOVO
		$scope.resumeTimer = function () {
			$scope.$broadcast('timer-resume');
			$scope.timer.active = true;			
			return;
		}

		$scope.stopTimer = function () {
			$scope.$broadcast('timer-stop');
			$scope.timer.active = false;
			$scope.isPause = false;
			//return;

			if ($scope.timer.handle != null)
				$interval.cancel($scope.timer.handle);
            $scope.timer.handle = undefined;
		}

		$scope.onTimerTick = function () {
			var step = $scope.getCurrentStep();
			/*if (step.isPulsed) {
				 var duration = 0;
				 var numSteps = step.duration / step.irradiationTime;
				 duration = numSteps * (step.irradiationTime + step.pauseTime);
					duration += step.duration % step.irradiationTime;
					step.progress = duration - $scope.timer.countdown / 1000;
			}*/
			if (!step.isPulsed) {
				 step.progress = step.duration - $scope.timer.countdown / 1000;
				 step.progress++;
				}
			
			$scope.allineaTimer();
		}

		$scope.allineaTimer = function () {
			//Se il trattamento è in pausa non è necessario fare nulla
			if ($scope.isPause || !$scope.timer.active)
			  return;
			var step = $scope.getCurrentStep();
			if ($scope.opticalUnitState != OPTICAL_UNIT_STATE.RUNNING && step != null && step.type != STEP_TYPE.IMBIBITION) {
				 console.log("allineaTimer.  Me ne esco perché non sono in stato running ma in "+$scope.opticalUnitState);
				 return;
				}
			
			//Se lo step è pulsato il timer è sempre aggiornato perché processa i messaggi asincroni relativi agli step trascorsi e non è necessario allinearlo 
			if (step.isPulsed){
				return;
				}	
			//In fase di imbibizione il timer è gestito dal solo software e la testa non è involved
			if (step.type && step.type == STEP_TYPE.IMBIBITION){
				return;
				}	

			var countdownInt = Math.round($scope.timer.countdown);
			

			var now = new Date().getTime();
			var supposedCountDown = $scope.timer.supposedEndTime - now;
			var diff = supposedCountDown-countdownInt;

			//Faccio il pstat o se ho rilevato errori nei calcoli oppure ogni 10 secondi
			if (isNaN(supposedCountDown) || (diff > 2000) || ((countdownInt/1000)%10 == 0)) {
				var errorMsg = "";
				if (isNaN(supposedCountDown))
					errorMsg += "Countdown NaN";
				if (diff > 2000)
					errorMsg += "diff > 2000";
				if ((countdownInt/1000)%10 == 0)
					errorMsg += "Tempo trascorso multiplo di 10";
				console.log ("Devo allinearmi con la testa perché "+errorMsg);
				
				if (step.progress != 0 && step.type == STEP_TYPE.IRRADIATION)				
					$scope.allineaTimerConTesta();
			}

			
			if ($scope.timer.allineamentoConTestaInProgress)
				return;

			//console.log("Countdown supposto: "+supposedCountDown+"; valore rilevato: "+countdownInt+".  Differenza: "+diff);
			if (Math.abs(diff) > 700) {
				//console.log("Aggiorno il timer a video");
				$scope.$broadcast('timer-set-countdown-seconds', supposedCountDown/1000);
				//$scope.$broadcast('timer-start');
			}

			if (now - $scope.timer.supposedEndTime > 5000) {
				 console.log("sono passati più di 5 secondi da quando sarebbe dovuto terminare il trattamento, avrei dovuto forzare il salto step?");
				 //$scope.endStep()
				}

			/*if (countdownInt == 0)
				$scope.endStep();*/
			return; 
			
			
			
			
			
			if (step.type == STEP_TYPE.IRRADIATION && !$scope.isPause && (countdownInt % 2 == 0 || countdownInt<2) 
					&& !step.isPulsed && $scope.opticalUnitState == OPTICAL_UNIT_STATE.RUNNING) {
				TreatmentService.queryStatusTreatment().then (
						function (secondsRemaining) {
							if (!isNaN(secondsRemaining) && secondsRemaining > 0) {
								$scope.$broadcast('timer-set-countdown-seconds', secondsRemaining);
								$scope.$broadcast('timer-start');
							}
							else if (secondsRemaining == 0 && $scope.timer.active) {
								//$scope.completeTimer();
								//RIMOSSO SALTO STEP
								//$scope.endStep();
							}
						}
					);
			}
		}

		$scope.allineaTimerConTesta = function () {
			//Se il trattamento è pulsato la sincronizzazione avviene processando i messaggi asincroni
			if ($scope.getCurrentStep().isPulsed)
				return;
			if ($scope.opticalUnitState != OPTICAL_UNIT_STATE.RUNNING) {
				console.log("Non allineo il timer con la testa perché non sono in stato running");
				return;
			}
			$scope.timer.allineamentoConTestaInProgress = true;
			TreatmentService.queryStatusTreatment().then (
				function (secondsRemaining) {
					if (secondsRemaining < 1) return;
					var now = new Date().getTime();
					$scope.timer.supposedEndTime = now + secondsRemaining*1000;
					$scope.timer.allineamentoConTestaInProgress = false;
				}
			);
		}

		//Setta il timer in countdown 
		$scope.setCountDown = function (seconds) {
			var promise = PromiseService.getPromise();
			
			$timeout(function() {	//sostituzione $apply
				var i;
				$scope.$broadcast('timer-stop');
				$scope.$broadcast('timer-set-countdown-seconds', seconds);
				$scope.hoursVisible = seconds > 3600;
				//$scope.apply();

				//$scope.$broadcast('timer-start');		//RIMOSSO PER PROVA TIMER
				$scope.timer.active = true;
				$scope.timer.handle = $interval($scope.onTimerTick, 1000);
				promise.resolvePromise(seconds);
				return;
				

				$scope.timer.active = false;
				$scope.stopTimer();
				$scope.timer.timestamp = seconds-1;
				$scope.timer.direction = TIMER_DIRECTION.DESC;	
				$scope.timer.active = true;
				//$scope.timer.handle = $interval($scope.onTimestamp, 1000);
				$scope.enableTimer();
			});
			return promise.promise;
		}

		$scope.resumeTimerOldCalls = function () {
			$scope.isPause = false;
			$scope.allineaTimerConTesta();
			$scope.$broadcast('timer-resume');
		}

		$scope.setTimerPause = function() {
			$scope.$broadcast('timer-stop');
			$scope.isPause = true;
		}
		




		$scope.apply = function() {
            if ($scope.$$phase) 
                return;
            try {
                $scope.$apply();
            }
            catch (err) {

            }
        }

        $scope.beep = function () {
    		HwCommunicationService.sendBeepMessage();
		}


        //Restituisce true se il pulsante di run va abilitato
        $scope.isRunEnabled = function () {
        	return 	$scope.getCurrentStep().type == STEP_TYPE.PAUSE || 
        			($scope.getCurrentStep().type == STEP_TYPE.IMBIBITION  && !$scope.timer.active) ||
        			$scope.getCurrentStep().type == STEP_TYPE.PATIENT_POSITION ||
        			$scope.isPause;
        }

        $scope.isPauseEnabled = function() {
        	return !$scope.isRunEnabled();
        }

        //Restituisce true se è abilitata la possibilità di saltare il timer
        $scope.isJumpEnabled = function () {
        	return $scope.getCurrentStep().type == STEP_TYPE.IMBIBITION  || $scope.getCurrentStep().type == STEP_TYPE.IRRADIATION/*&& $scope.timer.active*/;
        }

        //Restituisce true se il toggle del focus va abilitato
        $scope.isFocusEnabled = function() {
        	return $scope.isRunEnabled() && 
        			($scope.getCurrentStep().type == STEP_TYPE.IRRADIATION ||
        				$scope.getCurrentStep().type == STEP_TYPE.PAUSE);
        }

        $scope.isWhiteLedBarVisible = function () {
        	return $scope.getCurrentStep().type == STEP_TYPE.IMBIBITION && $scope.headReset;
        }

        $scope.pause = function ()  {
        	LoggingService.info("Trattamento messo in pausa");
        	$scope.setTimerPause();
        	TreatmentService.pauseTreatment();
        }

        $scope.resume = function () {
        	if ($scope.focus == 1) {
        		$scope.focus = 0;
        	}
        	if (!$scope.headReset) {
        		$scope.handleStepEnter($scope.getCurrentStep());
			}
			else if ($scope.getCurrentStep().type == STEP_TYPE.PAUSE)
				$scope.nextStep();
        	else
        		$scope.updateFocus().then(
	        		function () {
	        			if ($scope.getCurrentStep().type == STEP_TYPE.IMBIBITION) {
		        			if (!$scope.isPause) {
		        				$scope.handleStepEnter($scope.getCurrentStep());
		        				}
		        			else 
				        		$scope.resumeTimerOldCalls();
		        		}
		        	else {
		        		//01.10.21 if (!$scope.isPause) {
						/*if ($scope.opticalUnitState ==  OPTICAL_UNIT_STATE.STEP_COMPLETED) {
		        			$scope.nextStep();
		        		}
		        		else {*/
		        			LoggingService.info("Riprendo il trattamento");
		          			TreatmentService.resumeTreatment().then(
		          					function(result) {
										if (result.lenght >0 && result[0].indexOf('ERROR')>-1) {
											LoggingService.info("Errore nel resume.  Provo azione di emergenza");
											$scope.handleStepEnter($scope.getCurrentStep());
										}
										else	{
											if ($scope.opticalUnitState == OPTICAL_UNIT_STATE.PAUSE) {
												$scope.opticalUnitState = OPTICAL_UNIT_STATE.RUNNING;												
											   }
											$scope.resumeTimerOldCalls();
										}
		          					}
		          				);
		        		//}

		        	}
	        	  }
	        	);

        }

        $scope.updateWhiteLed = function () {
        	var promise = PromiseService.getPromise();
        	if ($scope.whiteLed) {
        		LoggingService.info("Accendo la luce bianca");
				TreatmentService.setWhiteLed(1).then(function(result) {
                    promise.resolvePromise(result);
                });
        	}
			else {
				LoggingService.info("Spengo la luce bianca");
				TreatmentService.setWhiteLed(0).then(function(result) {
                    promise.resolvePromise(result);
                });
			}
			return promise.promise;
        }

        $scope.updateFocus = function () {
        	if ($scope.focus) {
        		LoggingService.info("Accendo il focus");
				return TreatmentService.focusOn();
        	}
			else {
				LoggingService.info("Spengo il Focus");
				return TreatmentService.focusOff();
			}	
        }

        $scope.setWhiteLed = function (status) {
        	$scope.whiteLed = status;
        	var promise = PromiseService.getPromise();
        	$scope.updateWhiteLed().then(function(result){
        		promise.resolvePromise(result);
        	});
        	return promise.promise;
        }

        //Funzione che fa l'handle dell'evento di inizio trattamento
        $scope.startTreatmentHandle = function() {
        	$scope.setTimerPause();
        	$scope.handleStepEnter($scope.getCurrentStep());
        }

        //Funzione richiamata quando il trattamento è finito
        $scope.endTreatmentHandle = function() {
        	//$scope.gotToReport();
			//$scope.goToProtocolSelection();
			console.log("Sono in end Treatement chiamato da ");
			console.trace();
        	ReportService.saveCurrentTreatmentOnDB();
			CameraService.detachVideo();
			try {
				TreatmentService.clearCache();
				}
			catch (err) {
				
			}
        	$scope.gotToEndTreatment();
        }

        //Funzione che dirotta al report
        $scope.gotToReport = function () {
        	$state.go('report');
        }

        //Funzione che dirotta alla pagina che notifica la fine del trattamento
        $scope.gotToEndTreatment = function () {
        	LoggingService.info("Trattamento terminato");
        	$state.go('endtreatment');
        }

        $scope.getImbibitionDuration = function() {
        	var imbibition = 5;

        	if ($scope.protocol == 'Dresden') {
        		//caso off-label
        		if ($stateParams.dresdamode == 1) 
        			imbibition = 30;
        		else
        			imbibition = 60*2;
        	}
        	else if ($scope.protocol == 'Megaride') {
        		//in caso di miopia si imbibisce sempre per due minuti
        		if ($stateParams.diagnosis == 3)
        			imbibition = 2;
        		else
        			imbibition = 15;
        	}
        	else if ($scope.protocol == 'Demo')
        		imbibition = 0.17;
        	else if ($scope.protocol == 'PRK/LASIK' || $scope.protocol == 'Cheratiti')
        		imbibition = 2;
		else {			
			for (var i=0; i<$scope.tableSteps.length; i++) 
			  if ($scope.tableSteps[i].type == STEP_TYPE.IMBIBITION)
				 return $scope.tableSteps[i].duration;
			
		}
		imbibition = TreatmentService.getImbibitionOf(imbibition);
            
        	return imbibition;
        }

		$scope.getStepID = function() {        
        	$scope.treatmentHistory.counterSteps++;
        	return $scope.treatmentHistory.counterSteps;
        }

        //Inizializza gli step per il protocollo corrente
		$scope.initTableSteps = function() {
			TreatmentService.cacheTreatment($stateParams);
			if ($stateParams.stepsDetails != null && $stateParams.stepsDetails.length > 0) {
				var strString = $stateParams.stepsDetails;
				var stps = JSON.parse(strString);
				for (var i=0; i<stps.length; i++) {
					 var stp = stps[i];
					 stp.id = $scope.getStepID();
					 $scope.tableSteps.push(stp);
					}
				 $scope.logTableSteps();
				 return;
			   }

			$scope.tableSteps.push({type: STEP_TYPE.IMBIBITION, duration: $scope.getImbibitionDuration(), progress: 0, id: $scope.getStepID()});
			$scope.tableSteps.push({type: STEP_TYPE.PATIENT_POSITION, id: $scope.getStepID()});
			if ($scope.protocol == 'Dresden') {
				for (var i=1; i<7; i++) {
				  $scope.tableSteps.push({type: STEP_TYPE.IRRADIATION, duration: 5 *60, fromStart:0, power: 3000, "desc": "(step "+i+")", progress: 0, id: $scope.getStepID()});
				  if (i < 6)
				  	$scope.tableSteps.push({type: STEP_TYPE.PAUSE, id: $scope.getStepID()});	
				}
			}
			else if ($scope.protocol == 'Iontoforesi') {
				for (var i=0; i<3; i++) {
				  $scope.tableSteps.push({type: STEP_TYPE.IRRADIATION, duration: 3*60, fromStart:0, power: 10000, progress: 0, id: $scope.getStepID() });
				  if (i < 2)
				  	$scope.tableSteps.push({type: STEP_TYPE.PAUSE, id: $scope.getStepID()});	
				}
			}
			/*else if ($scope.protocol == 'Cheratiti') {
				  $scope.tableSteps.push({type: STEP_TYPE.IRRADIATION, duration: 5*60, fromStart:0, power: 10000});
				  $scope.tableSteps.push({type: STEP_TYPE.PAUSE});	
			}*/

			else if ($scope.protocol == 'PRK/LASIK' || $scope.protocol == 'Cheratiti') {
				  var secondsChera = Math.trunc($stateParams.duration * 60);
				  var powerChera = Math.trunc($stateParams.intensity*1000);
				  $scope.tableSteps.push({type: STEP_TYPE.IRRADIATION, duration: secondsChera, power: powerChera, progress: 0, id: $scope.getStepID()});

				  /*if ($stateParams.duration == 30) {
					  	for (var i=1; i<7; i++) {
							  $scope.tableSteps.push({type: STEP_TYPE.IRRADIATION, duration: 5 *60, fromStart:0, power: 3000, "desc": "(step "+i+")", progress: 0, id: $scope.getStepID()});
							  if (i < 6)
							  	$scope.tableSteps.push({type: STEP_TYPE.PAUSE, id: $scope.getStepID()});	
							}
					  	}
				  else if ($stateParams.duration == 10) {
				  		for (var i=1; i<3; i++) {
							  $scope.tableSteps.push({type: STEP_TYPE.IRRADIATION, duration: 5 *60, fromStart:0, power: 9000, "desc": "(step "+i+")", progress: 0, id: $scope.getStepID()});
							  if (i < 2)
							  	$scope.tableSteps.push({type: STEP_TYPE.PAUSE, id: $scope.getStepID()});	
							}
					  	}

				  else if ($stateParams.duration == 5) {
				  		for (var i=1; i<3; i++) {
							  $scope.tableSteps.push({type: STEP_TYPE.IRRADIATION, duration: 2.5 *60, fromStart:0, power: 18000, "desc": "(step "+i+")", progress: 0, id: $scope.getStepID()});
							  if (i < 2)
							  	$scope.tableSteps.push({type: STEP_TYPE.PAUSE, id: $scope.getStepID()});	
							}
					  	}*/
				  }



			else if ($scope.protocol == 'Megaride'){
				var secondsMega = Math.trunc($stateParams.duration * 60);
				var powerMega = Math.trunc($stateParams.intensity*1000);
				var stepsMega = $stateParams.steps;
				secondsMega = secondsMega / stepsMega;
				for (var i=0; i<stepsMega; i++) {
					$scope.tableSteps.push({type: STEP_TYPE.IRRADIATION, duration: secondsMega, power: powerMega, progress: 0, id: $scope.getStepID()});
					if (i < stepsMega-1)
						$scope.tableSteps.push({type: STEP_TYPE.PAUSE, id: $scope.getStepID()});
				}
			}
			else if ($scope.protocol == 'Demo') {
				for (var i=0; i<2; i++) {
				  $scope.tableSteps.push({type: STEP_TYPE.IRRADIATION, duration: 1*60, fromStart:0, power: 3000, "desc": "(Demo step "+(i+1)+")", progress: 0, id: $scope.getStepID()});
				  if (i < 1)
				  	$scope.tableSteps.push({type: STEP_TYPE.PAUSE, id: $scope.getStepID()});	
				}
			}
			$scope.tableSteps.push({type: STEP_TYPE.END_TREATMENT, id: $scope.getStepID()});
			$scope.logTableSteps();
		}

		$scope.logStep = function(step) {
			var strLine = step.id+") ";
				switch (step.type) {
					case STEP_TYPE.IMBIBITION: 
											strLine += "Imbibizione: "+(step.duration/ 60) +" min";
											break;
					case STEP_TYPE.PATIENT_POSITION: 
											strLine += "Posizionamento paziente";
											break;
					case STEP_TYPE.IRRADIATION: 
											strLine += "Irraggiamento: "+ (step.duration/60)+" min a "+(step.power/1000)+" mW";
											break;
					case STEP_TYPE.END_TREATMENT: 
											strLine += "Fine trattamento";
											break;
					case STEP_TYPE.PAUSE: 
											strLine += "Pausa";
											break;
					default: break;
				}
			LoggingService.info(strLine);
		}

		$scope.logTableSteps = function() {
			LoggingService.info("======ELENCO DEGLI STEP NECESSARI PER IL PROTOCOLLO SELEZIONATO ("+$scope.protocol+") "+"======");
			for (var i=0; i<$scope.tableSteps.length; i++) {
				$scope.logStep($scope.tableSteps[i]);
			}
			LoggingService.info("======ELENCO DEGLI STEP (FINE) ======");
		}

		$scope.getStepCountDown = function(step) {
			if (!step.isPulsed)
				return step.duration;
			var countdown = 0;
			var numSteps = step.duration / step.irradiationTime;
			countdown = numSteps * (step.irradiationTime + step.pauseTime);
			countdown += step.duration % step.irradiationTime;
			return countdown;
		}

		$scope.initReport = function () {
			ReportService.clear();
			ReportService.setProtocol($scope.getProtocol());
			
			var power = 0;
			var steps = 0;
			var duration =0;

			var pulsedData = {};
			pulsedData.isPulsed = false;

			for (var i=0; i<$scope.tableSteps.length; i++) {
				var step = $scope.tableSteps[i];
				if (step.type != STEP_TYPE.IRRADIATION)
					continue;
				steps++;
				if (step.power > power)
					power = step.power;
				duration += step.duration;
				if (step.isPulsed) {
					pulsedData.isPulsed = true;
					pulsedData.irradiationTime = step.irradiationTime;
					pulsedData.pauseTime = step.pauseTime;
				}
			}
			
			ReportService.setPulsedData(pulsedData);
			ReportService.setDuration(duration/ 60);
			ReportService.setPower(power/1000);
			ReportService.setImbibition($scope.getImbibitionDuration()/60);
			ReportService.setSteps(steps);

			ReportService.setVertexDistanceX ($stateParams.cornVertexDistanceX);
			ReportService.setVertexDistanceY($stateParams.cornVertexDistanceY);
        	ReportService.setKMax($stateParams.kerApex);
        	ReportService.setThickness($stateParams.thickness);
        	ReportService.setSpotDiameter($stateParams.spotDiameter);
        	ReportService.setCornVertexRadius($stateParams.cornVertexRadius);
        	ReportService.setDiagnosis(DiagnosisService.getDiagnosisDescription($stateParams.diagnosis));
		}

		$scope.doTreatmentStep = function (step){
			if (step == null)
				step = $scope.getCurrentStep();
			if (step.type !=  STEP_TYPE.IRRADIATION) {
				console.log ("Ecco il caso");
				return;
			}
			LoggingService.info('Mi preparo allo step: '+JSON.stringify(step));
			
			var promise = PromiseService.getPromise();

			var ct = $scope.getStepCountDown(step);
			$scope.timerVisible = ct>0;
			$scope.setCountDown(ct);

			if (step.isPulsed) {
				TreatmentService.stopTreatment().then(function(test) {
					TreatmentService.setTreatment(TreatmentService.Treatments.PULSED).then(function(result){
						LoggingService.info("TreatmentController.  Effettuo lo step "+JSON.stringify(step));
						LoggingService.info("TreatmentController.  Setto i seguenti parametri: "+step.duration+", "+step.power);
						TreatmentService.setPulsedModeParams (parseInt(step.irradiationTime), parseInt(step.pauseTime), parseInt(step.duration), parseInt(step.power)).then(function(result){							
									$scope.logStep(step);

									//Timeout per Giovanni
									$timeout(function() {
									$scope.OpticalUnitNotified = true;
									TreatmentService.startTreatment().then(function(result){
										//$scope.setCountDown($scope.getStepCountDown(step));
										//$scope.$broadcast('timer-start');										
										promise.resolvePromise(result);
										})

									}, 500)


										

							})
					});
				});
			}
			else {
				TreatmentService.stopTreatment().then(function(test) {
					TreatmentService.setTreatment(TreatmentService.Treatments.MANUAL).then(function(result){
						LoggingService.info("TreatmentController.  Effettuo lo step "+JSON.stringify(step));
						LoggingService.info("TreatmentController.  Setto i seguenti parametri: "+step.duration+", "+step.power);
						
						TreatmentService.setManualModeParams (parseInt(step.duration), parseInt(step.power)).then(function(result){							
								$scope.logStep(step);
								//return;

								//Timeout per Giovanni
								$timeout(function() {
									$scope.OpticalUnitNotified = true;
									TreatmentService.startTreatment().then(function(result){
									//$scope.setCountDown(step.duration);
									//$scope.$broadcast('timer-start');
									promise.resolvePromise(result);
								})
							
							}, 500)




							})
						});
					});
				}
			return promise.promise;
					
		}

		$scope.setProtocol = function(start) {
			var promise = PromiseService.getPromise();
			TreatmentService.stopTreatment().then(function(result) {
				$scope.doTreatmentStep().then(function(result) {
					promise.resolvePromise(result);
				});
			});
			return promise.promise;
		}

		//Restituisce lo step corrente
		$scope.getCurrentStep = function() {
			return $scope.tableSteps[$scope.currentStep];
		}

		//Passa allo step successivo
		$scope.nextStep = function () {
		  $scope.stopTimer();
		  if ($scope.getCurrentStep() == null) {
			  LoggingService.info("Treatment Controller.  nextStep.  Lo step corrente è nullo.  Indice: "+$scope.currentStep+" Tabella:"+ JSON.stringify($scope.tableSteps));
			  return;
		  }

		  if ($scope.getCurrentStep().type && $scope.getCurrentStep().type != STEP_TYPE.PAUSE && $scope.getCurrentStep().type != STEP_TYPE.PATIENT_POSITION)
		  	$scope.beep();
		  $scope.currentStep++;

		 /* 
		  console.log("Passato allo step "+$scope.currentStep);
		  console.log(new Error().stack);
		  */

		  $scope.opticalUnitState = OPTICAL_UNIT_STATE.IDLE;
		  $scope.handleStepEnter($scope.getCurrentStep());
		}

		//Fa l'handle in uscita dallo step
		$scope.handleStepExit = function (step) {
			var promise = PromiseService.getPromise();
			switch (step.type) {
				/*case STEP_TYPE.PATIENT_POSITION		: 
	  													TreatmentService.focusOff().then(function(result){
	  														$scope.setProtocol().then(function(result){
	  															promise.resolvePromise(result);
	  														});	
	  													});
	  													break;*/
	  			default: 								
	  													promise.resolvePromise("");
	  													break;						
			}
			return promise.promise;
		}

		//verifica se lasciare acceso o no il wifi a seconda della configurazione corrente
		$scope.handleWifiForStep = function (stepType) {
			if (stepType == STEP_TYPE.IRRADIATION && $rootScope.disableWifiDuringEmission) {
				 SystemCalls.switchOffWifi();
				 $scope.wifiSuspended = true;
				}
			else if ($rootScope.wifiEnabled && $scope.wifiSuspended  && $rootScope.disableWifiDuringEmission) {
				$scope.wifiSuspended = false;
				SystemCalls.switchOnWifi();
			}
		}

		//Fa l'handle in ingresso allo step
		$scope.handleStepEnter = function(step) {
			var promise = PromiseService.getPromise();
			$scope.OpticalUnitNotified = false;
			try{
				$scope.handleWifiForStep(step.type);
				}
			catch (err) {}
			switch (step.type) {
				case STEP_TYPE.PATIENT_POSITION		:
													$scope.remindDiameter();
													$scope.stopTimer();
													$scope.timerVisible = false;
													$scope.$broadcast('timer-set-countdown-seconds', 0);
													$scope.setWhiteLed(false).then(function(result){
														//questo serve a non far spegnere i led rossi dopo il timeout
														TreatmentService.setManualMode().then(function(result){
															TreatmentService.focusOn().then(function(result){
																ShutdownService.setOperation(ShutdownService.OPERATIONS.SELECT_PROTOCOL);
																promise.resolvePromise(result);
															})
														})		
													});
	  												break;
	  			case STEP_TYPE.IMBIBITION 		:
	  												$scope.setCountDown(step.duration).then(function (result){
														$scope.$broadcast('timer-start')
													  });
	  												//ShutdownService.setOperation(ShutdownService.OPERATIONS.NO_OP);
	  												ShutdownService.setOperation(ShutdownService.OPERATIONS.STOP_TREATMENT);
	  												promise.resolvePromise("");
	  												break;
				case STEP_TYPE.IRRADIATION		:   
													ShutdownService.setOperation(ShutdownService.OPERATIONS.STOP_TREATMENT);
													TreatmentService.focusOff().then(function(result){
														$scope.doTreatmentStep(step);
														});
													promise.resolvePromise("");
													break;

				case STEP_TYPE.END_TREATMENT	:  	$scope.endTreatmentHandle();
													ShutdownService.setOperation(ShutdownService.OPERATIONS.SELECT_PROTOCOL);
													promise.resolvePromise("");
													break;

				case STEP_TYPE.PAUSE            :   
													//$scope.timer.timestamp = 0;
													//$scope.updateTimerDisplay();
													$scope.stopTimer();
													$scope.$broadcast('timer-set-countdown-seconds', 0);
													
													promise.resolvePromise("");
													break;


				default: 							
													promise.resolvePromise("");
													break;
			}
			//$scope.cacheTreatmentStatus();
			return promise.promise;			
		}

		$scope.getStepDescription = function(step) {
			if (step.desc != null)
				return step.desc;
			var msg = "";
			switch (step.type) {
				case STEP_TYPE.IRRADIATION	:   
												msg =  "Treatment.Steps.Irradiation";
												break;
				case STEP_TYPE.IMBIBITION	:   
												msg =  "Treatment.Steps.Imbibition";
												break;
				case STEP_TYPE.PAUSE	:   
												msg = "Treatment.Steps.Pause";
												break;
				case STEP_TYPE.PATIENT_POSITION:
												msg = "Treatment.Steps.PatientPosition";
												break;
				case STEP_TYPE.END_TREATMENT:
												msg = "Treatment.Steps.End";
												break;
				default: break;
			}
			return $translate.instant(msg);
			}

		$scope.getCurrentStepDescription = function() {
			var step = $scope.getCurrentStep();
			return $scope.getStepDescription(step); 			
		}

		$scope.getProtocol = function() {
			if ($scope.protocol == "Dresden") {
				return $translate.instant("Protocols.Dresden");
			}
			else if ($scope.protocol == "Iontoforesi") {
				return $translate.instant("Protocols.Iontophoresis");
			}
			else if ($scope.protocol == "Cheratiti") {
				return $translate.instant("Protocols.Keratitis");
			}
			else if ($scope.protocol == "Demo") {
				return $translate.instant("Protocols.Demo");
			}
			else if ($scope.protocol != 'Megaride')
				return $scope.protocol;
			return "Custom Fast";
		}

		$scope.getStepMessage = function() {
			var step = $scope.getCurrentStep();
			switch (step.type) {
				case STEP_TYPE.IRRADIATION	:   
												return "Treatment.StepsDescription.Irradiation";
				case STEP_TYPE.IMBIBITION	:   
												return "Treatment.StepsDescription.Imbibition";
				case STEP_TYPE.PAUSE	:   
												return "Treatment.StepsDescription.Pause";
				case STEP_TYPE.PATIENT_POSITION:
												return "Treatment.StepsDescription.PatientPosition";
				case STEP_TYPE.END_TREATMENT:
												return "Treatment.StepsDescription.End";
				default: break;
			}
		}

	   
		//Gestione della disconnessione della testa ottica.
		//Se la connessione viene persa per più di 10 secondi il sistema va in pausa da entrambi i lati
		$scope.seriesStatusListener = function()  {
			var hwStatus = HwCommunicationService.isInited();
			if (hwStatus)
				return;
			$timeout(
				function() {
					hwStatus = HwCommunicationService.isInited();
					if (hwStatus)
						return;
					var step = $scope.getCurrentStep();
					if (step.type == STEP_TYPE.IRRADIATION) 
					 	$scope.pause();
				}, 10000);
            };

            //Funzione richiamata al click sul timer
            $scope.timerDoClick = function () {
            	if (!DebugModeService.isTimerShortCutEnabled())
            		return;
            	$scope.timer.numClick ++;
            	if ($scope.timer.numClick == 3) {
            		$scope.doJump();
            	}
            }

    //Questa funzione consente di saltare l'imbibizione
    $scope.doJump = function() {
    	if (!$scope.headReset)
    		return;
    	var successCallBack = function() {
				LoggingService.info("Cronometro interrotto");
				$scope.getCurrentStep().progress = $scope.getCurrentStep().duration;
				var step = $scope.getCurrentStep();
				if (step && step.type && step.type == STEP_TYPE.IRRADIATION && $scope.OpticalUnitNotified) {
						TreatmentService.stopTreatment().then(function(result) {
								$scope.nextStep();
						});
					}
				else {
					$scope.nextStep();
				}				
    		
    		};
    	$scope.pause();
    	CameraService.detachVideo();
        var confirm = $mdDialog.confirm()
              .title($translate.instant('Treatment.JumpImbibitionTitle'))
              .textContent($translate.instant('Treatment.JumpImbibitionMessage'))
              .ok($translate.instant('Messages.OK'))
              .cancel($translate.instant('Messages.DISCARD'));

        $mdDialog.show(confirm).then(
        	function(res) {
        		CameraService.attachCamera();
        		if (res == true)
                	successCallBack();
                else {
                	 $scope.resume();
                	}
            	}, 
            function() {
            	CameraService.attachCamera();
            });
    }

    $scope.getStepProgessPercentage = function (step) {
    	var progress = 0;
    	if (step.progress != null)
    		progress = step.progress;
    	var duration = 1;
		  if (step.duration != null)
    		duration = $scope.getStepCountDown(step);
    	return (progress * 100) / duration;
    }

    $scope.getStepDescStyle = function (step) {
    	var currentStep = $scope.getCurrentStep();
    	if (step == currentStep) {
    		return {color: '#FF0000', "font-size":"20px"};
    	}
    	else if (step.id < currentStep.id)
    		return {color: '#00AA00', "font-size":"20px"};
    	else 
    		return {color: '#1F497D', "font-size":"20px"};
    }

   $scope.getStepIconStyle = function (step) {
   		var currentStep = $scope.getCurrentStep();
    	if (step == currentStep) {
    		return {'font-size':'24px', 'color': '#FF0000'};
    	}
    	else if (step.id < currentStep.id)
    		return {'font-size':'24px', 'color': '#00AA00'};	
    	else 
    		return {'font-size':'24px', 'color': '#1F497D'};
    }



    $scope.isStepDoing = function(step) {
    	var currentStep = $scope.getCurrentStep();
    	return step == currentStep || step.id == currentStep.id;
    }

     $scope.isStepDone = function(step) {
    	var currentStep = $scope.getCurrentStep();
    	return step.id < currentStep.id;
    }

	$scope.isStepToDo = function(step) {
    	var currentStep = $scope.getCurrentStep();
    	return step.id > currentStep.id;    	
    }


    $scope.isMirinoEnabled = function() {
    	return $scope.mirinoEnabled;
    }

    $scope.setMirinoEnabled = function(val) {
		$timeout(function() {	//sostituzione $apply
			try {
				window.localStorage.setItem('mirino', val);    	
				$scope.mirinoEnabled = val;
				if (val)
					document.getElementById('canvasDisegno').style.zIndex=10;
				else    		
					document.getElementById('canvasDisegno').style.zIndex=-1;
				//$scope.apply();
				}
			catch (ex) {
				console.log(ex);
			}
		});
    }

    $scope.getToastInfoMessage = function() {
    	var result = '';
    	var treatmentData = ReportService.getCurrentReportData();

    	//Protocollo
    	result += $translate.instant('General.protocol');
    	result += ": "+treatmentData.protocol;
    	result += "    ";

    	//Intensità
    	result += $translate.instant('Megaride.intensity');
    	//result += ": "+(treatmentData.power/1000);
    	result += ": "+(treatmentData.power);
    	result += "mW/cm^2    ";

    	//Durata
    	result += $translate.instant('Megaride.time');
    	result += ": "+(treatmentData.duration)+" min";
    	result += "    ";
    	//ReportService.getDuration() +' min'

    	//if (treatmentData.protocol == 'Megaride') {
		if (treatmentData.spotDiameter) {
	    	result += $translate.instant('Megaride.spotDiam');
	    	result += ": "+(treatmentData.spotDiameter);
	    	result += " mm    ";
    	}

    	//Diametro

    	return result;
    }

    $scope.getToastDiameterMessage = function () {
		if ($stateParams.spotDiameter != null)
			return $translate.instant('Megaride.remindDiameter')+" ("+$stateParams.spotDiameter+" mm)";
		return "";
    }

    $scope.showTreatmentInfo = function () {
    	 $mdToast.show(
		      $mdToast.simple()
		        .textContent($scope.getToastInfoMessage())
		        .position('top left')
		        .hideDelay(3000)
		        .capsule(true)
		    );
    }

    $scope.remindDiameter = function() {
    	if ($scope.protocol != 'Megaride')
    		return;
    	$mdToast.show(
		      $mdToast.simple()
		        .textContent($scope.getToastDiameterMessage())
		        .position('bottom right')
		        .hideDelay(5000)
		        .capsule(true)
		    );
    }
   

    $scope.reloadCamera = function() {
    	CameraService.resetCamera();
    	return;
    }

    $scope.resizeCanvas = function () {
    	if ($scope.overlayCanvas == null)
    		return;
    	var element = CameraService.getVideoElement();
    	if (element == null)
    		return;
    	var w = element.offsetWidth;
	 	var h = element.offsetHeight;
	 	var rect = element.getBoundingClientRect();	  	
	  	$scope.overlayCanvas.width = w;
	  	$scope.overlayCanvas.height =h;
	  	$scope.overlayCanvas.style.top = rect.top+"px";
  		$scope.overlayCanvas.style.left = rect.left+"px";
  		$scope.overlayCanvas.style.position = "absolute";
  		$scope.overlayCanvas.style.zIndex=10;
  		//$scope.apply();
    }


    $scope.initCanvasOverlay = function() {
    	if ($scope.overlayCanvas == null)
    		$scope.overlayCanvas = document.getElementById('canvasDisegno');
    	if ($scope.overlayCanvas == null)
    		return;
    	var videoEl = CameraService.getVideoElement();
    	if ($scope.designContext == null)
  			$scope.designContext = $scope.overlayCanvas.getContext('2d');
  		$scope.resizeCanvas();
    }

    $scope.drawCanvas = function() {
    	
    	//$scope.designContext.strokeStyle = '#1F497D';
    	$scope.designContext.strokeStyle = '#FFFFFF';
    	$scope.designContext.lineWidth=10;
    	
    	var fractDimensionLine = 3;		//Denominatore della frazione per cui dividere la linea; 3 significa che la linea occupa 1/3 dello schermo...
    	
    	//Linea sinistra 
    	$scope.designContext.beginPath();		
		$scope.designContext.moveTo(0 ,$scope.overlayCanvas.clientHeight/2);
		$scope.designContext.lineTo($scope.overlayCanvas.clientWidth/fractDimensionLine, $scope.overlayCanvas.clientHeight/2);
		$scope.designContext.stroke();
    	
    	//Linea destra 
    	$scope.designContext.beginPath();		
		$scope.designContext.moveTo($scope.overlayCanvas.clientWidth-$scope.overlayCanvas.clientWidth/fractDimensionLine -1, $scope.overlayCanvas.clientHeight/2);
		$scope.designContext.lineTo($scope.overlayCanvas.clientWidth -1, $scope.overlayCanvas.clientHeight/2);
		$scope.designContext.stroke();

		//Linea superiore
    	$scope.designContext.beginPath();		
		$scope.designContext.moveTo($scope.overlayCanvas.clientWidth/2, 0);
		$scope.designContext.lineTo($scope.overlayCanvas.clientWidth/2, $scope.overlayCanvas.clientHeight/fractDimensionLine);
		$scope.designContext.stroke();

		//Linea inferiore
    	$scope.designContext.beginPath();		
		$scope.designContext.moveTo($scope.overlayCanvas.clientWidth/2, $scope.overlayCanvas.clientHeight -1 );
		$scope.designContext.lineTo($scope.overlayCanvas.clientWidth/2, $scope.overlayCanvas.clientHeight - 
								$scope.overlayCanvas.clientHeight/fractDimensionLine);
		$scope.designContext.stroke();

		//Cerchio centrale per pupilla
		$scope.designContext.beginPath();		
		//$scope.designContext.moveTo($scope.overlayCanvas.clientWidth/2, $scope.overlayCanvas.clientHeight/2);
		$scope.designContext.arc($scope.overlayCanvas.clientWidth/2,$scope.overlayCanvas.clientHeight/2, 20, 0, 2*Math.PI);
		$scope.designContext.stroke();

		//Ellisse per iride
		/*$scope.designContext.beginPath();		
		//$scope.designContext.moveTo($scope.overlayCanvas.clientWidth/2, $scope.overlayCanvas.clientHeight/2);
		$scope.designContext.ellipse($scope.overlayCanvas.clientWidth/2,$scope.overlayCanvas.clientHeight/2, $scope.overlayCanvas.clientWidth/fractDimensionLine, 
						$scope.overlayCanvas.clientHeight/fractDimensionLine, 0, 0, 2*Math.PI);
		$scope.designContext.stroke();*/

    	/*$scope.designContext.beginPath();		
		$scope.designContext.moveTo($scope.overlayCanvas.clientWidth/2 ,$scope.overlayCanvas.clientHeight/2);
		$scope.designContext.lineTo($scope.overlayCanvas.clientWidth, $scope.overlayCanvas.clientHeight);
		$scope.designContext.stroke();*/
    }

    $scope.exeCamera = function() {
    	return exe4Camera;
    }

    $scope.checkConnection = function () {

		try {
			isOnline().then(online => {
					$scope.statusConnection = online;
				});
			}
		catch (ex) {
				console.log('CheckingOnline. Exception');
				console.log(ex);
			}        

    }

    $scope.openNetworkConfiguration = function () {
        if (SystemCalls == null)
            return;
        SystemCalls.openNetworkConfiguration();
     }

     $scope.openRemoteAssistance = function() {
        if (SystemCalls == null)
            return;
        SystemCalls.openRemoteAssistance();  
     }

     $scope.initTimer = function () {
     	$timeout(
     		function () {
		     	$scope.$broadcast('timer-set-countdown-seconds', $scope.getImbibitionDuration());
			  }, 100);
	 }
	 
	 $scope.cacheTreatmentStatus = function() {
		 return;
		$timeout(function (){
			 TreatmentService.cacheTreatmentStatus($scope.tableSteps, $scope.getCurrentStep(), $scope.currentStep, $scope.timer);
			}, 100);
	 }

	 $scope.padTo2Digits = function (num) {
		return num.toString().padStart(2, '0');
	  }

	 $scope.calcTimerString = function () {
		var IntSeconds = Math.floor($scope.timer.countdown / 1000);
  		var IntMinutes = Math.floor(IntSeconds / 60);
  		var IntHours = Math.floor(IntMinutes / 60);

		IntSeconds = IntSeconds % 60;
		IntMinutes = IntMinutes % 60;
		IntHours = IntHours % 24;

		$scope.timer.timerString = "";
		if (IntHours > 0)
			$scope.timer.timerString = $scope.padTo2Digits(IntHours)+":";
		$scope.timer.timerString += $scope.padTo2Digits(IntMinutes)+":"+$scope.padTo2Digits(IntSeconds);

	 }

     $scope.timerTick = function (millis)  {
			 $scope.timer.countdown = millis;
			 $scope.cacheTreatmentStatus();	
			 $scope.allineaTimer();
			 $scope.calcTimerString();		 
			 /*var step = $scope.getCurrentStep();
			 if (millis <1010 && step.type && (step.type == STEP_TYPE.IRRADIATION) && $scope.opticalUnitState == OPTICAL_UNIT_STATE.RUNNING ) {					
					if ($scope.currentStep == $scope.tableSteps.length-2 && 
							$scope.tableSteps[$scope.currentStep+1].type == STEP_TYPE.END_TREATMENT) {
								LoggingService.info("TreatmentController.  FORZO IL JUMP FINE TRATTAMENTO");
								$scope.completeTimer();
								$scope.nextStep();
							}
			 }*/
	 }
	 
	 $scope.initParamsCamera = function () {
		if (!$rootScope.configCamera || SystemCalls == null)
			return;
		SystemCalls.getBrightness(
			function (brightness) {
				 try {
					$scope.paramsCamera.brightness = Number(brightness);
				     }
				 catch(err) {
					}
				},
			function (error) {
				 console.log("TreatmentController. GetBrightness. Got error" + error);
				} 
			
		);
		SystemCalls.getSaturation(
			function (saturation) {
				 try {
					$scope.paramsCamera.saturation = Number(saturation);
				     }
				 catch(err) {
					}
				},
			function (error) {
				 console.log("TreatmentController. GetSaturation. Got error" + error);
				} 
			
		);
		SystemCalls.getContrast(
			function (contrast) {
				 try {
					$scope.paramsCamera.contrast = Number(contrast);
				     }
				 catch(err) {
					}
				},
			function (error) {
				 console.log("TreatmentController. getContrast. Got error" + error);
				} 
			
		);
		SystemCalls.getSharpness(
			function (sharpness) {
				 try {
					$scope.paramsCamera.sharpness = Number(sharpness);
				     }
				 catch(err) {
					}
				},
			function (error) {
				 console.log("TreatmentController. getSharpness. Got error" + error);
				} 
			
		);
	 }

	$scope.setBrightness = function () {
		 //SystemCalls.setBrightness($scope.paramsCamera.brightness);
		 CameraService.setParams($scope.paramsCamera);
	}

	$scope.setSaturation = function () {
		 //SystemCalls.setSaturation($scope.paramsCamera.saturation);
		 CameraService.setParams($scope.paramsCamera);
	}

	$scope.setContrast = function () {
		 //SystemCalls.setContrast($scope.paramsCamera.contrast);
		 CameraService.setParams($scope.paramsCamera);
	}

	$scope.setSharpness = function () {
		 //SystemCalls.setSharpness($scope.paramsCamera.sharpness);
		 CameraService.setParams($scope.paramsCamera);
	}

	$scope.processPulsedStepMessage = function  (msg) {
		var pos1Parentesis = msg.indexOf("(");
		var pos2Parentesis = msg.indexOf(")");
		var posVirgola = msg.indexOf(",");
		var currStep = msg.substring(pos1Parentesis+1, posVirgola);
		var totSteps = msg.substring(posVirgola+1, pos2Parentesis);
		var stepsToEnd = Number (totSteps) - Number(currStep);
		//console.log("Ho capito di aver terminato lo step "+currStep+" di "+totSteps+" So brav eh?");

		var step = $scope.getCurrentStep();
		var stepPeriod = step.irradiationTime + step.pauseTime;

		var actualDate = new Date();
		var secsToEnd = stepPeriod * stepsToEnd;
		step.progress = Number(currStep) * stepPeriod;
		//$scope.timer.supposedEndTime = actualDate.getTime() + msToEnd;
		$scope.timer.countdown = Number(secsToEnd)*1000;
		$scope.$broadcast('timer-set-countdown-seconds', Number(secsToEnd));
		

		//var totalDuration = stepPeriod * Number (totSteps);
		//var countdown = totalDuration - (stepPeriod * Number(currStep));

		//fine attesa adesso + stepPeriod * Number (totSteps) - Number(currStep);
		
		//$scope.$broadcast('timer-set-countdown-seconds', countdown-1);
		//$scope.$broadcast('timer-start');
	//	console.log("Durata irradiazione: "+step.irradiationTime+"; Pausa: "+step.pauseTime+". Il timer va messo a "+countdown);
	/*	if (Number(currStep) == Number(totSteps)) {
			$scope.completeTimer();
			$scope.nextStep();
		}*/


		//step.irradiationTime, step.pauseTime, step.duration, step.power;
	}

	$scope.endStep = function () {
		$scope.opticalUnitState = OPTICAL_UNIT_STATE.STEP_COMPLETED;
		var step = $scope.getCurrentStep();
		/*if (step != null && step.isPulsed)
			 return;*/
		$scope.$broadcast('timer-set-countdown-seconds', 0);
		$scope.$broadcast('timer-stop');
		$scope.completeTimer();
	}

	


     $scope.initListeners = function () {
    	 $scope.$on('stepCompleted', function (event, message) {
				//$scope.opticalUnitState = OPTICAL_UNIT_STATE.STEP_COMPLETED;
				  if ($scope.getCurrentStep() && $scope.getCurrentStep().isPulsed) {
						$scope.processPulsedStepMessage(message);
					}
    	 		/*else if ($scope.timer.active && $scope.timer.timestamp > 0)
					 //$scope.completeTimer();
					 $scope.endStep();*/
			  });
			  
    	 $scope.$on('hardwareStatus', function (event, message) {
				$timeout(function() {	//sostituzione $apply
    	 			$scope.headReset = true;
					//$scope.apply();
				});
              });
		 $scope.$on('stopTreatment', function (event, message) {
			 //$scope.timer.active = false;
			$scope.opticalUnitState = OPTICAL_UNIT_STATE.IDLE; 
    	 	$scope.stopTimer();
    	 	LoggingService.info("TreatmentController.  Step Completato");
    	 });
    	 $scope.$on('stepRunning', function (event, message) { 
			 if ($scope.opticalUnitState == OPTICAL_UNIT_STATE.RUNNING ) {
				 console.log("Treatment Controller.  Scarto lo step Running perché già processato");
				 return;
			 	}
			 if ($scope.getCurrentStep() == null) {
				 console.log("Treatment Controller.  Rilevata anomalia.  Ho ricevuto step running dalla testa ma il current step ("+$scope.currentStep+") pare nullo ");
				 console.log("La tabella degli step è "+JSON.stringify($scope.tableSteps));				
				 return;
			 }
			$scope.opticalUnitState = OPTICAL_UNIT_STATE.RUNNING; 
			 //$scope.resumeTimerOldCalls();
			 $scope.timer.countdown = $scope.getStepCountDown($scope.getCurrentStep());
			 $scope.$broadcast('timer-set-countdown-seconds', $scope.timer.countdown);			 
			 $scope.timer.startingDate = new Date();
			 
			 var stepCountDown = $scope.timer.countdown*1000;
			 $scope.timer.supposedEndTime = $scope.timer.startingDate.getTime() + stepCountDown;

			 TreatmentService.pausePolling();
			  
			 $scope.$broadcast('timer-start');
			 });

		  $scope.$on('processPaused', function(event, message) {
			$scope.opticalUnitState = OPTICAL_UNIT_STATE.PAUSE;
		 });
			 
		 $scope.$on('processCompleted', function (event, message) { 
				LoggingService.info("TreatmentController.  Reagisco all'evento process completed");
				/*if ($scope.opticalUnitState ==  OPTICAL_UNIT_STATE.STEP_COMPLETED) {
					LoggingService.info("L'evento è stato già processato.  Scarto la notifica");
					return;
				}*/
				if ($scope.opticalUnitState !=  OPTICAL_UNIT_STATE.RUNNING) {
					LoggingService.info("Controller Treatment.  La testa non era in running state.  Scarto la notifica");
					return;
				}
				$scope.endStep();
				$scope.nextStep();
				TreatmentService.resumePolling();
			});


    	 $scope.$on('timer-tick', function  (event, details){
    	 	if (!$scope.treatmentHistory.visible)
			 	return;
			 $scope.timerTick(details.millis);
    	 });

		 $scope.$on('timer-set-countdown-seconds', function (event, seconds) {
			console.log('Ricevuto evento set countdown');
			$scope.timer.countdown = seconds * 1000;
			$scope.calcTimerString();
		 });
    }

	  $scope.init = function () {
	  	$scope.initTableSteps();
	  	$scope.initReport();
	  	//$scope.updateTimerDisplay();
	  	$interval(function() {$scope.timer.numClick = 0}, 3000);
	  	$interval($scope.checkConnection, 3000);
	  	$scope.initListeners();
		$scope.checkConnection();
		$scope.initParamsCamera();
		$scope.timer.jumpEnabled = DebugModeService.isJumpStepActive();
//	  	HwCommunicationService.sendRebootNP();
$scope.headReset = true;

	  	//video = document.querySelector("#videoElement");

	  	ShutdownService.setOperation(ShutdownService.OPERATIONS.SELECT_PROTOCOL);
	  	CameraService.attachCamera();
	  	$scope.initCanvasOverlay();
	  	if (window.localStorage.getItem('mirino') == null)
    		window.localStorage.setItem('mirino', true);
    	$scope.setMirinoEnabled(localStorage.getItem('mirino') == 'true' ? true : false);

	  	$scope.initTimer();
	  	$scope.drawCanvas();
	  	
		}

		HwCommunicationService.setSerialStatusListener($scope.seriesStatusListener);
		 
		
		$scope.init();
		
	}
);
   
    	
