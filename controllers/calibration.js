
angular.module('megaride')    
.controller('CtrlCalibration', 
    function ($scope, $stateParams, $rootScope, $state, $mdDialog, $timeout, CalibrationService, HwCommunicationService, hotkeys, 
                CameraService, usePromises) {

    	$scope.state = {};
    	$scope.state.numSteps = 3;		//Numero di step richiesti per completare la calibrazione
    	$scope.state.step = 1;
        $scope.state.calibration = 0;
        $scope.state.insertEnable = 0;
        $scope.calibrationTable = [];

        $scope.calibrationcurrent = '';

        var CALIBRATION_STATES = {
            "IDLE": 0,                      //In attesa
            "FOCUS_LED": 1,                 //Accendo i led per determinare il piano di fuoco
            "RESET_CALIBRATION": 2,         //Avvio la procedura
            "RESET_CALIBRATION_DONE": 3,    //Comando clearcalib ricevuto dalla scheda
            "SET_CURRENT": 4,               //In questo momento l'utente inserisce la corrente da inviare al dispositivo
            "LED_ENABLE" : 5,               //Accendo il led
            "SET_POWER": 6,                 //Dico al device la potenza letta sull'uv-meter
            "STORE": 7,                     //Memorizzo la tabella di calibrazione
            "AUTOPOWER": 8                  //Stato per generazione automatica valori di calibrazione
        }
    	
    	//0: Avvio procedura; 
    	//1: posizionare uv; 
    	//2: Inserimento valore;	
    	//3: Calibrazione completata
    	$scope.state.status = 0;
        $scope.state.idxCalibration = -1;

        $scope.proposedCalibration = [];
        $scope.requiredPowers = [];

        //Passaggio allo stato successivo
        $scope.goNext = function () {
            //$scope.gotoProtocolSelection();
            //$scope.gotoAdministration();
            $scope.gotoSplash();
        }

        //Va alla selezione del protocollo
        $scope.gotoProtocolSelection = function () {
            $state.go('selectProtocol');
        }

        $scope.gotoAdministration = function () {
             $state.go('administration',  {userProfile:"super"});
        }

        $scope.gotoSplash = function () {
            $state.go('splash');
        }

        $scope.back = function() {
            var callBack = function (result) {
                $state.go('administration', {
                     userProfile: $rootScope.userProfile}
                    );
            }
            if ($scope.state.calibration == CALIBRATION_STATES.IDLE)
                callBack();
            else if ($scope.state.calibration == CALIBRATION_STATES.FOCUS_LED)
                CalibrationService.setFocus(0).then(callBack);
            else 
                CalibrationService.sendCalibrationDone().then(callBack);
            
        }

        //Funzione generica che avvia la procedura di verifica o di calibrazione
        $scope.startCalibration = function() {
           CalibrationService.startCalibration();
           HwCommunicationService.pausePolling();
           $scope.state.calibration = CALIBRATION_STATES.RESET_CALIBRATION;
        } 
       
    	$scope.valueUpdated = function() {
    		//CalibrationService.updateVal($scope.state.valUvMeter);
            $scope.setCalibPower($scope.state.valUvMeter);
    	}

        //Questa funzione viene chiamata quando il dispositivo conferma di aver ricevuto la calibrazione
        $scope.updateDone = function () {
            $timeout(function() {	//sostituzione $apply
                $scope.calibrationTable.push({"current": $scope.calibrationcurrent, 
                                                "power": $scope.state.valUvMeter});
                $scope.calibrationcurrent = '';
                $scope.state.valUvMeter = '';
                $scope.state.calibration = CALIBRATION_STATES.SET_CURRENT;
                //$scope.apply();           
            });
        }

        //Accende Led in calibrazione
        $scope.setCurrentLed = function () {

            if (usePromises) {
                CalibrationService.setCurrentLed($scope.calibrationcurrent).then(function() {
                        $scope.state.calibration = CALIBRATION_STATES.SET_POWER;
                        $scope.apply();
                    });
                }
            else {
                CalibrationService.setCurrentLed($scope.calibrationcurrent);
                $scope.state.calibration = CALIBRATION_STATES.SET_POWER;
                }
        }

        //Comunica al dispositivo la potenza letta dal power meter
        $scope.setCalibPower = function (val) {
            $timeout(function() {	//sostituzione $apply
                $scope.state.insertEnable = 0;
                if (usePromises) {
                    CalibrationService.setCalibPower(val).then(function () {
                            $scope.updateDone();
                            $scope.calibrationcurrent = $scope.getNextCurrentValue();  
                            //$scope.apply();
                        });
                    }
                else {
                    CalibrationService.setCalibPower(val);
                    $scope.updateDone();
                    $scope.calibrationcurrent = $scope.getNextCurrentValue();  
                    //$scope.apply();
                }
            });
        }

        $scope.apply = function() {
            if ($scope.$$phase) 
                return;
            try {
                $scope.$apply();
            }
            catch (err) {

            }
        }


        $scope.okButton = function () {
            switch($scope.state.calibration) {
                case CALIBRATION_STATES.IDLE:
                            $scope.calibrationTable = [];
                            //$scope.apply();
                            if (usePromises) {
                                CalibrationService.startCalibration().then(function () {
                                            $scope.state.calibration = CALIBRATION_STATES.FOCUS_LED;
                                        });
                                }
                            else {
                                    CalibrationService.startCalibration();
                                    $scope.state.calibration = CALIBRATION_STATES.FOCUS_LED;
                                }
                            break;
                case CALIBRATION_STATES.FOCUS_LED:
                            CalibrationService.setFocus(0).then(function() {
                                    $scope.state.calibration = CALIBRATION_STATES.AUTOPOWER;
                                    $scope.nextCalibrationStep();
                                });
                            /*if (usePromises) {
                                    CalibrationService.setFocus(0).then(function() {
                                        $scope.state.calibration = CALIBRATION_STATES.SET_CURRENT;
                                        $scope.calibrationcurrent = $scope.getNextCurrentValue();
                                    });
                                }
                            else {
                                CalibrationService.setFocus(0);
                                $scope.state.calibration = CALIBRATION_STATES.SET_CURRENT;
                                $scope.calibrationcurrent = $scope.getNextCurrentValue();
                            }*/
                            CameraService.detachVideo();
                            break;
                case CALIBRATION_STATES.SET_CURRENT:
                            $scope.setCurrentLed();
                            //$scope.state.calibration = CALIBRATION_STATES.SET_POWER;
                            break;
                case CALIBRATION_STATES.SET_POWER:
                            $scope.setCalibPower($scope.state.valUvMeter);
                            break; 
                case CALIBRATION_STATES.AUTOPOWER:
                            //$scope.setCalibPower($scope.state.valUvMeter);
                            //$scope.nextCalibrationStep();
                            $scope.applyCalibPower();
                            break;                            
                default: break; 
            }
        }

        $scope.stopButton = function () {
            if (usePromises) {
                CalibrationService.sendCalibrationDone().then(function (){
                        $scope.goNext();
                    });
                }
            else {
                CalibrationService.sendCalibrationDone();
                $scope.goNext();
            }
        }


        $scope.isStopButtonVisible = function() {
            return  $scope.state.calibration == CALIBRATION_STATES.SET_CURRENT || 
                    $scope.state.calibration == CALIBRATION_STATES.LED_ENABLE || 
                    $scope.state.calibration == CALIBRATION_STATES.SET_POWER ||
                    $scope.state.calibration == CALIBRATION_STATES.AUTOPOWER;
        }

        //Modalità vecchia.  Restituisce il prossimo valore di corrente 
        $scope.getNextCurrentValue = function () {
            $scope.state.idxCalibration++;
            if ($scope.state.idxCalibration > $scope.proposedCalibration.length)
                return '';
            return $scope.proposedCalibration[$scope.state.idxCalibration];
        }

        $scope.initProposedCalibration = function () {
            $scope.proposedCalibration = [];
            $scope.proposedCalibration[0] = 10;
            $scope.proposedCalibration[1] = 20;
            $scope.proposedCalibration[2] = 30;
            $scope.proposedCalibration[3] = 40;
            $scope.proposedCalibration[4] = 50;
            $scope.proposedCalibration[5] = 100;
            $scope.proposedCalibration[6] = 150;
            $scope.proposedCalibration[7] = 200;
            $scope.proposedCalibration[8] = 250;
            $scope.proposedCalibration[9] = 300;
            $scope.proposedCalibration[10] = 350;
        }


        $scope.loadCalibrationTable = function() {
            CalibrationService.getCalibrationTable().then(function(rows) {
                 for (var i=0; i<rows.length; i++)
                    $scope.calibrationTable.push(rows[i]);
                 // $scope.apply();
                });
        }

        $scope.initRequiredPowers = function() {
            $scope.requiredPowers = [];
            /*$scope.requiredPowers[0] = 1.2;
            $scope.requiredPowers[1] = 1.8;
            $scope.requiredPowers[2] = 2;
            $scope.requiredPowers[3] = 2.4;
            $scope.requiredPowers[4] = 2.7;
            $scope.requiredPowers[5] = 3.0;
            $scope.requiredPowers[6] = 3.2;
            $scope.requiredPowers[7] = 3.5;
            $scope.requiredPowers[8] = 4.8;
            $scope.requiredPowers[9] = 5.2;
            $scope.requiredPowers[10] = 8.8;
            $scope.requiredPowers[11] = 9.2;
            $scope.requiredPowers[12] = 11.8;
            $scope.requiredPowers[13] = 12.2;
            $scope.requiredPowers[14] = 14.8;
            $scope.requiredPowers[15] = 15.2;
            $scope.requiredPowers[16] = 17.8;
            $scope.requiredPowers[17] = 18.2;
            $scope.requiredPowers[18] = 25;
            $scope.requiredPowers[19] = 28;
            $scope.requiredPowers[20] = 29.2;
            $scope.requiredPowers[21] = 29.4;
            $scope.requiredPowers[22] = 29.6;
            $scope.requiredPowers[23] = 29.999;*/

            $scope.requiredPowers[0] = 1.9;
            $scope.requiredPowers[1] = 2.4;
            $scope.requiredPowers[2] = 3.65;
            $scope.requiredPowers[3] = 5;
            $scope.requiredPowers[4] = 8;
            $scope.requiredPowers[5] = 12;
            $scope.requiredPowers[6] = 18;
            $scope.requiredPowers[7] = 18;
            $scope.requiredPowers[8] = 22;
            $scope.requiredPowers[9] = 29;
            $scope.requiredPowers[10] = 29.2;
            $scope.requiredPowers[11] = 29.4;
            $scope.requiredPowers[12] = 29.6;
	        $scope.requiredPowers[13] = 30.2;
        }

        $scope.roundTo = function(value, decimalpositions)  {
            var i = value * Math.pow(10,decimalpositions);
            i = Math.round(i);
            return i / Math.pow(10,decimalpositions);
           } 

        $scope.getNextAutoCurrent = function () {
            /*if ($scope.calibrationTable.length == 0)
                return 2;
            else if ($scope.calibrationTable.length == 1)
                return 4;
            else {
                var idxPower = $scope.calibrationTable.length -2;
                //Condizione di uscita
                if (idxPower >= $scope.requiredPowers.length)
                    return -1;
                var power = $scope.requiredPowers[idxPower];
                var p0 = $scope.calibrationTable[$scope.calibrationTable.length-2].power;
                var p1 = $scope.calibrationTable[$scope.calibrationTable.length-1].power;
                var c0 = $scope.calibrationTable[$scope.calibrationTable.length-2].current;
                var c1 = $scope.calibrationTable[$scope.calibrationTable.length-1].current;
                return $scope.roundTo((power - p0)* (c1-c0) / (p1-p0) + c0, 0);
            }*/
            if ($scope.calibrationTable.length == 0)
                return 5;
            else if ($scope.calibrationTable.length == 1)
                return 10;
            else if ($scope.calibrationTable.length == 2)
                return 15;
            else {
                  var lastPower = $scope.calibrationTable[$scope.calibrationTable.length-1].power;
                  var lastCurrent = $scope.calibrationTable[$scope.calibrationTable.length-1].current;
                  if (lastPower > 30.1)
                    return -1;
                  var nextPower = -1;
                  var current = 0;
                  //Cerco il prossimo valore di potenza
                  for (var i=0; i<$scope.requiredPowers.length && nextPower == -1; i++) {
                      if ($scope.requiredPowers[i] > 3.5 && $scope.requiredPowers[i] < 25){
                        if ($scope.requiredPowers[i] > lastPower * 1.2) 
                            nextPower = $scope.requiredPowers[i];
                        }
                      else if ($scope.requiredPowers[i] > lastPower)
                        nextPower = $scope.requiredPowers[i];                      
                    }
                  if (nextPower == -1)
                    return -1;
                  var p0 = $scope.calibrationTable[$scope.calibrationTable.length-2].power;
                  var p1 = $scope.calibrationTable[$scope.calibrationTable.length-1].power;
                  var c0 = $scope.calibrationTable[$scope.calibrationTable.length-2].current;
                  var c1 = $scope.calibrationTable[$scope.calibrationTable.length-1].current;
                  current = $scope.roundTo((nextPower - p0)* (c1-c0) / (p1-p0) + c0, 0);
                  if (current <= (lastCurrent+2))
                    current = lastCurrent +3;
                  return current;
                }
        }

		$scope.goToHome = function() {
			 $state.go('splash');
		    }

        $scope.applyCalibPower = function () {
            var val = $scope.state.valUvMeter;
            if (val == null || val == '')
                return;
            CalibrationService.setCalibPower(val).then(function () {
                /*$scope.calibrationTable.push({  "current": $scope.calibrationcurrent, 
                                                "power": $scope.state.valUvMeter});*/
                $scope.calibrationTable[$scope.calibrationTable.length-1].power = $scope.state.valUvMeter;
                $scope.calibrationcurrent = '';
                $scope.state.valUvMeter = '';
                $scope.nextCalibrationStep();
            });
        }

        $scope.nextCalibrationStep = function() {
            //
            $scope.calibrationcurrent = $scope.getNextAutoCurrent();
            if ($scope.calibrationcurrent == -1 || $scope.calibrationcurrent > 400) {
                CalibrationService.sendCalibrationDone();
                $scope.goNext();
                HwCommunicationService.resumePolling();
                return;
            }
            $scope.calibrationTable.push({"current": $scope.calibrationcurrent});
            CalibrationService.setCurrentLed($scope.calibrationcurrent);
        }
        
        $scope.init = function () {
            //INIZIALIZZAZIONE VARIABILI
            $scope.state.step = 1;
            $scope.state.numSteps = $scope.calibrationTable.length;
            $scope.initProposedCalibration();
            $scope.initRequiredPowers();
            $scope.loadCalibrationTable();
            CameraService.attachCamera();
            $scope.state.calibration = CALIBRATION_STATES.IDLE;

        }

        $scope.init();
	}
);
   
    	
