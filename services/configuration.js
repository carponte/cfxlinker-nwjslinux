'use strict';

//const {google} = require('googleapis');

try {
	const {google} = require('googleapis');
 }
catch (ex) {

}

angular.module('megaride')
.service('ConfigurationService', function($translate, $http){
            	
        var ConfigurationService = {}

        var config = {};
		var inited = false;

		ConfigurationService.credentials = {"installed":{"client_id":"23387111246-s4iicqei0lprvnhr5uop4sndui4u2apb.apps.googleusercontent.com","project_id":"quickstart-1570608230436","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://oauth2.googleapis.com/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"3-M6bTo4ZeY8sSqOXVg9xJJ4","redirect_uris":["urn:ietf:wg:oauth:2.0:oob","http://localhost"]}};
		ConfigurationService.token = {"access_token":"ya29.Il-bBzKMUZ4o8S0onIZyY5rVwy01SLfvG6zBdlRnZ8WoomqY2wdqiqZAgvq99V1viWakPp21UuPC3AEY817YeVEmhvc3MxDbboU8V2mIRtOqEC1ZQCKE0aYTpHlDrgHFoA","refresh_token":"1//03M78lZpHPIBiCgYIARAAGAMSNwF-L9Ir9EGgWmY1eZP9BcnWO4FVlIKaEZiMd5FOgmZT04JBwtepNubchUS65u--wCm1il2EjBU","scope":"https://www.googleapis.com/auth/spreadsheets","token_type":"Bearer","expiry_date":1571131536139};
		
        

        ConfigurationService.init = function() {
        	if (window.localStorage.getItem("configuration") == null)
        		return;
        	try {
        		config = angular.fromJson(window.localStorage.getItem("configuration"));
        		inited = true;
        	}
        	catch(err) {

        	}
        }

        ConfigurationService.setLocale = function(locale) {
        	config.locale = locale;
        	ConfigurationService.save();
        	$translate.use(locale);
        }

        ConfigurationService.getLocale = function() {
        	return config.locale;
        }

        ConfigurationService.getSuggestedLanguage = function() {
        	 var language = navigator.language.split("-");
      		 var language_root = (language[0]);
      		 return language_root;
        }

        ConfigurationService.getTranslationLanguage= function() {
        	if (ConfigurationService.getLocale() != null)
        		return ConfigurationService.getLocale();
        	return ConfigurationService.getSuggestedLanguage();
        }
		
		ConfigurationService.save = function() {
        	window.localStorage.setItem("configuration", angular.toJson(config));
		}
	 
		ConfigurationService.storeUpdateInCloud = function (serialNumber, commit, callBack) {
		  try {
			const {client_secret, client_id, redirect_uris} = ConfigurationService.credentials.installed;
				const {google} = require('googleapis');
				const oAuth2Client = new google.auth.OAuth2(
					client_id, client_secret, redirect_uris[0]);
				oAuth2Client.setCredentials(ConfigurationService.token);
			
				const sheets = google.sheets({version: 'v4', oAuth2Client});
				sheets.spreadsheets.values.append({
				spreadsheetId: "1MD2CJ5bcYrYqiM4WpLvoGMypmQHWSqLejnob9X-xD9A",
				range: 'Aggiornamenti',
				valueInputOption: 'RAW',
				insertDataOption: 'INSERT_ROWS',
				resource: {
					values: [
					[new Date().toISOString(), serialNumber, commit]
					],
				},
				auth: oAuth2Client
				}, (err, response) => {
				/*if (err) {
					alert("Errori durante esportazione tabella")
					return console.error(err);
					}
				else {
					alert("Tabella esportata");
					}*/
				callBack();
				});
			}
	
			catch (ex) {
					alert("Errore durante esportazione.  Verificare la connessione ad internet e che le google APIs siano state installate")
				}
		}
		


		ConfigurationService.saveCalibTableInCloud = function (driverSerialNumber, calibTable) { 
			try {
				/*var credentials = {"installed":{"client_id":"23387111246-s4iicqei0lprvnhr5uop4sndui4u2apb.apps.googleusercontent.com","project_id":"quickstart-1570608230436","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://oauth2.googleapis.com/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"3-M6bTo4ZeY8sSqOXVg9xJJ4","redirect_uris":["urn:ietf:wg:oauth:2.0:oob","http://localhost"]}};
				var token = {"access_token":"ya29.Il-bBzKMUZ4o8S0onIZyY5rVwy01SLfvG6zBdlRnZ8WoomqY2wdqiqZAgvq99V1viWakPp21UuPC3AEY817YeVEmhvc3MxDbboU8V2mIRtOqEC1ZQCKE0aYTpHlDrgHFoA","refresh_token":"1//03M78lZpHPIBiCgYIARAAGAMSNwF-L9Ir9EGgWmY1eZP9BcnWO4FVlIKaEZiMd5FOgmZT04JBwtepNubchUS65u--wCm1il2EjBU","scope":"https://www.googleapis.com/auth/spreadsheets","token_type":"Bearer","expiry_date":1571131536139};
				*/
			
				const {client_secret, client_id, redirect_uris} = ConfigurationService.credentials.installed;
				const {google} = require('googleapis');
				const oAuth2Client = new google.auth.OAuth2(
					client_id, client_secret, redirect_uris[0]);
				oAuth2Client.setCredentials(ConfigurationService.token);
			
				const sheets = google.sheets({version: 'v4', oAuth2Client});
				sheets.spreadsheets.values.append({
				spreadsheetId: "1suCM3T4SOvK48hjT7_LkFRhWW2me50vQ2y0j3fWGQ7s",
				range: 'TabelleCalibrazione',
				valueInputOption: 'RAW',
				insertDataOption: 'INSERT_ROWS',
				resource: {
					values: [
					[new Date().toISOString(), driverSerialNumber, JSON.stringify(calibTable)]
					],
				},
				auth: oAuth2Client
				}, (err, response) => {
				if (err) {
					alert("Errori durante esportazione tabella")
					return console.error(err);
					}
				else {
					alert("Tabella esportata");
					}
				});
			}
	
			catch (ex) {
					alert("Errore durante esportazione.  Verificare la connessione ad internet e che le google APIs siano state installate")
				}
		  }
		
		ConfigurationService.getDataFromCloud = function (serialNumber, returnFunc) {
			var sn = parseInt(serialNumber);
			if (isNaN(sn)) {
				console.log("Immesso valore non numerico, esco");
				return;
			}
			var deviceData = {};
			var BreakException = {};

			$http({
				method: 'GET',
				url: 'https://sheets.googleapis.com/v4/spreadsheets/1qPbkyytSiQL-MqSdOK_XDiw5orDzIZeE-ZG0jP_Bstg/values/Riassunto!A3:J500?key=AIzaSyAkQoe8j-nViFI5j2UpCN_zqXl9RG22y1Y'
			  }).then(function successCallback(response) {
				  try {
					response.data.values.forEach(element => {	
						if (parseInt(element[0]) == sn) {
							console.log("Element "+element);
							deviceData.serialNumber = element[0];
							deviceData.serCfxLinker = element[0];		//Serial Number
							deviceData.serTesta = element[2];			//Seriale testa ottica
							deviceData.serAlimentatore = element[3];		//Seriale alimentatore
							deviceData.serSkDriver = element[5];		//Seriale scheda driver
							deviceData.serControlUnit = element[9];		//Seriale unita controllo

							if (returnFunc != null) 
							  returnFunc(deviceData);

							throw BreakException;
						}
					});
					if (returnFunc != null)  {
						deviceData.serCfxLinker = serialNumber;
						returnFunc(deviceData);
					}					  
				}
				catch (e){
					if (e !== BreakException) throw e;
				}
				  
				  
				}, function errorCallback(response) {
				  // called asynchronously if an error occurs
				  // or server returns response with an error status.
				});
		}


		ConfigurationService.storecalibTableIntoCloud = function () {	
			var driverSerialNumber = 23;
			var calibTable = {'test':'test'};	
			ConfigurationService.saveCalibTableInCloud(driverSerialNumber, calibTable);
		}

		


	    ConfigurationService.init();
        
        return ConfigurationService;
        
    })