
angular.module('megaride')    
.controller('CtrlMegaride', 
function ($scope, $stateParams, $rootScope, $state) 
	{
		//CORPO controller

	  var video;

	  $scope.thickness = $stateParams.thickness;

	  $scope.init = function () {

	  	video = document.querySelector("#videoElement");
			 
	  	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
			 
			if (navigator.getUserMedia) {       
			    navigator.getUserMedia({video: true}, $scope.handleVideo, $scope.videoError);
			}

			document.addEventListener('DOMContentLoaded', function(){
		    v = document.getElementById('videoElement');
		    canvas = document.getElementById('canvas');
		    context = canvas.getContext('2d');
			w = canvas.width;
		    h = canvas.height;

			},false);
		}
		 
		$scope.handleVideo = function (stream) {
		    video.src = window.URL.createObjectURL(stream);
		}
		 
		$scope.videoError = function(e) {
		    // do something
		}

		$scope.init();
		
	}
);
   
    	