﻿/*var pdfMake = require('bower_components/pdfmake/build/pdfmake.js'); 
var pdfFonts = require('bower_components/pdfmake/build/vfs_fonts.js'); 
pdfMake.vfs = pdfFonts.pdfMake.vfs; */
pdfMake.fonts={
	NotoSansSC:  {
		normal : 'NotoSansSC-Regular.otf',
		bold: 'NotoSansSC-Medium.otf',
		italics: 'NotoSansSC-Italic.otf',
		bolditalics: 'NotoSansSC-Italic.otf'
	},

	NotoSerifSC:  {
		normal : 'NotoSerifSC-Regular.ttf',
		bold: 'NotoSerifSC-Medium.ttf',
		italics: 'NotoSerifSC-Italic.ttf',
		bolditalics: 'NotoSerifSC-Italic.ttf'
	},	
	Roboto: {
		normal : 'Roboto-Regular.ttf',
		bold: 'Roboto-Medium.ttf',
		italics: 'Roboto-Italic.ttf',
		bolditalics: 'Roboto-Italic.ttf'
	}
}
/*pdfMake.fonts = {
	方正黑体简体: {
		normal: '方正黑体简体.TTF',
		bold: '方正黑体简体.TTF',
		italics: '方正黑体简体.TTF',
		bolditalics: '方正黑体简体.TTF',
		}
	};*/
window.pdfMake = pdfMake;


angular.module('megaride')    
.controller('CtrlReport', 
function ($scope, $stateParams, $rootScope, $state, $timeout, $interval, $sce, $translate, $translate, $mdDialog, 
		ReportService, LoggingService, TreatmentService, ShutdownService, version) 
	{
		
		
		
		$scope.struttura = {};
		$scope.struttura.Indirizzo = 'Parco della Vittoria 11 - Monopoli';

		$scope.contenutoReferto = '';

		$scope.winReferto = null;
		$scope.debug = false;

		$scope.winPreviewReport = null;
		$scope.idReport = null;

 		$scope.pdf = {};
		$scope.pdf.data = null;
		$scope.currentUser = null;

		if ($stateParams.user != null) {
			ReportService.getCurrentReportData().patient.operator = '';
			if ($stateParams.user.Name) {
				 ReportService.getCurrentReportData().patient.operator += $stateParams.user.Name;
				 if ($stateParams.user.Surname)
					ReportService.getCurrentReportData().patient.operator += ' ';
				}
			if ($stateParams.user.Surname) 
				ReportService.getCurrentReportData().patient.operator += $stateParams.user.Surname;
		}


		//CORPO CONTROLLER

		$scope.checkReportData = function () {
			$scope.reportData = ReportService.getCurrentReportData();
			if ($scope.reportData.patient.surname == null)
				$scope.reportData.patient.surname = '';
			if ($scope.reportData.patient.name == null)
				$scope.reportData.patient.name = '';
			if ($scope.reportData.patient.age == null)
				$scope.reportData.patient.age = '';
			if ($scope.reportData.patient.sex == null)
				$scope.reportData.patient.sex = '';
			if ($scope.reportData.patient.pathology == null)
				$scope.reportData.patient.pathology = '';
			if ($scope.reportData.patient.operator == null)
				$scope.reportData.patient.operator = '';

			if ($scope.reportData.thickness == null)
				$scope.reportData.thickness = '';
			if ($scope.reportData.kMax == null)
				$scope.reportData.kMax = '';
			if ($scope.reportData.VertexDistanceX == null)
				$scope.reportData.VertexDistanceX = '';
			if ($scope.reportData.VertexDistanceY == null)
				$scope.reportData.VertexDistanceY = '';

			$scope.reportData.protocol = ReportService.getProtocol();
			if (ReportService.getDuration() != null)
				$scope.reportData.duration = ReportService.getDuration();
			else
				$scope.reportData.duration = '';
			if (ReportService.getSteps() != null) 
				$scope.reportData.steps = ReportService.getSteps();
			else
				$scope.reportData.steps = '';
			if ($scope.reportData.imbibition == null)
				$scope.reportData.imbibition = '';
			if ($scope.reportData.power == null)
				$scope.reportData.power = '';
			/*if ($scope.reportData.spotDiameter == null)
				$scope.reportData.spotDiameter = '';*/
			$scope.reportData.swVersion = version;
			ReportService.updateConfigReport();
		}

		$scope.back = function() {
			if ($scope.winPreviewReport) {
				 $scope.winPreviewReport.close();
				 $scope.winPreviewReport = null;
				}
			else {
				$scope.checkReport();
				$state.go('selectProtocol');
			} 
		}

		$scope.checkReport = function () {
			ReportService.checkReport($scope.idReport);
		}

		//Questa funzione costruisce l'header
		$scope.getHeader = function () {
			var imgWidth = Math.min(document.getElementById('logoImg').width, 400);
			var imgHeight = document.getElementById('logoImg').height;
			
			var fixedHeight = 100;  

			var imgWidth = (imgWidth * fixedHeight) / imgHeight;

			return {
					  margin: 10,
					  layout: 'noBorders',	
					  table:	{
					  		headerRows: 1,
    						layout: 'noBorders',
    						widths: [imgWidth, '*'],
    						color: 'black',
					  		body: 
					  			[
									[{image: $scope.imgLogo, width: imgWidth
									}]
								 ]
					   	}
						
					}
			}

		//Questa funzione costruisce il footer
		$scope.getFooter = function () {
			return {
					  margin: 12,
					  columns:	[
								{image: $scope.imgIromed,
							 	 width: 100
								}, 
								{text: ' CF X - Linker \t- Software ver: '+$scope.reportData.swVersion+'\t Copyright 2016-2018 IROMED GROUP srl \t All rights reserved', style: 'footer'}, /*, 
						 		{text: 'Patient: '+$scope.reportData.patient.surname +' ' + $scope.reportData.patient.name, style: 'subheader' 
						 		}*/
					   	]
					}
			}

		//Questa funzione costruisce gli stili associati
		$scope.getStyles = function() {
			return {
						header: {
							fontSize: 18,
							bold: true,
							margin: [0, 0, 0, 10]
						},
						footer: {
							fontSize: 8,
							italic: true,
							margin: [0, 0, 0, 10]	
						},
						headerTable: {
							fontSize: 18,
							bold: true,
							margin: [100, 0, 0, 10]
						},

						subheader: {
							fontSize: 16,
							bold: true,
							margin: [0, 10, 0, 5]
						},
						tableExample: {
							margin: [0, 5, 0, 15]
						},
						tableReport: {
							margin: [0, 5, 0, 15],
							color: 'blue'
						},
						tableHeader: {
							bold: true,
							fontSize: 13,
							color: 'black'
						},
						dataHeader: {
							bold: true,
							fontSize: 14,
							color: '#0E82C5'
						}
					}
		}

		//Questa funzione costruisce lo stile della tabella (per intenderci, il riquadro)
		$scope.getSubTableLayout = function () {
			return {
					hLineWidth: function (i, node) {
						return i === 0 ;
						return (i === 0 || i === node.table.body.length) ? 2 : 1;
					},
					vLineWidth: function (i, node) {
						return 0;
						return (i === 0 || i === node.table.widths.length) ? 2 : 0;
					},
					hLineColor: function (i, node) {
						return (i === 0 || i === node.table.body.length) ? '#0E82C5' : 'white';
					},
					vLineColor: function (i, node) {
						return (i === 0 || i === node.table.widths.length) ? '#0E82C5' : 'white';
					},
					// paddingLeft: function(i, node) { return 4; },
					// paddingRight: function(i, node) { return 4; },
					// paddingTop: function(i, node) { return 2; },
					// paddingBottom: function(i, node) { return 2; },
					// fillColor: function (i, node) { return null; }
				}
			}

		$scope.getSubTableLayoutNoBorders = function() {
			return {
					hLineWidth: function (i, node) {
						return 0;
					},
					vLineWidth: function (i, node) {
						return 0;
					},
					hLineColor: function (i, node) {
						return (i === 0 || i === node.table.body.length) ? '#0E82C5' : 'white';
					},
					vLineColor: function (i, node) {
						return (i === 0 || i === node.table.widths.length) ? '#0E82C5' : 'white';
					}
				}
		}

		$scope.getSubTableLayoutAllBorders = function() {
			return {
					hLineWidth: function (i, node) {
						//return i === 0 ;
						return (i === 0 || i === node.table.body.length) ? 2 : 1;
					},
					vLineWidth: function (i, node) {
						//return 0;
						return (i === 0 || i === node.table.widths.length) ? 2 : 0;
					},
					hLineColor: function (i, node) {
						return (i === 0 || i === node.table.body.length) ? '#black' : 'white';
						return (i === 0 || i === node.table.body.length) ? '#0E82C5' : 'white';
					},
					vLineColor: function (i, node) {
						return (i === 0 || i === node.table.widths.length) ? 'black' : 'white';
						return (i === 0 || i === node.table.widths.length) ? '#0E82C5' : 'white';
					}
				}
		}

		//Questa funzione costruisce i dati generali
		$scope.getDatiGenerali = function () {
			return [
					//INIZIO DATI GENERALI
					{text: $scope.translateAndUpper('Report.General'), style: 'dataHeader', alignment: 'left'},									
					//{text: '', style: 'dataHeader', alignment: 'center'},

					{
						style: 'tableReport',
						color: 'black',
						table: {
							width: "100%",
							widths: [200, '*'],
							headerRows: 0,
							// keepWithHeaderRows: 1,
							body: [
									//[{text:'\r'},{}],
									[{text: $scope.translateAndUpper('ControlPanel.Reports.Date'), style: 'tableHeader', alignment: 'left'}, {text: $scope.currentDate, alignment: 'left'}],
									/*[{text:'\r'},{}],*/
									[{text: $scope.translateAndUpper('Report.Institution'), style: 'tableHeader', alignment: 'left'}, {text: ReportService.getReportConfiguration().nomeStruttura + ' - '+
											ReportService.getReportConfiguration().IndirizzoStruttura, alignment: 'left'}],
									/*[{text:'\r'},{}],*/
									[{text: $scope.translateAndUpper('Report.Operator'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.patient.operator, alignment: 'left'}],
									[{text:'\r'},{}]
								]
							},
						layout: $scope.getSubTableLayout()
					}
					]
					//FINE DATI GENERALI
		}

		//Questa funzione costruisce i dati del paziente
		$scope.getPatientData = function() {
			return [
					//INIZIO DATI DEL PAZIENTE
					{text: $scope.translateAndUpper('Report.PatientData'), style: 'dataHeader', alignment: 'left'},									
					{text: '', style: 'dataHeader', alignment: 'center'},

					{
						style: 'tableReport',
						color: 'black',
						table: {
							width: "100%",
							widths: [200, '*'],
							headerRows: 0,
							// keepWithHeaderRows: 1,
							body: [
									//[{text:'\r'},{}],
									[{text: $scope.translateAndUpper('Report.Name') +"/ "+ $scope.translateAndUpper('Report.Surname'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.patient.name 
											+' ' + $scope.reportData.patient.surname, alignment: 'left'}],
//STO GIOCANDO PER CINESE
/*				[{text: ' CIAO 漢字漢字漢字漢字', style: 'tableHeader', alignment: 'left', font:'NotoSansSC'}, 
				{text: $scope.reportData.patient.name +' ' + $scope.reportData.patient.surname, alignment: 'left', font:'Roboto'}],
*/
//FINE						

									/*[{text:'\r'},{}],*/
									[{text: $scope.translateAndUpper('Report.Sex'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.patient.sex, alignment: 'left'}],
									/*[{text:'\r'},{}],*/
									[{text: $scope.translateAndUpper('Report.Age'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.patient.age, alignment: 'left'}],
									/*[{text:'\r'},{}],*/
									[{text: $scope.translateAndUpper('Report.Eye'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.eye, alignment: 'left'}],
									[{text:'\r'},{}]
									
								]
							},
						layout: $scope.getSubTableLayout()
					}]
					//FINE DATI DEL PAZIENTE
			}

		//Questa funzione costruisce i dati relativi ai parametri del trattamento
		$scope.getTreatmentBody = function () {
			var result = 
			 			[

							//[{text:'\r'},{}, {}],
							[{text: $scope.translateAndUpper('Report.Pathology'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.patient.pathology, alignment: 'left'}, {text: '', style: 'tableHeader', alignment: 'left'}],
							/*[{text:'\r'},{}, {}],*/
							[{text: $scope.translateAndUpper('ControlPanel.Reports.Protocol'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.protocol, alignment: 'left'}, {text: '', style: 'tableHeader', alignment: 'left'}],
							/*[{text:'\r'},{}, {}],*/
							[{text: $scope.translateAndUpper('Report.Solution'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.solution, alignment: 'left'}, {text: '', style: 'tableHeader', alignment: 'left'}],
							/*[{text:'\r'},{}, {}],*/
							[{text: $scope.translateAndUpper('Report.TreatmentData') , colSpan: 2, style: 'tableHeader', alignment: 'left'}, {}, {}],

						];
			if ($scope.reportData.protocol == 'Custom Fast'){
				result.push(
							//PARAMETRI CORNEALI
							[{text: '', style: 'tableHeader', alignment: 'left'}, {text: $scope.translateAndUpper('Megaride.thickness'), alignment: 'left'}, 
																				  {text: $scope.reportData.thickness+' μ', alignment: 'left'}]
							);
			
					if ($scope.reportData.patient.pathology != $translate.instant('Megaride.Diagnosis.DIAGNOSIS_5')) 
							result.push(
			
							[{text: '', style: 'tableHeader', alignment: 'left'}, {text: $scope.translateAndUpper('Megaride.kerApex'), alignment: 'left'},  
																				  {text: $scope.reportData.kMax+" (D)", alignment: 'left'}],
							[{text: '', style: 'tableHeader', alignment: 'left'}, {text: $scope.translateAndUpper('Report.Coordinates'), alignment: 'left'},  
																				  {text: $scope.getCoordinates()+" (mm)", alignment: 'left'}],
							[{text: '\r', style: 'tableHeader', alignment: 'left'},{},{}]
							//FINE PARAMETRI CORNEALI																										  
							);
				}
			result.push(
							[{text: '', style: 'tableHeader', alignment: 'left'}, {text: $scope.translateAndUpper('Treatment.Steps.Imbibition'), alignment: 'left'}, 
																				  {text: $scope.reportData.imbibition+ " min", alignment: 'left'}],
							[{text: '', style: 'tableHeader', alignment: 'left'}, {text: $scope.translateAndUpper('Treatment.Steps.Irradiation'), alignment: 'left'}, 
																				  {text: $scope.reportData.duration +' min', alignment: 'left'}]);
			if ($scope.reportData.protocol == 'Custom Fast')
				result.push(
							[{text: '', style: 'tableHeader', alignment: 'left'}, {text: $scope.translateAndUpper('SelectProtocol.MegarideOptions.Description'), alignment: 'left'}, 
																				  {text: $scope.reportData.steps, alignment: 'left'}]);
			var powerString = $scope.reportData.power+ " mW/cm2";
			if ($scope.reportData.pulsedData && $scope.reportData.pulsedData.isPulsed)
					powerString += " "+$scope.reportData.pulsedData.irradiationTime+" on / "+$scope.reportData.pulsedData.pauseTime+" off";
			var EnergyString = $scope.roundTo(TreatmentService.toJoules(($scope.reportData.power * $scope.reportData.duration * 60)), 2)+" J/cm2";
			result.push(
							[{text: '', style: 'tableHeader', alignment: 'left'}, {text: $scope.translateAndUpper('SelectProtocol.FullCustomOptions.TotalEnergy'), alignment: 'left'}, 
																					{text: EnergyString, alignment: 'left'}],
							[{text: '', style: 'tableHeader', alignment: 'left'}, {text: $scope.translateAndUpper('Megaride.intensity'), alignment: 'left'}, 
																				  {text: powerString, alignment: 'left'}]
						);
			if ($scope.reportData.spotDiameter)
					result.push(

						 [{text: '', style: 'tableHeader', alignment: 'left'}, {text:  $scope.translateAndUpper('Megaride.spotDiam'), alignment: 'left'}, 
						 {text: $scope.reportData.spotDiameter +" mm", alignment: 'left'}],
						);

			return result;
		}

		//Questa funzione costruisce il riquadro esterno ed affida a getTreatmentBody() il compito di costruirne il contenuto
		$scope.getTreatmentData = function() {
			return [
					{text: $scope.translateAndUpper('Report.TreatmentData'), style: 'dataHeader', alignment: 'left'},
					{text: '', style: 'dataHeader', alignment: 'center'},
					{
						style: 'tableReport',
						color: 'black',
						table: {
							width: "100%",
							widths: [200, '*', '*'],
							headerRows: 0,
							// keepWithHeaderRows: 1,
							body: $scope.getTreatmentBody()
							},
						layout: $scope.getSubTableLayout()
					},
					//FINE DATI DEL TRATTAMENTO
			];
		}

		//Questa funzione costruisce i dati generali
		$scope.getRiquadroOperatore = function () {
			return [
					//INIZIO RIQUADRO OPERATORE
					/*{text: $scope.translateAndUpper('Report.OperatorSignature'), style: 'dataHeader', alignment: 'left'},									
					{text: '', style: 'dataHeader', alignment: 'center'},*/

					{
						style: 'tableReport',
						color: 'black',
						table: {
							width: "100%",
							widths: ['*', 180],
							headerRows: 0,
							// keepWithHeaderRows: 1,
							body: [
									[{text:'\r'},{}],
//									[{text:'\r'},{}],
									[{text:  $scope.translateAndUpper('Report.OperatorSignature'), style: 'tableHeader', alignment: 'right'}, {text: '_____________________________', alignment: 'right'}]
//									[{text:'\r'},{}]
									/*[{text:'\r'},{}],
									[{text: $scope.translateAndUpper('Report.Institution'), style: 'tableHeader', alignment: 'left'}, {text: ReportService.getReportConfiguration().nomeStruttura + ' - '+
											ReportService.getReportConfiguration().IndirizzoStruttura, alignment: 'left'}],
									[{text:'\r'},{}],
									[{text: $scope.translateAndUpper('Report.Operator'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.patient.operator, alignment: 'left'}],
									[{text:'\r'},{}]*/
								]
							},
						layout: $scope.getSubTableLayoutNoBorders()
					}
					]
					//FINE RIQUADRO OPERATORE
		}

		//Questa funzione costruisce i dati generali
		$scope.getRiquadroCommento = function () {
			return [
					//INIZIO DATI DEL PAZIENTE
					{text: $scope.translateAndUpper('Report.Notes'), style: 'dataHeader', alignment: 'left'},									
					{text: '', style: 'dataHeader', alignment: 'center'},

					{
						style: 'tableReport',
						color: 'black',
						table: {
							width: "100%",
							widths: [200, '*'],
							headerRows: 0,
							// keepWithHeaderRows: 1,
							body: [
									[{text:'\r'},{}],
									[{text:'\r'},{}],
									[{text:'\r'},{}],
									[{text:'\r'},{}]
								/*	[{text: $scope.translateAndUpper('Report.Name') +"/ "+ $scope.translateAndUpper('Report.Surname'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.patient.name 
											+' ' + $scope.reportData.patient.surname, alignment: 'left'}],
									[{text: $scope.translateAndUpper('Report.Sex'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.patient.sex, alignment: 'left'}],
									[{text: $scope.translateAndUpper('Report.Age'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.patient.age, alignment: 'left'}],
									[{text: $scope.translateAndUpper('Report.Eye'), style: 'tableHeader', alignment: 'left'}, {text: $scope.reportData.eye, alignment: 'left'}],
									[{text:'\r'},{}]*/
									
								]
							},
						layout: $scope.getSubTableLayoutAllBorders()
					}]
					//FINE DATI DEL PAZIENTE
		}


		//Questa funzione costruisce il contenuto del report
		$scope.getContent = function () {
			var content = [];
			content.push({text: '\r', style: 'header'});
			content.push($scope.getDatiGenerali());
			content.push($scope.getPatientData());
			content.push($scope.getTreatmentData());
			content.push($scope.getRiquadroCommento());
			content.push($scope.getRiquadroOperatore());
			return content;
		}

		$scope.getDocDefinition = function () {
			$scope.loadImages();
			$scope.currentDate =  new Date();
			try {
				$scope.currentDate = $scope.currentDate.toLocaleFormat('%d-%b-%Y');
				}
			catch (err) {
					 try {
					 	$scope.currentDate = new Intl.DateTimeFormat('en-GB').format(new Date());
					 }
					 catch (err1) {
						$scope.currentDate = ' ';
					 }
				}
			if ($scope.reportData.protocol == null)
				$scope.reportData.protocol = '';
			var docDefinition = {
					header: $scope.getHeader(),
					footer: $scope.getFooter(),							
					content: $scope.getContent(),
					styles: $scope.getStyles(),
					//defaultStyle: {font: 'NotoSerifSC'}
					defaultStyle: {}
				}
			return docDefinition;
		}

		$scope.updateReportService = function () {
			ReportService.setCurrentTreatmentPatientInfo($scope.reportData.patient);			
			$scope.archiveReport();
		}

		$scope.apply = function() {
            if ($scope.$$phase) 
                return;
            try {
                $scope.$apply();
            }
            catch (err) {

            }
        }

        $scope.embedVisible = function () {
        	return false;
        	//return nwgui == null;
        }

		$scope.updateReport = function () {
			if (nwgui != null)
				return;
			ReportService.setCurrentTreatmentPatientInfo($scope.reportData.patient);
			pdfMake.createPdf($scope.getDocDefinition()).getBase64(function(encodedString) {
					var strPdf = 'data:application/pdf;base64,'+encodedString;
					strPdf += '#view=FitH';
				    $scope.contenutoReferto = $sce.trustAsResourceUrl(strPdf);
				    $scope.apply();
				});
		}

		$scope.pdfMaker = function() {
				
				$scope.updateReportService();
				
				
				if ($scope.debug) {
					try {
			        	pdfMake.createPdf($scope.getDocDefinition()).open();
			        	}
			        catch (ex) {
			        	console.log(ex);
			        	LoggingService.info("Controller Report.  Errore in pdfMake" + ex);
			        }
			        return;
			    }

		        try {
		        	LoggingService.info("Report Controller.  RICHIESTO REPORT");
		        	pdfMake.createPdf($scope.getDocDefinition()).getBase64(function(encodedString) {
						var strPdf = 'data:application/pdf;base64,'+encodedString;
						
						ReportService.printReferto(strPdf);

						var confirm = $mdDialog.confirm()
					              //.title($translate.instant('Report.JumpImbibitionTitle'))
					              .textContent($translate.instant('Report.PrintSent'))
					              .ok($translate.instant('Messages.OK'))
					              //.cancel($translate.instant('Messages.DISCARD'));

					        $mdDialog.show(confirm).then(
					        	function(res) {
					        		/*if (res == true)
					                	successCallBack();
					                else
					                	$scope.resume();*/
					            	}, 
					            function() {
					            });
						
						});
					 
		        }
		        catch (ex) {
		        	LoggingService.info("Report Controller.  Errore "+ ex);
		        }
		    }

		$scope.downloadPdf = function() {
			$scope.updateReportService();
		    //pdfMake.createPdf(docDefinition).download();
		    pdfMake.createPdf(docDefinition).download('nome.pdf');
		  };

		$scope.getImageB64 = function(idElementSrc) {

			var imgVar = document.getElementById(idElementSrc);
	  		var canvas = document.createElement("canvas");
	    	canvas.width = imgVar.width;
	    	canvas.height = imgVar.height;

	    	// Copy the image contents to the canvas
	    	var ctx = canvas.getContext("2d");
	    	ctx.drawImage(imgVar, 0, 0);

	    	// Get the data-URL formatted image
	    	// Firefox supports PNG and JPEG. You could check img.src to
	    	// guess the original format, but be aware the using "image/jpg"
	    	// will re-encode the image.
				//return canvas.toDataURL("image/jpeg");
				return canvas.toDataURL("image/png");

		}

		$scope.getCoordinates = function() {
			if  ($scope.reportData.cornVertexRadius != null && $scope.reportData.cornVertexRadius != '')
				return $scope.reportData.cornVertexRadius;
			else if ($scope.reportData.VertexDistanceX != '' && $scope.reportData.VertexDistanceY != '') 
				return $scope.reportData.VertexDistanceX+'; '+$scope.reportData.VertexDistanceY;
			return '';
		}

		 
		 $scope.loadImgReferto = function() {
		 	$scope.imgLogo = $scope.getImageB64('logoImg');
		  }

		 $scope.loadImgIromed = function() {
		 	$scope.imgIromed = $scope.getImageB64('logoIromed');
		  }

		  $scope.loadImages = function() {
		  	$scope.loadImgReferto();
		  	$scope.loadImgIromed();
		  }

		  $scope.translateAndUpper = function(str) {
			var strApp;
			if ($translate.use()=='cn')
				strApp = $translate.instant(str, {}, null, "gb", null);
			else
				strApp = $translate.instant(str);
		  	return strApp;
		  	return strApp.toUpperCase();
		  }

		  $scope.clickOutOfReport = function ()  {
		  	if ($scope.winPreviewReport) {
				 $scope.winPreviewReport.close();
				 $scope.winPreviewReport = null;
				}
		  }

		  $scope.hasValue = function (str) {
		  	return str != null && str != '';
			}
			
/*			$scope.checkPenDrive = function () {
				if ($scope.intervalPenDrive != null)
					return;
				$scope.intervalPenDrive	 = $interval (
					function () {
						//command = 'ls /dev/disk/by-label/';			
						command = 'ls /media/cfxlinker/';			
						const exec = require('child_process').exec;
							 const child = exec(command,
								 (error, stdout, stderr) => {
									 if (stdout !== null && stdout != "") {
										 $scope.pathPenDrive = "/media/cfxlinker/"+stdout.trim()+"/reports/";
										 }
									 else {
										$scope.pathPenDrive = null;
									 }
									 if (error !== null) {
										$scope.pathPenDrive = null;
									 }
							 });   
					}
					, 3000);
			}*/


		  $scope.saveReport = function() {
		  	$scope.updateReportService();
		  	var strPdf = $scope.getDocDefinition();

				 const pdfDocGenerator = pdfMake.createPdf(strPdf);
			   pdfDocGenerator.getBuffer((buffer) => {
				 var file = new Blob([buffer], {type:'application/pdf'});
         var fileURL = URL.createObjectURL(file);
				
		 		 $scope.pdf.data = $sce.trustAsResourceUrl(fileURL);
				 //$scope.pdf.path = fileUrl;

				 if ($scope.hasValue($scope.reportData.patient.name) || $scope.hasValue($scope.reportData.patient.surname)) {
						if ($scope.hasValue($scope.reportData.patient.surname))
						  fName = $scope.reportData.patient.surname+ "_"+ $scope.reportData.patient.name;
						else
						  fName = $scope.reportData.patient.name;
					}
				else {
					fName = "ANONYM_"+Math.floor(Math.random() * 1000);
				}
				
				fName+="_"+$scope.currentDate.replace(/\//g, "_");
				if ($scope.reportData.eye != null)
				   fName+="_"+$scope.reportData.eye;

				 if (ReportService.saveReportOnPath(buffer, $rootScope.pathPenDrive, fName+".pdf"))
				 	 alert($translate.instant('Report.ReportSaved')+" "+fName+".pdf");
				});
			}
			
			$scope.saveInDir = function (dir, pdfContent) {
				var fName = "";
				if (dir != null)
					fName = dir;
				if ($scope.hasValue($scope.reportData.patient.name) || $scope.hasValue($scope.reportData.patient.surname)) {
					if ($scope.hasValue($scope.reportData.patient.surname))
					 fName += $scope.reportData.patient.surname+ "_"+ $scope.reportData.patient.name;
					else
					 fName += $scope.reportData.patient.name;
				 }
			 else {
				 fName += "ANONYM_"+Math.floor(Math.random() * 1000);
			 }
			 if ($scope.reportData.eye != "")
				 fName += "_"+$scope.reportData.eye;
			 
			// fName += "-"+$scope.currentDate;
			 fName += ".pdf";
			 pdfMake.createPdf(pdfContent).download(fName);
			}

		  $scope.editReport = function() {
			 $scope.pdf.data = null;
			}

		  $scope.doPrint = function() {
			 ReportService.printTempReport();
			}

			$scope.roundTo = function(value, decimalpositions)  {
				var i = value * Math.pow(10,decimalpositions);
				i = Math.round(i);
				return i / Math.pow(10,decimalpositions);
			 } 


		  $scope.printReport = function() {
		  	$scope.updateReportService();
		  	
		  	const topOfWin = 265;

		  	var w = window.screen.width;
		  	var h = window.screen.height -topOfWin;


			const pdfDocGenerator = pdfMake.createPdf($scope.getDocDefinition());
			pdfDocGenerator.getBuffer((buffer) => {
				var file = new Blob([buffer], {type:'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
				$scope.pdf.data = $sce.trustAsResourceUrl(fileURL);
				$scope.pdf.path = fileURL;
				ReportService.saveTempReport(buffer);		
			});
			return;


		  	
		  	$scope.winPreviewReport = window.open("http://www.iromedgroup.com", "report", "width="+w+",height="+h+", left=0"+", top="+topOfWin+", left=0");
			pdfMake.createPdf($scope.getDocDefinition()).print({}, $scope.winPreviewReport);
			$scope.winPreviewReport.moveTo(0, topOfWin);
		  }

		  $scope.createPreview = function() {
		    //$scope.updateReportService();
		    const pdfDocGenerator = pdfMake.createPdf($scope.getDocDefinition());
			pdfDocGenerator.getBuffer((buffer) => {
				/*ReportService.archiveReport(buffer, $scope.reportData, $scope.idReport);
                                ReportService.saveTempReport(buffer);*/
				var file = new Blob([buffer], {type:'application/pdf'});
                                var fileURL = URL.createObjectURL(file);
				$scope.pdf.data = $sce.trustAsResourceUrl(fileURL);
				$scope.pdf.path = fileURL;						
			});
		  }

		  $scope.setIdReport = function(res, results) {
		  	if (results != null && results.rows != null && results.rows.length > 0)
		  		$scope.idReport = results.rows[0].ID;
		  }

		  $scope.archiveReport = function() {
		  	//var strPdf = pdfMake.createPdf($scope.getDocDefinition());
		  	const pdfDocGenerator = pdfMake.createPdf($scope.getDocDefinition());
			pdfDocGenerator.getBuffer((buffer) => {
				ReportService.archiveReport(buffer, $scope.reportData, $scope.idReport);	
			});
		  	
			}
			
			$scope.saveOnArchive = function () {
				//Funzione richiamata dopo verifica dell'ID
				 var afterIdCheck = function() {
						ReportService.setCurrentTreatmentPatientInfo($scope.reportData.patient);
						$scope.archiveReport();
						alert($translate.instant('Report.ReportSaved'));
						};

				if ($scope.idReport == null)
					ReportService.getIdNextReport(function (res, results){
						$scope.setIdReport(res, results);
						afterIdCheck();
					});
				else 
					afterIdCheck();

			}

			$scope.loadCurrentUser = function () {
				if ($stateParams.user == null)
					return;
				try {
					$scope.currentUser = JSON.parse($stateParams.user);	
				}
				catch (err) {
					$scope.currentUser = null;
				}
				ReportService.setCurrentUser($scope.currentUser);
			  }

		  $scope.init = function() {
			  $scope.checkReportData();
			  $scope.loadCurrentUser();

				$scope.reportData.eye = '';
		  	$scope.reportData.solution = '';

				$scope.parameters = {};
		  	$scope.parameters.eyes = [];
		  	$scope.parameters.eyes.push({description: $translate.instant("Report.Right"), id:0});
		  	$scope.parameters.eyes.push({description: $translate.instant("Report.Left"), id:1});

				$scope.parameters.solutions = [];

				//$scope.reportData.protocol = 'Custom Fast';

				if ($rootScope.Fidia &&  $scope.reportData.protocol =='Custom Fast') {
				 	 $scope.parameters.solutions.push({description: "Ribocross TE", id:0});
					 $scope.parameters.solutions.push({description: "Ribofast", id:1});
				 }
				else if ($rootScope.Fidia &&  $scope.reportData.protocol =='Iontoforesi') {
					 $scope.parameters.solutions.push({description: "Ricrolin+", id:3});
					}
				else
					{$scope.parameters.solutions.push({description: "Ribocross TE", id:0});
					 $scope.parameters.solutions.push({description: "Ribofast", id:1});
					 if ($rootScope.Fidia)
					 	  $scope.parameters.solutions.push({description: "Ricrolin+", id:3});
					 $scope.parameters.solutions.push({description: $translate.instant("Report.Other"), id:2});
					}
		  }

		 $scope.init();
		 ShutdownService.setOperation(ShutdownService.OPERATIONS.SPLASH);
	}
);
   
    	
