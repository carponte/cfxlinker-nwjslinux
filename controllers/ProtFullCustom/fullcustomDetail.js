angular.module('megaride')    
.controller('CtrlFullCustomDetail', 
    function ($scope, $stateParams, $rootScope, $state, $translate, ShutdownService, LoggingService, TreatmentService, CalibrationService, MaxEnergyFullCustom) {

        $scope.STEP_TYPES = TreatmentService.getStepTypes();

       
        $scope.selectedTreatmentDetails = [];
        $scope.protocolName = null;
        
        $scope.treatmentSummary = {};
        $scope.treatmentSummary.totalEnergy = "";
        $scope.treatmentSummary.totalDuration = "";
        $scope.treatmentSummary.numSteps = 1;
        $scope.treatmentSummary.meanIntensity = "";
        $scope.treatmentSummary.imbibition = "";
        $scope.treatmentSummary.pulsed = {};
        $scope.treatmentSummary.pulsed.irradiationTime = 1;
        $scope.treatmentSummary.pulsed.pauseTime = 1;

        $scope.MaxEnergy = MaxEnergyFullCustom;
        $scope.maxNumSteps = 12;


        
        $scope.SELECTION_TYPES = {
            "INTENSITY": 0,
            "DURATION": 1,
            "POWER": 2
        }
        $scope.ordineSelezione = [];
        $scope.ordineSelezione.push($scope.SELECTION_TYPES.INTENSITY);
        $scope.ordineSelezione.push($scope.SELECTION_TYPES.DURATION);
        $scope.ordineSelezione.push($scope.SELECTION_TYPES.POWER);

        $scope.isPulsedVisible = false;     //Se true vuol dire che la testa è abilitata alla pulsata, false altrimenti
        $scope.isPulsedEnabled = false;     //Stato della toggle relativa alla pulsatata


        $scope.back = function() {
             $state.go('fullCustom', {edit: 1});
        }

        $scope.roundTo = function(value, decimalpositions)  {
            var i = value * Math.pow(10,decimalpositions);
            i = Math.round(i);
            return i / Math.pow(10,decimalpositions);
           } 

        $scope.PushOrdineSelezione = function (ordineSelezione){
            var index = $scope.ordineSelezione.indexOf(ordineSelezione);
            if (index > -1) {
              $scope.ordineSelezione.splice(index, 1);
            }
            $scope.ordineSelezione.unshift(ordineSelezione);
        }

        //Funzione chiamata quando viene cambiato qualche elemento del riassunto superiore
        $scope.onChangeResume = function (tipoSelezione) {
            //Verifico che il valore sia formalmente corretto;
            switch (tipoSelezione) {
                case $scope.SELECTION_TYPES.INTENSITY:
                        if (isNaN ($scope.treatmentSummary.meanIntensity))
                            $scope.treatmentSummary.meanIntensity = '';
                       break;
                case $scope.SELECTION_TYPES.DURATION:
                        if (isNaN ($scope.treatmentSummary.totalDuration))
                            $scope.treatmentSummary.totalDuration = '';
                       break;
                case $scope.SELECTION_TYPES.POWER:
                        if (isNaN ($scope.treatmentSummary.totalEnergy))
                            $scope.treatmentSummary.totalEnergy = '';
                       break;
               }            
            
                //Proseguo con le operazioni per fare il push del valore inserito
            $scope.checkFieldsRange();
            if (tipoSelezione != null)
                $scope.PushOrdineSelezione(tipoSelezione);
            
            //Va ricalcolato sempre il valore contenuto nel terzo elemento dell'array (Ultimo modificato)
            var toBeCalculated = $scope.ordineSelezione [2];

            switch (toBeCalculated) {
                 case $scope.SELECTION_TYPES.INTENSITY:
                        $scope.treatmentSummary.meanIntensity = $scope.roundTo($scope.treatmentSummary.totalEnergy * 1000 / 
                                ($scope.treatmentSummary.totalDuration *60), 2);
                        break;
                 case $scope.SELECTION_TYPES.DURATION:
                        $scope.treatmentSummary.totalDuration = 
                                $scope.roundTo(($scope.treatmentSummary.totalEnergy *1000)/ 
                                    ($scope.treatmentSummary.meanIntensity * 60), 2);
                        break;
                 case $scope.SELECTION_TYPES.POWER:
                        $scope.treatmentSummary.totalEnergy = $scope.roundTo(TreatmentService.toJoules($scope.treatmentSummary.totalDuration * 60 *
                                        $scope.treatmentSummary.meanIntensity), 2);
                        break;
                }

            //Ricalcolo gli step nella lista inferiore
            $scope.recalcSteps();
           
        }

        $scope.isEmpty = function (val) {
            return val == null || val == "" || val == 0;
        }

        //Riporta il valore immesso a sole due cifre decimali
        $scope.hasMoreThanTwoDecimals = function (value) {
            var strval = ""+value;
            var posDot = strval.indexOf('.');
            if (posDot == -1)
                return false;
            return posDot < (strval.length -2);
        }

        $scope.checkFieldsRange = function () {
            if  ($scope.roundTo($scope.treatmentSummary.imbibition, 0) > 60)
                $scope.treatmentSummary.imbibition = 60;
            if  ($scope.roundTo($scope.treatmentSummary.numSteps, 0) > $scope.maxNumSteps)
                $scope.treatmentSummary.numSteps = $scope.maxNumSteps;
            var maxIntensity = CalibrationService.getMaxMwAvailable();
            if  ($scope.roundTo($scope.treatmentSummary.meanIntensity, 0) > maxIntensity) {
                 $scope.treatmentSummary.meanIntensity = maxIntensity;
                 $scope.treatmentSummary.totalEnergy = $scope.roundTo(TreatmentService.toJoules($scope.treatmentSummary.totalDuration * 60 *
                    $scope.treatmentSummary.meanIntensity), 2);
                }
            
            if ($scope.hasMoreThanTwoDecimals($scope.treatmentSummary.meanIntensity))
                $scope.treatmentSummary.meanIntensity = $scope.roundTo($scope.treatmentSummary.meanIntensity, 2);
            if ($scope.hasMoreThanTwoDecimals($scope.treatmentSummary.totalDuration))
                $scope.treatmentSummary.totalDuration = $scope.roundTo($scope.treatmentSummary.totalDuration, 2);
            if ($scope.hasMoreThanTwoDecimals($scope.treatmentSummary.totalEnergy))
                $scope.treatmentSummary.totalEnergy = $scope.roundTo($scope.treatmentSummary.totalEnergy, 2);
            if ($scope.hasMoreThanTwoDecimals($scope.treatmentSummary.imbibition))
                $scope.treatmentSummary.imbibition = $scope.roundTo($scope.treatmentSummary.imbibition, 2);
        }

        //Ricalcola gli step nella lista inferiore 
        $scope.recalcSteps = function () {
            $scope.checkFieldsRange();
            $scope.selectedTreatmentDetails = [];

            if ($scope.isEmpty($scope.treatmentSummary.totalEnergy))
                return;
            if ($scope.isEmpty($scope.treatmentSummary.totalDuration))
                return;

            if ($scope.isEmpty($scope.treatmentSummary.numSteps))
                return;
            if ($scope.isEmpty($scope.treatmentSummary.meanIntensity))
                return;
            if ($scope.isEmpty($scope.treatmentSummary.imbibition))
                return;
           
            $scope.selectedTreatmentDetails.push({type: $scope.STEP_TYPES.IMBIBITION, duration: $scope.treatmentSummary.imbibition, 
                    progress: 0, id: 0});
            $scope.selectedTreatmentDetails.push({type: $scope.STEP_TYPES.PATIENT_POSITION, id: 1});
            for (var i=0; i<$scope.treatmentSummary.numSteps; i++) {
                 var step = {type: $scope.STEP_TYPES.IRRADIATION, duration: $scope.roundTo($scope.treatmentSummary.totalDuration /
                    $scope.treatmentSummary.numSteps, 2), fromStart:0, power: $scope.treatmentSummary.meanIntensity, "desc": "(step "+(i+1)+")", progress: 0, id: 2+i*2};
                 if ($scope.isPulsedEnabled) {
                     step.isPulsed = true;
                     step.irradiationTime = $scope.roundTo($scope.treatmentSummary.pulsed.irradiationTime, 2);
                     step.pauseTime = $scope.roundTo($scope.treatmentSummary.pulsed.pauseTime, 2);
                 }
                 $scope.selectedTreatmentDetails.push(step);
                 if (i<$scope.treatmentSummary.numSteps-1)
                   $scope.selectedTreatmentDetails.push({type: $scope.STEP_TYPES.PAUSE, id: 3+i*2});
                }
            $scope.selectedTreatmentDetails.push({type: $scope.STEP_TYPES.END_TREATMENT, id: 3+$scope.treatmentSummary.numSteps*2});
            $scope.loadItemEnergy();
        }


        $scope.getStypeTypeDescription = function(stepType) {
            result = "";
            switch (stepType) {
                case $scope.STEP_TYPES.IRRADIATION  :   
                                                msg =  "Treatment.Steps.Irradiation";
                                                break;
                case $scope.STEP_TYPES.IMBIBITION   :   
                                                msg =  "Treatment.Steps.Imbibition";
                                                break;
                case $scope.STEP_TYPES.PAUSE    :   
                                                msg = "Treatment.Steps.Pause";
                                                break;
                case $scope.STEP_TYPES.PATIENT_POSITION:
                                                msg = "Treatment.Steps.PatientPosition";
                                                break;
                case $scope.STEP_TYPES.END_TREATMENT:
                                                msg = "Treatment.Steps.End";
                                                break;
                default: break;
            }
            result += $translate.instant(msg);
            return result;
        }

        $scope.loadNumSteps = function() {
            $scope.treatmentSummary.numSteps = 1;
            if ($scope.selectedTreatmentDetails == null || $scope.selectedTreatmentDetails.length == 0)
                return;
            $scope.treatmentSummary.numSteps = 0;
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++)
                if ($scope.selectedTreatmentDetails[i].type == $scope.STEP_TYPES.IRRADIATION)
                    $scope.treatmentSummary.numSteps ++;
        }

        $scope.loadTreatmentDuration = function () {
            if ($scope.selectedTreatmentDetails == null || $scope.selectedTreatmentDetails.length == 0)
                return;
            $scope.treatmentSummary.totalDuration = 0;
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++)
                if ($scope.selectedTreatmentDetails[i].type == $scope.STEP_TYPES.IRRADIATION)
                    $scope.treatmentSummary.totalDuration += $scope.selectedTreatmentDetails[i].duration;
            $scope.treatmentSummary.totalDuration = $scope.roundTo($scope.treatmentSummary.totalDuration, 2);
        }

        $scope.loadTreatmentImbibition = function () {
            if ($scope.selectedTreatmentDetails == null || $scope.selectedTreatmentDetails.length == 0)
                return;
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++)
                if ($scope.selectedTreatmentDetails[i].type == $scope.STEP_TYPES.IMBIBITION)
                    $scope.treatmentSummary.imbibition = $scope.selectedTreatmentDetails[i].duration;
        }

        $scope.getTreatmentPower = function () {
            return TreatmentService.getTreatmentJoules($scope.selectedTreatmentDetails);
        }

        $scope.getItemPower = function (item) {
            if (!isNaN(item.power) && ! isNaN(item.duration))
                return TreatmentService.toJoules((item.power * item.duration)) +" J";
            return "";
        }

        $scope.loadMeanIntensity = function() {
            $scope.treatmentSummary.meanIntensity = "";
            var durCount = 0;

            if ($scope.selectedTreatmentDetails == null || $scope.selectedTreatmentDetails.length == 0)
                return;
            $scope.treatmentSummary.meanIntensity = 0;
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++)
                if ($scope.selectedTreatmentDetails[i].type == $scope.STEP_TYPES.IRRADIATION) {
                    durCount += $scope.selectedTreatmentDetails[i].duration;
                    $scope.treatmentSummary.meanIntensity += $scope.selectedTreatmentDetails[i].power * 
                                                                $scope.selectedTreatmentDetails[i].duration;
                }
            $scope.treatmentSummary.meanIntensity = $scope.roundTo(($scope.treatmentSummary.meanIntensity /durCount), 2); 
        }

        $scope.convertuWtomW = function () {
            if ($scope.selectedTreatmentDetails == null || $scope.selectedTreatmentDetails.length == 0)
                return;
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++) {
                if ($scope.selectedTreatmentDetails[i].type == $scope.STEP_TYPES.IRRADIATION && 
                    $scope.selectedTreatmentDetails[i].power >=1000)
                    $scope.selectedTreatmentDetails[i].power = $scope.selectedTreatmentDetails[i].power / 1000;
            }
        }

        $scope.convertSecondsToMinutes = function () {
            if ($scope.selectedTreatmentDetails == null || $scope.selectedTreatmentDetails.length == 0)
                return;
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++) {
                if (($scope.selectedTreatmentDetails[i].type == $scope.STEP_TYPES.IRRADIATION || 
                    $scope.selectedTreatmentDetails[i].type == $scope.STEP_TYPES.IMBIBITION)&& 
                    $scope.selectedTreatmentDetails[i].duration >= 60)
                    $scope.selectedTreatmentDetails[i].duration = $scope.roundTo($scope.selectedTreatmentDetails[i].duration / 60, 2);
            }
        }

        $scope.loadItemEnergy = function() {
            if ($scope.selectedTreatmentDetails == null || $scope.selectedTreatmentDetails.length == 0)
                return;
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++) {
                $scope.selectedTreatmentDetails[i].energy = $scope.roundTo(TreatmentService.toJoules(
                    ($scope.selectedTreatmentDetails[i].power * $scope.selectedTreatmentDetails[i].duration* 60)), 2);    
            }
            
        }

        $scope.isSaveEnabled = function() {
            return $scope.treatmentSummary.totalEnergy > 0 && $scope.treatmentSummary.totalEnergy <= $scope.MaxEnergy;
        }

        $scope.getTotalEnergyStyle = function() {
            var strRes = "height:40px; font-size: 30px; width:200px; font-weight:bold; border:3px solid ";
            if ($scope.isSaveEnabled)
                strRes += "#1F497D";
            else
                strRes += "#FF0000";
            return strRes;
        }

        $scope.startTreatment = function() {
            var app = [];
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++) {
                var itm = $scope.selectedTreatmentDetails[i]; 
                if (itm.duration != null)
                  itm.duration = itm.duration *60;
                if (itm.power != null)
                    itm.power = itm.power * 1000;
                app.push(itm);
               }
            $state.go('treatment', {protocol: $scope.protocolName, dresdamode: 1,
                      duration:0 , intensity:0, thickness:0, steps:0, cornVertexDistanceX:0, cornVertexDistanceY:0, cornVertexRadius:0, kerApex:0, spotDiameter:null,
                      diagnosis: -1, stepsDetails: JSON.stringify(app)
                });
        }

        $scope.saveTreatment = function() {
            var app = [];
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++) {
                var itm = $scope.selectedTreatmentDetails[i]; 
                if (itm.duration != null)
                  itm.duration = itm.duration *60;
                if (itm.power != null)
                    itm.power = itm.power * 1000;
                if (itm.type == $scope.STEP_TYPES.IRRADIATION && itm.isPulsed) {
                    itm.irradiationTime = $scope.roundTo($scope.treatmentSummary.pulsed.irradiationTime, 2);
                    itm.pauseTime = $scope.roundTo($scope.treatmentSummary.pulsed.pauseTime, 2);
                }
                //VERIFICO SE C'E' UNA PAUSA PRIMA DEL FINE TRATTAMENTO, NEL CASO LA SALTO NON SALVANDOLA
                if (itm.type == $scope.STEP_TYPES.PAUSE && i== $scope.selectedTreatmentDetails.length -2) {
                   continue;
                    }
                else
                    app.push(itm);
            }
            TreatmentService.saveFullCustomTreatment($scope.protocolName, app);
            $scope.recalcStepDetails();
            alert($translate.instant('SelectProtocol.FullCustomOptions.TreatmentSaved'));
        }

        $scope.loadTreatmentDetails = function () {
            $scope.selectedTreatmentDetails = [];
            if ($scope.selectedIndex === null)
                return;
            $scope.selectedTreatmentDetails = TreatmentService.getFullCustomTreatmentDetails($scope.protocolName);
            //Verifico se è richiesta la pulsata
            $scope.isPulsedEnabled = false;
            if ($scope.selectedTreatmentDetails == null)
                return;
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++) {
                $scope.isPulsedEnabled |= $scope.selectedTreatmentDetails[i].isPulsed;
                if ($scope.isPulsedEnabled && $scope.selectedTreatmentDetails[i].isPulsed) {
                    $scope.treatmentSummary.pulsed.irradiationTime = $scope.selectedTreatmentDetails[i].irradiationTime;
                    $scope.treatmentSummary.pulsed.pauseTime = $scope.selectedTreatmentDetails[i].pauseTime;
                }
            }
            
        }

        $scope.recalcStepDetails = function () {
            $scope.loadTreatmentDetails();
            $scope.convertuWtomW();
            $scope.convertSecondsToMinutes();

            $scope.loadTreatmentDuration();
            $scope.loadNumSteps();
            $scope.loadMeanIntensity();
            $scope.loadItemEnergy();
            $scope.loadTreatmentImbibition();
            $scope.treatmentSummary.totalEnergy = $scope.roundTo(TreatmentService.toJoules($scope.treatmentSummary.totalDuration * 60 *
                                        $scope.treatmentSummary.meanIntensity), 2);
        }

        $scope.isNumber = function(n) {
            if (n== null)
           return false;
                if (n.endsWith('.'))
                   return false;
                 return !isNaN(parseFloat(n)) && isFinite(n) ;
               }

        $scope.changeStatusPulsed = function() {
            $scope.isPulsedEnabled = ! $scope.isPulsedEnabled;
            $scope.recalcSteps();
        }

        $scope.checkPausePulse = function (val) {
            var posPoint = val.indexOf('.');
            var retVal = '';
            if (posPoint == -1)
              retVal = val.substring(0, 2);
            else
              retVal = val.substring(0, 2+posPoint);
            if ($scope.isNumber(val)) {
              var x = Number (val);
              if (x < 0)
                retVal = 1;
              else if (x > 10)
                retVal = 10;
            }
            return retVal;
        }

        $scope.checkPause = function () {
            if ($scope.treatmentSummary.pulsed.pauseTime)
              $scope.treatmentSummary.pulsed.pauseTime = $scope.checkPausePulse($scope.treatmentSummary.pulsed.pauseTime);
              
        }

        $scope.checkIrradiation = function () {
            if ($scope.treatmentSummary.pulsed.irradiationTime)
                $scope.treatmentSummary.pulsed.irradiationTime = $scope.checkPausePulse($scope.treatmentSummary.pulsed.irradiationTime);
        }

        $scope.init = function() {
            ShutdownService.setOperation(ShutdownService.OPERATIONS.SELECT_PROTOCOL);
            $scope.protocolName = $stateParams.name;
            $scope.isPulsedVisible = TreatmentService.isPulsedAvailable();
            $rootScope.$on('hardwareStatus', function (event, message) {
                $scope.isPulsedVisible= TreatmentService.isPulsedAvailable();                
             });

            if ($stateParams.name == null)
                return;
             $scope.recalcStepDetails();
        }

        $scope.init();

    	
	}
);
