'use strict';

angular.module('megaride')
.service('MegarideService', function($translate, DiagnosisService){
            	
        var MegarideService = {}

        var opzioni = [];

        var r=0.45;
        // raggio standard della cornea espresso in cm  

        var I0=1.2;
        // Intensità di riferimento e intensità minima efficace espressa in mW/cm2  

        var S = Math.PI*r*r;
        // area della superficie corneale, espressa in cm2  

        var p=0.114;
        // probabilità di scissione di una molecola di riboflavina  

        //double gamma=0.55;
        var gamma = 812492;
        // costante gamma espressa in m2 s-2  

        var epsilon=13100/376.36;
        // costante di assorbimento molare della riboflavina, espressa in mg-1 cm2  

        var alfa=7000/376.36;
        // costante di assorbimento molare del lumicromo, espressa in mg-1 cm2 riferita ad una mole di riboflavina  

        //double beta=0.22;
        var beta=19.068;
        // costante di assorbimento della cornea nuda, espressa in cm-1  

        var W=0.018;
        // quantità media di riboflavina assorbita da una cornea, espressa in mg  

        var d0=0.05;
        // spessore centrale corneale standard, espresso in cm  

        var delta_t=0.01;
        // passo temporale adimensionale  

        var delta_z=0.01;
        // passo spaziale adimensionale  

        var t_MAX=20*60;
        // durata massima del trattamento (20 min) espresso in s  

        var t0=t_MAX;
        // tempo di riferimento, espresso in s  

        var E0=I0*t0*S/1000;
        // energia di riferimento, espressa in J  

        var Id=0.35;
        // Intensità massima che può raggiungere la retina, espressa in mW/cm2  

        var punti= parseInt(1/delta_z);
        // numero di punti di campionamento in cornea  

        var istanti= parseInt(1/delta_t);
        // numero di istanti di campionamento del trattamento  
        
        
        //var R_A_custom = [][];
        var R_A_custom = [];
        // protocollo custom: matrice della distribuzione di densità di riboflavina in cornea in funzione del tempo e della profondità in cornea  

        //var I_custom = [][];
        var I_custom = [];
        // protocollo custom: matrice della distribuzione dell'intensità del fascio UV in cornea, in funzione del tempo e della profondità in cornea  

        //var R_A_Mega = [][];
        var R_A_Mega = [];
        // protocollo Megaride: matrice della distribuzione di densità di riboflavina in cornea in funzione del tempo e della profondità in cornea  

        //var I_Mega = [][];
        var I_Mega = [];
        // protocollo Megaride: matrice della distribuzione dell'intensità del fascio UV in cornea, in funzione del tempo e della profondità in cornea  

        var I0_custom = [];
        // protocollo custom: insieme di valori di intensità costanti del fascio UV  

        //var I0_Mega = [][];
        var I0_Mega = [];
        // protocollo Megaride: vettore dell'andamento temporale dell'intensità del fascio UV generato [mW/cm2][s] 

        var deposito;
        // variabile di comodo  

        var d;
        // spessore corneale centrale in cm del paziente trattato  

        var t_custom = [];



        MegarideService.init = function() {
           /* R_A_custom = new double [101][101];
         // protocollo custom: matrice della distribuzione di densità di riboflavina in cornea in funzione del tempo e della profondità in cornea  

        I_custom = new double [101][101];
         // protocollo custom: matrice della distribuzione dell'intensità del fascio UV in cornea, in funzione del tempo e della profondità in cornea  

        R_A_Mega = new double [101][101];
         // protocollo Megaride: matrice della distribuzione di densità di riboflavina in cornea in funzione del tempo e della profondità in cornea  

        I_Mega = new double [101][101];
         // protocollo Megaride: matrice della distribuzione dell'intensità del fascio UV in cornea, in funzione del tempo e della profondità in cornea  

        I0_custom = new double [10];
         // protocollo custom: insieme di valori di intensità costanti del fascio UV  

        I0_Mega = new double[101][2];
         // protocollo Megaride: vettore dell'andamento temporale dell'intensità del fascio UV generato [mW/cm2][s] 

        t_custom= new double [10];
         // tempi di esposizione  */
        for(var i=0; i<=istanti; i++) {
            I_Mega[i] = new Array();
            I_Mega[i][punti]=Id/I0;
            }
        }

       
       MegarideService.setSpessoreCorneale = function(sp) {
        d = sp;
        opzioni = [];

        // Inizializzazione delle matrici 
        R_A_Mega[0] = new Array();
        R_A_custom[0] = new Array();
        
        for(var j=0;j<=punti;j++) {
            R_A_Mega[0][j]=1;
            R_A_custom[0][j]=1;
            I_Mega[0][punti-j]=I_Mega[0][punti]*Math.exp((epsilon*W/(S*d0)+beta)*j*d*delta_z);
            }
        //Fine inizializzazione matrici
        
        // Profilo di intesità del protocollo Megaride
        for(var i=1;i<=istanti;i++) {
            R_A_Mega[i] = new Array();
            for(var j=0;j<=punti;j++)   {
                R_A_Mega[i][j]=R_A_Mega[i-1][j]+(-1000000*epsilon*p/gamma*E0/S*I_Mega[i-1][j]*
                                        R_A_Mega[i-1][j])*delta_t;
                                }
            for(var j=0;j<=punti;j++)   {
                if(j>0) {
                        I_Mega[i][punti-j]= I_Mega[i][punti-j+1]+
                                delta_z*((epsilon-alfa)*R_A_Mega[i][punti-j+1]*
                                    W/S*d/d0+alfa*W/S*d/d0+beta*d)*I_Mega[i][punti-j+1];
                    }
                }
            }

        // inizializzazione 
        for(var i=0;i<=9;i++) {
            I0_custom[i]=0;
            t_custom[i]=0;
            }
   

        // Introduco un nuovo indice di comodo per non confonderlo con k usato più avanti 
        var n=0;


        I0_custom[0]=parseInt(10*I_Mega[0][0]);
        I0_custom[0] = I0_custom[0]/10;
        while(n<9) {
            if(I0_custom[n]>(I0+0.1)) {
                I0_custom[n+1]=I0_custom[n]-0.1;
                deposito=n;
                n++;
                }
            else {
                 n=10;
                 }
            }
        //il vettore delle I0_custom possibili contiene tutti i valori possibili efficaci
       
        // Introduco un indice di comodo k 
        var k;

        // Calcolo valori del protocollo custom 
        I_custom[0] = new Array();
        for(k=0;k<=9;k++) {
            if(I0_custom[k]>0) {
                t_custom[k]=istanti*delta_t;
                // inizializzazione vettore intensità custom 
                for(var j=0;j<=punti;j++)   {
                    I_custom[0][j]=I0_custom[k]*Math.exp(-(epsilon*W/(S*d0)+beta)*j*d*delta_z);
                    }
                for(var i=1;i<=istanti;i++) {
                    I_custom[i] = new Array();
                    I_custom[i][0]=I0_custom[k];
                    R_A_custom[i] = new Array();
                    for(var j=0;j<=punti;j++)  {
                        R_A_custom[i][j]=R_A_custom[i-1][j]+
                            (-1000000*epsilon*p/gamma*E0/S*I_custom[i-1][j]*R_A_custom[i-1][j])*delta_t;
                        }
                    for(var j=0;j<=punti;j++)  {
                        if(j<punti)  {
                            I_custom[i][j+1]=I_custom[i][j]+
                                (-delta_z*((epsilon-alfa)*R_A_custom[i][j]*W/S*d/d0+alfa*W/S*d/d0+beta*d)*I_custom[i][j]);
                            }
                        if((I_custom[i][punti]>Id/I0)&&(I_custom[i-1][punti]<=Id/I0))   {
                            t_custom[k]=i*delta_t;
                            } 
                        }
                    }
                }
            }

         // Ricostruzione dei valori dimensionali delle intensità in mW/cm2 e del tempo in secondi 
        // vettori dei trattamenti custom 

        for(k=0;k<=9;k++)  {
            I0_custom[k]*=I0;
            t_custom[k]*=t0;
            }
         
        // vettore del trattamento Megaride 

        for(var i=0;i<=istanti;i++) {
            I0_Mega[i] = new Array();
            I0_Mega[i][0]=I_Mega[i][0]*I0;
            I0_Mega[i][1]=i*t0*delta_t;
            }        


        for(k=0;k<deposito;k++)  {
            opzioni.push({intensity:I0_custom[k], time: t_custom[k]});
            //console.log((k+2)+"Intensità  "+I0_custom[k]+" per tempo "+t_custom[k]);
            } 

       }

       //Restituisce le opzioni disponibili per lo spessore corneale inserito
       MegarideService.getOptions = function() {
         return opzioni;
       }

       /***
       VECCCHIE PROPOSTE
       ***/
       MegarideService.getProposals = function (thickness) {
        thickness = Number(thickness);
        var proposals = [];
        if (thickness < 200)
            return proposals;

        /*var lessSignificative = thickness % 10;
        thickness = Math.floor(thickness /10) *10;
        if (lessSignificative >5)
            thickness = thickness + 10;*/

        if (thickness >= 200 && thickness <250) {
                proposals.push({"intensity": 2.0, "duration": 9, "spotDiameter": 4.6});
                //proposals.push({"intensity": 1.9, "duration": 10, "spotDiameter": 4.8});
                //proposals.push({"intensity": 1.8, "duration": 11, "spotDiameter": 5.0});
            }
        else if (thickness >= 250 && thickness < 350) {
                proposals.push({"intensity": 2.3, "duration": 11, "spotDiameter": 4.3});
                //proposals.push({"intensity": 2.2, "duration": 12, "spotDiameter": 4.4});
                //proposals.push({"intensity": 2.1, "duration": 13, "spotDiameter": 4.6});
            }
        else if (thickness >= 350 && thickness < 400) {
                proposals.push({"intensity": 2.4, "duration": 12, "spotDiameter": 4.1});
                //proposals.push({"intensity": 2.2, "duration": 14, "spotDiameter": 4.3});
                //proposals.push({"intensity": 1.9, "duration": 16, "spotDiameter": 4.5});
            }

       else if (thickness >= 400 && thickness < 450) {
                proposals.push({"intensity": 2.7, "duration": 13, "spotDiameter": 3.9});
                //proposals.push({"intensity": 2.6, "duration": 14, "spotDiameter": 4.0});
                //proposals.push({"intensity": 2.5, "duration": 15, "spotDiameter": 4.1});
            }
       
       else if (thickness >= 450 && thickness < 500) {
                proposals.push({"intensity": 2.8, "duration": 12.5, "spotDiameter": 3.5});
                //proposals.push({"intensity": 2.6, "duration": 14, "spotDiameter": 3.7});
                //proposals.push({"intensity": 2.4, "duration": 15, "spotDiameter": 3.9});
            }

        else {
                proposals.push({"intensity": 2.9, "duration": 12, "spotDiameter": 3.2});
                //proposals.push({"intensity": 2.7, "duration": 13, "spotDiameter": 3.3});
                //proposals.push({"intensity": 2.5, "duration": 14, "spotDiameter": 3.4});
        }

        return proposals;
       }

       MegarideService.getProposal = function (thickness, kerApex, CornealVertexDistance, diagnosis) {
        /*var proposals = MegarideService.getProposals(thickness);
        if (proposals.length > 0)
            return proposals[0];*/
        thickness = Number(thickness);
        CornealVertexDistance = Number(CornealVertexDistance);
        kerApex = Number(kerApex);
        if(isNaN(thickness))
            return null;

        switch (diagnosis) {
                //CHERATOCONO, ECTASIE SECONDARIE IATROGENE POST-LASIK O PRK
                case 0:
                case 1:
                        if (thickness<200 || thickness >600)
                            return null;
                        if (isNaN(CornealVertexDistance) || isNaN(kerApex))
                            return null;
                        if (CornealVertexDistance <= 3) {
                                if (thickness <250)
                                    return {"intensity": 2.4, "duration": 13, "spotDiameter": 4.5};
                                else if (thickness >=250 && thickness <350)
                                    return {"intensity": 2.6, "duration": 14, "spotDiameter": 4.5};
                                else if (thickness >= 350 && thickness <400)
                                    return {"intensity": 2.7, "duration": 14.5, "spotDiameter": 5};
                                else if (thickness >= 400 && thickness <450)
                                    return {"intensity": 2.8, "duration": 15, "spotDiameter": 5};
                                else if (thickness >= 450 && thickness < 500)
                                    return {"intensity": 2.8, "duration": 15, "spotDiameter": 5.5};
                                else if (thickness >= 500)
                                    return {"intensity": 2.9, "duration": 15, "spotDiameter": 5.5};
                                else return null;
                            }
                        else {
                                if (thickness <250)
                                    return {"intensity": 2.4, "duration": 13, "spotDiameter": 5.5};
                                else if (thickness >=250 && thickness <350)
                                    return {"intensity": 2.6, "duration": 14, "spotDiameter": 5.5};
                                else if (thickness >= 350 && thickness <400)
                                    return {"intensity": 2.7, "duration": 14.5, "spotDiameter": 6};
                                else if (thickness >= 400 && thickness <450)
                                    return {"intensity": 2.8, "duration": 15, "spotDiameter": 6};
                                else if (thickness >= 450 && thickness < 500)
                                    return {"intensity": 2.8, "duration": 15, "spotDiameter": 6.5};
                                else if (thickness >= 500)
                                    return {"intensity": 2.9, "duration": 15, "spotDiameter": 6.5};
                                else return null;
                        }
                        break;
                //DEGENERAZIONE PELLUCIDA (restituisco le proposte di cui al caso precedente dopo essermi accertato che la distanza dal vertice corneale sia > 3)
                case 2:        
                        if (CornealVertexDistance <= 3)
                            return null;
                        return MegarideService.getProposal(thickness, kerApex, CornealVertexDistance, 1);
               
                //MIOPIA
                case 3:
                        //Questo caso va trattato nel controller
                        return null;

                //ECTASIA SECONDARIA POST RK (restituisco le proposte di cui al primo caso ma cambiandone il diametro
                case 4:
                        if (isNaN(CornealVertexDistance)) 
                            CornealVertexDistance = 3;
                        if (isNaN(kerApex))
                            kerApex = 50;
                        var prop = MegarideService.getProposal(thickness, kerApex, CornealVertexDistance, 1);
                        if (prop != null)
                            prop.spotDiameter = 6;
                        return prop;

                default: return null;
            }
        
        

       }


	    MegarideService.init();
        
        return MegarideService;
        
    })