function isWindows () {
  return process.platform != "linux";
}

function execCommand (command) {
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    
} 

function shutDown () {
  var command = 'shutdown -p';
  if (!isWindows())
	command = "shutdown now"
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    
}

function restart () {
  var command = 'shutdown -r';
  if (!isWindows())
	command = "shutdown -r now"
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    
}

function switchUser () {
  var command = 'shutdown /l';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    
} 

function beep() {
  if (!isWindows()) {
     /*var player = require('play-sound')(opts = {});
     player.play('tone.mp3', function(err){
	   if (err) throw err
        })*/
        const spawn = require('child_process').spawn;
        const bat = spawn('play', ['tone.mp3']);
        bat.stdout.on('data', (data) => {
          console.log(data);
        });
    
        bat.stderr.on('data', (data) => {
          console.log(data);
        });
    
        bat.on('exit', (code) => {
          console.log(`Beep done`);
          //console.log(`Child exited with code ${code}`);    
        });        

    }
  else {
	  const spawn = require('child_process').spawn;
	  const bat = spawn('cmdmp3win', ['tone.mp3']);
	  bat.stdout.on('data', (data) => {
	    console.log(data);
	  });

	  bat.stderr.on('data', (data) => {
	    console.log(data);
	  });

	  bat.on('exit', (code) => {
	    console.log(`Beep done`);
	    //console.log(`Child exited with code ${code}`);    
	  });
	}
}


function startGUI() {
  const spawn = require('child_process').spawn;
  const bat = spawn('node_modules\\nw\\nwjs\\nw', ['.']);
  bat.stdout.on('data', (data) => {
    console.log(data);
  });

  bat.stderr.on('data', (data) => {
    console.log(data);
  });

  bat.on('exit', (code) => {
    console.log(`Child exited with code ${code}`);
    process.exit();
  });

}


function openNetworkConfiguration() {
  var command = 'ncpa.cpl';
  if (!isWindows()) {
	command = 'nm-connection-editor';
	}
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    

}

function focusTeamViewer() {
  var command = 'wmctrl -a TeamViewer';  
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    

}


function openRemoteAssistance() {
  //var command = 'LiveLetN';
  var command = 'teamviewer';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);            
          }
          else {
            focusTeamViewer();
          }
      });    

}

function showKeyboard() {
  var command = 'TabTip';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    

}

function addPrinter () {
  var command = 'rundll32 printui.dll,PrintUIEntry /il';
  if (!isWindows())
     command = 'system-config-printer';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    

}

function openPrinters () {
  var command = 'control printers';
  if (!isWindows())
     command = 'system-config-printer';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      }); 
}

function openDeviceManager () {
  var command = 'devmgmt.msc';
  if (!isWindows())
    command = 'hardinfo';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });
}

function openCommandPrompt () {
  var command = 'start cmd';
  if (!isWindows())
    command = 'gnome-terminal';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });
}

function openTaskManager () {
  var command = 'start taskmgr';
  if (!isWindows())
    command = 'gnome-system-monitor';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });
}

function printFile (path, jobName) {
  var command = 'lpt '+path;
  if (!isWindows()) {
    command = 'lp '+path;
    if (jobName != null)
    command += ' -t "'+jobName+'"';
  }
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });
}


function openManagement() {
  const spawn = require('child_process').spawn;
  const bat = spawn('.\\nwjs\\nw', ['.\\management\\.']);
  bat.stdout.on('data', (data) => {
    console.log(data);
  });

  bat.stderr.on('data', (data) => {
    console.log(data);
  });

  bat.on('exit', (code) => {
    console.log(`Child exited with code ${code}`);
    process.exit();
  });

}



const isOnline = require('is-online');

function checkIsOnline (callBack) {
  isOnline().then(online => {
    callBack(online);
  });
}

function getBrightness(success, error) {
  if (!isWindows()) {
        const spawn = require('child_process').spawn;
        const bat = spawn('uvcdynctrl',  ['-g', 'Brightness']);
        bat.stdout.on('data', (data) => {
          console.log(data);
          if (success != null) 
            success(data, false);
        });
    
        bat.stderr.on('data', (data) => {
          console.log(data);
          error(data, true);
        });
    
        bat.on('exit', (code) => {
          console.log(`done`);          
        });        

    }
  }

function setBrightness(val, success, error) {
  if (!isWindows()) {
        const spawn = require('child_process').spawn;
        const bat = spawn('uvcdynctrl',  ['-s', 'Brightness', ''+val]);
        bat.stdout.on('data', (data) => {
          console.log(data);
          if (success != null)  
            success(data, false);
        });
    
        bat.stderr.on('data', (data) => {
          console.log(data);
          if (error != null)
            error(data, true);
        });
    
        bat.on('exit', (code) => {
          console.log(`done`);          
        });        

    }
  }

  function getSaturation(success, error) {
    if (!isWindows()) {
          const spawn = require('child_process').spawn;
          const bat = spawn('uvcdynctrl',  ['-g', 'Saturation']);
          bat.stdout.on('data', (data) => {
            console.log(data);
            if (success != null) 
              success(data, false);
          });
      
          bat.stderr.on('data', (data) => {
            console.log(data);
            error(data, true);
          });
      
          bat.on('exit', (code) => {
            console.log(`done`);          
          });        
  
      }
    }

  function setSaturation(val, success, error) {
    if (!isWindows()) {
          const spawn = require('child_process').spawn;
          const bat = spawn('uvcdynctrl',  ['-s', 'Saturation', ''+val]);
          bat.stdout.on('data', (data) => {
            console.log(data);
            if (success != null) 
              success(data, false);
          });
      
          bat.stderr.on('data', (data) => {
            console.log(data);
            if (error != null)
              error(data, true);
          });
      
          bat.on('exit', (code) => {
            console.log(`done`);          
          });        
  
      }
    }

  function getContrast(success, error) {
      if (!isWindows()) {
            const spawn = require('child_process').spawn;
            const bat = spawn('uvcdynctrl',  ['-g', 'Contrast']);
            bat.stdout.on('data', (data) => {
              console.log(data);
              if (success != null) 
                success(data, false);
            });
        
            bat.stderr.on('data', (data) => {
              console.log(data);
              error(data, true);
            });
        
            bat.on('exit', (code) => {
              console.log(`done`);          
            });        
    
        }
      }

  function setContrast(val, success, error) {
      if (!isWindows()) {
            const spawn = require('child_process').spawn;
            const bat = spawn('uvcdynctrl',  ['-s', 'Contrast', ''+val]);
            bat.stdout.on('data', (data) => {
              console.log(data);
              if (success != null) 
                success(data, false);
            });
        
            bat.stderr.on('data', (data) => {
              console.log(data);
              if (error != null)
                error(data, true);
            });
        
            bat.on('exit', (code) => {
              console.log(`done`);          
            });        
    
        }
      }

  function getSharpness(success, error) {
      if (!isWindows()) {
            const spawn = require('child_process').spawn;
            const bat = spawn('uvcdynctrl',  ['-g', 'Sharpness']);
            bat.stdout.on('data', (data) => {
              console.log(data);
              //success(data, false);
            });
        
            bat.stderr.on('data', (data) => {
              console.log(data);
              error(data, true);
            });
        
            bat.on('exit', (code) => {
              console.log(`done`);          
            });        
    
        }
      }

  function setSharpness(val, success, error) {
      if (!isWindows()) {
            const spawn = require('child_process').spawn;
            const bat = spawn('uvcdynctrl',  ['-s', 'Sharpness', ''+val]);
            bat.stdout.on('data', (data) => {
              console.log(data);
              if (success != null) 
                success(data, false);
            });
        
            bat.stderr.on('data', (data) => {
              console.log(data);
              if (error != null)
                error(data, true);
            });
        
            bat.on('exit', (code) => {
              console.log(`done`);          
            });        
    
        }
      }
      
      
  function setCameraParameters(Brightness, Saturation,  Contrast, Sharpness, success, error) {
        if (!isWindows()) {
              const spawn = require('child_process').spawn;
              const bat = spawn('uvcdynctrl',  ['-s', 'Brightness', ''+Brightness, 'Saturation', ''+Saturation,'Contrast', ''+Contrast,'Sharpness', ''+Sharpness]);
              bat.stdout.on('data', (data) => {
                console.log(data);
                if (success != null) 
                  success(data, false);
              });
          
              bat.stderr.on('data', (data) => {
                console.log(data);
                if (error != null)
                  error(data, true);
              });
          
              bat.on('exit', (code) => {
                console.log(`done`);          
              });        
      
          }
        }

  function configCaribou() {
      var command = '';
      if (!isWindows())
        command = "firefox https://extensions.gnome.org/extension/1326/block-caribou/";
      const exec = require('child_process').exec;
          const child = exec(command,
            (error, stdout, stderr) => {
              console.log(`stdout: ${stdout}`);
              console.log(`stderr: ${stderr}`);
              if (error !== null) {
                console.log(`exec error: ${error}`);
              }
          });  
      
      }

  function switchOffWifi () {
        var command = 'start taskmgr';
        if (!isWindows())
          command = 'nmcli radio wifi off';
        const exec = require('child_process').exec;
            const child = exec(command,
              (error, stdout, stderr) => {
                console.log(`stdout: ${stdout}`);
                console.log(`stderr: ${stderr}`);
                if (error !== null) {
                  console.log(`exec error: ${error}`);
                }
            });
      }


  function switchOnWifi () {
        var command = 'start taskmgr';
        if (!isWindows())
          command = 'nmcli radio wifi on';
        const exec = require('child_process').exec;
            const child = exec(command,
              (error, stdout, stderr) => {
                console.log(`stdout: ${stdout}`);
                console.log(`stderr: ${stderr}`);
                if (error !== null) {
                  console.log(`exec error: ${error}`);
                }
            });
      }

  function WifiStatus (callback) {
        var command = 'start taskmgr';
        if (!isWindows())
          command = 'nmcli radio wifi';
        const exec = require('child_process').exec;
            const child = exec(command,
              (error, stdout, stderr) => {
                console.log(`stdout: ${stdout}`);
                console.log(`stderr: ${stderr}`);
                if (error !== null) {
                  console.log(`exec error: ${error}`);
                }
                else 
                  callback({wifiEnabled:stdout.includes("enabled")})

            });
      }
  
  function getFileChecksum(filePath, success, error) {
    if (!isWindows()) {
          const spawn = require('child_process').spawn;
          const bat = spawn('sha256sum',  [filePath]);
          bat.stdout.on('data', (data) => {
            if (success != null)  {
              var checksum = (data+"").split(" ");
              success(checksum[0], false);
            }
          });
      
          bat.stderr.on('data', (data) => {
            console.log(data);
            error(data, true);
          });
      
          bat.on('exit', (code) => {
            console.log(`done`);          
          });        
  
      }
    }


exports.shutDown = shutDown;
exports.restart = restart;
exports.switchUser = switchUser;
exports.beep = beep;
exports.startGUI = startGUI;
exports.openNetworkConfiguration = openNetworkConfiguration;
exports.checkIsOnline = checkIsOnline;
exports.openRemoteAssistance = openRemoteAssistance;
exports.openManagement = openManagement;
exports.showKeyboard = showKeyboard;
exports.addPrinter = addPrinter;
exports.printFile = printFile;
exports.openPrinters = openPrinters;
exports.openDeviceManager = openDeviceManager;
exports.openCommandPrompt = openCommandPrompt;
exports.openTaskManager = openTaskManager;
exports.getBrightness = getBrightness;
exports.getSaturation = getSaturation;
exports.getContrast = getContrast;
exports.getSharpness = getSharpness;
exports.setBrightness = setBrightness;
exports.setSaturation = setSaturation;
exports.setContrast = setContrast;
exports.setSharpness = setSharpness;
exports.setCameraParameters = setCameraParameters;
exports.configCaribou = configCaribou;
exports.switchOnWifi = switchOnWifi;
exports.switchOffWifi = switchOffWifi;
exports.WifiStatus = WifiStatus;
exports.execCommand = execCommand;
exports.getFileChecksum = getFileChecksum;
