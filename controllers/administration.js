var fs;
try {
 	fs = require('fs')
 }
catch (ex) {
	fs = null;
}

var SystemCalls;
    try {
      SystemCalls = require('./modules/SystemCalls.js');
    }
    catch (err) {
    }


angular.module('megaride')    
.controller('CtrlAdministration', 
function ($scope, $stateParams, $rootScope, $state, $timeout, $mdDialog, $translate, DebugModeService, DatabaseService, CalibrationService, 
				HwCommunicationService, ConfigurationService, version, CameraService, UpdateService, GDPREnabled) 
	{
		$scope.sheets = [];
		$scope.sheets.push({name: "ControlPanel.Sheets.GENERAL", id : 0});
		$scope.sheets.push({name: "ControlPanel.Sheets.CONFIGURATION", id: 1});
		$scope.sheets.push({name: "ControlPanel.Sheets.CALIBRATION", id: 2});
		$scope.sheets.push({name: "ControlPanel.Sheets.REPORTS", id: 3});
		//$scope.sheets.push({name: "ControlPanel.Sheets.LOG", id: 4});		
		$scope.sheets.push({name: "ControlPanel.Sheets.DIAGNOSTICS", id: 4});		
		$scope.sheets.push({name: "ControlPanel.Sheets.REPORT", id: 5});
		if (GDPREnabled)
			$scope.sheets.push({name: "ControlPanel.Sheets.PRIVACY", id: 6});
		//$scope.sheets.push({name: "ControlPanel.Sheets.DIAGNOSTICS", id: 6});

		$scope.localCommit = '';
		$scope.remoteCommit = '';

		

		$scope.selectedSheet = $scope.sheets[0];

		$scope.configuration = {}; 
		$scope.systemProperties = {};
		$scope.statisticheTrattamenti = [];
		$scope.verificheCalibrazione = [];
		$scope.swVersion = version+ ' ('+ UpdateService.localCommit+')';
		$scope.isReadonly = true;
		$scope.numClick = 0;
		
		$scope.protocolStaticsFilter = 'TOTALE';
		$scope.protocolStaticsDetails = [];

		$scope.applicationLogsDetails = [];
		$scope.serialLogsDetails = [];

		$scope.applicationLogContent = '';
		$scope.serialLogContent = '';

		$scope.configReport = {};
		$scope.pathLogoStruttura = '';
		$rootScope.userProfile = '';
		$scope.calibrationHref = '';
		//$rootScope.isAdmin = true;
		$scope.QRCode = null;
		$scope.privacy = [];
		$scope.privacy.isCRUDUser = false;  //Questa variabile viene messa a true quando si effettua operazione CRUD su utenti
		$scope.privacy.userSelected = [];  //Questa variabile contiene l'utente selezionato nella griglia


		$scope.onclick = function () {
            $scope.numClick ++;
            if ($scope.numClick == 3 && $scope.isSuperProfile()) {
            	if ($scope.selectedSheet.id == 0)
                	$scope.isReadonly = false;
                else if ($scope.selectedSheet.id == 2) 
                	CalibrationService.insertFakeVerify();

            }
            $timeout( function() {$scope.numClick = 0}, 3000);            
        }

        $scope.doResetEEPROM = function () {
        	HwCommunicationService.initEEprom();
        }

        $scope.importCalib = function() {
        	document.getElementById('inputCalibFile').click();
        }

        $scope.deleteFolderRecursive = function(path) {
		  if (fs == null)
        		return;
		  if (fs.existsSync(path)) {
		    fs.readdirSync(path).forEach(function(file, index){
		      var curPath = path + "/" + file;
		      if (fs.lstatSync(curPath).isDirectory()) { // recurse
			$scope.deleteFolderRecursive(curPath);
		      } else { // delete file
			fs.unlinkSync(curPath);
		      }
		    });
		    fs.rmdirSync(path);
		  }
	};

        $scope.resetDevice = function () {
		 DatabaseService.resetFabbrica();
		 CameraService.setParamsDefault();
	   $scope.deleteFolderRecursive("./log");
	   $scope.deleteFolderRecursive("./reports");
	   ConfigurationService.setLocale(null);
           alert("Reset di fabbrica eseguito");	
	   $state.go('splash');
	}

        $scope.importFromFile = function(sender) {
        	if (fs == null)
        		return;
        	//Controllo che il file esista
        	fs.exists(sender.value, function(exists){
        			if (exists) {
        				//Leggo il contenuto
        				fs.readFile(sender.value, function readFileCallback(err, data){
        					//se c'è un errore segnalo ed esco
        					if (err)  {
        						alert('Error');
        						return;
        					}
       						var obj = JSON.parse(data); 
        					CalibrationService.loadCalibrationFromValues(obj).then(
        						function (result) {
        							 if (result == 0) {
        							 	alert("Tabella importata correttamente");
        							 }
        							 else
        							 	alert("Errore durante importazione della tabella");	
        							}
        						);
        				});
        			}
        			else {
        				console.log("File NON trovato");	
        			}

        		});
        }
		

		$scope.getButtonClass = function (sheet) {
			var classButton = "md-raised";
			if (sheet.id == $scope.selectedSheet.id)
				classButton +=" md-primary";
			return classButton;
		}

		$scope.sheetChanged = function (sheet) {
			$scope.selectedSheet = sheet;
		}

		//CORPO CONTROLLER
		$scope.saveConfiguration = function() {
			 DebugModeService.saveConfiguration($scope.configuration);
			}

		$scope.saveProperties = function () {
			DatabaseService.setSystemProperties($scope.systemProperties);
		}

		$scope.isSheetVisible = function(sheet) {

			switch (sheet.id) {
										case 0:	return true;
										case 1: return $scope.isSuperProfile();
										case 2: return true;//$scope.isSuperSupportProfile() || $scope.isSuperProfile();
										case 3:	return true;
										//case 4: return $scope.isSuperSupportProfile() || $scope.isSuperProfile();
										case 4: return $scope.isSuperProfile();
										case 5: return $scope.configuration.showReport;
										case 6: return $stateParams.userProfile== 'superSupport' || $stateParams.userProfile == 'super' || $stateParams.userProfile == 'admin';
										default: return false;
			}
			/*if (sheet.id == 1 && !$scope.isSuperProfile())
				return false;
			if (sheet.id == 5 && !$scope.configuration.showReport)
				return false;
			return true;*/
		}


		$scope.apply = function () {
			try {
				 if (!$scope.$$phase)
                 	$scope.$apply();
                }
            catch (err) {
            }
		}

		$scope.updateSystemProperties = function () {
			$scope.configuration = DebugModeService.getConfiguration();
			var callBack = function (result) {
				$timeout($scope.systemProperties = result);
				//$scope.apply();
			}
			DatabaseService.getSystemProperties(callBack);
		}

		$scope.updateStatisticheTrattamenti = function () {
			$scope.statisticheTrattamenti = [];
			//$scope.statisticheTrattamenti.push({"Protocol": "TOTALE", Totale: 0});
			var totale = 0;
			var callBack = function (tx, result) {
				for (var i=0; i<result.rows.length; i++) {
					$scope.statisticheTrattamenti.push(result.rows[i]);
					//$scope.statisticheTrattamenti[0].Totale += result.rows[i].Totale;
					totale += result.rows[i].Totale;
				}
				$scope.statisticheTrattamenti.push({"Protocol": "TOTALE", Totale: totale});
				$scope.apply;
			}
			DatabaseService.getStatisticheTrattamenti(callBack);
		}

		$scope.updateStatisticheCalibrazione = function () {
			$timeout(function() {	//sostituzione $apply
				$scope.verificheCalibrazione = [];
				var callBack = function (tx, result) {
					for (var i=0; i<result.rows.length; i++) {
						$scope.verificheCalibrazione.push(result.rows[i]);					
					}
					//$scope.apply();
				 }
				 DatabaseService.getCalibrationChecks(callBack);
				});
		}


		$scope.updateSerialLogsDetails = function () {
			$timeout(function() {	//sostituzione $apply
				$scope.serialLogsDetails = [];
				var callBack = function (tx, result) {
					for (var i=0; i<result.rows.length; i++) {
						$scope.serialLogsDetails.push(result.rows[i]);
					}
					//$scope.apply;
				}
				DatabaseService.getDettaglioSerialLogs(callBack);
			});
		}		

		$scope.updateApplicationLogsDetails = function () {
			$scope.applicationLogsDetails = [];
			var callBack = function (tx, result) {
				for (var i=0; i<result.rows.length; i++) {
					$scope.applicationLogsDetails.push(result.rows[i]);
				}
				$scope.apply;
			}
			DatabaseService.getDettaglioApplicationLogs(callBack);
		}

		$scope.updateSettingsReferto = function () {
			$timeout(function() {	//sostituzione $apply
				$scope.configReport = {};
				var callBack = function (result) {
					$scope.configReport = result;
					//$scope.apply();
				}
				DatabaseService.getConfigReferto(callBack);
			});
		}

		$scope.getMsgEsitoVerifica = function(detail) {
			if (detail.esitoVerifica == 0)
				return "OK";
			else if (detail.esitoVerifica == 1)
				return $translate.instant('ControlPanel.Calibration.results.slightlyOutOfRange');
			else if (detail.esitoVerifica == 2)
				return $translate.instant('ControlPanel.Calibration.results.veryOutOfRange');
		 }

		$scope.getPercentageErrorCalibration = function (detail) {
		 	return (Math.abs((detail.potenzaEmessa - detail.potenzaMisurata)/detail.potenzaEmessa)*100).toFixed(2);
		}

		$scope.doCheckCalibration = function () {
			$state.go('verifyOptHead');
		}

		$scope.doCalibration = function () {
			$state.go('calibration');
		}



		$scope.formatLogData = function (data) {
			var find = "\n";
			var re = new RegExp(find, 'g');
			return data.replace(re, "\r");
		}

		$scope.selectSerialLogDetail = function (detail) {
			$scope.serialLogContent = detail.pathFile;
			if (fs == null)
				return;
			fs.readFile(detail.pathFile, 'utf8', function (err,data) {
				  if (err) 
					    $scope.serialLogContent = "ERRORE "+err;
				  else
				  	$scope.serialLogContent = data;
				  $scope.apply();
				});

		}

		$scope.selectApplicationLogDetail = function (detail) {
			$scope.applicationLogContent = detail.pathFile;
			if (fs == null)
				return;
			fs.readFile(detail.pathFile, 'utf8', function (err,data) {
				  if (err) 
					    $scope.applicationLogContent = "ERRORE "+err;
				  else
				  	$scope.applicationLogContent = $scope.formatLogData(data);
				  //$scope.apply();
				});
		}

		$scope.selectStatistcs = function (detail) {
			$timeout(function() {	//sostituzione $apply
				$scope.protocolStaticsFilter = detail.Protocol;
				var filterXDB = null;
				if ($scope.protocolStaticsFilter != 'TOTALE')
					filterXDB = $scope.protocolStaticsFilter;
				$scope.protocolStaticsDetails = [];
				var callBack = function (tx, result) {
					for (var i=0; i<result.rows.length; i++) {
						$scope.protocolStaticsDetails.push(result.rows[i]);
						$scope.protocolStaticsDetails[$scope.protocolStaticsDetails.length-1].power = result.rows[i].power / 1000;
					}
					//$scope.apply;
				}
				DatabaseService.getDettaglioStatisticheTrattamento(filterXDB, callBack);
			});
		}

		$scope.fileNameChanged = function (sender) {
			$timeout(function() {	//sostituzione $apply
				var f = sender.files[0];
				fs.copyFile(f.path, './images/logoRefertoUser', (err) => {
					if (err) throw err;
					UpdateService.replaceReportLogo(
						function (){
							document.getElementById('previewLogo').src = "./images/logoReferto?"+ new Date().getTime();;
							//$scope.apply();
						}
					);
				});
			});
			
		}

		$scope.saveSettingReferto = function () {
			DatabaseService.setConfigReferto($scope.configReport);
			if (fs == null)
				return;
			/*if (document.getElementById('logoXRefertoImg').files == null || document.getElementById('logoXRefertoImg').files.length == 0)
				return;
			var f = document.getElementById('logoXRefertoImg').files[0];
			fs.createReadStream(f.path).pipe(fs.createWriteStream("./images/logoReferto.jpg"));
			document.getElementById('previewLogo').src='images/logoReferto.jpg';*/
		}

		$scope.changeUser = function() {
			HwCommunicationService.sendSwitchUser();
		}

		$scope.goToHome = function() {
			$state.go('splash');
		}

		$scope.isSuperProfile = function () {
			return $rootScope.userProfile == "super";
		}

		$scope.isSuperSupportProfile = function () {
			return $rootScope.userProfile == "superSupport";
		}

		$scope.openDeviceManager = function() {
			if (SystemCalls != null)
				SystemCalls.openDeviceManager();
		}

		$scope.openRemoteAssistance = function () {
			if (SystemCalls == null)
                return;
            SystemCalls.openRemoteAssistance();  
		}

		$scope.openPrinterManagement = function() {
			if (SystemCalls != null)
				SystemCalls.openPrinters();
		}

		$scope.goToFullCustom = function () {
			$state.go('fullCustom', {edit: 1});
		}

		$scope.goToSerialTerminal = function () {
			$state.go('serialTerminal', {});
		}

		$scope.setConfigCamera = function () {
			$rootScope.configCamera = true;
		}

		$scope.setWifiDisabled = function () {
			$rootScope.disableWifiDuringEmission = false;
		}

		
		$scope.loadFromOpticalHead = function () {
			HwCommunicationService.sendVer().then(function(result){
				if (result == "")
					return;
				if (result == null || result.length == 0)
					return;

				var strApp;
				for (var i=0; i<result.length; i++) {
						strApp = result [i];
						if (strApp.indexOf("VER")>=0)
							break;
					}
				if (strApp.indexOf("VER")<0)
					return;
				var splittedString = strApp.split(",");
				$scope.systemProperties.serSkDriver = splittedString[2];
			});
			$scope.calibrationHref = "data:application/json;charset=utf-8,jsonString";
			CalibrationService.initCalibrationTable().then(
				function (result) {
					var TableJson = JSON.stringify(CalibrationService.getCalibTable());
			  		$scope.calibrationHref = "data:application/json;charset=utf-8,"+TableJson;
				});
		}

		//Consente di recuperare dal cloud i dati di targa del dispositivo
		$scope.getDataFromCloud = function (){
			$timeout(function() {	//sostituzione $apply
				$ationService.getDataFromCloud($scope.systemProperties.serCfxLinker, function (result){
					$scope.systemProperties = result;
					//$scope.apply();
				});
			});
		}

		$scope.storeCalibTableToCloud = function() {
			var serialDriver = "";
			var calibTable = {};

			HwCommunicationService.sendVer().then(function(result){
				if (result == "")
					return;
				if (result == null || result.length == 0)
					return;

				var strApp;
				for (var i=0; i<result.length; i++) {
						strApp = result [i];
						if (strApp.indexOf("VER")>=0)
							break;
					}
				if (strApp.indexOf("VER")<0)
					return;
				var splittedString = strApp.split(",");
				serialDriver = splittedString[2];

				CalibrationService.initCalibrationTable().then(
					function (result) {
						 calibTable = CalibrationService.getCalibTable();
						 ConfigurationService.saveCalibTableInCloud (serialDriver, calibTable);						  
					});
			});

		}

		$scope.configCaribou = function() {
			if (SystemCalls != null)
				SystemCalls.configCaribou();
		}

		

		$scope.initQRCode = function() {
			var typeNumber = 4;
			var errorCorrectionLevel = 'L';
			var qr = qrcode(typeNumber, errorCorrectionLevel);


			var currentLang = $translate.use();
			var linkBase = "";

			
			if (currentLang=="gb") {
				linkBase = "https://forms.gle/5X69tfFi96rZ8HxXA";
			}
			else if (currentLang=="it") {
				linkBase = "https://forms.gle/woRBJiBupy9voMWB7";
			}
			else if (currentLang=="cn") {
				linkBase = "https://forms.gle/nQF5A4ocpNxcuomD8";
			}
			else if (currentLang=="de") {
				linkBase = "https://forms.gle/4dTv4UhzcmYNgGETA";
			}
			else if (currentLang=="es") {
				linkBase = "https://forms.gle/ZW5F4CkhMg1EAPkPA";
			}
			else if (currentLang=="fr") {
				linkBase = "https://forms.gle/RzKMZ8KYmLcCvcth9";
			}
			else if (currentLang=="pl") {
				linkBase = "";
			}
			

			qr.addData(linkBase);
			qr.make();
			$scope.QRCode = qr.createImgTag(4, 16, '');
			//document.getElementById('placeHolder').innerHTML = qr.createImgTag();
			//IT: https://docs.google.com/forms/d/e/1FAIpQLSeT8xrO8phHRRL-6PBk4p6vflki664SYHX6P_0Lkcp049aoDg/viewform
			
		}


		$scope.gridUsers = {
			enableSorting: true,
			rowHeight:50,
			enableFiltering: true,
			paginationPageSizes: [5, 10, 15, 20],
			paginationPageSize: 10,
			columnDefs: [
	
					{ field: 'Name', displayName: $translate.instant('Report.Name'), enableFiltering: true}, 
					{ field: 'Surname', displayName: $translate.instant('Report.Surname') },
					{ field: 'Username', displayName: $translate.instant('ControlPanel.Username'), enableFiltering: true },
					{ field: 'Role',  displayName: $translate.instant('GDPR.Role'), enableFiltering: true },
					{ field: 'show', displayName:'', enableFiltering: false, enableSorting: false, enableHiding:false, 
					  cellTemplate: '<md-button class="md-icon-button md-accent" aria-label="Favorite" style="width:100px; height: 30px" '+
									  'ng-click="grid.appScope.editUser(row.entity)"><md-icon md-font-icon="far fa-file" class="fa s32 md-primary md-hue-2" '+
									  'style="font-size:40px; width:100px; '+
									'height: 100px; color: #6079a0"></md-icon></md-button>'+
						
									'<md-button class="md-icon-button md-accent" aria-label="Favorite" style="width:100px; height: 30px" '+
									  'ng-click="grid.appScope.deleteUser(row.entity)"><md-icon md-font-icon="far fa-trash" class="fa s32 md-primary md-hue-2" '+
									  'style="font-size:40px; width:100px; '+
									'height: 100px; color: #6079a0"></md-icon></md-button>'
						 }
					],
	
			onRegisterApi: function (gridApi) {
				$scope.grid1Api = gridApi;
				}
	
			};

			$scope.loadUsers = function () {
				$scope.users = [];
				var callBack = function (tx, result) {
						$scope.dataLoading = false;
						for (var i=0; i<result.rows.length; i++) {
							var user = {};
							user.ID = result.rows[i].ID;
							user.Name = result.rows[i].Name;
							user.Surname = result.rows[i].Surname;
							user.Username = result.rows[i].Username;
							user.Role = result.rows[i].Role;
							user.UserAlias = result.rows[i].UserAlias;
							user.Password = result.rows[i].Password;
							user.PasswordToBeChanged = result.rows[i].PasswordToBeChanged; 
							user.userDefault= result.rows[i].userDefault;
							user.passwordExpiryDate = result.rows[i].passwordExpiryDate;
							if (user.userDefault == 0) {
								user.PasswordValidity = DatabaseService.getDaysBeforeExpiry(user);
								if (user.PasswordValidity <= 0)
									user.PasswordToBeChanged = 1;
							}
							$scope.users.push(user);
						}
						$scope.gridUsers.data = $scope.users;
					}
				DatabaseService.getUsers(callBack);
			}

			$scope.passwordChanged = function () {
				$scope.privacy.userSelected.PasswordToBeChanged = 1;
			}

			$scope.setUserOperator = function () {
				$scope.privacy.userSelected.Role='operator';
				$scope.privacy.operator=true;
				$scope.privacy.admin=false;
			}

			$scope.setUserAdmin = function () {
				$scope.privacy.userSelected.Role='admin';
				$scope.privacy.operator=false;
				$scope.privacy.admin=true;
			}

			$scope.editUser = function(user) {
				$scope.privacy.userSelected = user;
				if ($scope.privacy.userSelected.Role == 'admin') 
				 	$scope.setUserAdmin();
				else if ($scope.privacy.userSelected.Role == 'operator')
					$scope.setUserOperator();
				$scope.privacy.isCRUDUser = true;
			}
			
			$scope.newUser = function () {
				$scope.privacy.userSelected.ID = -1;
				$scope.privacy.userSelected.Username = '';
				$scope.privacy.userSelected.Name = '';
				$scope.privacy.userSelected.Surname = '';
				$scope.privacy.userSelected.UserAlias = '';
				$scope.privacy.userSelected.Password = '';
				$scope.setUserOperator();
				$scope.privacy.userSelected.PasswordToBeChanged = 1;
				$scope.privacy.userSelected.userDefault = 0;
				$scope.privacy.isCRUDUser = true;
			}

			$scope.crudUserNameChanged = function (){
				$scope.privacy.ErrorMsg = '';
			}

			$scope.deleteUser = function (user) {
				var confirm = $mdDialog.confirm()
					.title($translate.instant('GDPR.CONFIRM_CANCEL_TITLE'))
					.textContent($translate.instant('GDPR.CONFIRM_CANCEL_TEXT'))
					.ok($translate.instant('Messages.OK'))
					.cancel($translate.instant('Messages.DISCARD'));
			
					$mdDialog.show(confirm).then(function(res) {
					if (res == true) {
						 DatabaseService.deleteUser(user);
						 $scope.loadUsers();
						}
					}, function() {
					
				});


			}

			$scope.saveCRUDUser = function() {
				//Verifico se è un nuovo utente
				if ($scope.privacy.userSelected.ID == -1) {
					//verifico che la username non sia in uso
					DatabaseService.checkCanAddUser($scope.privacy.userSelected, 
						function (can) {
							if (can) {
								 DatabaseService.addUser($scope.privacy.userSelected);
								 $scope.loadUsers();
								 $scope.privacy.isCRUDUser = false;
								}
							 else {
									$scope.privacy.ErrorMsg = $translate.instant('GDPR.userNameAlreadyInUse');
								}
							}
						);
				}
				//Se va aggiornato faccio update
				else {
					DatabaseService.updateUser($scope.privacy.userSelected);
					$scope.loadUsers();
					$scope.privacy.isCRUDUser = false;
				} 




				
			}

		$scope.exitCRUDUser = function () {
			$scope.privacy.isCRUDUser = false;
		}
	

		$scope.init = function () {
			$scope.updateSystemProperties();
			$scope.updateStatisticheTrattamenti();
			$scope.updateStatisticheCalibrazione();
			$scope.updateApplicationLogsDetails();
			$scope.updateSerialLogsDetails();
			$scope.updateSettingsReferto();
			$scope.loadFromOpticalHead();
			$scope.loadUsers();
			$rootScope.userProfile = $stateParams.userProfile;

			$scope.initQRCode();

			
			$rootScope.$on('updateSW', function (event, message) {
				$timeout(function() {	//sostituzione $apply
					$scope.updateAvailable = true;
					$scope.localCommit = UpdateService.localCommit;
					$scope.remoteCommit = UpdateService.remoteCommit;
					$scope.swVersion = version+ ' ('+ UpdateService.localCommit+')';
					//$scope.apply();
				});
			  });
		  
		  $rootScope.$on('branchUpdated', function (event, message) {
			$timeout(function() {	//sostituzione $apply
				$scope.localCommit = UpdateService.localCommit;
				$scope.swVersion = version+ ' ('+ UpdateService.localCommit+')';
				//$scope.apply();
			});
		  });

		  
		  $rootScope.$on('configurationChanged', function (event, message) {
			$scope.updateSystemProperties();
		  });
		}



		$scope.init();

		
	}
);
   
    	
