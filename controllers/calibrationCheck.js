//Questo controller serve a verificare la calibrazione della testa ottica

angular.module('megaride')    
.controller('CtrlCalibrationCheck', 
    function ($scope, $stateParams, $rootScope, $state, $mdDialog, $timeout, CalibrationService, HwCommunicationService, hotkeys, 
                TreatmentService, CameraService) {

    	$scope.state = {};
    	
        $scope.state.secondsUV = 60 * 5;
        $scope.state.idxCalibration = -1;
        $scope.valuesToCheck = [];
	$scope.valuePower = '';
	$scope.valuePowerEmitting = '';

        //Passaggio allo stato successivo
        $scope.goNext = function () {
            //$scope.gotoProtocolSelection();
            $scope.goToAdministrationPage();
        }

        $scope.goToAdministrationPage = function() {
            CalibrationService.setFocus(0).then(function (result) {
                TreatmentService.stopTreatment().then(function() {
                    $state.go('administration', {userProfile: $rootScope.userProfile});
                });
            });
        }

        //Va alla selezione del protocollo
        $scope.gotoProtocolSelection = function () {
            $state.go('selectProtocol');
        }

        
        $scope.apply = function() {
            if ($scope.$$phase) 
                return;
            try {
                $scope.$apply();
            }
            catch (err) {

            }
        }

       $scope.isInputVisible = function() {
	  return $scope.state.idxCalibration > -1 && $scope.state.idxCalibration < $scope.valuesToCheck.length;
	}

        $scope.getButtonText = function () {
            if ($scope.state.idxCalibration == -1)
                return "START";
            else if ($scope.state.idxCalibration >= $scope.valuesToCheck.length)
                return "END"
            else return "APPLY";
        }

        $scope.setPower = function () {
            var seconds = $scope.state.secondsUV;
            var power = $scope.valuePower*1000;
            CalibrationService.setFocus(0).then(function (result) {
                TreatmentService.stopTreatment().then(function() {
                        TreatmentService.setManualMode().then(function(){
                            TreatmentService.setManualModeParams(seconds, power).then(function() {
                                    TreatmentService.startTreatment().then(function(result){
                                        $scope.valuePowerEmitting = $scope.valuePower;
                                        if (($scope.valuesToCheck[$scope.state.idxCalibration]/1000) == $scope.valuePower) {
					     $scope.state.idxCalibration ++;	
					   }
                                        $scope.valuePower = $scope.valuesToCheck[$scope.state.idxCalibration]/1000;
                                        $scope.apply();
                                    });
                                })
                            })
                    }); 
                });    
        }

        $scope.btnClick = function () {
	   if ($scope.state.idxCalibration == -1){
                 $scope.state.idxCalibration ++;
                 $scope.valuePower = $scope.valuesToCheck[$scope.state.idxCalibration]/1000;
		 return;
		}

	    else if ($scope.state.idxCalibration >= $scope.valuesToCheck.length) {
		 TreatmentService.stopTreatment();
		 $scope.goNext();		 
		}
            else 
		$scope.setPower();

            $scope.apply();
        }

        $scope.stopButton = function () {
            TreatmentService.stopTreatment().then(function(result){
                $scope.goNext();
            }); 
        }

        $scope.initValuesToCheck = function () {
            $scope.valuesToCheck[0] = 1500;
            $scope.valuesToCheck[1] = 3000;
            $scope.valuesToCheck[2] = 5000;
            $scope.valuesToCheck[3] = 9000;
            $scope.valuesToCheck[4] = 12000;
            $scope.valuesToCheck[5] = 15000;
            $scope.valuesToCheck[6] = 18000;
            $scope.valuesToCheck[7] = 30000;
        }

        $scope.back = function() {
            CalibrationService.PowerOff().then(function (result) {
                $state.go('administration', {
                     userProfile: $rootScope.userProfile}
                    );
            });
        }
        
        $scope.init = function () {
            //INIZIALIZZAZIONE VARIABILI
            $scope.initValuesToCheck();
            $scope.state.numSteps = $scope.valuesToCheck.length;
            CalibrationService.setFocus(1).then(function (result) {
                    HwCommunicationService.setManualMode();
                });
            }
            CameraService.attachCamera();
            //$timeout(function() {HwCommunicationService.setManualMode(); }, TreatmentService.getCommandDelay());
        //}

        $scope.init();
	}
);
   
    	
