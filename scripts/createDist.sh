#!/bin/bash

rm -rf /home/cfxlinker/repo
cd /home/cfxlinker/
mkdir repo
cd repo

git clone https://carponte@bitbucket.org/carponte/cfxlinker-nwjslinux.git


cp -af /home/cfxlinker/cfxlinker/modules /home/cfxlinker/repo/cfxlinker-nwjslinux
cp -af /home/cfxlinker/cfxlinker/bower_components /home/cfxlinker/repo/cfxlinker-nwjslinux
cp -af /home/cfxlinker/cfxlinker/nw /home/cfxlinker/repo/cfxlinker-nwjslinux
cp -af /home/cfxlinker/cfxlinker/nwdebug /home/cfxlinker/repo/cfxlinker-nwjslinux


cd /home/cfxlinker/repo/cfxlinker-nwjslinux

commit=$(cd /home/cfxlinker/repo/cfxlinker-nwjslinux; git log -1 --format=%h)
echo $commit

cd /home/cfxlinker/repo 
tar cvfz /home/cfxlinker/cfxlinker-nwjslinux-$commit.tar.gz cfxlinker-nwjslinux/
rm -rf /home/cfxlinker/repo/cfxlinker-nwjslinux
mv /home/cfxlinker/cfxlinker-nwjslinux-$commit.tar.gz /home/cfxlinker/repo/
cp -af /home/cfxlinker/cfxlinker/scripts/autorun.sh /home/cfxlinker/repo/
zip /home/cfxlinker/cfxlinker-nwjslinux-$commit.zip *.*
mv /home/cfxlinker/cfxlinker-nwjslinux-$commit.zip /home/cfxlinker/Scrivania
rm -rf /home/cfxlinker/repo

dir=$(ls /media/cfxlinker/)
finalDir="/media/cfxlinker/"$dir/
SUB='cfxlinker'
mv /home/cfxlinker/Scrivania/cfxlinker-nwjslinux-$commit.zip $finalDir

