angular.module('megaride')    
.controller('CtrlFullCustom', 
    function ($scope, $stateParams, $rootScope, $state, $translate, $mdDialog, ShutdownService, LoggingService, TreatmentService, CalibrationService) {

        var STEP_TYPES = TreatmentService.getStepTypes();

        $scope.fullCustoms = [];
        $scope.selectedIndex = null;
        $scope.selectedTreatmentDetails = [];
        $scope.options = {};
        $scope.canDoTreatment = false;



        $scope.back = function() {
             $state.go('selectProtocol');
        }

        $scope.setSelected = function (index) {
            if ($scope.selectedIndex === null) {
                $scope.selectedIndex = index;
              }
            else if ($scope.selectedIndex === index) {
                $scope.selectedIndex = null;
              }
            else {
                $scope.selectedIndex = index;
              } 
            $scope.loadTreatmentDetails();
        }


        $scope.getTreatmentItemDescription = function(step) {
            result = "";
            switch (step.type) {
                case STEP_TYPES.IRRADIATION  :   
                                                msg =  "Treatment.Steps.Irradiation";
                                                break;
                case STEP_TYPES.IMBIBITION   :   
                                                msg =  "Treatment.Steps.Imbibition";
                                                break;
                case STEP_TYPES.PAUSE    :   
                                                msg = "Treatment.Steps.Pause";
                                                break;
                case STEP_TYPES.PATIENT_POSITION:
                                                msg = "Treatment.Steps.PatientPosition";
                                                break;
                case STEP_TYPES.END_TREATMENT:
                                                msg = "Treatment.Steps.End";
                                                break;
                default: break;
            }
            result += $translate.instant(msg);

            if (step.type == STEP_TYPES.IMBIBITION)
                result += " ("+($scope.roundTo(step.duration/60, 2))+" min)";
            else if (step.type == STEP_TYPES.IRRADIATION) 
                result += " ("+($scope.roundTo(step.duration/60, 2))+" min@"+(step.power/1000)+"mW)";
            if (step.isPulsed) {
                result += " / "+step.irradiationTime+" s - "+step.pauseTime+" s";
            }
            return result;
        }

        $scope.NewProtocol = function () {
            $state.go('fullCustomDetail', {name:null});
        }

        $scope.editProtocol = function () {
           $state.go('fullCustomDetail', {name: $scope.fullCustoms[$scope.selectedIndex]});
        }

        $scope.deleteProtocol = function() {
             var confirm = $mdDialog.confirm()
                  .title($translate.instant('SelectProtocol.FullCustomOptions.RemoveTreatmentTitle'))
                  .textContent($translate.instant('SelectProtocol.FullCustomOptions.RemoveTreatmentMessage'))
                  //.ariaLabel('Lucky day')
                  //.targetEvent(ev)
                  .ok($translate.instant('Messages.OK'))
                  .cancel($translate.instant('Messages.DISCARD'));
            $mdDialog.show(confirm).then(function(res) {
              if (res == true) {
                    TreatmentService.removeFullCustomTreatment($scope.fullCustoms[$scope.selectedIndex]); 
                    $scope.fullCustoms = TreatmentService.getFullCustomTreatments();
                    $scope.selectedIndex = null;
                    $scope.loadTreatmentDetails();
                }
              
            }, function() {
                
            });

        }


        $scope.getTreamentJoules = function() {
           return $scope.roundTo(TreatmentService.getTreatmentJoules($scope.selectedTreatmentDetails), 2);
        }

        $scope.getTreamentDuration = function() {
            return $scope.roundTo(TreatmentService.getTreatmentDuration($scope.selectedTreatmentDetails), 2);
        }


        $scope.loadTreatmentDetails = function () {
            $scope.selectedTreatmentDetails = [];
            if ($scope.selectedIndex === null)
                return;
            $scope.totalIrradiationDuration = 0;
            var treatmentName = $scope.fullCustoms[$scope.selectedIndex];
            $scope.selectedTreatmentDetails = TreatmentService.getFullCustomTreatmentDetails(treatmentName);
            $scope.canDoTreatment =true;
            for (var i=0; i<$scope.selectedTreatmentDetails.length; i++) {
                var step = $scope.selectedTreatmentDetails[i];
                if (step.type == STEP_TYPES.IRRADIATION && !CalibrationService.isValueInRange(step.power/1000))
                    $scope.canDoTreatment = false;
                if (step.isPulsed && step.type == STEP_TYPES.IRRADIATION) {
                    $scope.canDoTreatment = TreatmentService.isPulsedAvailable();
                    var countdown = 0;
                    var numSteps = step.duration / step.irradiationTime;
                    countdown = numSteps * (step.irradiationTime + step.pauseTime);
                    countdown += step.duration % step.irradiationTime;
                    $scope.totalIrradiationDuration += countdown;
                }
                else if (step.duration && step.type == STEP_TYPES.IRRADIATION)
                  $scope.totalIrradiationDuration += step.duration;
            }
            $scope.totalIrradiationDuration = $scope.roundTo($scope.totalIrradiationDuration / 60, 2);
        }


        $scope.init = function() {
            $scope.fullCustoms = TreatmentService.getFullCustomTreatments();
            $scope.options.edit = $stateParams.edit == "1";
        }

        $scope.roundTo = function(value, decimalpositions)  {
            var i = value * Math.pow(10,decimalpositions);
            i = Math.round(i);
            return i / Math.pow(10,decimalpositions);
           } 

        $scope.startProtocol = function () {
            var treatmentName = $scope.fullCustoms[$scope.selectedIndex];
            $scope.selectedTreatmentDetails = TreatmentService.getFullCustomTreatmentDetails(treatmentName);

            $state.go('treatment', {protocol: treatmentName, dresdamode: 1,
                      duration:0 , intensity:0, thickness:0, steps:0, cornVertexDistanceX:0, cornVertexDistanceY:0, cornVertexRadius:0, kerApex:0, spotDiameter:null,
                      diagnosis: -1, stepsDetails: JSON.stringify($scope.selectedTreatmentDetails)
                });
        }

        $scope.showEdit = function() {
            return true;
            //return $scope.options.edit;
        }

        


        $scope.setProtocolOption = function(opt) {
            var optString;
            if (opt == 1)
                optString = "rapida";
            else
                optString = "normale";
            LoggingService.info("Selezionato protocollo di dresda con durata "+optString);
            $state.go('treatment', {protocol: 'Dresden', dresdamode: opt,
                      duration:0 , intensity:0, thickness:0, steps:0, cornVertexDistanceX:0, cornVertexDistanceY:0, cornVertexRadius:0, kerApex:0, spotDiameter:null,
                      diagnosis: -1
                });
        }

        $scope.init();

        ShutdownService.setOperation(ShutdownService.OPERATIONS.SELECT_PROTOCOL);

    	
	}
);