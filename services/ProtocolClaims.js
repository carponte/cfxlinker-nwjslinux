'use strict';

angular.module('megaride')
.service('ProtocolClaimsService', function($log){
            	
        var ProtocolClaimsService = {};

        ProtocolClaimsService.getClaims = function (protocol) {
            if (protocol == 'Dresden')
                return ProtocolClaimsService.getClaimsDresden();
            else if (protocol == 'Megaride' || protocol == 'Custom Fast') 
                return ProtocolClaimsService.getClaimsMegaride();
            else if (protocol == 'Iontoforesi') 
                return ProtocolClaimsService.getClaimsIonto();
            else if (protocol == 'Cheratiti') 
                return ProtocolClaimsService.getClaimsCheratiti();
            else if (protocol == 'PRK/LASIK') 
                return ProtocolClaimsService.getClaimsPrkLasik();
            else if (protocol == 'Demo') 
                return ProtocolClaimsService.getClaimsDemo();
            else if (protocol == 'Full Custom') 
                return ProtocolClaimsService.getFullCustom();
        }

        ProtocolClaimsService.getFullCustom  = function () {
            var result = [];
            result.push({"row": "ProtocolClaims.FullCustom.CLAIM_7"});
            return result;
            result.push({"row": "ProtocolClaims.FullCustom.CLAIM_1"});
            result.push({"row": "ProtocolClaims.FullCustom.CLAIM_2"});
            result.push({"row": "ProtocolClaims.FullCustom.CLAIM_3"});
            result.push({"row": "ProtocolClaims.FullCustom.CLAIM_4"});
            result.push({"row": "ProtocolClaims.FullCustom.CLAIM_5"});
            result.push({"row": "ProtocolClaims.FullCustom.CLAIM_6"});
            return result;
        }

        ProtocolClaimsService.getClaimsDresden = function () {
            var result = [];
            result.push({"row": "ProtocolClaims.Dresden.CLAIM_7"});
            result.push({"row": "ProtocolClaims.Dresden.CLAIM_1"});
            /*result.push({"row": "ProtocolClaims.Dresden.CLAIM_2", 
                "inner": [{"desc": "ProtocolClaims.Dresden.CLAIM_2_item1"}, 
                          {"desc" : "ProtocolClaims.Dresden.CLAIM_2_item2"}
                         ]});*/
            result.push({"row": "ProtocolClaims.Dresden.CLAIM_2"});
            result.push({"row": "ProtocolClaims.Dresden.CLAIM_3"});
            result.push({"row": "ProtocolClaims.Dresden.CLAIM_4"});
            result.push({"row": "ProtocolClaims.Dresden.CLAIM_5"});
            result.push({"row": "ProtocolClaims.Dresden.CLAIM_6"});
            return result;
        }

        ProtocolClaimsService.getClaimsMegaride = function () {
            var result = [];
            result.push({"row": "ProtocolClaims.Megaride.CLAIM_3"});
            result.push({"row": "ProtocolClaims.Megaride.CLAIM_1"});
            result.push({"row": "ProtocolClaims.Megaride.CLAIM_2", 
                        "inner": [{"desc": "ProtocolClaims.Megaride.CLAIM_2_item1"},
                                  {"desc": "ProtocolClaims.Megaride.CLAIM_2_item2"}, 
                                  {"desc": "ProtocolClaims.Megaride.CLAIM_2_item3"},
                                  {"desc": "ProtocolClaims.Megaride.CLAIM_2_item4"}
                    ]});
            return result;
        }


        ProtocolClaimsService.getClaimsIonto = function () {
            var result = [];

            result.push({"row": "ProtocolClaims.Ionto.CLAIM_7"});
            result.push({"row": "ProtocolClaims.Ionto.CLAIM_1"});
            result.push({"row": "ProtocolClaims.Ionto.CLAIM_2"});
            result.push({"row": "ProtocolClaims.Ionto.CLAIM_3"});
            result.push({"row": "ProtocolClaims.Ionto.CLAIM_4"});
            result.push({"row": "ProtocolClaims.Ionto.CLAIM_5"});
            result.push({"row": "ProtocolClaims.Ionto.CLAIM_6"});

            return result;
        }

        ProtocolClaimsService.getClaimsCheratiti = function () {
            var result = [];

            result.push({"row": "ProtocolClaims.Cheratiti.CLAIM_5"});
            result.push({"row": "ProtocolClaims.Cheratiti.CLAIM_1"});
            result.push({"row": "ProtocolClaims.Cheratiti.CLAIM_2"});
            result.push({"row": "ProtocolClaims.Cheratiti.CLAIM_3"});
            result.push({"row": "ProtocolClaims.Cheratiti.CLAIM_4"});
            return result;
        }

        ProtocolClaimsService.getClaimsPrkLasik = function () {
            var result = [];

            result.push({"row": "ProtocolClaims.PRK.CLAIM_1"});
            result.push({"row": "ProtocolClaims.PRK.CLAIM_2"});
            result.push({"row": "ProtocolClaims.PRK.CLAIM_3"});
            return result;
        }

        ProtocolClaimsService.getClaimsDemo = function () {
            var result = [];

            result.push({"row": "DEMO MODE"});
            return result;
        }
        
        return ProtocolClaimsService;
        
    })