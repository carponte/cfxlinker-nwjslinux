
angular.module('megaride')    
.controller('CtrlEndTreatment', 
    	function ($scope, $stateParams, $rootScope, $state, ConfigurationService, 
                 MegarideService, LoggingService, HwCommunicationService, ShutdownService, DebugModeService, ReportService,
                 CameraService) 
    		{

                $scope.showReport = false;

            $scope.goToProtocols = function() {
                    $state.go('selectProtocol');
                }

            $scope.goToReport = function () {

                if (DebugModeService.getConfiguration().privacyEnabled) {
                    $state.go('pwdAdministration', {
                      nextState: 'report'
                    });                  
                  }
                  else {
                    $state.go('report', 
                    {user: null}); 
                  }
            }

            $scope.shutdown = function() {
                ShutdownService.doShutdown();
            }

           
            $scope.init = function() {
                $scope.showReport = DebugModeService.isReportVisible();
                HwCommunicationService.stopTreatment().then(
                     function(res) {
                        HwCommunicationService.sendFocusMessage(0).then(
                            function() {}
                            );
                     });
                CameraService.detachVideo();
            }

             $scope.init();

    		}
    	);
   
    	