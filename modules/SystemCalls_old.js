function shutDown () {
  //return;
  //exec("sudo shutdown now", puts);
  
  var command = 'shutdown -p';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    
}

function switchUser () {
  var command = 'shutdown /l';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    
} 

function beep() {
  const spawn = require('child_process').spawn;
  const bat = spawn('cmdmp3win', ['tone.mp3']);
  bat.stdout.on('data', (data) => {
    console.log(data);
  });

  bat.stderr.on('data', (data) => {
    console.log(data);
  });

  bat.on('exit', (code) => {
    console.log(`Beep done`);
    //console.log(`Child exited with code ${code}`);    
  });

}


function startGUI() {
  const spawn = require('child_process').spawn;
  const bat = spawn('node_modules\\nw\\nwjs\\nw', ['.']);
  bat.stdout.on('data', (data) => {
    console.log(data);
  });

  bat.stderr.on('data', (data) => {
    console.log(data);
  });

  bat.on('exit', (code) => {
    console.log(`Child exited with code ${code}`);
    process.exit();
  });

}


function openNetworkConfiguration() {
  var command = 'ncpa.cpl';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    

}

function openRemoteAssistance() {
  var command = 'LiveLetN';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    

}

function showKeyboard() {
  var command = 'TabTip';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    

}

function addPrinter () {
  var command = 'rundll32 printui.dll,PrintUIEntry /il';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });    

}

function openPrinters () {
  var command = 'control printers';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      }); 
}

function openDeviceManager () {
  var command = 'devmgmt.msc';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });
}

function openCommandPrompt () {
  var command = 'start cmd';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });
}

function openTaskManager () {
  var command = 'start taskmgr';
  const exec = require('child_process').exec;
      const child = exec(command,
        (error, stdout, stderr) => {
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
          if (error !== null) {
            console.log(`exec error: ${error}`);
          }
      });
}


function openManagement() {
  const spawn = require('child_process').spawn;
  const bat = spawn('.\\nwjs\\nw', ['.\\management\\.']);
  bat.stdout.on('data', (data) => {
    console.log(data);
  });

  bat.stderr.on('data', (data) => {
    console.log(data);
  });

  bat.on('exit', (code) => {
    console.log(`Child exited with code ${code}`);
    process.exit();
  });

}

const isOnline = require('is-online');

function checkIsOnline (callBack) {
  isOnline().then(online => {
    callBack(online);
  });
}
 

exports.shutDown = shutDown;
exports.switchUser = switchUser;
exports.beep = beep;
exports.startGUI = startGUI;
exports.openNetworkConfiguration = openNetworkConfiguration;
exports.checkIsOnline = checkIsOnline;
exports.openRemoteAssistance = openRemoteAssistance;
exports.openManagement = openManagement;
exports.showKeyboard = showKeyboard;
exports.addPrinter = addPrinter;
exports.openPrinters = openPrinters;
exports.openDeviceManager = openDeviceManager;
exports.openCommandPrompt = openCommandPrompt;
exports.openTaskManager = openTaskManager;
