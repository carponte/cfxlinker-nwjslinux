'use strict';

var net = require('net');
//Creare un link all'eseguibile (click destro -> crea collegamento).  Spostare il link in c:\programdata\Microsoft\Windows\startMenu\Programs\Startup

angular.module('megaride')
.service('CameraService', function($log, $rootScope, exe4Camera, DebugModeService, configDir){
            	
        var CameraService = {};
        CameraService.videoPlaying = false;
        CameraService.exeHnd = null;
        CameraService.client = null;
        
        CameraService.is1stConnection = true;
        CameraService.clientInterval = null;
        CameraService.socketConnected = false;

        CameraService.paramsCamera = {};

        CameraService.exePath = './camera/CFXCamera.exe';

        CameraService.initCamera = function() {
            if (exe4Camera) {
                return;
            }

             if (CameraService.stream) {
                CameraService.stream.getTracks().forEach(function(track) {
                  track.stop();
                });
            }

            navigator.getUserMedia = navigator.webkitGetUserMedia  || navigator.getUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
            if (navigator.getUserMedia)        
                navigator.getUserMedia({video: true}, CameraService.handleVideo, CameraService.videoError);
            
            $rootScope.$on('configurationChanged', function (event, message) { 
                CameraService.checkCameraMirror();
            });
        }

        CameraService.checkCameraMirror = function() {
            var canvas = document.getElementById("videoCanvas");
            if (canvas == null)
                return;
            var mirror = DebugModeService.getConfiguration().MirrorCamera;
            var rotation = DebugModeService.getConfiguration().RotateCamera;
            var canvasStyle = "";
            if (mirror)
                canvasStyle  =  "scale(1,-1)";
            else
                canvasStyle  = "scale(-1,-1)";
            
            if (mirror && rotation) {
                console.log("Ecco il caso");
                canvasStyle  =  "scale(-1,1)";
                }
            else if (rotation)
                canvasStyle += ";rotate(180deg)";
            canvas.style["-webkit-transform"]  = canvasStyle;
        }

        CameraService.isCameraAttached = function() {
            return CameraService.exeHnd != null || CameraService.client != null;
        }

        CameraService.clearInterval = function () {
            if (CameraService.clientInterval == null) 
                return;
            clearInterval(CameraService.clientInterval);                    
            CameraService.clientInterval = null;
        }

        CameraService.attachToServer = function() {
                if (CameraService.client == null) {
                    CameraService.client = new net.Socket();
                    CameraService.client.on('data', function(data) {
                        console.log('Received: ' + data);
                       });
                    CameraService.client.on('close', function() {
                        console.log('Connection closed');
                        CameraService.socketConnected = false;
                      });
                }
                if (!CameraService.socketConnected) {
                    try {
                        CameraService.client.connect(8082, '127.0.0.1', function() {
                            //CameraService.client.write('Hello, server! Love, Client.\n');
                            CameraService.is1stConnection = false;
                            CameraService.socketConnected = true;
                            var msg = '';
                            try {
                               var canvas = document.getElementById("videoCanvas");
                               var rect = canvas.getBoundingClientRect();
                               msg = "top="+rect.top+",left="+(rect.left-30)+",width="+(rect.width+30)+",height="+rect.height;                                
                              }
                            catch (err) {

                            }
                            CameraService.client.write(msg+'\n');
                            CameraService.clearInterval();
                        });

                        
                    }
                    catch (err) {
                       console.log(err);
                       if (CameraService.client != null)
                         CameraService.client.destroy();
                       CameraService.client = null; 
                    }
                }
            else
               CameraService.client.write('Hello.\n'); 
        }


        CameraService.attachWithTCPIP = function () {
            if (CameraService.is1stConnection) 
                CameraService.clientInterval = setInterval(CameraService.attachToServer, 1000);
            else
                CameraService.attachToServer();
        }

        
        CameraService.attachCamera = function (scope) {
            if (exe4Camera){
                CameraService.attachWithTCPIP();
                return;
                const spawn = require('child_process').spawn;
                var canvas = document.getElementById("videoCanvas");
                var rect = canvas.getBoundingClientRect();
                CameraService.exeHnd = spawn(CameraService.exePath, ["top="+rect.top, "left="+(rect.left-30), "width="+(rect.width+30), "height="+rect.height]);
                return;
            }
            if (CameraService.videoSrc == null)
                CameraService.initCamera();
            CameraService.video = document.getElementById("videoCanvas");
            CameraService.checkCameraMirror();
            CameraService.video.src = CameraService.videoSrc;
            CameraService.video.srcObject = CameraService.stream;
            CameraService.video.ondblclick = CameraService.resetCamera;
            
            var playPromise;
            try { 
                 playPromise = CameraService.video.play();
                }
            catch (err) {
                console.log(err);
            }

            
           /* document.addEventListener('DOMContentLoaded', function(){
                v = document.getElementById('videoElementVC');
                canvas = document.getElementById('canvas');
                context = canvas.getContext('2d');
                w = canvas.width;
                h = canvas.height;

                },false);*/

                /*
                ESEMPIO
                
                <div>
                  <video id="camFeed" width="320" height="240" autoplay>
                </video>
              </div>

                <div>
                    <canvas id="photo" width="320" height="240">
                    </canvas>
                </div>

                function takePhoto()
                    {
                        var c = document.getElementById('photo');
                        var v = document.getElementById('camFeed');
                        c.getContext('2d').drawImage(v, 0, 0, 320, 240);
                    }
                */
        }

        CameraService.resetCamera = function() {
            alert("Resetting camera");
            CameraService.detachVideo();
            CameraService.attachCamera();
        }

        CameraService.handleVideo = function (stream) {
            CameraService.stream = stream;
           // CameraService.videoSrc = window.URL.createObjectURL(CameraService.stream);
             CameraService.videoSrc = stream;
 
        }
         
        CameraService.videoError = function(e) {
            // do something
            console.log("CameraService. Video Error "+e);
            CameraService.videoSrc = null;
            //CameraService.video.poster = "img/video-poster.png";
        }

        CameraService.detachVideo = function() {
            if (exe4Camera && CameraService.client != null/*CameraService.exeHnd != null*/){
                 CameraService.client.write('Ciao\n');
                 CameraService.client.destroy();
                 CameraService.client = null;
                 CameraService.clearInterval();
                 return;
                 CameraService.exeHnd.kill();
                 CameraService.exeHnd = null;
                }
            if (CameraService.video != null && CameraService.videoPlaying) {
                    CameraService.video.pause();
                    if (CameraService.stream != null) {
                        var tracks = CameraService.stream.getVideoTracks();
                        for (var i=0; i<tracks.length; i++)
                            tracks[i].stop();
                    }
                    CameraService.videoPlaying = false;
                }
        }

        CameraService.getVideoElement = function() {
            return CameraService.video;
        }

        CameraService.getParamsDefault = function () {
            var params = {};
            params.brightness = 0;
            params.saturation = 2;
            params.contrast = 7;
            params.sharpness = 4;
            return params;
        }

        CameraService.loadActualParams = function () {
            SystemCalls.getBrightness(
                function (brightness) {
                     try {
                        CameraService.paramsCamera.brightness = Number(brightness);
                         }
                     catch(err) {
                        }
                    },
                function (error) {
                     console.log("TreatmentController. GetBrightness. Got error" + error);
                    } 
                
            );
            SystemCalls.getSaturation(
                function (saturation) {
                     try {
                        CameraService.paramsCamera.saturation = Number(saturation);
                         }
                     catch(err) {
                        }
                    },
                function (error) {
                     console.log("TreatmentController. GetSaturation. Got error" + error);
                    } 
                
            );
            SystemCalls.getContrast(
                function (contrast) {
                     try {
                        CameraService.paramsCamera.contrast = Number(contrast);
                         }
                     catch(err) {
                        }
                    },
                function (error) {
                     console.log("TreatmentController. getContrast. Got error" + error);
                    } 
                
            );
            SystemCalls.getSharpness(
                function (sharpness) {
                     try {
                        CameraService.paramsCamera.sharpness = Number(sharpness);
                         }
                     catch(err) {
                        }
                    },
                function (error) {
                     console.log("TreatmentController. getSharpness. Got error" + error);
                    } 
                
            );
        }

        CameraService.setParams = function (params, mW) {
            CameraService.paramsCamera.brightness = params.brightness;
            CameraService.paramsCamera.saturation = params.saturation;
            CameraService.paramsCamera.contrast = params.contrast;
            CameraService.paramsCamera.sharpness = params.sharpness;
            SystemCalls.setBrightness(CameraService.paramsCamera.brightness);
            SystemCalls.setSaturation(CameraService.paramsCamera.saturation);
            SystemCalls.setContrast(CameraService.paramsCamera.contrast);
            SystemCalls.setSharpness(CameraService.paramsCamera.sharpness);

            var confDir ='./'+configDir+'/';

            if (!fsys.existsSync(confDir)){
                fsys.mkdirSync(confDir);
            }
            fs.writeFile(confDir+'camera.json', JSON.stringify(CameraService.paramsCamera), function(err) {
                if(err) {
                    return console.log(err);
                }
             });
        }

        CameraService.setParamsDefault = function () {
            var params = CameraService.getParamsDefault();
            CameraService.setParams(params);
        }

        CameraService.loadParametersFromFile = function() {
            var confFile ='./'+configDir+'/'+'camera.json';
            if (!fsys.existsSync(confFile)) {
                CameraService.setParamsDefault();
                return;
              }
            fs.readFile(confFile,  function(err, data) {
                    if(err) {
                        return console.log(err);
                    }
                  CameraService.paramsCamera = JSON.parse(data);
                  CameraService.setParams(CameraService.paramsCamera);
                 }); 
        }


      
        
        CameraService.initCamera();
        CameraService.loadParametersFromFile();

        
        return CameraService;
        
    })