//INIZIO LOGGER SERIALE
var dateFormatSL = require('dateformat');
var dateSuffixSL = dateFormatSL(new Date(), "yyyy_mm_dd");
var winstonSL = require('winston');

var buffer = "";


function getSerialLoggerPath() {
   return 'log/serial_'+dateSuffixSL+'.log';
}

var serialLogger = new (winstonSL.Logger)({
 transports: [
    new (winstonSL.transports.Console)({ json: false, timestamp:  function () {
       return (new Date()).toLocaleString("it-IT");
      } 
    }),
    new winstonSL.transports.File({filename: getSerialLoggerPath(), json: false, timestamp:  function () {
       return (new Date()).toLocaleString("it-IT");
      }
    })
  ],
  exceptionHandlers: [
    new (winstonSL.transports.Console)({ json: false, timestamp: 
     function () {
            return (new Date()).toLocaleString("it-IT");
          }
     }),
    new winstonSL.transports.File({filename:'log/exceptions.log', json: false })
  ],
  exitOnError: false
});

function info (str) {
  buffer += str;
  if (str.indexOf("\n") != -1) {
    serialLogger.info(buffer);
    buffer = "";
  }
  //serialLogger.info(str);
}

//FINE LOGGER SERIALE

exports.info = info;
exports.getSerialLoggerPath=getSerialLoggerPath;