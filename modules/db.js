var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('CFXLinker.sqlite');

var currentDBVersion = 0;
 
function createDB() {
	db.serialize(function() {
	  	db.run("CREATE TABLE IF NOT EXISTS Treatments (ID integer primary key autoincrement, patientName TEXT, patientSurname TEXT, patientAge TEXT, patientSex TEXT, Protocol TEXT, "+
	  		"Diagnosys TEXT, DateOfTreatment Date, Duration INTEGER, ThPoint INTEGER, KMax INTEGER, Dx INTEGER, Dy INTEGER)");

	  	db.run("CREATE TABLE IF NOT EXISTS Configuration (ID integer primary key, jumpVerifiyCalibration INTEGER, fakeImbibition INTEGER, resetWhiteLed INTEGER, demoEnabled INTEGER, Protocol TEXT, "+
	  		"timerSCEnabled INTEGER, showReport INTEGER)");

		db.run("CREATE TABLE IF NOT EXISTS DBVersion (ID integer primary key, currentVersion INTEGER)");

		
		
	  /*var stmt = db.prepare("INSERT INTO lorem VALUES (?)");
	  for (var i = 0; i < 10; i++) {
	      stmt.run("Ipsum " + i);
	  }
	  stmt.finalize();
	 
	  db.each("SELECT rowid AS id, info FROM lorem", function(err, row) {
	      console.log(row.id + ": " + row.info);
	  });*/
	});
	updateDB();
	console.log("Database creato");
}

function getCurrentVersion() {
	if (currentDBVersion > 0)
		return currentDBVersion;
	db.each("SELECT * FROM DBVersion", function(err, row) {
	      currentDBVersion = row.currentVersion;
	  	});
	if (currentDBVersion == 0) {
		setCurrentVersion(1);
	} 
}

function setCurrentVersion(version) {
		if (version > 1)
			db.run("UPDATE DBVersion SET currentVersion=version WHERE ID = 1");
		else
			db.run("INSERT INTO DBVersion (currentVersion) VALUES (1)");
		currentDBVersion = version;
	}

function updateDB() {
	if (getCurrentVersion() == 1) {
		// INSERT...
		//setCurrentVersion(2);
	}
}


function closeDB () {
	db.close();
}

 

exports.createDB = createDB;
exports.closeDB = closeDB;
