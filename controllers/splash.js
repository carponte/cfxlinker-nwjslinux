var SystemCalls;
try {
  SystemCalls = require('./modules/SystemCalls.js');
}
catch (err) {
    SystemCalls = null;
}


angular.module('megaride')    
.controller('splashCtrl', 
    	function ($scope, $stateParams, $rootScope, $state, $timeout, ConfigurationService, CalibrationService, $mdDialog,
                 MegarideService, LoggingService, HwCommunicationService, ShutdownService, DebugModeService, CameraService, 
                 version, UpdateService, hotkeys, $interval) 
    		{$scope.progress = {};
             $scope.progress.message = 'System Loading';
             $scope.progress.val = 0;
             $scope.progress.show = false;
             $scope.progress.inited = true;

            $scope.messages = [];
            $scope.okEnabled = false;

            $scope.swVersion = version;

            $scope.statusConnection = false;

            $scope.verifyNeeded = false;
            $scope.password = "";

            $scope.shortCutManagement = [];
            $scope.numSteps4Management = 2;

            //$scope.updateAvailable = false;
            $scope.updateChecked = false;


            $scope.goNext = function () {
                $scope.goToVerifyCalibration(); 
            }


             var goOut = function() {
                if (ConfigurationService.getLocale() == null)
                   $scope.goToLanguage(); 
                   $scope.progress.show = false;
                   /* try {
                        $scope.$apply();
                    }
                    catch (err) {
                    }*/
             }

             //setTimeout(goOut, 1000);
             $timeout(goOut, 1);


             $scope.goToProtocols = function() {
                LoggingService.debug('SplashService.  goToProtocols');
                $state.go('selectProtocol');
             }

             $scope.goToCalibration = function () {
                LoggingService.debug('SplashService.  goToCalibration');
                $state.go('calibration');
             }

             $scope.goToVerifyCalibration = function () {
                LoggingService.debug('SplashService.  goToCalibration');
                $state.go('verifycalibration');
             }

             $scope.goToLanguage = function() {
                LoggingService.debug('SplashService.  goToLanguage');
                $state.go('language');
             }

            $scope.updateSoftware = function() {

                LoggingService.debug('SplashService.  updateSoftware');
                //$state.go('language');
                UpdateService.update();
             }

             $scope.goToArchive = function () {
                LoggingService.debug('SplashService.  goToArchive');
                if (DebugModeService.getConfiguration().privacyEnabled) {
                  $state.go('pwdAdministration', {
                    nextState: 'archive'
                  });                  
                }
                else {
                  $state.go('archive', 
                  {user: null}); 
                }
             }

//INIZIO GESTIONE ACCESSO AL PANNELLO AMMINISTRAZIONE CON PASSWORD

             $scope.goToManagement = function () {
                LoggingService.info('Apro la pagina di amministrazione');
                ShutdownService.setOperation(ShutdownService.OPERATIONS.SPLASH);
                $state.go('administration');
             }

             $scope.openManagement = function (ev) {
                 //$scope.showPasswordDialog(ev);
                //$scope.goToManagement();
                $state.go('pwdAdministration', {
                  nextState: 'administration'
                });
             }



/*
            function DialogController($scope, $mdDialog, $state) {
                $scope.password = '';
                $scope.hide = function() {
                  $mdDialog.hide();
                };

                $scope.cancel = function() {
                  $mdDialog.cancel();
                };

                $scope.answer = function(answer) {
                  $mdDialog.hide(answer);
                };

                $scope.okButton = function (ev) {
                    if ($scope.password == 'eRc1936') {
                        $mdDialog.hide();
                        LoggingService.info('Apro la pagina di amministrazione');
                        ShutdownService.setOperation(ShutdownService.OPERATIONS.SPLASH);
                        $scope.password = '';
                        $state.go('administration');
                    }
                    else {
                        $mdDialog.show(
                          $mdDialog.alert()
                            //.parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Attenzione')
                            .textContent('Password errata')
                            .ariaLabel('Password errata')
                            .ok('Ok')
                            .targetEvent(ev)                        
                        );
                    }

                }
              }

              $scope.showPasswordDialog = function (ev) {
                $mdDialog.show({
                      controller: DialogController,
                      template: 'Password: <form ng-submit="okButton()"><input type=\'password\' ng-model=\'password\' autofocus></input><md-button class="md-raised" ng-click="okButton()">Ok</md-button></form>',
                      parent: angular.element(document.body),
                      targetEvent: ev,
                      clickOutsideToClose:true,
                      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                    })
                    .then(function(answer) {
                      //$scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                      //$scope.status = 'You cancelled the dialog.';
                    });
            }*/




//FINE GESTIONE ACCESSO AL PANNELLO AMMINISTRAZIONE CON PASSWORD



             $scope.checkHWInited = function () {
                $timeout(function() {	//sostituzione $apply
                  $scope.okEnabled = HwCommunicationService.isInited();
                  $scope.progress.inited = true;
                  /*if (!$scope.$$phase)
                      $scope.$apply();*/
                });
                
             }

             $scope.isNetworkStatusVisible = function () {
                return true;
             }

             $scope.checkOnLine = function()  {
                try {
                     if (!UpdateService.localCommit)
                        UpdateService.checkUpdates();
                    isOnline().then(online => {
                            $scope.statusConnection = online;
                            
                            if (online && ! $scope.updateChecked) {
                               $scope.updateChecked = true;
                               UpdateService.checkUpdates();
                            }
                            
                        });
                    }
                catch (ex) {
                        console.log('CheckingOnline. Exception');
                        console.log(ex);
                    }   
             }

             $scope.openNetworkConfiguration = function () {
                if (SystemCalls == null)
                    return;
                //SystemCalls.openNetworkConfiguration();
                $state.go('wifi');
             }

             $scope.openRemoteAssistance = function() {
                if (SystemCalls == null)
                    return;
                SystemCalls.openRemoteAssistance();  
             }


             $scope.initSteps4Management = function() {
                for (var i=1; i<=$scope.numSteps4Management; i++) {
                    $scope.shortCutManagement[i] = false;
                }
             }

             $scope.doClick4Management = function (step){
                $scope.shortCutManagement[step] = true;
                for (var i=1; i<=$scope.numSteps4Management; i++) {
                    if ($scope.shortCutManagement[i] == false)
                        return;
                }
                $scope.openManagement();
             }

             $scope.initEventListeners = function() {
              $rootScope.$on('hardwareInitedEvent', function (event, message) {
                      $scope.checkHWInited();
                      if (HwCommunicationService.isInited())
                        HwCommunicationService.unFreezeApplication();
                    });
                $rootScope.$on('hardwareStatus', function (event, message) {
                      $scope.checkHWInited();
                      if (HwCommunicationService.isInited())
                        HwCommunicationService.unFreezeApplication();
                    });
                $rootScope.$on('VerifyCalibrationToBeChecked', function (event, message) {
                      $scope.verifyNeeded = message.checkNeeded;
                      //$scope.apply();
                    });

                $rootScope.$on('updateSW', function (event, message) {
                      $rootScope.updateAvailable = true;
                      $scope.localCommit = UpdateService.localCommit;
                      $scope.remoteCommit = UpdateService.remoteCommit;
                      //$scope.apply();
                    });
                
                $rootScope.$on('branchUpdated', function (event, message) {
                  $scope.localCommit = UpdateService.localCommit;
                  //$scope.apply();
                });

             }


             $scope.apply = function() {
                if ($scope.$$phase) 
                    return;
                try {
                    $scope.$apply();
                }
                catch (err) {

                }
            }


            $scope.init = function() {
               // MegarideService.init();
                $scope.checkHWInited();
                $scope.initEventListeners();
                ShutdownService.setOperation(ShutdownService.OPERATIONS.SHUTDOWN);
                $scope.initSteps4Management();
                $scope.checkOnLine();
                $interval($scope.checkOnLine, 5000);
                CameraService.detachVideo();
                CalibrationService.checkIfNeededVerify();
                LoggingService.debug("CIAO");

               /* $timeout(function() {
                 if (!HwCommunicationService.isInited())
                    HwCommunicationService.freezeApplication();
                }, 15000);*/
            }

             $scope.init();
    		}
    	);
   
    	