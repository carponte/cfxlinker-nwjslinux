angular.module('megaride')    
.controller('CtrlMegarideSteps', 
    function ($scope, $stateParams, $rootScope, $state, ShutdownService, LoggingService) {

    	$scope.setProtocolOption = function(opt) {

            LoggingService.info("Selezionato protocollo Megaride.  Inseriti i seguenti dati:");
            LoggingService.info("Spessore: "+$stateParams.thickness);
            LoggingService.info("Durata: "+$stateParams.duration);
            LoggingService.info("Potenza: "+$stateParams.intensity);
            LoggingService.info("Numero di step: "+opt);
            LoggingService.info("CornVertex: ["+$stateParams.cornVertexDistanceX+", "+$stateParams.cornVertexDistanceY+"]");
            LoggingService.info("kerApex: "+$stateParams.kerApex);
            LoggingService.info("CornVertexRadius: "+$stateParams.cornVertexRadius);
            LoggingService.info("Diagnosis: "+$stateParams.diagnosis);


            $state.go('treatment', {
                protocol:"Megaride",
                duration: $stateParams.duration, 
            	intensity: $stateParams.intensity, 
                thickness: $stateParams.thickness, 
                steps: opt,
                dresdamode: 0,
                cornVertexDistanceX: $stateParams.cornVertexDistanceX,
                cornVertexDistanceY: $stateParams.cornVertexDistanceY, 
                cornVertexRadius: $stateParams.cornVertexRadius,
                kerApex: $stateParams.kerApex,
                spotDiameter: $stateParams.spotDiameter,
                diagnosis: $stateParams.diagnosis,
                stepsDetails: null
            });
        }

        $scope.isForMiopia = function () {
            return $stateParams.diagnosis == 3;
        }

        $scope.setMiopiaParameters = function(minutes, intensity) {
            LoggingService.info("Selezionato protocollo Megaride (Miopia).  Inseriti i seguenti dati:");
            LoggingService.info("Spessore: "+$stateParams.thickness);
            LoggingService.info("Durata: "+minutes);
            LoggingService.info("Potenza: "+intensity);
            LoggingService.info("Numero di step: 1");
            LoggingService.info("CornVertex: ["+$stateParams.cornVertexDistanceX+", "+$stateParams.cornVertexDistanceY+"]");
            LoggingService.info("kerApex: "+$stateParams.kerApex);
            LoggingService.info("CornVertexRadius: "+$stateParams.cornVertexRadius);
            LoggingService.info("Diagnosis: "+$stateParams.diagnosis);

            $state.go('treatment', {
                protocol:"Megaride",
                duration: minutes, 
                intensity: intensity, 
                thickness: $stateParams.thickness, 
                steps: 1,
                dresdamode: 0,
                cornVertexDistanceX: $stateParams.cornVertexDistanceX,
                cornVertexDistanceY: $stateParams.cornVertexDistanceY, 
                cornVertexRadius: $stateParams.cornVertexRadius,
                kerApex: $stateParams.kerApex,
                spotDiameter: $stateParams.spotDiameter,
                diagnosis: $stateParams.diagnosis
            });
        }


        $scope.back = function() {
             //$state.go('selectProtocol');
             if ($stateParams.oldState != null && $stateParams.oldState != '')
                $state.go($stateParams.oldState);
             else
                $state.go('megaride', {
                        thickness: $stateParams.thickness, cornVertexDistanceX: $stateParams.cornVertexDistanceX, 
                        cornVertexDistanceY: $stateParams.cornVertexDistanceY, kerApex:$stateParams.kerApex, oldState: '', cornVertexRadius:$stateParams.cornVertexRadius,
                        diagnosis: $stateParams.diagnosis
                    });
        }

        ShutdownService.setOperation(ShutdownService.OPERATIONS.SELECT_PROTOCOL);
    	
	}
);