#!/bin/bash

dirRepo=/home/cfxlinker/repo
rm -rf $dirRepo
mkdir -p $dirRepo

dir=$(ls /media/cfxlinker/)
finalDir="/media/cfxlinker/"$dir/
SUB='cfxlinker'

#Verifico se c'è un file nello stesso livello dell'autorun
toSearch=`ls -f *.tar.gz`
for f in $toSearch
do
    if [[ "$f" == *"$SUB"*.tar.gz ]];then
      echo 'Upgrading from file found in the current directory'
      tar -xf "$f" -C /home/cfxlinker/repo/     
      break
    fi
done

#Se non l'ho trovato lo cerco sulla pendrive
if [ ! -d  "/home/cfxlinker/repo/cfxlinker-nwjslinux" ] 
then
    echo 'No files found on the current directory, trying to get files from USB'
    ls "$finalDir" | while read name 
    do
        if [[ "$name" == *"$SUB"* ]];then
          tar -xf "$finalDir$name" -C /home/cfxlinker/repo/
          break
        fi
    done
fi

#Se non ho trovato alcun file prendo la cartella dal repository
if [ ! -d  "/home/cfxlinker/repo/cfxlinker-nwjslinux" ] 
then
    echo 'No files found locally, trying to get files from repository'
    cd /home/cfxlinker/repo
    git clone https://carponte@bitbucket.org/carponte/cfxlinker-nwjslinux.git
fi

#Rimuovo i file inutili delle vecchie versioni e faccio stash
for f in $(find /home/cfxlinker/cfxlinker/customization -name 'index.html' -or -name 'app.js'); do rm $f; done

cp -af /home/cfxlinker/repo/cfxlinker-nwjslinux/. /home/cfxlinker/cfxlinker/
rm -rf /home/cfxlinker/repo
chmod +x /home/cfxlinker/cfxlinker/cfxlinker
chmod +x /home/cfxlinker/cfxlinker/exportEnvironment.sh
cd /home/cfxlinker

export cfxLinkerCustomization=$(bash "/home/cfxlinker/cfxlinker/exportEnvironment.sh")

question="Type 1 for Iromed, 2 for Fidia, 3 for China, 4 for Canada"

if [ "$cfxLinkerCustomization" = "" ]; then
  while true; do
      read -p "Customization not set.  $question " yn
      case $yn in
          [1]* ) export cfxLinkerCustomization=iromed;break;;
          [2]* ) export cfxLinkerCustomization=fidia;break;;
          [3]* ) export cfxLinkerCustomization=china;break;;
          [4]* ) export cfxLinkerCustomization=canada;break;;
          * ) echo "$question ";;
      esac
  done; 
fi

echo "Applying Customization: $cfxLinkerCustomization "
suffixFolder="_files"
export cfxLinkerCustomization="$cfxLinkerCustomization$suffixFolder"
cp -rT /home/cfxlinker/cfxlinker/customization/$cfxLinkerCustomization/ /home/cfxlinker/cfxlinker/


