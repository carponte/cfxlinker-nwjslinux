'use strict';

var nwgui;
try {
    nwgui = require("nw.gui");  
} 
catch (ex) {
    nwgui = null;
}

var SystemCalls;
try {
  SystemCalls = require('./modules/SystemCalls.js');
}
catch (err) {
    SystemCalls = null;
}

const reportsDir = "reports";


angular.module('megaride')
.service('ReportService', function($log, $mdDialog, $translate, DatabaseService, LoggingService){
            	
        var ReportService = {};

        ReportService.currentReport = {};
        ReportService.currentReport.patient = {};

        ReportService.winReferto = null;
        ReportService.configReport = {};
        ReportService.currentUser = null;

        ReportService.clear = function () {
            ReportService.currentReport = {};
        }

        ReportService.getProtocol = function () {
            return ReportService.currentReport.protocol;
        }
        
        ReportService.setProtocol = function (protocol) {
            ReportService.currentReport.protocol = protocol;             
        }

        ReportService.getDuration = function () {
            return ReportService.currentReport.duration;
        }

        ReportService.setDuration = function (duration) {
            ReportService.currentReport.duration = duration;             
        }

       ReportService.setPower = function (power) {
            ReportService.currentReport.power = power;             
        }

        ReportService.getSteps = function () {
            return ReportService.currentReport.steps;
        }


       ReportService.setSteps = function (steps) {
            ReportService.currentReport.steps = steps;             
        }

        ReportService.setVertexDistanceX = function(xDist) {
            ReportService.currentReport.VertexDistanceX = xDist;
        }

        ReportService.setVertexDistanceY = function(yDist) {
            ReportService.currentReport.VertexDistanceY = yDist;
        }

        ReportService.setKMax = function(kMax) {
            ReportService.currentReport.kMax = kMax;
        }

        ReportService.setThickness= function(thick) {
            ReportService.currentReport.thickness = thick;
        }

        ReportService.setPatientName= function(patName) {
            ReportService.currentReport.patientName = patName;
        }        

        ReportService.setPatientSurname= function(patSurName) {
            ReportService.currentReport.patientSurName = patSurName;
        }        
        
        ReportService.setPatientAge= function(patAge) {
            ReportService.currentReport.patientAge = patAge;
        }

        ReportService.setPatientSex= function(patSex) {
            ReportService.currentReport.patientSex = patSex;
        }

        ReportService.setPatientPathology= function(patPat) {
            ReportService.currentReport.patientPathology = patPat;
        }

        ReportService.setOperator= function(operator) {
            ReportService.currentReport.operator = operator;
        }

        ReportService.setCurrentDate = function(currDate) {
            ReportService.currentReport.date = currDate;
        }

        ReportService.setSpotDiameter = function(diameter) {
           ReportService.currentReport.spotDiameter = diameter;
        }

        ReportService.setCornVertexRadius = function(cornVertexRadius) {
           ReportService.currentReport.cornVertexRadius = cornVertexRadius;   
        }

        ReportService.setImbibition = function(imbibition) {
             ReportService.currentReport.imbibition = imbibition;
        }

        ReportService.setDiagnosis= function(diagnosis) {
            ReportService.getCurrentReportData();
            ReportService.currentReport.patient.pathology = diagnosis;
        }

        ReportService.setPulsedData = function (pulsedData) {
            ReportService.currentReport.pulsedData = pulsedData;
        }


        ReportService.getCurrentReportData = function () {
            if (ReportService.currentReport == null)
                ReportService.currentReport = {};
            if (ReportService.currentReport.patient == null)
                ReportService.currentReport.patient = {};
            return ReportService.currentReport;
        }

        ReportService.saveCurrentTreatmentOnDB  = function (){
            DatabaseService.saveTreatmentOnDB(ReportService.currentReport);
        }

        ReportService.setCurrentTreatmentPatientInfo = function (patientInfo) {
            ReportService.currentReport.patient = patientInfo;
            //DatabaseService.updateTreatment(patientInfo);
        }

        ReportService.updateConfigReport = function () {
            var callBack = function (result) {
                ReportService.configReport = result;
            }
            DatabaseService.getConfigReferto(callBack);
        }

        ReportService.getReportConfiguration = function () {
            return ReportService.configReport;
        }

        ReportService.printReferto = function (strPdf) {
            if (nwgui == null) {
                LoggingService.info("Report Service.  La gui NW non è stata inizializzata ");
                return;
               }


            ReportService.winReferto = nwgui.Window.open(strPdf, {
                                position: 'center',
                                width:1,
                                height:1,
                                focus: false,
                                show:false
                              }, function(win) {
                                win.on ('loaded', function(){
                                  ReportService.winReferto = this;

                                  this.print({});

                                  $timeout(function () {
                                        this.hide();
                                        this.close(true)
                                    }, 10000);
                                  //setTimeout(this.close(true), 10000);
                                })
                              });
             
            }

        ReportService.unlockWinReferto = function () {
            if (ReportService.winReferto != null) {
                ReportService.winReferto.close(true);
            }
        }

        ReportService.testPrinters = function() {
            console.log(nwgui.Window.getPrinters());
        }

        ReportService.getIdNextReport = function(callBack) {
            DatabaseService.getNextReportID(callBack);
        }

        ReportService.setCurrentUser = function (user) {
            ReportService.currentUser = user;
        }

        ReportService.archiveReport = function(content, data, idReport) {
            var userID = -1;
            if (ReportService.currentUser != null)
              userID = ReportService.currentUser.ID;
            DatabaseService.updateReportData(idReport, data.patient, userID);
            if (!fsys.existsSync(reportsDir)){
                fsys.mkdirSync(reportsDir);
            }
            fs.writeFile(ReportService.getReportPath(idReport), content, function(err) {
                if(err) {
                    return console.log(err);
                }
             });
        }

	ReportService.saveTempReport = function(content) {
	  if (!fsys.existsSync(reportsDir)){
                fsys.mkdirSync(reportsDir);
            }
            fsys.writeFile(reportsDir+"/temp.pdf" , content, function(err) {
                if(err) {
                    return console.log(err);
                }
             });
    }
    
    ReportService.saveReportOnPath = function (content, pathDir, fileName) {
        if (!fsys.existsSync(pathDir)){
            fsys.mkdirSync(pathDir);
        }
        try {
            fsys.writeFileSync(pathDir+"/"+fileName , content);
            return true;
        }
        catch (e) {
            return false;
        }
        
    }

        ReportService.printReport = function (reportName, jobName) {
            SystemCalls.printFile(reportsDir+"/"+reportName+".pdf", jobName);

            var confirm = $mdDialog.confirm()
            .title($translate.instant('Messages.PRINT_TITLE'))
            .textContent($translate.instant('Messages.PRINT_MESSAGE'))
            //.ariaLabel('Lucky day')
            //.targetEvent(ev)
            .ok($translate.instant('Messages.OK'))
            //.cancel($translate.instant('Messages.DISCARD'));
            $mdDialog.show(confirm).then(function(res) {
            /*if (res == true)
                HwCommunicationService.sendShutDownMessage(); 
            else 
                ShutdownService.reattachCamera();*/
            }, function() {
                
            });
	}

	ReportService.printTempReport= function () {
	   ReportService.printReport("temp");
	   
	}

        ReportService.deleteReportRecord = function(idReport) {
            DatabaseService.deleteReportRecord(idReport);
        }  

        ReportService.deleteReport = function (idReport) {
            var repPath = ReportService.getReportPath(idReport);
            if (fsys.existsSync(repPath)){
                fsys.unlinkSync(repPath);
            }
            ReportService.deleteReportRecord(idReport);
        }


        ReportService.checkReport = function(idReport) {            
            if (!fsys.existsSync(ReportService.getReportPath(idReport))){
                ReportService.deleteReportRecord(idReport);
            }
        }        

        ReportService.getReportPath = function(idReport) {
            return reportsDir+"/"+idReport+".pdf"; 
        }

	ReportService.readReport = function (idReport, callBack) {
	  fsys.readFile(ReportService.getReportPath(idReport), callBack);
	}
        
        return ReportService;
        
    })
