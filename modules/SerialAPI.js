/****
SerialPort
***/
var SerialLogger = require('./SerialLogger.js');
var ApplicationLogger = require('./ApplicationLogger.js');

var serialport = require('serialport');

//var portName = '/dev/ttyUSB0';
var megaPort;
var SERIAL_MANUFACTURER = "FTDI";
//SERIAL_MANUFACTURER = "wch.cn";   //clone di arduino per testing


var connection;

function serialLog(str) {
  SerialLogger.info(str);
}

function Log(str) {
  ApplicationLogger.info(str);
}



function getEndLine() {
  return String.fromCharCode(13);
}

function checkSerialPortStatus() {
  var OldPortStatus = -1;
  setInterval(function() {
      var portStatus = 0;
        try{
          if (megaPort == null)
            portStatus = 0;
          else {
                serialport.list(function (err, ports) {
                  portStatus = 0;
                  for (var i=0; i<ports.length; i++) {
                    if (ports[i].manufacturer == SERIAL_MANUFACTURER) {
                        portStatus = 1;
                        Log('Giro della seriale.  Porta analizzata: '+ports[i].manufacturer+' Stato: '+portStatus+" MAN Cercato: "+ SERIAL_MANUFACTURER);
                        break;
                      }
                    }
                  if (OldPortStatus != portStatus) {
                    Log('Giro della seriale.  Cambio di stato.  Stato attuale: '+portStatus);
                    OldPortStatus = portStatus;
                    if (portStatus == 0 && megaPort != null) 
                      serialPortDoClose();
                    sendSerialPortStatus();
                  }
            });
          }
        }
      catch( err) {

      }


    }
      , 1000);
}

function autoDetectSerialPort () {
    setInterval( function() {
    if (megaPort != null)
      return;
    serialport.list(function (err, ports) {
      for (var i=0; i<ports.length; i++) {
        if (ports[i].manufacturer == SERIAL_MANUFACTURER) {
            openSerialPort(ports[i].comName);
            //console.log("Aprirò la seriale "+ports[i].comName);
            return;
          }
        }
       //Log("Impossibile connettersi alla seriale");

      //Log("Impossibile connettersi alla seriale.  Questa la lista:");
      //Log(JSON.stringify(ports));
    });
  }, 2000);
}

function sendSerialData(data) {
   serialLog("<-"+data);
   return;
   checkCurrentPacket();
   currentPacket.console.push(data);
   serialLog("<-"+data);
  // console.log("SP.  Ricevuto: "+data);

   if(data.indexOf("OK") >1 || data.indexOf("FAIL") >1){
    if(data.indexOf("OK") >1)
      currentPacket.resultMessage = data;
    else if (data.indexOf("FAIL") >1)
      currentPacket.errorMessage = data;
    var respPacket = JSON.stringify(currentPacket);
    var canSend = currentPacket.command != null;

    if (canSend) {
      Log("\nRicevuto: ");
      for (var i=1; i<currentPacket.console.length; i++)
        Log(currentPacket.console[i]);
      Log('--------\n\n');
      if (connection != null)
      	connection.sendUTF(respPacket);
      currentPacket.rows = [];
      currentPacket = null;
    }
  }
  else
    currentPacket.rows.push(data);
}


function rebootDevice() {
  var packet = {};
  megaPort.write('reboot'+getEndLine(), 
      function (err, results){
          console.log('Dispositivo riavviato');
          setupServer();
        }
    );

}

function sendSerialPortStatus() {
  if (connection == null)
    return;
  Log('Sending serial port status');
  var packet = {};
  packet.pckType = 'SER_STATUS';
  if (megaPort == null) {
      packet.resultMessage = false;
    }
  else {
      packet.resultMessage = true;
    }
  var respPacket = JSON.stringify(packet);
  try {
    connection.sendUTF(respPacket);  
  }
  catch (err) {
  }
}

function closeCallback() {
	Log("closeCallback");
}


function serialPortDoClose() {
  Log("FORZO LA CHIUSURA DELLA SERIALE");
  if (megaPort != null)
    try  {
        megaPort.close(closeCallback);
    }
    catch (err) {
      console.log("Errore in serialPortDoClose "+err);
    }
  megaPort = null;
  flushWebSocketPacket();
}



function serialPortError () {
   Log("SerialPortError");
   serialLog("SerialPortError");
   //serialPortDoClose();
}

function serialPortClose () {
   Log("SerialPortClose");
   serialLog("SerialPortClose");
   serialPortDoClose();
}

function serialPortDisconnect () {
   Log("SerialPort Disconnected");
   serialLog("SerialPort Disconnected");
   serialPortDoClose();
   sendSerialPortStatus();
}

function onPortOpen() {
   Log('port open. Data rate: ' + megaPort.options.baudRate);
   serialLog('serialPort open');
   //setTimeout(sendSerialPortStatus, 30);
   
   writeCommand("focus*1", 
    	function(res, err){
      	console.log("Result: "+res);
    	}
  	);
}

function openSerialPort(portName) {
   if (megaPort != null)
    return;
   var parserS = serialport.parsers.readline("\n");
   try  {     		
      megaPort= new serialport(portName, {
           baudRate: 115200,
           parser: parserS
         });
     megaPort.on('open', onPortOpen);
     megaPort.on('data', sendSerialData);
     megaPort.on('error', serialPortError);
     megaPort.on('close', serialPortClose);
     megaPort.on('disconnect', serialPortDisconnect);
    }
  catch (err) {
    Log("Errore in apertura porta seriale "+portName+": "+err);
    setTimeout(serialPortDoClose(), 30);
  }
   //Log("Aperta porta seriale "+portName);
   //setupServer();//   
}



function isSerialPortOpen() {
	return megaPort != null && megaPort.isOpen();
}

function writePacket(packet) {
    megaPort.write(packet.command+getEndLine(), 
          function (err, results){
            if (err != null) {
              Log("Errore durante la scrittura "+err);
              setTimeout(serialPortDoClose(), 30);
            }
            packet.errorMessage = err;
            packet.resultMessage = results;
//            serialLog(results);
          }
      );
}

function writeCommand(cmd, callback) {
	if (megaPort == null) {
		Log("La porta è undefined");
	}
	else
		Log("La porta è defined");
    megaPort.write(cmd+getEndLine(), 
          function (err, results){
            if (callback != null)
            	callback(results, err);
          }
      );
}

function flushWebSocketPacket() {

}



exports.checkSerialPortStatus = checkSerialPortStatus;
exports.autoDetectSerialPort = autoDetectSerialPort;
exports.sendSerialData = sendSerialData;
exports.rebootDevice = rebootDevice;
exports.sendSerialPortStatus = sendSerialPortStatus;
exports.serialPortDoClose = serialPortDoClose;
exports.serialPortError = serialPortError;
exports.serialPortClose = serialPortClose;
exports.serialPortDisconnect = serialPortDisconnect;
exports.onPortOpen = onPortOpen;
exports.openSerialPort= openSerialPort;
exports.isSerialPortOpen = isSerialPortOpen;
exports.writePacket = writePacket;
exports.writeCommand = writeCommand;
exports.megaPort = megaPort;
