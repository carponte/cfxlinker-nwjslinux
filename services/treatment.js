'use strict';

const fullCustomDir = "fcTreatments";
const cacheDir = "cache";
const cacheTreatmentFileName = "treatment";
const cacheTreatmentStatusFileName = "treatmentStatus";

angular.module('megaride')
.service('TreatmentService', function($log, HwCommunicationService, DebugModeService, PromiseService, LoggingService, CalibrationService){
            	
        var TreatmentService = {};

        TreatmentService.Treatments = {
            "DRESDA": 1,
            "MEGARIDE": 2,
            "IONTO": 3,
            "CHERATITI": 4,
            "MANUAL": 5,
            "PULSED": 6            
            }

        TreatmentService.STEP_TYPES = {
            "PAUSE": 0,
            "IRRADIATION": 1,
            "IMBIBITION": 2,
            "END_TREATMENT": 3,
            "PATIENT_POSITION": 4
        }

        var treatment;
        TreatmentService.TEMP_NAME = "temp";
 
        TreatmentService.setTreatment = function (treat) {
            var promise = PromiseService.getPromise();
            treatment = treat;
            switch (treat) {
                case TreatmentService.Treatments.DRESDA:
                            HwCommunicationService.setDresdaMode().then(function(result){
                                promise.resolvePromise(result);
                                });
                            break;

                case TreatmentService.Treatments.MEGARIDE:
                            HwCommunicationService.setMegarideMode().then(function(result){
                                promise.resolvePromise(result);
                                });
                            break;

                case TreatmentService.Treatments.IONTO:
                            HwCommunicationService.setIontoMode().then(function(result){
                                promise.resolvePromise(result);
                                });
                            break;

                case TreatmentService.Treatments.CHERATITI:
                            HwCommunicationService.setManualMode().then(function(result){
                                promise.resolvePromise(result);
                                });
                            break;

                case TreatmentService.Treatments.MANUAL:
                            HwCommunicationService.setManualMode().then(function(result){
                                promise.resolvePromise(result);
                                });
                            break;
                
                case TreatmentService.Treatments.PULSED:
                            HwCommunicationService.setPulsedMode().then(function(result){
                                promise.resolvePromise(result);
                                });
                            break;

                default: 
                            HwCommunicationService.setManualMode().then(function(result){
                                promise.resolvePromise(result);
                                });
                            break;
            }
            return promise.promise; 
        }

        TreatmentService.startTreatment = function() {
            var promise = PromiseService.getPromise();
            HwCommunicationService.startTreatment().then( function(result) {
                    promise.resolvePromise(result);
                });
             return promise.promise;
        }


        TreatmentService.stopTreatment = function() {
            var promise = PromiseService.getPromise();
            HwCommunicationService.stopTreatment().then( function(result) {
                    promise.resolvePromise(result);
                });
             return promise.promise;
        }

        TreatmentService.pauseTreatment = function() {
            return HwCommunicationService.pauseTreatment();
        }

        TreatmentService.resumeTreatment = function() {
           return HwCommunicationService.resumeTreatment();
        }

        TreatmentService.nextStepTreatment = function() {
            HwCommunicationService.nextStepTreatment();
        }

        TreatmentService.setManualMode = function () {
            var promise = PromiseService.getPromise();
            HwCommunicationService.setManualMode().then( function(result) {
                    promise.resolvePromise(result);
                });
             return promise.promise;
        }

        TreatmentService.queryStatusTreatment = function () {
            var promise = PromiseService.getPromise();
            HwCommunicationService.queryStatusTreatment().then( function(result) {
                    var countdown = -1;
                    for (var i=0; i<result.length && countdown == -1; i++) {
                        if (result[i].indexOf('PSTAT')> -1 && (result[i].indexOf('PSTAT') == result[i].lastIndexOf('PSTAT'))&& result[i].charAt(result[i].length-1) == ')'){
                            var pstats = result[i].split(',');
                            var posCounter = 4;
                            if (HwCommunicationService.isPulsedAvailable())
                                posCounter = 5;
                            countdown = Number(pstats[posCounter].substring(0, pstats[posCounter].length-1));
                            countdown = HwCommunicationService.countDownInSeconds(countdown);
                        }
                    }
                    promise.resolvePromise(countdown);
                });
             return promise.promise;
        }


        TreatmentService.setManualModeParams = function(seconds, power) {
            var promise = PromiseService.getPromise();
            if (power == 30000)
                power = CalibrationService.get30MwValue()*1000;
            HwCommunicationService.setManualModeParams(seconds, power).then(function(result) {
                    promise.resolvePromise(result);
                });
             return promise.promise;
        }

        TreatmentService.setPulsedModeParams = function(secondsOn, secondsOff, secondsIrraggiamento, power) {
            var promise = PromiseService.getPromise();
            if (power == 30000)
                power = CalibrationService.get30MwValue()*1000;
            HwCommunicationService.setPulsedModeParams(secondsOn, secondsOff, secondsIrraggiamento, power).then(function(result) {
                    promise.resolvePromise(result);
                });
             return promise.promise;
        }
      
        TreatmentService.setMegaridePoints = function(nPoints) {
           HwCommunicationService.setMegaridePoints(nPoints);
        }

        TreatmentService.setMegaridePoint = function(seconds, power) {
            HwCommunicationService.setMegaridePoint(seconds, power);
        }

        TreatmentService.focusOn = function () {
            var promise = PromiseService.getPromise();
            HwCommunicationService.sendFocusMessage(1).then(function(result) {
                    promise.resolvePromise(result);
                });
            return promise.promise;
        }

        TreatmentService.focusOff = function () {
            var promise = PromiseService.getPromise();
            HwCommunicationService.sendFocusMessage(0).then(function(result) {
                    promise.resolvePromise(result);
                });
             return promise.promise;
        }

        TreatmentService.setWhiteLed = function (status) {
            var promise = PromiseService.getPromise();
            HwCommunicationService.setWhiteLed(status).then(function(result) {
                    promise.resolvePromise(result);
                });
             return promise.promise;
        }

        TreatmentService.getCommandDelay = function() {
            return HwCommunicationService.getCommandDelay();
        }

        //Restituisce il valore relativo all'effettivo.
        //Controlla se si è in debug, in caso affermativo accorcia i tempi
        TreatmentService.getImbibitionOf = function(seconds){
            if (DebugModeService.isFakeImbibition())
                return 5;
            /*if (HwCommunicationService.isDebugMode())
                return seconds;*/
            return seconds * 60;
        }

        TreatmentService.setDresdaParams = function() {
            HwCommunicationService.setDresdaParams();
        }

        TreatmentService.getStepTypes = function () {
            return TreatmentService.STEP_TYPES;
        }

        TreatmentService.tomWs = function (joules) {
            return 1000*joules;
        }

        TreatmentService.toJoules = function (mws) {
            return mws * 0.001; 
        }

        TreatmentService.getTreatmentJoules = function (steps) {
            var joules=0;
            for (var i=0; i<steps.length; i++) {
                if (steps[i].type == TreatmentService.STEP_TYPES.IRRADIATION)
                    joules += TreatmentService.toJoules(steps[i].duration * (steps[i].power/1000));
            }
            return joules;
        }

        TreatmentService.getTreatmentDuration = function (steps) {
            var seconds =0;
            for (var i=0; i<steps.length; i++) {
                if (steps[i].type == TreatmentService.STEP_TYPES.IRRADIATION)
                    seconds += steps[i].duration;
            }
            return seconds / 60;
        }


        //Restituisce l'elenco dei trattamenti full Custom presenti sul dispositivo

        TreatmentService.getFullCustomTreatments = function() {
            var result = [];
            if (!fsys.existsSync(fullCustomDir)) {
                return result;
            }
            if (fsys != null)
                fsys.readdirSync(fullCustomDir).forEach(file => {
                    if (file != TreatmentService.TEMP_NAME)
                        result.push(file);
                });
            return result;
        }

        

        TreatmentService.removeFullCustomTreatment = function(treatmentName) {
            if (!fsys.existsSync(fullCustomDir+"/"+treatmentName)) 
                return;
            try {
                fsys.unlinkSync(fullCustomDir+"/"+treatmentName);
                }
            catch (ex) {

            }
            
        }

        TreatmentService.saveFullCustomTreatment = function(treatmentName, details) {
            if (treatmentName == null || treatmentName == "")
                treatmentName = TreatmentService.TEMP_NAME;
            var json = JSON.stringify(details);
            try {
                fs.writeFileSync(fullCustomDir+"/"+treatmentName, json, 'utf8');
                }
            catch (ex) {
                LoggingService.log("TreatmentService. Error during save treatment "+JSON.stringify(ex));
            }
        }

        TreatmentService.getFullCustomTreatmentDetails = function(treatmentName) {
            try {
                 var fContent = fs.readFileSync(fullCustomDir+"/"+treatmentName);
                 return JSON.parse(fContent);
            }
            catch (ex) {
                    LoggingService.log("TreatmentService. Error during reading treatment details "+JSON.stringify(ex));
                    return null;
              }
        }

        //Verifica che vi sia un trattamento in cache
        TreatmentService.checkCache = function() {
            var result = [];
            if (!fsys.existsSync(cacheDir)) {
                fs.mkdirSync(cacheDir, { recursive: true })
                return result;
            }
            if (fsys != null)
                fsys.readdirSync(cacheDir).forEach(file => {
                    result.push(file);
                });
            return result;
        }

        //Salva il trattamento in cache
        TreatmentService.cacheTreatment = function(treatment) {
            var data = JSON.stringify(treatment);
            fs.writeFileSync(cacheDir+'/'+cacheTreatmentFileName, data);
        }

        //Elimina il trattamento dalla cache
        TreatmentService.clearCache = function() {
            fsys.unlinkSync(cacheDir+'/'+cacheTreatmentFileName);
            fsys.unlinkSync(cacheDir+'/'+cacheTreatmentStatusFileName);
        }

        TreatmentService.cacheTreatmentStatus = function (treatment, currentStep, currentStepIndex, timer) {
            var treatmentStatus = {};
            treatmentStatus.treatment = treatment;
            treatmentStatus.currentStep = currentStep;
            treatmentStatus.currentStepIndex = currentStepIndex;
            treatmentStatus.timer = timer;
            var data = JSON.stringify(treatmentStatus);
            fs.writeFileSync(cacheDir+'/'+cacheTreatmentStatusFileName, data);
        }


        TreatmentService.isPulsedAvailable = function() {
            return HwCommunicationService.isPulsedAvailable();
        }

        TreatmentService.pausePolling = function () {
            return HwCommunicationService.pausePolling();
        }

        TreatmentService.resumePolling = function () {
            return HwCommunicationService.resumePolling();
        }


        TreatmentService.checkCache();
        return TreatmentService;
        
    })