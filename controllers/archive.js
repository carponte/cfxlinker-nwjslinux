
angular.module('megaride')    
.controller('CtrlArchive', 
function ($scope, $stateParams, $rootScope, $state, $timeout, $translate, $sce, $mdDialog, DatabaseService, ReportService, LoggingService) 
	{

		$scope.lang = $translate.use();
		$scope.winPreviewReport  = null;
		$scope.dataLoading = true;
 		$scope.pdf = {};
		$scope.pdf.data = null;
		$scope.showExport = false;
		$scope.currentUser = null;
		

	$scope.goToHome = function() {
			$scope.closePreview();
			$state.go('splash');
		}


	var checkDate = function (term, value, row, column) {
		if (!term)
			return true;
		var now = moment(value, "DD/MM/YYYY");
		return now.isSame(term, 'day');
		}


	$scope.gridOptions = {
		enableSorting: true,
		rowHeight:50,
		enableFiltering: true,
		paginationPageSizes: [5, 10, 15, 20],
		paginationPageSize: 10,
		columnDefs: [

				{ field: 'ID', displayName: 'ID', enableFiltering: true}, 
				{ field: 'PatientName', displayName: $translate.instant('Report.Name'), enableFiltering: true}, 
				{ field: 'PatientSurname', displayName: $translate.instant('Report.Surname') },
				{ /*field: 'DateOfTreatment',  displayName: $translate.instant('ControlPanel.Reports.Date'), filters: [
			          {
			            type: 'date'
			          }
			        ]},*/
			       field: 'DateOfTreatment',  displayName: $translate.instant('ControlPanel.Reports.Date'),
			       cellFilter: 'date:"DD/MM/YYYY"',
        		   filters:[{ condition: checkDate, type: 'date'}],
        		},
				{ field: 'Operator', displayName: $translate.instant('Report.Operator'), enableFiltering: true },
				{ field: 'show', displayName:'', enableFiltering: false, enableSorting: false, enableHiding:false, 
				  cellTemplate: '<md-button class="md-icon-button md-accent" aria-label="Favorite" style="width:60px; height: 30px" '+
				  			    'ng-click="grid.appScope.showReport(row.entity)"><md-icon md-font-icon="far fa-file" class="fa s32 md-primary md-hue-2" '+
				  			    'style="font-size:36px; width:40px; '+
								'height: 100px; color: #6079a0"></md-icon></md-button>'+
					
								'<md-button class="md-icon-button md-accent" aria-label="Favorite" style="width:60px; height: 30px" '+
				  			    'ng-click="grid.appScope.deleteReport(row.entity)"><md-icon md-font-icon="far fa-trash" class="fa s32 md-primary md-hue-2" '+
				  			    'style="font-size:36px; width:40px; '+
								'height: 100px; color: #6079a0"></md-icon></md-button>' +
	
								'<md-button class="md-icon-button md-accent" aria-label="Favorite" style="width:60px; height: 30px" '+
								'ng-click="grid.appScope.showHideReport(row.entity)" ng-if="grid.appScope.isSuperUser()">'+

								'<md-icon ng-if="row.entity.visible==1" md-font-icon="far fa-eye-slash" class="fa s32 md-primary md-hue-2" '+
								'style="font-size:36px; width:40px; '+
							  	'height: 100px; color: #6079a0"></md-icon>'+

								'<md-icon ng-if="row.entity.visible!=1" md-font-icon="far fa-eye" class="fa s32 md-primary md-hue-2" '+
								'style="font-size:36px; width:40px; '+
							  	'height: 100px; color: #6079a0"></md-icon>'+
								
								'</md-button>' 
					 }
				],

		onRegisterApi: function (gridApi) {
			$scope.grid1Api = gridApi;
			}

		};

	$scope.closePreview = function() {
		if ($scope.winPreviewReport == null)
			return;
		$scope.winPreviewReport.close();
		$scope.winPreviewReport = null;
	}

	$scope.deleteReport = function (row) {
		$scope.pdf.idReport = row.ID;

		var confirm = $mdDialog.confirm()
		.title($translate.instant('Report.DeleteTitle'))
		.textContent($translate.instant('Report.DeleteConfirm'))
		.ok($translate.instant('Messages.OK'))
		.cancel($translate.instant('Messages.DISCARD'));
		
		$mdDialog.show(confirm).then(function(res) {
			if (res == true) {
				 ReportService.deleteReport($scope.pdf.idReport);
				 $scope.loadReports();
				}
		}, function() {
			
		});
		
	}

	$scope.showHideReport =function (row) {
		//DatabaseService.showHideReport(row);
		var usrID = -1;
		if ($scope.currentUser != null)
			usrID = $scope.currentUser.ID;
		if (row.visible == 1)
		  row.visible = 0;
		else 
		  row.visible = 1;
		DatabaseService.showHideReportData (row.ID, row, $scope.loadReports) ;
		
	}

	$scope.showReport = function(row) {
		$scope.pdf.idReport = row.ID;
		//Inizio assegnazione nome proposto per il download
		$scope.pdf.proposedFileName = '';		
		if (row.PatientSurname)		
		  $scope.pdf.proposedFileName += row.PatientSurname;
		if (row.PatientSurname && row.PatientName)
		  $scope.pdf.proposedFileName += "_";
		if (row.PatientName)	
		  $scope.pdf.proposedFileName += row.PatientName;
		if (!$scope.pdf.proposedFileName)
		  $scope.pdf.proposedFileName = $scope.pdf.idReport;

	    $scope.pdf.path = ReportService.getReportPath(row.ID);

		//Fine assegnazione nome proposto per il download
		ReportService.readReport(row.ID, function(err, contents) {
			 var file = new Blob([contents], {type:'application/pdf'});
                         var fileURL = URL.createObjectURL(file);
			 $scope.pdf.data = $sce.trustAsResourceUrl("https://drive.google.com/viewerng/"+
				"viewer?embedded=true&url="+fileURL);			 
			});		

		return;
		const topOfWin = 190;
		var w = window.screen.width
		var h = window.screen.height -topOfWin;
		$scope.winPreviewReport = window.open(ReportService.getReportPath(row.ID), "report", "width="+w+",height="+h+", left=0"+", top="+topOfWin);
	}

	$scope.exportHref = "data:application/json;charset=utf-8,jsonString";

	$scope.loadReports = function () {
		$scope.reports = [];
		var callBack = function (tx, result) {
				$scope.dataLoading = false;
				for (var i=0; i<result.rows.length; i++) {
					var strDate = result.rows[i].DateOfTreatment;
					var splitted = strDate.split("-");
					var appRow = {};
					appRow.ID = result.rows[i].ID;
					if (result.rows[i].visible == 1) {
						appRow.PatientName = result.rows[i].PatientName;
						appRow.PatientSurname= result.rows[i].PatientSurname;
						appRow.Operator= result.rows[i].Operator;
						appRow.DateOfTreatment= splitted[2]+"/"+splitted[1]+"/"+splitted[0];
						appRow.visible = result.rows[i].visible;
					}
					$scope.reports.push(appRow);
				}
				$scope.gridOptions.data = $scope.reports;
				var TableJson = JSON.stringify($scope.reports);
			  	$scope.exportHref = "data:application/json;charset=utf-8,"+TableJson;
			}
		var userID;
		if ($scope.isSuperUser() || $scope.currentUser == null)
		   userID = -1;
		else
		   userID = $scope.currentUser.ID;
		DatabaseService.getReports(userID, callBack);
	}

	$scope.showList = function () {
		$scope.pdf.data = null; 
	}

	$scope.doPrint = function () {
		ReportService.printReport($scope.pdf.idReport, $scope.pdf.proposedFileName); 
	}

	$scope.getReportPath = function () {
	  return ReportService.getReportPath($scope.pdf.idReport);
	}

	$scope.saveReport = function() {
		var fName = $scope.pdf.proposedFileName+".pdf";
		try  {
			fsys.copyFileSync("reports/"+$scope.pdf.idReport+".pdf", $rootScope.pathPenDrive+"/"+fName);
			LoggingService.log("Salvato report "+"reports/"+$scope.pdf.idReport+".pdf"+" su file "+ $rootScope.pathPenDrive+"/"+fName);
			alert($translate.instant('Report.ReportSaved')+" " +fName);
		}
		catch (e){
			//alert($translate.instant('Report.ReportSavingError')+" " +e);
			alert($translate.instant('Report.ReportSavingError'));
			LoggingService.log("Errore durante salvataggio report "+"reports/"+$scope.pdf.idReport+".pdf"+" su file "+ $rootScope.pathPenDrive+"/"+fName+".  Dettaglio errore: "+e);
 		} 
	  }

	  $scope.isSuperUser = function () {
			return $scope.currentUser != null && ($scope.currentUser.Role == 'admin' || $scope.currentUser.Role == 'support' || $scope.currentUser.Role == 'super');
	  }

	  $scope.loadCurrentUser = function () {
		if ($stateParams.user == null)
			return;
		try {
			$scope.currentUser = JSON.parse($stateParams.user);	
		}
		catch (err) {
			$scope.currentUser = null;
		}
		$scope.showExport = $scope.isSuperUser();	
	  }

	$scope.reports = [
		{ PatientName: "Madhav", PatientSurname:"Sai", DateOfTreatment: '10/11/2018', Operator: 'Nagpur' },
		{ PatientName: "Suresh", PatientSurname:" Dasari", DateOfTreatment: '28/09/2018', Operator: 'Chennai' },
		{ PatientName: "Rohini", PatientSurname:" Alavala", DateOfTreatment: '30/08/2018', Operator: 'Chennai' },
		{ PatientName: "Praveen", PatientSurname:" Kumar", DateOfTreatment: '28/09/2018', Operator: 'Bangalore' },
		{ PatientName: "Sateesh", PatientSurname:" Chandra", DateOfTreatment: '28/09/2018', Operator: 'Vizag' },

		{ PatientName: "Siva", PatientSurname:" Prasad", DateOfTreatment: '10/11/2018', Operator: 'Nagpur' },
		{ PatientName: "Sudiheer", PatientSurname:"ayana", DateOfTreatment: '10/11/2018', Operator: 'Kakinada' },
		{ PatientName: "Honiey", PatientSurname:"emineni", DateOfTreatment: '10/11/2018', Operator: 'Nagpur' },
		{ PatientName: "Mahiendra", PatientSurname:"asari", DateOfTreatment: '10/11/2018', Operator: 'Vijayawada' },
		{ PatientName: "Mahiesh", PatientSurname:"asari", DateOfTreatment: '10/11/2018', Operator: 'California' },
		{ PatientName: "Nagiaraju", PatientSurname:"asari", DateOfTreatment: '10/11/2018', Operator: 'Atlanta' },
		{ PatientName: "Gopi", PatientSurname:"Krishna", DateOfTreatment: '10/11/2018', Operator: 'Repalle' },
		{ PatientName: "Sudiheer", PatientSurname:"ppala", DateOfTreatment: '10/11/2018', Operator: 'Guntur' },
		{ PatientName: "Susihmita", PatientSurname:" ", DateOfTreatment: '10/11/2018', Operator: 'Vizag' }

		];

	$scope.loadCurrentUser();
	$scope.reports = [];
	$scope.gridOptions.data = $scope.reports;
	$scope.loadReports();
	
		
	}
);
   
    	
