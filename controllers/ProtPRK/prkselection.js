angular.module('megaride')    
.controller('CtrlPrkLasik', 
    function ($scope, $stateParams, $rootScope, $state, $translate, ShutdownService, LoggingService, DiagnosisService, CalibrationService) {

    	$scope.setProtocolOption = function(opt) {
    		var duration = 0;
    		var power = 0;

    		switch (opt) {
    			case 1:
    					duration = 30;
    					power = 3;
    					break; 
    			case 2:
    					duration = 10;
    					power = 9;
    					break; 
    			case 3:
    					duration = 5;
    					power = 18;
    					break; 
    			case 4: 
    				$state.go('megaride', {
                    thickness:0, cornVertexDistanceX: 0, cornVertexDistanceY: 0, kerApex:0, oldState: 'prkLasik', cornVertexRadius: 0, oldState : 'prkLasik', diagnosis: DiagnosisService.CHERATITE
                });
    				return; 
    		}
    		    			

            //var protName = $stateParams.protocol;
            var protName ='Cheratiti';
            LoggingService.info("Selezionato protocollo "+protName);
            LoggingService.info("Potenza: "+power);
            LoggingService.info("Durata: "+duration);


            $state.go('treatment', {
                protocol: protName, intensity: power, duration: duration,
                thickness:0, steps:0, dresdamode:0, cornVertexDistanceX:0, cornVertexDistanceY:0, kerApex:0, spotDiameter:0, cornVertexRadius: 0,
                diagnosis: DiagnosisService.CHERATITE, stepsDetails: null

            });            

        }

        $scope.back = function() {
             $state.go('selectProtocol');
        }


        $scope.setCheratitiParameters = function(minutes, intensity) {
            //var protName = $stateParams.protocol;
            var protName ='Cheratiti';
            LoggingService.info("Selezionato protocollo "+protName);
            LoggingService.info("Potenza: "+intensity);
            LoggingService.info("Durata: "+minutes);

            $state.go('treatment', {
                protocol: protName,
                duration: minutes, 
                intensity: intensity, 
                thickness: 0, 
                steps: 1,
                dresdamode: 0,
                cornVertexDistanceX: 0,
                cornVertexDistanceY: 0, 
                cornVertexRadius: 0,
                kerApex: 0,
                spotDiameter: 9,
                diagnosis: DiagnosisService.CHERATITE,
                stepsDetails: null
            });
        }

        $scope.isValueCalibrated = function (val) {
            return CalibrationService.isValueInRange(val);
        }



        $scope.protocol = $stateParams.protocol;

        ShutdownService.setOperation(ShutdownService.OPERATIONS.SELECT_PROTOCOL);

    	
	}
);