
angular.module('megaride')    
.controller('CtrlSerialTerminal', 
function ($scope, $stateParams, $rootScope, $state, $timeout, HwCommunicationService, LoggingService) 
	{
		$scope.htmlContent ='';
		$scope.command = '';

		$scope.goToHome = function() {
			$state.go('splash');
			LoggingService.setSerialListener(null);
		}

		$scope.scrollTerminal = function() {
			var objDiv = document.getElementById("Terminal");
			objDiv.scrollTop = objDiv.scrollHeight - objDiv.clientHeight;
		}

		$scope.sendCommand = function() {
			var command = $scope.command;
			$scope.htmlContent += '<b>'+command+'</b><br>';
			$scope.command = '';
			HwCommunicationService.writeCMD(command).then(
				function(result) {

					}
				);
		}

		$scope.serialMessage = function (msg) {
			$scope.htmlContent +='<br>'+msg+'<br>';
			$timeout($scope.scrollTerminal, 1000);
		}


		$scope.init = function() {
			LoggingService.setSerialListener($scope.serialMessage);
		}

		$scope.init();
	}
);
   
    	